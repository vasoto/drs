#ifndef _BENCHMARK_TEST_H_
#define _BENCHMARK_TEST_H_

#include <iostream>
#include <chrono>
#include <string>

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::string;
using std::cout;
using std::endl;

template <typename TEST>
void time_test (TEST t, const string name) {
    long ticks_per_second;

    auto start = high_resolution_clock::now();
    t();
    auto end = high_resolution_clock::now();
    auto elapsed = duration_cast<std::chrono::milliseconds>(end - start);

    std::cout << "elapsed: "
              << elapsed.count() << " ms "
			  << name
			  << std::endl;
}

#endif
