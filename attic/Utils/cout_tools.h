/*
 * cout_tools.h
 *
 *  Created on: Aug 25, 2016
 *      Author: Vassil Z. Verguilov
 */
#pragma once
#include <iomanip>
#include <iostream>

#include <Utils/type_tools.h>

#define TOHEX(length) std::hex << std::showbase << std::internal << std::setfill('0') << std::setw(length+2)
#define DEBUG(msg) std::cout << "[debug] " << msg << "\t" << "[" << __FUNCTION__ << "] " << __FILE__ << ":" << __LINE__ << std::endl;
#define TODEC() std::dec << std::setw(0)
#define DUMP_ENUM(enum) #enum << ":\t" << TOHEX() << enum_to_int(enum) << "\n"

