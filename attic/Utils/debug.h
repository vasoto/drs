/*
 * debug.h
 *
 *  Created on: Aug 25, 2016
 *      Author: Vassil Z. Verguilov
 */

#pragma once


#define DBG(comment) std::cout << "Line: " << __LINE__ << " Function: " << __FUNCTION__ << ": " << comment << std::endl

