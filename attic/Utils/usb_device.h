/*
 * usb_device.h
 *
 *  Created on: Sep 4, 2016
 *      Author: vasoto
 */
#pragma once

// STD
#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>
// Third party
#include <libusb-1.0/libusb.h>

#include <Utils/cout_tools.h>
#include <Utils/type_tools.h>

typedef uint32_t word;

const int USB_DEBUG_LEVEL = LIBUSB_LOG_LEVEL_DEBUG;
const unsigned char EP_OUT = 0x01;
const unsigned char EP_IN = 0x81;

/*
 * device info:
 * 'udevadm info -a -n /dev/bus/usb/004/011 | less'
 * 'lsusb -vvv -d 0b95:'
 */








namespace drs{
namespace utils{
namespace usb{
namespace detail{
/*
typedef std::shared_ptr<libusb_device_handle> HandlePointer;
typedef std::shared_ptr<libusb_device> DevicePointer;
typedef std::shared_ptr<libusb_device_descriptor> DescriptorPointer;

// Deleter functor
struct usb_device_deleter{
	void operator()(libusb_device* dev) { libusb_unref_device(dev); };
};

struct usb_handle_deleter{
	void operator()(libusb_device_handle* dev_handle){ libusb_close(dev_handle); }
};

class USBDeviceImpl{
public:
	USBDeviceImpl(libusb_device* device):_device(libusb_ref_device(device),
												 usb_device_deleter()),
			_handle(nullptr),
			_descriptor(nullptr){};
	DevicePointer get_device(){return( _device);}
	HandlePointer get_handle(){return( _handle);}
	DescriptorPointer& get_descriptor(){
		// Lazy load
		if(!_descriptor){
			libusb_device_descriptor* tmp = new libusb_device_descriptor();
			auto dev = _device.get();
			auto result = libusb_get_device_descriptor(dev, tmp);
			if(result != libusb_error::LIBUSB_SUCCESS){
				std::cerr << "libusb_get_device_descriptor() failed." << std::endl;
			}
			_descriptor = DescriptorPointer(std::move(tmp));
		}
		return( _descriptor);
	}

	DevicePointer _device;
	HandlePointer _handle;
	DescriptorPointer _descriptor;
};
}
struct USBDevice;
std::string get_usb_string(USBDevice*, const int);

struct USBDevice{
	typedef std::shared_ptr<USBDevice> Pointer;
	USBDevice() = delete;
	USBDevice(libusb_device* dev):impl(dev),
			is_open(false){};

	~USBDevice() = default;

	USBDevice(USBDevice& dev):impl(dev.impl),
			is_open(false){}

	void open(){
		libusb_device_handle * handle;// = new libusb_device_handle();
		auto result = libusb_open(impl._device.get(), &handle);
		if(result!=0){
			std::cerr << "Error: libusb_open failed: "
					  <<  libusb_strerror(int_to_enum<libusb_error>(result))
					  << std::endl;
			is_open = false;
		}
		impl._handle = detail::HandlePointer(std::move(handle),
											 detail::usb_handle_deleter());
		is_open = true;
	};


	static Pointer create(libusb_device* dev){
		USBDevice::Pointer dev_ptr = Pointer(std::move(new USBDevice(dev)));
		return(dev_ptr);
	}

	void info(){
		auto descr = *impl.get_descriptor();
		std::cout << "Descriptor: " << descr << std::endl
				  << "Speed: "
				  	  << libusb_get_device_speed(impl._device.get()) << std::endl
				  << "Output endpoint max. packet size: "
				  	  << libusb_get_max_packet_size(impl._device.get(), EP_OUT) << std::endl
				  << "Input endpoint max. packet size: "
				  	  << libusb_get_max_packet_size(impl._device.get(), EP_IN) << std::endl;
		libusb_config_descriptor * config;
		libusb_get_active_config_descriptor(impl._device.get(), &config);
		std::cout << std::setw(8) << "Config: " << std::endl << *config << std::endl;
		std::cout << "Manifacturer: " << get_usb_string(this, impl.get_descriptor()->iManufacturer) << std::endl;
		std::cout << "Product: " << get_usb_string(this, impl.get_descriptor()->iProduct) << std::endl;
		std::cout << "Serial Number: " << get_usb_string(this, impl.get_descriptor()->iSerialNumber) << std::endl;
		libusb_free_config_descriptor(config);

	}

	bool is_open;
	detail::USBDeviceImpl impl;
};

typedef std::vector<USBDevice::Pointer> DeviceList;

struct SessionDeleter{
public:
	void operator()(libusb_context* context) { libusb_exit(context); };

};

class USBSession : public std::enable_shared_from_this<USBSession> {
public:
	typedef std::shared_ptr<libusb_context> ContextPointer;
	typedef std::shared_ptr<USBSession> Pointer;


	USBSession(){}

	static void initialize(){
		if(!_context){
			libusb_context* context = nullptr;
			auto result = libusb_init(&context);
			if (result != libusb_error::LIBUSB_SUCCESS){
				std::cerr << "Error while initializing context: "
						  << libusb_strerror(int_to_enum<libusb_error>(result)) << std::endl;
				return; // exit function and leave _context = nullptr;
			}
			libusb_set_debug(context, USB_DEBUG_LEVEL);
			// Store in a shared_ptr
			_context.reset(context, SessionDeleter());
		}
	}

	static std::shared_ptr<USBSession> create(){
		USBSession * session = new USBSession();
		if(!_context){
			session->initialize();
		}
		return(std::move(Pointer(session)));
	}

	static ContextPointer get_context(){return _context;}
private:
	static ContextPointer _context;
};
*/


}
}
}


