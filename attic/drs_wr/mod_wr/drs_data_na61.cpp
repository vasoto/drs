/********************************************************************\

  Name:         drs_exam.cpp
  Created by:   Stefan Ritt

  Contents:     Simple example application to read out a DRS4
                evaluation board

  $Id: drs_exam.cpp 21308 2014-04-11 14:50:16Z ritt $

\********************************************************************/

#include <math.h>

#ifdef _MSC_VER

#include <windows.h>

#elif defined(OS_LINUX)

#define O_BINARY 0

#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <errno.h>


#define DIR_SEPARATOR '/'

#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "/home/alex/work/DRS4/C_scripts/read_data.h"

#include "strlcpy.h"
#include "DRS.h"




//#include <bitset> // my
//#include <iomanip> // my

/*------------------------------------------------------------------*/
void WriteTimeInfo(int j,FILE *f,DRSBoard *b);
void WriteADC(int ICH,FILE *f,float wave_array[8][1024]);

int main()
{
  //FNA61 = 0; // PSI
  FNA61 = 1; // NA61
  
  //////////////////////////////////////////////////////
  
  printf("\n");
  printf("++++++++++ drs_data_na61 ++++++++++\n");
  printf("\n");
  
  //////////////////////////////////////////////////////

   int i, nBoards;
   DRS *drs;
   DRSBoard *b;
   float time_array[8][1024];
   float wave_array[8][1024];
   FILE *f = 0;

   ////////////////////////////////////////////////////////
   
   unsigned short int FCH = 0;
   FCH |= (1<<0); 
   //FCH |= (1<<1);
   FCH |= (1<<2); // NA61
   //FCH |= (1<<3);
   //FCH |= (1<<4);
   //FCH |= (1<<5);
   //FCH |= (1<<6);
   //FCH |= (1<<7);
   //FCH |= (1<<255);
   printf("FCH = %i\n",FCH);
   //exit(0);
   
   
   ////////////////////////////////////////////////////////
   
   /* do initial scan */
   drs = new DRS();

   /* show any found board(s) */
   for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {
      b = drs->GetBoard(i);
      printf("Found DRS4 evaluation board, serial #%d, firmware revision %d\n", 
         b->GetBoardSerialNumber(), b->GetFirmwareVersion());
   }

   /* exit if no board found */
   nBoards = drs->GetNumberOfBoards();
   if (nBoards == 0) {
      printf("No DRS4 evaluation board found\n");
      return 0;
   }

   /* continue working with first board only */
   b = drs->GetBoard(0);
   usleep(100000);
   
   /* initialize board */
   b->Init();
   //usleep(100000); // my
   
   /* set sampling frequency */
   b->SetFrequency(5., true);

   
   //printf("--------- calib --------------\n");
 
   //b->CalibrateTiming(0);
   
   printf("------------------------------\n");

   /*
   b->SetChannelConfig(0,8,8);
   b->SetDecimation(0);
   b->SetDominoMode(1);
   b->SetReadoutMode(1);
   b->SetDominoActive(1);
   usleep(100000);
   */
   //b->SetReadoutMode(0);

   /* enable transparent mode needed for analog trigger */
   b->SetTranspMode(1);
   
   /* set input range to -0.5V ... +0.5V */
   if(FNA61==0) b->SetInputRange(0); // PSI
   
   /* use following line to set range to 0..1V */
   if(FNA61==1) b->SetInputRange(0.5); // NA61
   
   /// use following line to turn on the internal 100 MHz clock connected to all channels
   //b->EnableTcal(1);
   b->EnableTcal(0);
   //usleep(100 * 1000);
   
   //double volt = 0.1;
   //b->EnableAcal( 1, volt );
   

   /* use following lines to enable hardware trigger on CH1 at 50 mV positive edge */
   if (b->GetBoardType() >= 8) {        // Evaluaiton Board V4&5
     /*
     b->EnableTrigger(0, 1);
     b->SetTriggerSource(0);
     b->SetFrequency(5.12, true);
     b->EnableAcal( 0, 0 );
     b->SetCalibVoltage(0);
     b->SetChannelConfig(0,8,8);
     b->SetFrequency(5, true);
     b->IsPLLLocked();
     b->SelectClockSource(0);
     */
     b->EnableTrigger(1, 0);           // enable hardware trigger
     //
     b->SetTriggerSource(1<<0);        // 1, set CH1 as source
     //b->SetTriggerSource(1<<1);        // 2, set CH2 as source
     //b->SetTriggerSource(1<<2);        // 4, set CH3 as source
     //b->SetTriggerSource(1<<3);        // 8, set CH4 as source
     //b->SetTriggerSource(0xF);        // 16, set CH1|2|3|4 as source

     
   } else if (b->GetBoardType() == 7) { // Evaluation Board V3
      b->EnableTrigger(0, 1);           // lemo off, analog trigger on
      b->SetTriggerSource(0);           // use CH1 as source
   }
   
   
   
   ////////// thresholds /////////
   if(FNA61) {
     //b->SetTriggerLevel(0.5); // <20 ns
     //b->SetTriggerLevel(0.6); // <62 ns
     b->SetTriggerLevel(0.7);
     //b->SetTriggerLevel(0.87);     // NA61, big delay
   } else {
     b->SetTriggerLevel(-0.51);   // PSI
   }
   
   
   
   //b->SetTriggerPolarity(false);        // positive edge
   b->SetTriggerPolarity(true); // falling edge

   /* use following lines to set individual trigger elvels */
   //b->SetIndividualTriggerLevel(1, 0.);
   //b->SetIndividualTriggerLevel(2, 0.);
   //b->SetIndividualTriggerLevel(3, 0.);
   //b->SetIndividualTriggerLevel(4, 0.);
   //b->SetTriggerSource(15);
   
   b->SetTriggerDelayNs(0);             // zero ns trigger delay
   
   b->SetTriggerDelayPercent(0); // new
   
   /* use following lines to enable the external trigger */
   //if (b->GetBoardType() == 8) {     // Evaluaiton Board V4
   //   b->EnableTrigger(1, 0);           // enable hardware trigger
   //   b->SetTriggerSource(1<<4);        // set external trigger as source
   //} else {                          // Evaluation Board V3
   //   b->EnableTrigger(1, 0);           // lemo on, analog trigger off
   // }

   //////////////////////////////////////////////////////////////////
   
   b->IsPLLLocked();
   printf(" Nominal Frequency is fs = %f GHz\n",b->GetNominalFrequency());
   
   
   
   
   //////////////////////////////////////////////////////////////////
   ////////////////// write /////////////////////////////////////////
   
   char cfile[100];
   sprintf(cfile,"data_na61.dat");
   
   /// open file to save waveforms
   printf("\n");
   f = fopen(cfile, "w");
   if (f == NULL) {
      perror("ERROR: Cannot open file\n");
      return 1;
   }
   printf("Write to file %s\n",cfile);
   
   
   //////////////////////////////////////////////////////////////////

   int NCH = 0;
   if(FNA61==0) NCH = 4; // PSI
   else         NCH = 8; // NA61
   
   fwrite("TIME", 4,  1, f);
   fwrite("B#", 2,  1, f);
   fwrite("01", 2,  1, f);
   
   float timeDT[1024];   
   for(short int ich=0;ich<NCH;ich++) {
     if( !(FCH&(1<<ich)) ) continue;
     fwrite("C0", 2,  1, f);
     //fwrite( "0", 1,  1, f);
     fwrite(&ich, 2,  1, f);
     b->GetTimeCalibration(0, ich, 0, timeDT);
     for(int icell=0;icell<1024;icell++) {
       tch[ich].tcal[icell] = timeDT[icell]; // not needed
       //printf("%i %i %f\n",ich,icell,timeDT[icell]);
       fwrite(timeDT+icell, 4,  1, f);
     }
     //printf("%i %i %f\n",ich,10,timeDT[10]);
   }
   //exit(0);
   
   //PrintTimeCalib(tch[0]);
   usleep(100000);
   //b->StartDomino();
   //usleep(100000);
   //b->SoftTrigger();

   //////////////////////////////////////////////////////////////////

   //b->EnableTrigger(0, 0);

   /*
   std::cout << "Registers: " << (bitset<8>) b->GetCtrlReg()
	     << " " << (bitset<8>) b->GetConfigReg()
	     << " " << (bitset<8>) b->GetStatusReg()
	     << std::endl;
   */

   printf("\n");
   std::cout << "Registers: " << b->GetCtrlReg()
	     << " " << b->GetConfigReg()
	     << " " << b->GetStatusReg()
	     << std::endl;
   printf("\n");
   
   
   //////////////////////////////////////////////////////////////////
   
   int NTRMAX = 1000;
   int NTR = 0;
   int CHTR = 0;

   //for (long int j=0 ; j<100000; j++) { // soft, NA61
   for (long int j=0 ; j<2000; j++) { // PSI
     
     if( NTR >= NTRMAX ) break;

     if(j%100==0) printf("------ Event #%li ----- \n", j);

     //usleep(1000);
      /* start board (activate domino wave) */
      b->StartDomino();
            
      usleep(1000);
      //printf(" --- soft trigger (stop domino)\n");
      //b->SoftTrigger();
      
      //b->EnableTrigger(1, 0);           // enable hardware
      //b->SetTriggerSource(0xFFFF);
      //b->SetTriggerLevel(0.1);
      
      /// wait for trigger
      //printf("Waiting for trigger...");
      
      fflush(stdout);
      while (b->IsBusy());
      
      /// read all waveforms
      b->TransferWaves(0, 8); // all 8 channels
      
      
      //////////// trigger /////////////////
      /*
      int FTRG = 0;
      
      //double Uth = 300; // mV, NA61
      //int iTth = 150; // ich, NA61
      
      double Uth = 200; // mV, PSI
      double Tth = 86; // ns, PSI
      
      b->GetTime(0, CHTR, b->GetTriggerCell(0), time_array[CHTR]); // NA61
      b->GetWave(0, CHTR, wave_array[CHTR]); // NA61
      
      ////////////////////////////////////////
      
      for(int icell = 0; icell<1024; icell++ ) {
	//printf("%2i %3i %.1f mV\n",CHTR,icell,wave_array[CHTR][icell]);
	if( fabs(time_array[CHTR][icell]-Tth) < 1 ) { // PSI
	//if( fabs(icell-iTth) < 10 ) { // NA61
	  if( fabs(wave_array[CHTR][icell]-Uth) < 10 ) {
	    if( wave_array[CHTR][icell-2] < wave_array[CHTR][icell] ) { // rise
	      FTRG = 1;
	      //printf("  Trigger!\n");
	      break;
	    }
	  }
	}
      }
      
      
      if( !FTRG ) continue;
      NTR++;
      //exit(0);
      
      if(NTR%10==0) printf("------ Trigger #%li (%.1f%%) ----- \n", NTR,100.*NTR/j);
      */

      /////////// write ///////////////////

      WriteTimeInfo(j,f,b);
      
      for(short int ICH = 0; ICH<NCH; ICH++ ) {
	
	if( !(FCH&(1<<ICH)) ) continue;

	/// read time (X) array of first channel in ns 
	if(FNA61==0)b->GetTime(0, 2*ICH, b->GetTriggerCell(0), time_array[ICH]); // PSI
	else        b->GetTime(0, ICH, b->GetTriggerCell(0), time_array[ICH]); // NA61
	
	/// decode waveform (Y) array of first channel in mV 
	if(FNA61==0) b->GetWave(0, 2*ICH, wave_array[ICH]); // PSI
	else         b->GetWave(0, ICH, wave_array[ICH]); // NA61
	
	//printf("%i  %6.2f %6.2f\n",ICH,time_array[ICH][10],time_array[ICH][11]);

	/////////// write ///////////////////
	
	WriteADC(ICH,f,wave_array);
	
      } // end of ICH
      
      
      /* print some progress indication */
      //printf("\rEvent #%li read successfully\n", j);

   } // end of loop over j
   
   fclose(f);
   printf("Data were written to the file %s\n",cfile);
   
   printf("\n");
   printf("Temp = %.1f C\n",b->GetTemperature());

   /* delete DRS object -> close USB connection */
   delete drs;
   return 1;
}




void WriteADC(int ICH,FILE *f,float wave_array[8][1024])
{
  fwrite("C0",2,1, f); // 
  fwrite( &ICH, 2,1, f);
	
  for(int icell = 0; icell<1024; icell++ ) {
    unsigned short iu = 0;
    if(FNA61==0) iu=(unsigned short)((wave_array[ICH][icell]/1000.+0.5)*65535); // PSI
    else         iu=(unsigned short)((wave_array[ICH][icell]/1000.    )*65535); // NA61
    //if(icell<10) printf("%i %4i %8.1f %8i\n",ICH,icell,wave_array[ICH][icell],(int)iu);
    fwrite( &iu, 2,1, f);
  }
  return;
}


void WriteTimeInfo(int j,FILE *f,DRSBoard *b)
{
  time_t now;
  time(&now);
  struct tm *lt = localtime(&now);
  short int Year         = lt->tm_year+1900;
  short int Month        = lt->tm_mon+1;
  short int Day          = lt->tm_mday;
  short int Hour         = lt->tm_hour;
  short int Minute       = lt->tm_min;
  short int Second       = lt->tm_sec;
  //
  fwrite("EHDR", 4,  1, f);
  //
  unsigned int isn = (int)j;
  fwrite(&isn,  sizeof(isn),  1, f);
  //
  fwrite(&Year,  sizeof(Year),  1, f); // my
  fwrite(&Month, sizeof(Month), 1, f); // my
  fwrite(&Day,   sizeof(Day),   1, f); // my
  fwrite(&Hour,  sizeof(Hour),  1, f); // my
  fwrite(&Minute,sizeof(Minute),1, f); // my
  fwrite(&Second,sizeof(Second),1, f); // my
  fwrite("0000",4,1, f); // milliseconds, reserved
  fwrite("B#",2,1, f); // 
  short int bsn = b->GetBoardSerialNumber();
  fwrite( &bsn, 2,1, f);
  fwrite("T#",2,1, f); // 
  short int tcell = b->GetTriggerCell(0);
  fwrite( &tcell, 2,1, f);
  
  return;
}
