/********************************************************************

  Name:         DRS.cpp
  Created by:   Stefan Ritt, Matthias Schneebeli

  Contents:     Library functions for DRS mezzanine and USB boards

  $Id: DRS.cpp 21309 2014-04-11 14:51:29Z ritt $

\********************************************************************/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <algorithm>
#include <sys/stat.h>
#include <fcntl.h>
#include "strlcpy.h"
#include "DRS.h"

#   include <unistd.h>
#   include <sys/time.h>

#define VENDOR_ID       0x04b4
#define PRODUCT_ID      0x00f1
#define EP_OUT          0x01
#define EP_IN           0x81
#define DPNC342_MAGIC   0xC0DE

inline unsigned int uint32_set_bitfield(unsigned int src, 
                                      unsigned int posh, 
                                      unsigned int posl, 
                                      unsigned int field){
/*
 * Because bit field numbering is implementation dependent we do this:
 * "little endian" bit numbering msb=31, lsb=0
 * 31>=posh>=0, 31>=posl>=0, posh>=posl
 * arguments NOT checked 
 * return copy of src with src[posh:posl] replaced by field[(posh-posl):0]
 */
signed int mask;
mask = ((1<<(posh-posl+1))-1); // min 0x00000001, max 0xffffffff
return ((src & ~(mask<<posl)) | ((field & mask)<<posl));
} 

inline void Sleep(useconds_t x)
{
   usleep(x * 1000);
}

#include <sys/ioctl.h>

int drs_kbhit()
{
   int n;

   ioctl(0, FIONREAD, &n);
   return (n > 0);
}
static inline int getch()
{
   return getchar();
}

#include <DRS.h>
#include <mxml.h>

/*---- minimal FPGA firmvare version required for this library -----*/
const int REQUIRED_FIRMWARE_VERSION_DRS2 = 5268;
const int REQUIRED_FIRMWARE_VERSION_DRS3 = 6981;
const int REQUIRED_FIRMWARE_VERSION_DRS4 = 15147;

/*---- calibration methods to be stored in EEPROMs -----------------*/

#define VCALIB_METHOD_V4 1
#define TCALIB_METHOD_V4 1

#define VCALIB_METHOD  2
#define TCALIB_METHOD  2 // correct for sampling frequency, calibrate every channel

/*---- USB addresses -----------------------------------------------*/
#define USB_TIMEOUT                     1000    // one second
#define USB_CTRL_OFFSET                 0x00    /* all registers 32 bit */
#define USB_STATUS_OFFSET               0x40
#define USB_RAM_OFFSET                  0x80
#define USB_CMD_IDENT                      0    // Query identification
#define USB_CMD_ADDR                       1    // Address cycle
#define USB_CMD_READ                       2    // "VME" read <addr><size>
#define USB_CMD_WRITE                      3    // "VME" write <addr><size>
#define USB_CMD_READ12                     4    // 12-bit read <LSB><MSB>
#define USB_CMD_WRITE12                    5    // 12-bit write <LSB><MSB>

#define USB2_CMD_READ                      1
#define USB2_CMD_WRITE                     2
#define USB2_CTRL_OFFSET             0x00000    /* all registers 32 bit */
#define USB2_STATUS_OFFSET           0x10000
#define USB2_FIFO_OFFSET             0x20000
#define USB2_RAM_OFFSET              0x40000

/*------------------------------------------------------------------*/


using namespace std;

#define CMD_BYTE_SIZE  (3 * sizeof(int))
#define USB2_BUFFER_SIZE (1024*1024+(3*sizeof(int)))
unsigned char static *usb2_buffer = NULL;

/*------------------------------------------------------------------*/


/*------------------------------------------------------------------*/

DRS::DRS()
:  fNumberOfBoards(0)
{
  printf("DRS()\n");
   MUSB_INTERFACE *usb_interface;
   int index=0;
   memset(fError, 0, sizeof(fError));

//   unsigned char buffer[512];
   int found, one_found, usb_slot;

   one_found = 0;
   usb_slot = 0;
   for (index = 0; index < 127; index++) {
      found = 0;

      /* check for DPNC342 */
      if (musb_open(&usb_interface, VENDOR_ID, PRODUCT_ID, index, 1, 0) == MUSB_SUCCESS) {

         /* check ID */
         if (musb_get_device(usb_interface) != 1) {
            /* no DRS evaluation board found */
            musb_close(usb_interface);
         } else {

            usb_interface->usb_type = 3;        // USB 3.0
            fBoard[fNumberOfBoards] = new DRSBoard(usb_interface, usb_slot++);
            if (!fBoard[fNumberOfBoards]->HasCorrectFirmware())
               sprintf(fError, "Wrong firmware version: board has %d, required is %d. Board may not work correctly.\n",
                      fBoard[fNumberOfBoards]->GetFirmwareVersion(),
                      fBoard[fNumberOfBoards]->GetRequiredFirmwareVersion());
            fNumberOfBoards++;
            found = 1;
            one_found = 1;
         }
      }

      if (!found) {
         if (!one_found)
            printf("USB successfully scanned, but no boards found\n");
         break;
      }
   }

   printf("end of DRS()\n");
   return;
}

/*------------------------------------------------------------------*/

DRS::~DRS()
{
   int i;
   for (i = 0; i < fNumberOfBoards; i++) {
      delete fBoard[i];
   }

   if (usb2_buffer) {
      free(usb2_buffer);
      usb2_buffer = NULL;
   }

}

/*------------------------------------------------------------------*/

void DRS::SortBoards()
{
   /* sort boards according to serial number (simple bubble sort) */
   for (int i=0 ; i<fNumberOfBoards-1 ; i++) {
      for (int j=i+1 ; j<fNumberOfBoards ; j++) {
         if (fBoard[i]->GetBoardSerialNumber() < fBoard[j]->GetBoardSerialNumber()) {
            DRSBoard* b = fBoard[i];
            fBoard[i] = fBoard[j];
            fBoard[j] = b;
         }
      }
   }
}

/*------------------------------------------------------------------*/

void DRS::SetBoard(int i, DRSBoard *b)
{
   fBoard[i] = b;
}

/*------------------------------------------------------------------*/

bool DRS::GetError(char *str, int size)
{
   if (fError[0])
      strlcpy(str, fError, size);

   return fError[0] > 0;
}

/*------------------------------------------------------------------*/

DRSBoard::DRSBoard(MUSB_INTERFACE * musb_interface, int usb_slot)
:  fDAC_COFSA(0)
    , fDAC_COFSB(0)
    , fDAC_DRA(0)
    , fDAC_DSA(0)
    , fDAC_TLEVEL(0)
    , fDAC_ACALIB(0)
    , fDAC_DSB(0)
    , fDAC_DRB(0)
    , fDAC_COFS(0)
    , fDAC_ADCOFS(0)
    , fDAC_CLKOFS(0)
    , fDAC_ROFS_1(0)
    , fDAC_ROFS_2(0)
    , fDAC_INOFS(0)
    , fDAC_BIAS(0)
    , fDRSType(0)
    , fBoardType(0)
    , fRequiredFirmwareVersion(0)
    , fFirmwareVersion(0)
    , fBoardSerialNumber(0)
    , fHasMultiBuffer(0)
    , fCtrlBits(0)
    , fNumberOfReadoutChannels(0)
    , fReadoutChannelConfig(0)
    , fADCClkPhase(0)
    , fADCClkInvert(0)
    , fExternalClockFrequency(0)
    , fUsbInterface(musb_interface)
    , fSlotNumber(usb_slot)
    , fNominalFrequency(0)
    , fMultiBuffer(0)
    , fDominoMode(0)
    , fDominoActive(0)
    , fChannelConfig(0)
    , fChannelCascading(1)
    , fChannelDepth(1024)
    , fWSRLoop(0)
    , fReadoutMode(0)
    , fReadPointer(0)
    , fNMultiBuffer(0)
    , fTriggerEnable1(0)
    , fTriggerEnable2(0)
    , fTriggerSource(0)
    , fTriggerDelay(0)
    , fTriggerDelayNs(0)
    , fSyncDelay(0)
    , fDelayedStart(0)
    , fTranspMode(0)
    , fDecimation(0)
    , fRange(0)
    , fCommonMode(0.8)
    , fAcalMode(0)
    , fAcalVolt(0)
    , fTcalFreq(0)
    , fTcalLevel(0)
    , fTcalPhase(0)
    , fTcalSource(0)
    , fRefclk(0)
    , fMaxChips(0)
    , fResponseCalibration(0)
    , fVoltageCalibrationValid(false)
    , fCellCalibratedRange(0)
    , fCellCalibratedTemperature(0)
    , fTimeData(0)
    , fNumberOfTimeData(0)
    , fDebug(0)
    , fTriggerStartBin(0)
{
   if (musb_interface->usb_type == 1)
      fTransport = TR_USB;
   else
      fTransport = TR_USB2;
   memset(fStopCell, 0, sizeof(fStopCell));
   memset(fStopWSR, 0, sizeof(fStopWSR));
   fTriggerBus = 0;
   ConstructBoard();
}

/*------------------------------------------------------------------*/

DRSBoard::~DRSBoard()
{
   int i;
//   if (fTransport == TR_USB || fTransport == TR_USB2)
      musb_close(fUsbInterface);


   // Response Calibration
   delete fResponseCalibration;

   // Time Calibration
   for (i = 0; i < fNumberOfTimeData; i++) {
      delete fTimeData[i];
   }
   delete[]fTimeData;
}

/*------------------------------------------------------------------*/

void DRSBoard::ConstructBoard()
{
  printf("ConstructBoard()\n");
  
   t_statusregs  my_statusregs;
   t_controlregs my_controlregs;

   fDebug = 0;
   fWSRLoop = 1;
   fCtrlBits = 0;

   fExternalClockFrequency = 1000. / 30.;
   strcpy(fCalibDirectory, ".");

   /* get status registers from FPGA */
   assert(Read(T_STATUS, &my_statusregs, 0, sizeof(my_statusregs)) == sizeof(my_statusregs)); 

   if (my_statusregs.board_magic != DPNC342_MAGIC) {
      fprintf(stdout,"Invalid magic number: %04X\n", my_statusregs.board_magic);
      return;
      } 
   fprintf(stdout,"Magic number = 0x%04X\n",my_statusregs.board_magic);

   fDRSType         = my_statusregs.drs_type;
   fBoardType       = my_statusregs.board_type;
   fFirmwareVersion = my_statusregs.version_fw;
   fprintf(stdout,"DRS Type     = 0x%02X\n",my_statusregs.drs_type);
   fprintf(stdout,"Board Type   = 0x%02X\n",my_statusregs.board_type);
   fprintf(stdout,"FW Version   = 0x%04X\n",my_statusregs.version_fw);

   fNumberOfChannels = 9;
   fNumberOfChips    = 4;

   /* set correct reference clock */
   fRefClock = 60 * 2.4576 / 2.5; // 60*.98304=58.9824 ??? DPNC342

   /* get mode from hardware */
   assert(Read(T_CTRL, &my_controlregs, 0, sizeof(my_controlregs)) == sizeof(my_controlregs)); 

   fMultiBuffer      = 0;
   fNMultiBuffer     = 0;
   fHasMultiBuffer   = 0;
   fDominoMode       = my_controlregs.dmode;
   fTriggerEnable1   = my_controlregs.enable_trigger;
   fTriggerEnable2   = 0;
   fTriggerSource    = 0;
   fReadoutMode      = my_controlregs.readout_mode;
   fADCClkInvert     = 0;
   fDominoActive     = my_controlregs.dactive;
   fNominalFrequency = 5.12;
   fNumberOfReadoutChannels = 9;
   fDAC_ROFS_1  = 7;
   fDAC_CALN    = 7;
   fDAC_CALP    = 7;
   fDAC_BIAS    = 7;
   fDAC_TLEVEL1 = 0;
   fDAC_TLEVEL2 = 1;
   fDAC_TLEVEL3 = 2;
   fDAC_TLEVEL4 = 3;
   fDAC_TLEVEL5 = 4;
   fDAC_VCAL    = 5;
}

/*------------------------------------------------------------------*/

void DRSBoard::ReadCalibration(void)
{
  printf("ReadCalibration()\n");
   assert(fDRSType == 4);
   assert(fBoardType == 9);
  
   unsigned short buf[1024*16]; // 32 kB
   int i, j, chip;

   fVoltageCalibrationValid = false;
   fTimingCalibratedFrequency = 0;

   memset(fCellOffset,  0, sizeof(fCellOffset));
   memset(fCellGain,    0, sizeof(fCellGain));
   memset(fCellOffset2, 0, sizeof(fCellOffset2));
   memset(fCellDT,      0, sizeof(fCellDT));
   

   /* read offsets and gain from eeprom */
   if (fBoardType == 9) {
      printf(" ReadCalibration(): reading Time and Voltage calibration\n");
      memset(buf, 0, sizeof(buf));
      ReadEEPROM(0, buf, 4096);
      
      /* check voltage calibration method */
      if ((buf[2] & 0xFF) == VCALIB_METHOD)
         fVoltageCalibrationValid = true;
      else {
	if( fFirmwareVersion == 11030 ) { // NA61
	  fCellCalibratedRange = 0.5;
	} else { // PSI
	  fCellCalibratedRange = 0.;
	}         
	fCellCalibratedTemperature = -100;
	 printf(" ReadCalibration(): \n   NO calibration!  \n   fCellCalibratedRange=%.1f,\n   fCellCalibratedTemperature=%.1f\n",fCellCalibratedRange,fCellCalibratedTemperature);
         return;
      }
      
      /* check timing calibration method */
      if ((buf[2] >> 8) == TCALIB_METHOD) {
         float fl; // float from two 16-bit integers
         memcpy(&fl, &buf[8], sizeof(float));
         fTimingCalibratedFrequency = fl;
      } else
         fTimingCalibratedFrequency = -1;
      
      fCellCalibratedRange = ((int) (buf[10] & 0xFF)) / 100.0; // -50 ... +50 => -0.5 V ... +0.5 V
      fCellCalibratedTemperature = (buf[10] >> 8) / 2.0;
      
      ReadEEPROM(1, buf, 1024*32);
      for (i=0 ; i<8 ; i++)
         for (j=0 ; j<1024; j++) {
            fCellOffset[i][j] = buf[(i*1024+j)*2];
            fCellGain[i][j]   = buf[(i*1024+j)*2 + 1]/65535.0*0.4+0.7;
         }
      
      ReadEEPROM(2, buf, 1024*32);
      for (i=0 ; i<8 ; i++)
         for (j=0 ; j<1024; j++)
            fCellOffset2[i][j]   = buf[(i*1024+j)*2];
      
   } else if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8) {
      memset(buf, 0, sizeof(buf));
      ReadEEPROM(0, buf, 32);

      /* check voltage calibration method */
      if ((buf[2] & 0xFF) == VCALIB_METHOD_V4) // board < 9 has "1", board 9 has "2"
         fVoltageCalibrationValid = true;
      else {
         fCellCalibratedRange = 0;
         return;
      }
      fCellCalibratedTemperature = -100;

      /* check timing calibration method */
      if ((buf[4] & 0xFF) == TCALIB_METHOD_V4) { // board < 9 has "1", board 9 has "2"
         fTimingCalibratedFrequency = buf[6] / 1000.0;
      } else
         fTimingCalibratedFrequency = -1;

      fCellCalibratedRange = ((int) (buf[2] >> 8)) / 100.0; // -50 ... +50 => -0.5 V ... +0.5 V
      ReadEEPROM(1, buf, 1024*32);
      for (i=0 ; i<8 ; i++)
         for (j=0 ; j<1024; j++) {
            fCellOffset[i][j] = buf[(i*1024+j)*2];
            fCellGain[i][j]   = buf[(i*1024+j)*2 + 1]/65535.0*0.4+0.7;
         }

      ReadEEPROM(2, buf, 1024*5*4);
      for (i=0 ; i<1 ; i++)
         for (j=0 ; j<1024; j++) {
            fCellOffset[i+8][j] = buf[(i*1024+j)*2];
            fCellGain[i+8][j]   = buf[(i*1024+j)*2 + 1]/65535.0*0.4+0.7;
         }

      for (i=0 ; i<4 ; i++)
         for (j=0 ; j<1024; j++) {
            fCellOffset2[i*2][j]   = buf[2*1024+(i*1024+j)*2];
            fCellOffset2[i*2+1][j] = buf[2*1024+(i*1024+j)*2+1];
         }

   } else if (fBoardType == 6) {
      ReadEEPROM(0, buf, 16);

      /* check voltage calibration method */
      if ((buf[2] & 0xFF) == VCALIB_METHOD)
         fVoltageCalibrationValid = true;
      else {
         fCellCalibratedRange = 0;
         return;
      }

      /* check timing calibration method */
      if ((buf[4] & 0xFF) == TCALIB_METHOD)
         fTimingCalibratedFrequency = buf[6] / 1000.0; // 0 ... 6000 => 0 ... 6 GHz
      else
         fTimingCalibratedFrequency = 0;

      fCellCalibratedRange = ((int) (buf[2] >> 8)) / 100.0; // -50 ... +50 => -0.5 V ... +0.5 V

      for (chip=0 ; chip<4 ; chip++) {
         ReadEEPROM(1+chip, buf, 1024*32);
         for (i=0 ; i<8 ; i++)
            for (j=0 ; j<1024; j++) {
               fCellOffset[i+chip*9][j] = buf[(i*1024+j)*2];
               fCellGain[i+chip*9][j]   = buf[(i*1024+j)*2 + 1]/65535.0*0.4+0.7;
            }
      }

      ReadEEPROM(5, buf, 1024*4*4);
      for (chip=0 ; chip<4 ; chip++)
         for (j=0 ; j<1024; j++) {
            fCellOffset[8+chip*9][j] = buf[j*2+chip*0x0800];
            fCellGain[8+chip*9][j]   = buf[j*2+1+chip*0x0800]/65535.0*0.4+0.7;
         }

      ReadEEPROM(7, buf, 1024*32);
      for (i=0 ; i<8 ; i++) {
         for (j=0 ; j<1024; j++) {
            fCellOffset2[i][j]   = buf[i*0x800 + j*2];
            fCellOffset2[i+9][j] = buf[i*0x800 + j*2+1];
         }
      }

      ReadEEPROM(8, buf, 1024*32);
      for (i=0 ; i<8 ; i++) {
         for (j=0 ; j<1024; j++) {
            fCellOffset2[i+18][j] = buf[i*0x800 + j*2];
            fCellOffset2[i+27][j] = buf[i*0x800 + j*2+1];
         }
      }

   } else
      return;

   
   /* read timing calibration from eeprom */
   if (fBoardType == 9) {
      if (fTimingCalibratedFrequency == 0) {
         for (i=0 ; i<8 ; i++)
            for (j=0 ; j<1024 ; j++) {
               fCellDT[0][i][j] = 1/fNominalFrequency;
            }
      } else {
	 printf(" ReadCalibration(): reading time calibration\n");
         ReadEEPROM(2, buf, 1024*32);
         for (i=0 ; i<8 ; i++)
            for (j=0 ; j<1024; j++) {
               fCellDT[0][i][j]   = (buf[(i*1024+j)*2+1] - 1000) / 10000.0;
            }
      }
   } else if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8) {
      if (fTimingCalibratedFrequency == 0) {
         for (i=0 ; i<1024 ; i++)
            fCellDT[0][0][i] = 1/fNominalFrequency;
      } else {
         ReadEEPROM(0, buf, 1024*sizeof(short)*2);
         for (i=0 ; i<8 ; i++) {
            for (j=0 ; j<1024; j++) {
               // use calibration for all channels
               fCellDT[0][i][j] = buf[j*2+1]/10000.0;
            }
         }
      }
   } else if (fBoardType == 6) {
      if (fTimingCalibratedFrequency == 0) {
         for (i=0 ; i<1024 ; i++)
            for (j=0 ; j<4 ; j++)
               fCellDT[0][j][i] = 1/fNominalFrequency;
      } else {
         ReadEEPROM(6, buf, 1024*sizeof(short)*4);
         for (i=0 ; i<1024; i++) {
            fCellDT[0][0][i] = buf[i*2]/10000.0;
            fCellDT[1][0][i] = buf[i*2+1]/10000.0;
            fCellDT[2][0][i] = buf[i*2+0x800]/10000.0;
            fCellDT[3][0][i] = buf[i*2+0x800+1]/10000.0;
         }
      }
   }
}

/*------------------------------------------------------------------*/

bool DRSBoard::HasCorrectFirmware()
{
  printf("HasCorrectFirmware() \n");
  
  //return true;

   /* check for required firmware version */
   return (fFirmwareVersion >= fRequiredFirmwareVersion);
}

/*------------------------------------------------------------------*/

int DRSBoard::InitFPGA(void)
{
  printf("InitFPGA() (noop for now)\n");
#ifdef NOTNOW
   if (fTransport == TR_USB2) {
      unsigned char buffer[1];
      int i, status;

      /* blink Cy7C68013A LED and issue an FPGA reset */
      buffer[0] = 0;            // LED off
      musb_write(fUsbInterface, 1, buffer, 1, 100);
      Sleep(50);

      buffer[0] = 1;            // LED on
      musb_write(fUsbInterface, 1, buffer, 1, 100);

      /* wait until EEPROM page #0 has been read */
      for (i=0 ; i<100 ; i++) {
         Read(T_STATUS, &status, REG_STATUS, 4);
         if ((status & BIT_SERIAL_BUSY) == 0)
            break;
         Sleep(10);
      }
   }
#endif /* NOTNOW */

   return 1;
}

/*------------------------------------------------------------------*/

/* Generic read function accessing VME or USB */

int DRSBoard::Write(int type, unsigned int addr, void *data, int size)
{
   unsigned char usb2_buffer[USB2_BUFFER_SIZE];
   assert(fTransport == TR_USB2);
   if (fTransport == TR_USB) {
      unsigned char buffer[64], ack;
      unsigned int base_addr;
      int i, j, n;

      if (type == T_CTRL)
         base_addr = USB_CTRL_OFFSET;
      else if (type == T_STATUS)
         base_addr = USB_STATUS_OFFSET;
      else if (type == T_RAM)
         base_addr = USB_RAM_OFFSET;
      else
         base_addr = 0;

      if (type != T_RAM) {

         /*---- register access ----*/

         if (size == 2) {
            /* word swapping: first 16 bit sit at upper address */
            if ((addr % 4) == 0)
               addr = addr + 2;
            else
               addr = addr - 2;
         }

         buffer[0] = USB_CMD_WRITE;
         buffer[1] = base_addr + addr;
         buffer[2] = size;

         for (i = 0; i < size; i++) {
            buffer[3 + i] = *((unsigned char *) data + i);
	    //printf("-- %i %i\n",i,buffer[3 + i]);
	 }

         /* try 10 times */
         ack = 0;
         for (i = 0; i < 10; i++) {
            n = musb_write(fUsbInterface, 2, buffer, 3 + size, USB_TIMEOUT);
            if (n == 3 + size) {
               for (j = 0; j < 10; j++) {
                  /* wait for acknowledge */
                  n = musb_read(fUsbInterface, 1, &ack, 1, USB_TIMEOUT);
                  if (n == 1 && ack == 1)
                     break;

                  printf("Redo receive\n");
               }
            }

            if (ack == 1) {
               return size;
            }

            printf("Redo send\n");
         }
      } else {
	
         /*---- RAM access ----*/

         buffer[0] = USB_CMD_ADDR;
         buffer[1] = base_addr + addr;
         musb_write(fUsbInterface, 2, buffer, 2, USB_TIMEOUT);

         /* chop buffer into 60-byte packets */
         for (i = 0; i <= (size - 1) / 60; i++) {
            n = size - i * 60;
            if (n > 60)
               n = 60;
            buffer[0] = USB_CMD_WRITE12;
            buffer[1] = n;

            for (j = 0; j < n; j++)
               buffer[2 + j] = *((unsigned char *) data + j + i * 60);

            musb_write(fUsbInterface, 2, buffer, 2 + n, USB_TIMEOUT);

            for (j = 0; j < 10; j++) {
               /* wait for acknowledge */
               n = musb_read(fUsbInterface, 1, &ack, 1, USB_TIMEOUT);
               if (n == 1 && ack == 1)
                  break;

               printf("Redo receive acknowledge\n");
            }
         }

         return size;
      }
   } else if (fTransport == TR_USB2) {
      unsigned int base_addr;
      int i;

//      if (usb2_buffer == NULL)
//         usb2_buffer = (unsigned char *) malloc(USB2_BUFFER_SIZE);
//      assert(usb2_buffer);

      /* only accept even address and number of bytes */
//      assert(addr % 2 == 0);
//      assert(size % 2 == 0);
      /* only accept longword aligned address and number of bytes */
      assert(addr % 4 == 0);
      assert(size % 4 == 0);

      /* check for maximum size */
      assert((unsigned int)size <= (USB2_BUFFER_SIZE - CMD_BYTE_SIZE));

      if (type == T_CTRL)
         base_addr = USB2_CTRL_OFFSET;
      else if (type == T_STATUS)
         base_addr = USB2_STATUS_OFFSET;
      else if (type == T_FIFO)
         base_addr = USB2_FIFO_OFFSET;
      else if (type == T_RAM)
         base_addr = USB2_RAM_OFFSET;
      else
         base_addr = 0;

//      if (type != T_RAM && size == 2) {
//         /* word swapping: first 16 bit sit at upper address */
//         if ((addr % 4) == 0)
//           addr = addr + 2;
//         else
//            addr = addr - 2;
//      }

      addr += base_addr;

      ((unsigned int *)usb2_buffer)[0] = USB2_CMD_WRITE;
      ((unsigned int *)usb2_buffer)[1] = addr;
      ((unsigned int *)usb2_buffer)[2] = size;
	 printf("--- cmd  %08x\n",((unsigned int *)usb2_buffer)[0]);
	 printf("--- addr %08x\n",((unsigned int *)usb2_buffer)[1]);
	 printf("--- size %08x\n",((unsigned int *)usb2_buffer)[2]);

      for (i = 0; i < size; i++) {
         usb2_buffer[(3*sizeof(int)) + i] = *((unsigned char *) data + i);
	 printf("--- %i %02x\n",i,usb2_buffer[3*sizeof(int) + i]);
      }

      i = musb_write(fUsbInterface, EP_OUT, usb2_buffer, (3*sizeof(int)) + size, USB_TIMEOUT);
      if ((unsigned int)i != ((3*sizeof(int))+ size))
         printf("musb_write error: %d\n", i);

      return size;
   }

   return 0;
}

/*------------------------------------------------------------------*/

/* Generic read function accessing VME or USB */

int DRSBoard::Read(int type, void *data, unsigned int addr, int size)
{
   assert(fTransport == TR_USB2);
   if (fTransport == TR_USB) {
   } else if (fTransport == TR_USB2) {
      unsigned char buffer[3*sizeof(int)]; // three command words
      unsigned int base_addr;
      int i;

      /* only accept even address and number of bytes */
//      assert(addr % 2 == 0);
//      assert(size % 2 == 0);
      /* only accept longword aligned address and number of bytes */
      assert(addr % 4 == 0);
      assert(size % 4 == 0);

      /* check for maximum size */
      assert((unsigned int)size <= (USB2_BUFFER_SIZE - CMD_BYTE_SIZE));

      if (type == T_CTRL)
         base_addr = USB2_CTRL_OFFSET;
      else if (type == T_STATUS)
         base_addr = USB2_STATUS_OFFSET;
      else if (type == T_FIFO)
         base_addr = USB2_FIFO_OFFSET;
      else if (type == T_RAM)
         base_addr = USB2_RAM_OFFSET;
      else
         base_addr = 0;

//      if (type != T_RAM && size == 2) {
//         /* word swapping: first 16 bit sit at upper address */
//         if ((addr % 4) == 0)
//            addr = addr + 2;
//         else
//            addr = addr - 2;
//      }

      addr += base_addr;

      ((unsigned int *)buffer)[0] = USB2_CMD_READ;
      ((unsigned int *)buffer)[1] = addr;
      ((unsigned int *)buffer)[2] = size;

      i = musb_write(fUsbInterface, EP_OUT, buffer, 3*sizeof(int), USB_TIMEOUT);
      if (i != CMD_BYTE_SIZE) printf("musb_write error %d\n", i);
      
      i = musb_read(fUsbInterface, EP_IN, data, size, USB_TIMEOUT);
      if (i != size) printf("musb_read error %d\n", i);
      
      
      return i;
   }

   return 0;
}

/*------------------------------------------------------------------*/

void DRSBoard::SetLED(int state)
{
   // Set LED state
   if (state)
      fCtrlBits |= BIT_LED;
   else
      fCtrlBits &= ~BIT_LED;
   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
}

/*------------------------------------------------------------------*/

int DRSBoard::SetChannelConfig(int firstChannel, int lastChannel, int nConfigChannels)
{
printf("SetChannelConfig(%i %i %i)\n",firstChannel,lastChannel,nConfigChannels);
  
//   unsigned short d;
unsigned int d;
//DPNC342
assert(fDRSType == 4);
assert(fBoardType == 9);

if (lastChannel < 0 || lastChannel > 10) {
   printf("Invalid number of channels: %d (must be between 0 and 10)\n", lastChannel);
   return 0;
   }

// Set number of channels
// upper four bits of register must contain last channel to read out starting from 9
//         Read(T_CTRL, &d, REG_CHANNEL_MODE, 2);
//         d = (d & 0xFF00) | (firstChannel << 4) | lastChannel; // keep higher 8 bits which are ADClkPhase
//         Write(T_CTRL, REG_CHANNEL_MODE, &d, 2);
//
//  Quick and Dirty
assert(Read(T_CTRL, &d, (REG_CHANNEL_MODE & 0xfffffffc),sizeof(int))==sizeof(int));
//         d = (d & 0xFFFFFF00) | (firstChannel << 4) | lastChannel;
d = uint32_set_bitfield(d,7,4,firstChannel);
d = uint32_set_bitfield(d,3,0,lastChannel);
fprintf(stderr,"debug  d = %08x\n",d);
assert(Write(T_CTRL, (REG_CHANNEL_MODE & 0xfffffffc), &d, sizeof(int))==sizeof(int));

// set bit pattern for write shift register
fChannelConfig = 0;
switch (nConfigChannels) {
   case 1:
   fChannelConfig = 0x01;
   fChannelCascading = 8;
   break;
   case 2:
   fChannelConfig = 0x11;
   fChannelCascading = 4;
   break;
   case 4:
   fChannelConfig = 0x55;
   fChannelCascading = 2;
   break;
   case 8:
   fChannelConfig = 0xFF;
   fChannelCascading = 1;
   break;

   default:
   printf("Invalid channel configuration\n");
   return 0;
   }

//  Quick and Dirty
assert(Read(T_CTRL, &d, (REG_CHANNEL_CONFIG & 0xfffffffc), sizeof(int))==sizeof(int));
d = uint32_set_bitfield(d,23,16,fChannelConfig);
d = uint32_set_bitfield(d,24,24,fDominoMode);
d = uint32_set_bitfield(d,25,25,0x01); // PLLEN
d = uint32_set_bitfield(d,26,26,fWSRLoop);
d = uint32_set_bitfield(d,31,27,0x1F);
fprintf(stderr,"debug  d = %08x\n",d);
//Write(T_CTRL, REG_CHANNEL_CONFIG, &d, 2);
assert(Write(T_CTRL, (REG_CHANNEL_CONFIG & 0xfffffffc), &d, sizeof(int))==sizeof(int));

fChannelDepth = fChannelCascading * (fDecimation ? kNumberOfBins/2 : kNumberOfBins);
fNumberOfReadoutChannels = lastChannel - firstChannel + 1;

printf("SetChannelConfig(): fNumberOfReadoutChannels = %i - %i + 1 = %i\n",
        lastChannel,
        firstChannel,
        fNumberOfReadoutChannels);

return 1;
}

/*------------------------------------------------------------------*/

void DRSBoard::SetNumberOfChannels(int nChannels)
{
   printf("SetNumberOfChannels(0,%i)\n",nChannels);
   
   SetChannelConfig(0, nChannels - 1, 12);
}

/*------------------------------------------------------------------*/

void DRSBoard::SetADCClkPhase(int phase, bool invert)
{
  printf("SetADCClkPhase()\n");

   unsigned short d;

   /* Set the clock phase of the ADC via the variable phase shift
      in the Xilinx DCM. One unit is equal to the clock period / 256,
      so at 30 MHz this is about 130ps. The possible range at 30 MHz
      is -87 ... +87 */

   // keep lower 8 bits which are the channel mode
   Read(T_CTRL, &d, REG_ADCCLK_PHASE, 2);
   d = (d & 0x00FF) | (phase << 8);
   Write(T_CTRL, REG_ADCCLK_PHASE, &d, 2);

   if (invert)
      fCtrlBits |= BIT_ADCCLK_INVERT;
   else
      fCtrlBits &= ~BIT_ADCCLK_INVERT;
   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   fADCClkPhase = phase;
   fADCClkInvert = invert;
}

/*------------------------------------------------------------------*/

void DRSBoard::SetWarmup(unsigned int microseconds)
{
  printf("SetWarmup(%i us)\n",microseconds);

   /* Set the "warmup" time. When starting the domino wave, the DRS4
      chip together with its power supply need some time to stabilize
      before high resolution data can be taken (jumping baseline
      problem). This sets the time in ticks of 900ns before triggers
      are accepted */

   unsigned short ticks;

   if (microseconds == 0)
      ticks = 0;
   else
      ticks = (unsigned short) (microseconds / 0.9 + 0.5) - 1;
   Write(T_CTRL, REG_WARMUP, &ticks, 2);
}

/*------------------------------------------------------------------*/

void DRSBoard::SetCooldown(unsigned int microseconds)
{
  printf("SetCooldown(%i us)\n",microseconds);
  
   /* Set the "cooldown" time. When stopping the domino wave, the 
      power supply needs some time to stabilize before high resolution 
      data can read out (slanted baseline problem). This sets the 
      time in ticks of 900 ns before the readout is started */

   unsigned short ticks;

   ticks = (unsigned short) (microseconds / 0.9 + 0.5) - 1;
   Write(T_CTRL, REG_COOLDOWN, &ticks, 2);
}

/*------------------------------------------------------------------*/

int DRSBoard::SetDAC(unsigned char channel, double value)
{
   t_controlregs my_controlregs;
  //printf(" SetDAC( channel=%i, value=%.3f )\n",channel,value);
  
   // Set DAC value
   unsigned short d;
   
   if( fFirmwareVersion == 11030 ) { // NA61
     
     /* normalize to 2.048V for 12 bit */
     if (value < 0)
       value = 0;
     if (value > 2.048)
       value = 2.048;

     d = static_cast < unsigned short >(value / 2.048 * 0xFFFF + 0.5);
     
     //printf(" SetDAC(): Write: (normalize to 2.048V for 12 bit)  d = (value / 2.048 * 0xFFFF + 0.5) = %i\n",(int)d);
     
   } else { // PSI
     
     /* normalize to 2.5V for 16 bit */
     if (value < 0)
       value = 0;
     if (value > 2.5)
       value = 2.5;
     d = static_cast < unsigned short >(value / 2.5 * 0xFFFF + 0.5);
     
     //printf(" SetDAC(): Write: (normalize to 2.5V for 16 bit)  d = (value / 2.5 * 0xFFFF + 0.5) = %i\n",(int)d);
     
   }
   
   //d=0;
   assert(Read(T_CTRL, &my_controlregs, 0, sizeof(my_controlregs)) == sizeof(my_controlregs)); 
// ENDIANNESS: the numbering is presently 
//            byte address  0x0:   dac1, byte address 0x2: dac0,
//            byte address  0x4:   dac3, byte address 0x6: dac2,
//            byte address  0x8:   dac5, byte address 0xa: dac4,
//            byte address  0xc:   dac7, byte address 0xe: dac6,
// because we do only 4-byte accesses to USB we have to update two DACs in one go
//
   if(channel & 0x00000001) my_controlregs.dac[channel-1] = d; // DAC 1, 3, 5 or 7
   else my_controlregs.dac[channel+1] = d;          // DAC 0, 2, 4 or 8

//   fprintf(stderr,"xxxx %08x\n",(unsigned long int)&(((t_controlregs *)0)->dac[(channel/2)*2]));
//   fprintf(stderr,"channel %08x\n",channel);
//   fprintf(stderr,"d %08x\n",d);
   assert(Write(T_CTRL,
         (unsigned long int)&(((t_controlregs *)0)->dac[(channel & 0xfffffffe)]),
         &(my_controlregs.dac[(channel & 0xfffffffe)]), 
         4)==4);
   
   /*
   for( int i=0; i<=7; i++ ) {
     d = 0;
     Read(T_CTRL, &d, REG_DAC_OFS + (i * 2), 2);
     printf("  SetDAC(): read back the value: ch = %i, d = %i\n",i,(int)d);
   }
   */
   
   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::ReadDAC(unsigned char channel, double *value)
{
   // Readback DAC value from control register
   unsigned char buffer[2];

   /* map 0->1, 1->0, 2->3, 3->2, etc. */
   //ofs = channel + 1 - 2*(channel % 2);

   Read(T_CTRL, buffer, REG_DAC_OFS + (channel * 2), 2);

   /* normalize to 2.5V for 16 bit */
   *value = 2.5 * (buffer[0] + (buffer[1] << 8)) / 0xFFFF;

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetRegulationDAC(double *value)
{
   // Get DAC value from status register (-> freq. regulation)
   unsigned char buffer[2];

   if (fBoardType == 1)
      Read(T_STATUS, buffer, REG_RDAC3, 2);
   else if (fBoardType == 2 || fBoardType == 3 || fBoardType == 4)
      Read(T_STATUS, buffer, REG_RDAC1, 2);

   /* normalize to 2.5V for 16 bit */
   *value = 2.5 * (buffer[0] + (buffer[1] << 8)) / 0xFFFF;

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::StartDomino()
{
  //printf("StartDomino()\n");

   // Start domino sampling
   fCtrlBits |= BIT_START_TRIG;
   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
   fCtrlBits &= ~BIT_START_TRIG;

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::Reinit()
{
  printf("Reinit()\n");
  
   // Stop domino sampling
   // reset readout state machine
   // reset FIFO counters
   fCtrlBits |= BIT_REINIT_TRIG;
   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
   fCtrlBits &= ~BIT_REINIT_TRIG;

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::Init()
{
printf("Init()\n");
assert(fDRSType == 4);
assert(fBoardType == 9);
assert(fFirmwareVersion == 11030);
  
// Init FPGA on USB2 board
InitFPGA();

// Reinitialize
fCtrlBits |= BIT_REINIT_TRIG;        // reset readout state machine
Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
fCtrlBits &= ~BIT_REINIT_TRIG;

SetDAC(fDAC_VCAL, 0.1 ); // 0 V
fRange = 0.5;
   
SetChannelConfig(0, fNumberOfReadoutChannels - 1, 8);
   
// set ADC clock phase
fADCClkPhase      = 0;
fADCClkInvert     = 0;

// default settings
fNMultiBuffer     = 0;
fDominoMode       = 1;
fReadoutMode      = 1;
fReadPointer      = 0;
fTriggerEnable1   = 0;
fTriggerEnable2   = 0;
fTriggerSource    = 0;
fTriggerDelay     = 0;
fTriggerDelayNs   = 0;
fSyncDelay        = 0;
fNominalFrequency = 5; // PSI = 1 GHz
fDominoActive     = 1;

// load calibration from EEPROM
ReadCalibration();

// get some settings from hardware
fRange = GetCalibratedInputRange();
if(fRange < 0 || fRange > 0.5) fRange = 0.5;
   
fNominalFrequency = GetCalibratedFrequency();
if(fNominalFrequency < 0.1 || fNominalFrequency > 6) fNominalFrequency = 5;
   
printf("fr = %f\n",fNominalFrequency);

SetDominoMode(fDominoMode);
SetReadoutMode(fReadoutMode);
EnableTrigger(fTriggerEnable1, fTriggerEnable2);
SetTriggerSource(fTriggerSource);
SetTriggerDelayPercent(0);
SetSyncDelay(fSyncDelay);
SetDominoActive(fDominoActive);
SetFrequency(fNominalFrequency, true);
SetInputRange(fRange);

printf("---- fRange = %f\n",fRange); 

SelectClockSource(0); // FPGA clock

// disable calibration signals
EnableAcal(0, 0);
SetCalibTiming(0, 0);
EnableTcal(0);

// got to idle state
Reinit();

//printf("fr = %f\n",fNominalFrequency); exit(0);
   
printf("  end of Init()\n\n");

usleep(100000); // my
return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetDominoMode(unsigned char mode)
{
unsigned int d;
printf("SetDominoMode(mode=%i)\n",(int)mode);
assert(fDRSType == 4);
assert(fBoardType == 9);

// Set domino mode
// mode == 0: single sweep
// mode == 1: run continously
//
fDominoMode = mode;

//  Quick and Dirty
assert(Read(T_CTRL, &d, (REG_CONFIG & 0xfffffffc), sizeof(int))==sizeof(int));
fChannelConfig = (d & 0x00ff0000) >> 16;
d = uint32_set_bitfield(d,23,16,fChannelConfig);
d = uint32_set_bitfield(d,24,24,fDominoMode);
d = uint32_set_bitfield(d,25,25,0x01); // PLLEN
d = uint32_set_bitfield(d,26,26,fWSRLoop);
d = uint32_set_bitfield(d,31,27,0x1F);
fprintf(stderr,"debug  d = %08x\n",d);
assert(Write(T_CTRL, (REG_CONFIG & 0xfffffffc), &d, sizeof(int))==sizeof(int));
return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetDominoActive(unsigned char mode)
{
  printf("SetDominoActive(mode=%i)\n",(int)mode);
  
   // Set domino activity
   // mode == 0: stop during readout
   // mode == 1: keep domino wave running
   //
   fDominoActive = mode;
   if (mode)
      fCtrlBits |= BIT_DACTIVE;
   else
      fCtrlBits &= ~BIT_DACTIVE;

   assert(Write(T_CTRL, REG_CTRL, &fCtrlBits, 4)==4);

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetReadoutMode(unsigned char mode)
{
   printf("SetReadoutMode(mode=%i)\n",(int)mode);

   // Set readout mode
   // mode == 0: start from first bin
   // mode == 1: start from domino stop
   //
   fReadoutMode = mode;
   if (mode)
      fCtrlBits |= BIT_READOUT_MODE;
   else
      fCtrlBits &= ~BIT_READOUT_MODE;

   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SoftTrigger(void)
{
  //printf("SoftTrigger()\n");
  
   // Send a software trigger
   fCtrlBits |= BIT_SOFT_TRIG;
   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
   fCtrlBits &= ~BIT_SOFT_TRIG;

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::EnableTrigger(int flag1, int flag2)
{
  printf("EnableTrigger(%i,%i)\n",flag1,flag2);
  
   // Enable external trigger
   fTriggerEnable1 = flag1;
   fTriggerEnable2 = flag2;
   if (flag1)
      fCtrlBits |= BIT_ENABLE_TRIGGER1;
   else
      fCtrlBits &= ~BIT_ENABLE_TRIGGER1;

   if (flag2)
      fCtrlBits |= BIT_ENABLE_TRIGGER2;
   else
      fCtrlBits &= ~BIT_ENABLE_TRIGGER2;

   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetDelayedTrigger(int flag)
{
  printf("SetDelayedTrigger(%i)\n",flag);

   // Select delayed trigger from trigger bus
   if (flag)
      fCtrlBits |= BIT_TRIGGER_DELAYED;
   else
      fCtrlBits &= ~BIT_TRIGGER_DELAYED;

   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetTriggerPolarity(bool negative)
{
printf("SetTriggerPolarity(%i)\n",(int)negative);

fTcalLevel = negative;
      
if (negative) fCtrlBits |= BIT_NEG_TRIGGER;
else fCtrlBits &= ~BIT_NEG_TRIGGER;
Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetTriggerLevel(double voltage)
{
printf("SetTriggerLevel(voltage=%.3f)\n",voltage);
  
SetIndividualTriggerLevel(0, voltage);
SetIndividualTriggerLevel(1, voltage);
SetIndividualTriggerLevel(2, voltage);
SetIndividualTriggerLevel(3, voltage);
SetIndividualTriggerLevel(4, voltage);
return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetIndividualTriggerLevel(int channel, double voltage)
{
printf("SetIndividualTriggerLevel( channel=%i, voltage=%.3f )\n",channel,voltage);

//SetDAC(fDAC_VCAL, voltage);
//EnableAcal(1, voltage);
      
switch (channel) {
   case 0: SetDAC(fDAC_TLEVEL1, voltage); break;
   case 1: SetDAC(fDAC_TLEVEL2, voltage); break;
   case 2: SetDAC(fDAC_TLEVEL3, voltage); break;
   case 3: SetDAC(fDAC_TLEVEL4, voltage); break;
   case 4: SetDAC(fDAC_TLEVEL5, voltage); break;
   default: return -1;
   }
return 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetTriggerDelayPercent(int delay)
/* set trigger delay in percent 0..100 */
{
  printf("SetTriggerDelayPercent(%i)\n",delay);
  
   short ticks; 
//   short reg;
   unsigned int d;
   fTriggerDelay = delay;

   if (fBoardType == 5 || fBoardType == 6 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
      // convert delay (0..100) into ticks
      ticks = (unsigned short) (delay/100.0*255+0.5);
      if (ticks > 255)
         ticks = 255;
      if (ticks < 0)
         ticks = 0;

      // convert delay into ns
      if (fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
	
	//if (fFirmwareVersion >= 17147) // PSI
	if (fFirmwareVersion >= 17147 || fFirmwareVersion == 11030) // NA61
            fTriggerDelayNs = ticks * 4.8;  // Spartan 3 Octal LUTs
         else
            fTriggerDelayNs = ticks * 2.1;  // Spartan 3 Quad LUTs

      } else {
	if (fFirmwareVersion >= 17382) 
            fTriggerDelayNs = ticks * 4.6;  // Virtex PRO II Octal LUTs
         else
            fTriggerDelayNs = ticks * 2.3;  // Virtex PRO II Quad LUTs
      }

      // adjust for fixed delay, measured and approximated experimentally
      fTriggerDelayNs += 21 + 28/fNominalFrequency;

//      Read(T_CTRL, &reg, REG_TRG_DELAY, 2);
//      reg = (reg & 0xFF00) | ticks;
//      Write(T_CTRL, REG_TRG_DELAY, &ticks, 2);
//  Quick and Dirty
         assert(Read(T_CTRL, &d, (REG_TRG_DELAY & 0xfffffffc), sizeof(int))==sizeof(int));
//         d = (d & 0xFF00FFFF) | (ticks << 16);
         d = uint32_set_bitfield(d,23,16,ticks);
         fprintf(stderr,"debug  d = %08x\n",d);
         assert(Write(T_CTRL, (REG_TRG_DELAY & 0xfffffffc), &d, sizeof(int))==sizeof(int));

      return 1;
   }

   return 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetTriggerDelayNs(int delay)
/* set trigger delay in nanoseconds */
{
  printf("SetTriggerDelayNs(%i)\n",delay);
   assert(fDRSType == 4);
   assert(fBoardType == 9);

   short ticks /*, reg */;
   unsigned int d;
   fTriggerDelayNs = delay;

   if (fBoardType == 5 || fBoardType == 6 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {

      // convert delay in ns into ticks
      if (fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
	//if (fFirmwareVersion >= 17147) // PSI
	if (fFirmwareVersion >= 17147 || fFirmwareVersion == 11030 ) // NA61
            ticks = (short int)(delay / 4.8 + 0.5);  // Spartan 3 Octal LUTs
         else
            ticks = (short int)(delay / 2.1 + 0.5);  // Spartan 3 Quad LUTs
      } else {
         if (fFirmwareVersion >= 17382)
            ticks = (short int)(delay / 4.6 + 0.5);  // Virtex PRO II Octal LUTs
         else
            ticks = (short int)(delay / 2.3 + 0.5);  // Virtex PRO II Quad LUTs
      }

      if (ticks > 255)
         ticks = 255;
      if (ticks < 0)
         ticks = 0;

      fTriggerDelay = ticks / 255 * 100;
      
      printf(" SetTriggerDelayNs(%i): fTriggerDelay=%i\n",delay,fTriggerDelay);

//      Read(T_CTRL, &reg, REG_TRG_DELAY, 2);
//      reg = (reg & 0xFF00) | ticks;
//      Write(T_CTRL, REG_TRG_DELAY, &ticks, 2);
//  Quick and Dirty
         assert(Read(T_CTRL, &d, (REG_TRG_DELAY & 0xfffffffc), sizeof(int))==sizeof(int));
//         d = (d & 0xFF00FFFF) | (ticks << 16);
         d = uint32_set_bitfield(d,23,16,ticks);
         fprintf(stderr,"debug  d = %08x\n",d);
         assert(Write(T_CTRL, (REG_TRG_DELAY & 0xfffffffc), &d, sizeof(int))==sizeof(int));

      return 1;
   }

   return 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetSyncDelay(int ticks)
{
  printf("SetSyncDelay(ticks=%i)\n",ticks);
  assert(fDRSType == 4);
  assert(fBoardType == 9);

   //short int reg;
   unsigned int d;

   if (fBoardType == 5 || fBoardType == 6 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
//      Read(T_CTRL, &reg, REG_TRG_DELAY, 2);
//      reg = (reg & 0xFF) | (ticks << 8);
//      Write(T_CTRL, REG_TRG_DELAY, &reg, 2);
//  Quick and Dirty
         assert(Read(T_CTRL, &d, (REG_TRG_DELAY & 0xfffffffc), sizeof(int))==sizeof(int));
//         d = (d & 0xFF00FFFF) | (ticks << 16);
         d = uint32_set_bitfield(d,23,16,ticks);
         fprintf(stderr,"debug  d = %08x\n",d);
         assert(Write(T_CTRL, (REG_TRG_DELAY & 0xfffffffc), &d, sizeof(int))==sizeof(int));

      return 1;
   }

   return 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetTriggerSource(int source)
{
  printf("SetTriggerSource(%i)\n",source);
  
//   short int reg;
   unsigned int d;
   assert(fDRSType == 4);
   assert(fBoardType == 9);

   fTriggerSource = source;
   if (fBoardType == 5 || fBoardType == 7) {
      // Set trigger source 
      // 0=CH1, 1=CH2, 2=CH3, 3=CH4
      if (source & 1)
         fCtrlBits |= BIT_TR_SOURCE1;
      else
         fCtrlBits &= ~BIT_TR_SOURCE1;
      if (source & 2)
         fCtrlBits |= BIT_TR_SOURCE2;
      else
         fCtrlBits &= ~BIT_TR_SOURCE2;

      Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
   } else if (fBoardType == 8 || fBoardType == 9) {
      // Set trigger configuration
      // OR  0=CH1, 1=CH2, 2=CH3, 3=CH4, 4=EXT
      // AND 8=CH1, 9=CH2, 10=CH3, 11=CH4, 12=EXT
//      reg = (unsigned short) source;
//      Write(T_CTRL, REG_TRG_CONFIG, &reg, 2);
//  Quick and Dirty
         assert(Read(T_CTRL, &d, (REG_TRG_CONFIG & 0xfffffffc), sizeof(int))==sizeof(int));
//         d = (d & 0x0000FFFF) | (source << 16);
         d = uint32_set_bitfield(d,31,16,source);
         fprintf(stderr,"debug  d = %08x\n",d);
         assert(Write(T_CTRL, (REG_TRG_CONFIG & 0xfffffffc), &d, sizeof(int))==sizeof(int));
   }

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetDelayedStart(int flag)
{
  printf("SetDelayedStart(%i)\n",flag);
  
   // Enable external trigger
   fDelayedStart = flag;
   if (flag)
      fCtrlBits |= BIT_DELAYED_START;
   else
      fCtrlBits &= ~BIT_DELAYED_START;

   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetTranspMode(int flag)
{
  printf("SetTranspMode(%i)\n",flag);

   // Enable/disable transparent mode
   fTranspMode = flag;
   if (flag)
      fCtrlBits |= BIT_TRANSP_MODE;
   else
      fCtrlBits &= ~BIT_TRANSP_MODE;

   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetStandbyMode(int flag)
{
  printf("SetStandbyMode(%i)\n",flag);
  
   // Enable/disable standby mode
   fTranspMode = flag;
   if (flag)
      fCtrlBits |= BIT_STANDBY_MODE;
   else
      fCtrlBits &= ~BIT_STANDBY_MODE;

   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetDecimation(int flag)
{
  printf("SetDecimation(%i)\n",flag);

   // Drop every odd sample
   fDecimation = flag;
   if (flag)
      fCtrlBits |= BIT_DECIMATION;
   else
      fCtrlBits &= ~BIT_DECIMATION;

   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   // Calculate channel depth
   fChannelDepth = fChannelCascading * (fDecimation ? kNumberOfBins/2 : kNumberOfBins);

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::IsBusy()
{
  //printf("IsBusy()\n");
  
   // Get running flag
   unsigned int status;

   Read(T_STATUS, &status, REG_STATUS, 4);
   
   //printf(" IsBusy(): status=%i\n",status);
   //std::cout << "IsBusy(): " << (bitset<8>) status << endl;
   
   return (status & BIT_RUNNING) > 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::IsEventAvailable()
{
  //printf("IsEventAvailable()\n");
  
   return GetMultiBufferWP() != fReadPointer;
}

/*------------------------------------------------------------------*/

int DRSBoard::IsPLLLocked()
{
  printf("IsPLLLocked()\n");

   // Get running flag
   unsigned int status;

   Read(T_STATUS, &status, REG_STATUS, 4);
   if (GetBoardType() == 6)
      return ((status >> 1) & 0x0F) == 0x0F;

   printf(" IsPLLLocked(): %i\n", (int)(status & BIT_PLL_LOCKED0) );
   
   return (status & BIT_PLL_LOCKED0) > 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::IsLMKLocked()
{
  printf("IsLMKLocked()\n");

   // Get running flag
   unsigned int status;

   Read(T_STATUS, &status, REG_STATUS, 4);
   if (GetBoardType() == 6)
      return (status & BIT_LMK_LOCKED) > 0;
   return 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::IsNewFreq(unsigned char chipIndex)
{
   unsigned int status;

   Read(T_STATUS, &status, REG_STATUS, 4);
   if (chipIndex == 0)
      return (status & BIT_NEW_FREQ1) > 0;
   return (status & BIT_NEW_FREQ2) > 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::ReadFrequency(unsigned char chipIndex, double *f)
{
  printf("ReadFrequency(chipIndex=%i,double *f) \n",chipIndex);

   if (fDRSType == 4) {

      if (fBoardType == 6) {
         *f = fNominalFrequency;
         return 1;
      }

      unsigned short ticks;

      Read(T_CTRL, &ticks, REG_FREQ_SET, 2);
      ticks += 2;

      /* convert rounded ticks back to frequency */
      if (ticks > 2)
	*f = 1.024 / ticks * fRefClock;
      else
         *f = 0;

      if( fFirmwareVersion == 11030 ) {// NA61
	*f = 5.12;
      }
      
      printf(" ReadFrequency(): f = %.4f,  ticks = %u\n", *f,ticks );

   } else {
      // Read domino sampling frequency
      unsigned char buffer[2];

      if (chipIndex == 0)
         Read(T_STATUS, buffer, REG_FREQ1, 2);
      else
         Read(T_STATUS, buffer, REG_FREQ2, 2);

      *f = (static_cast < unsigned int >(buffer[1]) << 8) +buffer[0];

      /* convert counts to frequency */
      if (*f != 0)
         *f = 1024 * 200 * (32.768E6 * 4) / (*f) / 1E9;
   }

   return 1;
}

/*------------------------------------------------------------------*/

double DRSBoard::VoltToFreq(double volt)
{
   if (fDRSType == 3) {
      if (volt <= 1.2001)
         return (volt - 0.6) / 0.2;
      else
         return 0.73 / 0.28 + sqrt((0.73 / 0.28) * (0.73 / 0.28) - 2.2 / 0.14 + volt / 0.14);
   } else
      return (volt - 0.5) / 0.2;
}

/*------------------------------------------------------------------*/

double DRSBoard::FreqToVolt(double freq)
{
   if (fDRSType == 3) {
      if (freq <= 3)
         return 0.6 + 0.2 * freq;
      else
         return 2.2 - 0.73 * freq + 0.14 * freq * freq;
   } else
      return 0.55 + 0.25 * freq;
}

/*------------------------------------------------------------------*/

int DRSBoard::ConfigureLMK(double sampFreq, bool freqChange, int calFreq, int calPhase)
{
   unsigned int data[] = { 0x80000100,   // RESET=1
                           0x0007FF00,   // CLKOUT0: EN=1, DIV=FF (=510) MUX=Div&Delay
                           0x00000101,   // CLKOUT1: Disabled
                           0x0082000B,   // R11: DIV4=0
                           0x028780AD,   // R13: VCO settings
                           0x0830000E,   // R14: PLL settings
                           0xC000000F }; // R15: PLL settings

   /* calculate dividing ratio */
   int divider, vco_divider, n_counter, r_counter;
   unsigned int status;
   double clk, vco;

   if (fTransport == TR_USB2) {
      /* 30 MHz clock */
      data[4]     = 0x028780AD;  // R13 according to CodeLoader 4
      clk         = 30;
      if (sampFreq < 1) {
         r_counter   = 1;
         vco_divider = 8;
         n_counter   = 5;
      } else {
         r_counter   = 1;
         vco_divider = 5;
         n_counter   = 8;
      }
   } else {
      
      if (fCtrlBits & BIT_REFCLK_SOURCE) {
         /* 19.44 MHz clock */
         data[4]     = 0x0284C0AD;  // R13 according to CodeLoader 4
         clk         = 19.44; // global clock through P2

         r_counter   = 2;
         vco_divider = 8;
         n_counter   = 16;
      } else {
         /* 33 MHz clock */
         data[4]     = 0x028840AD;  // R13 according to CodeLoader 4
         clk         = 33; // FPGA clock

         r_counter   = 2;
         vco_divider = 8;
         n_counter   = 9;
      }
   }

   vco = clk/r_counter*n_counter*vco_divider;
   divider = (int) ((vco / vco_divider / (sampFreq/2.048) / 2.0) + 0.5);

   /* return exact frequency */
   fNominalFrequency = vco/vco_divider/(divider*2)*2.048;

   /* return exact timing calibration frequency */
   fTCALFrequency = vco/vco_divider;

   /* change registers accordingly */
   data[1] = 0x00070000 | (divider << 8);   // R0
   data[5] = 0x0830000E | (r_counter << 8); // R14
   data[6] = 0xC000000F | (n_counter << 8) | (vco_divider << 26); // R15

   /* enable TCA output if requested */
   if (calFreq) {
      if (calFreq == 1)
         data[2] = 0x00050001 | (  1<<8) ; // 148.5 MHz  (33 MHz PLL)
                                           // 150 MHz    (30 MHz PLL)
                                           // 155.52 MHz (19.44 MHz PLL)
      else if (calFreq == 2) {
         data[2] = 0x00070001 | (  4<<8);  // above values divided by 8
         fTCALFrequency /= 8;
      } else if (calFreq == 3) {
         data[2] = 0x00070001 | (255<<8);  // above values divided by 510
         fTCALFrequency /= 510;
      }
   }

   /* set delay to adjsut phase */
   if (calPhase > 0)
      data[2] |= (( calPhase & 0x0F) << 4);
   else if (calPhase < 0)
      data[1] |= ((-calPhase & 0x0F) << 4);

   if (freqChange) {
      /* set all registers */    
      for (int i=0 ; i<(int)(sizeof(data)/sizeof(unsigned int)) ; i++) {
         Write(T_CTRL, REG_LMK_LSB, &data[i], 2);
         Write(T_CTRL, REG_LMK_MSB, ((char *)&data[i])+2, 2);
         // poll on serial_busy flag
         for (int j=0 ; j<100 ; j++) {
            Read(T_STATUS, &status, REG_STATUS, 4);
            if ((status & BIT_SERIAL_BUSY) == 0)
               break;
         }
      }
   } else {
      /* only enable/disable timing calibration frequency */
      Write(T_CTRL, REG_LMK_LSB, &data[1], 2);
      Write(T_CTRL, REG_LMK_MSB, ((char *)&data[1])+2, 2);

      /* poll on serial_busy flag */
      for (int j=0 ; j<100 ; j++) {
         Read(T_STATUS, &status, REG_STATUS, 4);
         if ((status & BIT_SERIAL_BUSY) == 0)
            break;
      }

      Write(T_CTRL, REG_LMK_LSB, &data[2], 2);
      Write(T_CTRL, REG_LMK_MSB, ((char *)&data[2])+2, 2);

      /* poll on serial_busy flag */
      for (int j=0 ; j<100 ; j++) {
         Read(T_STATUS, &status, REG_STATUS, 4);
         if ((status & BIT_SERIAL_BUSY) == 0)
            break;
      }
   }

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetFrequency(double demand, bool wait)
{
printf("SetFrequency(demand=%.3f, wait(bool)=%i)\n",demand,(int)wait);
  
// Set domino sampling frequency
double freq=0., voltage, delta_voltage;
unsigned short ticks;
int i, index, timeout;
int dominoModeSave = fDominoMode;
int triggerEnableSave1 = fTriggerEnable1;
int triggerEnableSave2 = fTriggerEnable2;
unsigned int d;

assert(fDRSType == 4);
assert(fBoardType == 9);

/* allowed range is 100 MHz to 6 GHz */
if (demand > 6 || demand < 0.1) return 0;


/* convert frequency in GHz into ticks counted by reference clock */
if (demand == 0) ticks = 0;             // turn off frequency generation
else ticks = static_cast < unsigned short >(1.024 / demand * fRefClock + 0.5);

printf("  ticks=%u, fRefClock=%.3f\n",ticks,fRefClock);
ticks -= 2;               // firmware counter need two additional clock cycles
//      Write(T_CTRL, REG_FREQ_SET, &ticks, 2);
//  Quick and Dirty
assert(Read(T_CTRL, &d, (REG_FREQ_SET & 0xfffffffc), sizeof(int))==sizeof(int));
d = uint32_set_bitfield(d,15,0,ticks);
fprintf(stderr,"debug  d = %08x\n",d);
assert(Write(T_CTRL, (REG_TRG_CONFIG & 0xfffffffc), &d, sizeof(int))==sizeof(int));
ticks += 2;

/* convert rounded ticks back to frequency */
if (demand > 0) demand = 1.024 / ticks * fRefClock;
fNominalFrequency = demand;

printf("  ticks=%u, fNominalFrequency=%.3f\n",ticks,fNominalFrequency);

/* wait for PLL lock if asked */
if (wait) {
   StartDomino();
   for (i=0 ; i<1000 ; i++) if (GetStatusReg() & BIT_PLL_LOCKED0) break;
   SoftTrigger();
   if (i == 1000) {
      printf("PLL did not lock for frequency %lf\n", demand);
      return 0;
      }
   }

printf(" SetFrequency(): sf=%.5f, freq=%f\n",GetTrueFrequency(),freq);

return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::RegulateFrequency(double demand)
{
  printf("RegulateFrequency(%f)\n",demand);

   // Set frequency regulation
   unsigned short target, target_hi, target_lo;

   if (demand < 0.42 || demand > 5.2)
      return 0;

   fNominalFrequency = demand;

   /* first iterate DAC value from host */
   if (!SetFrequency(demand, true))
      return 0;

   /* convert frequency in GHz into counts for 200 cycles */
   target = static_cast < unsigned short >(1024 * 200 * (32.768E6 * 4) / demand / 1E9);
   target_hi = target + 6;
   target_lo = target - 6;
   Write(T_CTRL, REG_FREQ_SET_HI, &target_hi, 2);
   Write(T_CTRL, REG_FREQ_SET_LO, &target_lo, 2);

   /* turn on regulation */
   fCtrlBits |= BIT_FREQ_AUTO_ADJ;
   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   /* optional monitoring code ... */
#if 0
   do {
      double freq;
      unsigned short dac, cnt;

      ReadFrequency(0, &freq);

      if (fBoardType == 1)
         Read(T_STATUS, &dac, REG_RDAC3, 2);
      else if (fBoardType == 2 || fBoardType == 3)
         Read(T_STATUS, &dac, REG_RDAC1, 2);

      Read(T_STATUS, &cnt, REG_FREQ1, 2);

      if (cnt < 65535)
         printf("%5d %5d %5d %1.5lf\n", dac, target, cnt, freq);

      Sleep(500);
   } while (1);
#endif

   return 1;
}

/*------------------------------------------------------------------*/

void DRSBoard::RegisterTest()
{
  printf("RegisterTest()\n");

   // Register test
#define N_REG 8

   int i, n, n_err;
   unsigned int buffer[N_REG], ret[N_REG];

   /* test single register */
   buffer[0] = 0x12345678;
   Write(T_CTRL, 0, buffer, 4);
   memset(ret, 0, sizeof(ret));
   i = Read(T_CTRL, ret, 0, 4);
   while (i != 4)
      printf("Read error single register!\n");

   printf("Reg.0: %08X - %08X\n", buffer[0], ret[0]);

   n_err = 0;
   for (n = 0; n < 100; n++) {
      for (i = 0; i < N_REG; i++)
         buffer[i] = (rand() << 16) | rand();
      Write(T_CTRL, 0, buffer, sizeof(buffer));

      memset(ret, 0, sizeof(ret));
      i = Read(T_CTRL, ret, 0, sizeof(ret));
      while (i != sizeof(ret)) {
         printf("Read error!\n");
         return;
      }

      for (i = 0; i < N_REG; i++) {
         if (n == 0)
            printf("Reg.%d: %08X - %08X\n", i, buffer[i], ret[i]);
         if (buffer[i] != ret[i]) {
            n_err++;
         }
      }
   }

   printf("Register test: %d errors\n", n_err);
}

/*------------------------------------------------------------------*/

int DRSBoard::RAMTest(int flag)
{
#define MAX_N_BYTES  128*1024   // 128 kB

   int i, j, n, bits, n_bytes, n_words, n_dwords;
   unsigned int buffer[MAX_N_BYTES/4], ret[MAX_N_BYTES/4];
   time_t now;

   bits = 24;
   n_words = 9*1024;
   n_bytes = n_words * 2;
   n_dwords = n_words/2;

   if (flag & 1) {
      /* integrety test */
      printf("Buffer size: %d (%1.1lfk)\n", n_words * 2, n_words * 2 / 1024.0);
      if (flag & 1) {
         for (i = 0; i < n_dwords; i++) {
            if (bits == 24)
               buffer[i] = (rand() | rand() << 16) & 0x00FFFFFF;   // random 24-bit values
            else
               buffer[i] = (rand() | rand() << 16);                // random 32-bit values
         }

         Reinit();
         Write(T_RAM, 0, buffer, n_bytes);
         memset(ret, 0, n_bytes);
         Read(T_RAM, ret, 0, n_bytes);
         Reinit();

         for (i = n = 0; i < n_dwords; i++) {
            if (buffer[i] != ret[i]) {
               n++;
            }
            if (i < 10)
               printf("written: %08X   read: %08X\n", buffer[i], ret[i]);
         }

         printf("RAM test: %d errors\n", n);
      }
   }

   /* speed test */
   if (flag & 2) {
      /* read continously to determine speed */
      time(&now);
      while (now == time(NULL));
      time(&now);
      i = n = 0;
      do {
         memset(ret, 0, n_bytes);

         for (j = 0; j < 10; j++) {
            Read(T_RAM, ret, 0, n_bytes);
            i += n_bytes;
         }

         if (flag & 1) {
            for (j = 0; j < n_dwords; j++)
               if (buffer[j] != ret[j])
                  n++;
         }

         if (now != time(NULL)) {
            if (flag & 1)
               printf("%d read/sec, %1.2lf MB/sec, %d errors\n", static_cast < int >(i / n_bytes),
                      i / 1024.0 / 1024.0, n);
            else
               printf("%d read/sec, %1.2lf MB/sec\n", static_cast < int >(i / n_bytes),
                      i / 1024.0 / 1024.0);
            time(&now);
            i = 0;
         }

         if (drs_kbhit())
            break;

      } while (1);

      while (drs_kbhit())
         getch();
   }

   return 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::ChipTest()
{
  printf("ChipTest()\n");
  
   int i, j, t;
   double freq, old_freq, min, max, mean, rms;
   float  waveform[1024];

   Init();
   SetChannelConfig(0, 8, 8);
   SetDominoMode(1);
   SetReadoutMode(1);
   SetDominoActive(1);
   SetTranspMode(0);
   EnableTrigger(0, 0);
   EnableTcal(1, 0);
   SelectClockSource(0);
   EnableAcal(1, 0);

   /* test 1 GHz */
   SetFrequency(1, true);
   StartDomino();
   Sleep(100);
   if (!(GetStatusReg() & BIT_PLL_LOCKED0)) {
      puts("PLL did not lock at 1 GHz");
      return 0;
   }

   /* test up to 6 GHz */
   for (freq = 5 ; freq < 6 ; freq += 0.1) {
      SetFrequency(freq, false);
      Sleep(10);
      if (!(GetStatusReg() & BIT_PLL_LOCKED0)) {
         printf("Max. frequency is %1.1lf GHz\n", old_freq);
         break;
      }
      ReadFrequency(0, &old_freq);
   }

   /* read and check at 0 calibration voltage */
   SetFrequency(5, true);
   Sleep(10);
   SoftTrigger();
   while (IsBusy());
   TransferWaves(0, 8);

   for (i=0 ; i<8 ; i++) {
      t = GetStopCell(0);
      GetWave(0, i, waveform, false, t, 0, false);
      for (j=0 ; j<1024; j++)
         if (waveform[j] < -100 || waveform[j] > 100) {
            if (j<5) {
               /* skip this cells */
            } else {
               printf("Cell error on channel %d, cell %d: %1.1lf mV instead 0 mV\n", i, j, waveform[j]);
               return 0;
            }
         }
   }

   /* read and check at +0.5V calibration voltage */
   EnableAcal(1, 0.5);
   StartDomino();
   SoftTrigger();
   while (IsBusy());
   TransferWaves(0, 8);

   for (i=0 ; i<8 ; i++) {
      t = GetStopCell(0);
      GetWave(0, i, waveform, false, t, 0, false);
      for (j=0 ; j<1024; j++)
         if (waveform[j] < 350) {
            if (j<5) {
               /* skip this cell */
            } else {
               printf("Cell error on channel %d, cell %d: %1.1lf mV instead 400 mV\n", i, j, waveform[j]);
               return 0;
            }
         }
   }

   /* read and check at -0.5V calibration voltage */
   EnableAcal(1, -0.5);
   StartDomino();
   Sleep(10);
   SoftTrigger();
   while (IsBusy());
   TransferWaves(0, 8);

   for (i=0 ; i<8 ; i++) {
      t = GetStopCell(0);
      GetWave(0, i, waveform, false, t, 0, false);
      for (j=0 ; j<1024; j++)
         if (waveform[j] > -350) {
            if (j<5) {
               /* skip this cell */
            } else {
               printf("Cell error on channel %d, cell %d: %1.1lf mV instead -400mV\n", i, j, waveform[j]);
               return 0;
            }
         }
   }

   /* check clock channel */
   GetWave(0, 8, waveform, false, 0, 0);
   min = max = mean = rms = 0;
   for (j=0 ; j<1024 ; j++) {
      if (waveform[j] > max)
         max = waveform[j];
      if (waveform[j] < min)
         min = waveform[j];
      mean += waveform[j];
   }
   mean /= 1024.0;
   for (j=0 ; j<1024 ; j++)
      rms += (waveform[j] - mean) * (waveform[j] - mean);
   rms = sqrt(rms/1024);

   if (max - min < 400) {
      printf("Error on clock channel amplitude: %1.1lf mV\n", max-min);
      return 0;
   }

   if (rms < 100 || rms > 300) {
      printf("Error on clock channel RMS: %1.1lf mV\n", rms);
      return 0;
   }

   return 1;
}

/*------------------------------------------------------------------*/

void DRSBoard::SetVoltageOffset(double offset1, double offset2)
{
  printf("SetVoltageOffset(%.3f,%.3f)\n",offset1,offset2);
  
   if (fDRSType == 3) {
      SetDAC(fDAC_ROFS_1, 0.95 - offset1);
      SetDAC(fDAC_ROFS_2, 0.95 - offset2);
   } else if (fDRSType == 2)
      SetDAC(fDAC_COFS, 0.9 - offset1);

   // let DAC settle
   Sleep(100);
}

/*------------------------------------------------------------------*/

int DRSBoard::SetInputRange(double center)
{
  printf("SetInputRange(center = %.3f)\n",center);
  
   if (fBoardType == 5 || fBoardType == 6 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
      // DRS4 USB Evaluation Boards + Mezzanine Board

      // only allow -0.5...0.5 to 0...1.0
      if (center < 0 || center > 0.5)
         return 0;

      // remember range
      fRange = center;

      // correct for sampling cell charge injection
      center *= 1.125;

      // set readout offset
      fROFS = 1.6 - center;
      
      if( fFirmwareVersion != 11030 ) // PSI
	SetDAC(fDAC_ROFS_1, fROFS);
      
   }

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetExternalClockFrequency(double frequencyMHz)
{
  printf("SetExternalClockFrequency(%.3f)\n",frequencyMHz);

   // Set the frequency of the external clock
   fExternalClockFrequency = frequencyMHz;
   return 0;
}

/*------------------------------------------------------------------*/

double DRSBoard::GetExternalClockFrequency()
{
   // Return the frequency of the external clock
   return fExternalClockFrequency;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetMultiBuffer(int flag)
{
   return 1;
}

/*------------------------------------------------------------------*/

void DRSBoard::ResetMultiBuffer(void)
{
   Reinit(); // set WP=0
   fReadPointer = 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetMultiBufferRP(void)
{
   return fReadPointer;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetMultiBufferRP(unsigned short rp)
{
   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetMultiBufferWP(void)
{
   unsigned short wp;

   wp = 0;
   return wp;
}

/*------------------------------------------------------------------*/

void DRSBoard::IncrementMultiBufferRP()
{
}

/*------------------------------------------------------------------*/

int DRSBoard::TransferWaves(int numberOfChannels)
{
   return TransferWaves(fWaveforms, numberOfChannels);
}

/*------------------------------------------------------------------*/

int DRSBoard::TransferWaves(unsigned char *p, int numberOfChannels)
{
   return TransferWaves(p, 0, numberOfChannels - 1);
}

/*------------------------------------------------------------------*/

int DRSBoard::TransferWaves(int firstChannel, int lastChannel)
{
   int offset;

   if (fTransport == TR_USB)
      offset = firstChannel * sizeof(short int) * kNumberOfBins;
   else
      offset = 0;               //in VME and USB2, always start from zero

   return TransferWaves(fWaveforms + offset, firstChannel, lastChannel);
}

/*------------------------------------------------------------------*/

int DRSBoard::TransferWaves(unsigned char *p, int firstChannel, int lastChannel)
{
  printf("TransferWaves(firstChannel=%i,lastChannel=%i)\n",firstChannel,lastChannel);
  
   // Transfer all waveforms at once from VME or USB to location
   unsigned int d;
   int n, i, offset, n_requested, n_bins;
   unsigned int   dw;
 //  unsigned short w;
   unsigned char *ptr;

   assert(fDRSType == 4);
   assert(fBoardType == 9);
   if (lastChannel >= fNumberOfChips * fNumberOfChannels)
      lastChannel = fNumberOfChips * fNumberOfChannels - 1;
   if (lastChannel < 0) {
      printf("Error: Invalid channel index %d\n", lastChannel);
      return 0;
   }

   if (firstChannel < 0 || firstChannel > fNumberOfChips * fNumberOfChannels) {
      printf("Error: Invalid channel index %d\n", firstChannel);
      return 0;
   }

   if (fTransport == TR_USB2) {
      /* USB2 FPGA contains 9 (Eval) or 10 (Mezz) channels */
      firstChannel = 0;
      if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
	if( fFirmwareVersion == 11030 ) {// NA61
	  lastChannel = 7;
	} else { // PSI
	  lastChannel = 8;
	}
      }

      else if (fBoardType == 6)
         lastChannel = 9;
   }

   else if (fTransport == TR_USB) {
      /* USB1 FPGA contains only 16 channels */
      if (lastChannel > 15)
         lastChannel = 15;
   }

   n_bins = fDecimation ? kNumberOfBins/2 : kNumberOfBins;
   n_requested = (lastChannel - firstChannel + 1) * sizeof(short int) * n_bins;
   offset = firstChannel * sizeof(short int) * n_bins;

   if (fBoardType == 6 && fFirmwareVersion >= 17147)
      n_requested += 16; // add trailer four chips
      
   if ((fBoardType == 7  || fBoardType == 8 || fBoardType == 9) 
       //&& fFirmwareVersion >= 17147 // PSI
       && (fFirmwareVersion >= 17147 || fFirmwareVersion == 11030 ) // NA61
       ) {
     n_requested += 4;  // add trailer one chip   
      
      //printf(" ------------ 1\n");
   }
   
   //n_requested += 106;
   //offset+=1;
   
   if (fMultiBuffer)
      offset += n_requested * fReadPointer;

   n = Read(T_RAM, p, offset, n_requested);
   
   printf("TransferWaves(): n_requested=%i, offset=%i, n_bins=%i\n",n_requested,offset,n_bins);
   
   if (fMultiBuffer)
      IncrementMultiBufferRP();

   if (n != n_requested) {
      printf("Error: only %d bytes read instead of %d\n", n, n_requested);
      return n;
   }

   // read trigger cells
   if (fDRSType == 4) {
      if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
         if ((fBoardType == 7  || fBoardType == 8 || fBoardType == 9)
	     //&& fFirmwareVersion >= 17147 // PSI
	     && (fFirmwareVersion >= 17147 || fFirmwareVersion == 11030 ) // NA61
	     ) {
	   
	   //printf(" ------------ 2\n");
	   
	   // new code reading trailer
            ptr = p + n_requested - 4;
            fStopCell[0] = *((unsigned short *)(ptr));
            fStopWSR[0]  = *(ptr + 2);
	    
	    /*
	    printf(" --- fStopCell[0] (new) = %i\n",fStopCell[0]);
	    printf(" --- fStopWSR[0] (new) = %i\n",fStopWSR[0]);
	    
	    
	    //printf("   n_requested - 4 = %i\n",n_requested - 4);
	    int ich = 0, icell = 0;
	    for(int ii=0;ii<n_requested;ii=ii+2) {
	      if( ii%2048 == 0 ) {
		printf("-------- ch = %i (from 0) ---------\n",ich); 
		ich++;
		icell = 0;
	      }
	      //printf("%i %i\n", ii, *((unsigned short *)(fWaveforms+ii)) );

	      unsigned short v = *((unsigned short *)(fWaveforms+ii));
	      //unsigned short v1 = v&0xF;
	      //unsigned short v2 = (v>>4)&0x3FF;

	      short a;
	      memcpy(&a,&v,sizeof(a));
	     
	      cout<< setw(4) << ii
		  << "  " << setw(4) << icell
		  //
		  << ",  " << setw(10) << (bitset<16>) v
		  << "  " << setw(6) << v
		  //
		  << "  " << setw(9) << v / 65536. 
		  //
		  //<< ",  " << setw(10) << (bitset<16>) a
		  //<< "  " << setw(6) << a // "-"
		  //
		  //<< ",  " << setw(10) << (bitset<16>) v1
		  //<< "  " << setw(4) << v1
		  //
		  //<< ",  " << setw(10) << (bitset<16>) v2
		  //<< "  " << setw(4) << v2
		  //
		  //<< ",  " << setw(6) << v2-icell
		  //
		  <<endl;
	      icell++;
	    }
	    */
	    
         } // else {
            // old code reading status register
//            fprintf(stderr,"check NA61\n");
            //Read(T_STATUS, fStopCell, REG_STOP_CELL0, 2);
            //Read(T_STATUS, &w, REG_STOP_WSR0, 2);
            Read(T_STATUS, &d, REG_STOP_CELL0, sizeof(int));
            fStopCell[0] = d>>16;
            Read(T_STATUS, &d, REG_STOP_WSR0, sizeof(int));
            fStopWSR[0]  = (d >> 24) & 0xFFFF;
	    
	    printf(" --- fStopCell[0] (old) = %i\n",fStopCell[0]);
	    printf(" --- fStopWSR[0] (old) = %i\n",fStopWSR[0]);
	 //}
	 
      } else {

         if (fBoardType == 6) {
            // new code reading trailer
            ptr = p + n_requested - 16;
            for (i=0 ; i<4 ; i++) {
               fStopCell[i] = *((unsigned short *)(ptr + i*2));
               fStopWSR[i]  = *(ptr + 8 + i);
            }
            fTriggerBus = *((unsigned short *)(ptr + 12));
         } else {
            // old code reading registers
            Read(T_STATUS, &dw, REG_STOP_CELL0, 4);
            fStopCell[0] = (dw >> 16) & 0xFFFF;
            fStopCell[1] = (dw >>  0) & 0xFFFF;
            Read(T_STATUS, &dw, REG_STOP_CELL2, 4);
            fStopCell[2] = (dw >> 16) & 0xFFFF;
            fStopCell[3] = (dw >>  0) & 0xFFFF;

            Read(T_STATUS, &dw, REG_STOP_WSR0, 4);
            fStopWSR[0] = (dw >> 24) & 0xFF;
            fStopWSR[1] = (dw >> 16) & 0xFF;
            fStopWSR[2] = (dw >>  8) & 0xFF;
            fStopWSR[3] = (dw >>  0) & 0xFF;

            Read(T_STATUS, &fTriggerBus, REG_TRIGGER_BUS, 2);
         }
      }
   }
  
   return n;
}

/*------------------------------------------------------------------*/

int DRSBoard::DecodeWave(unsigned int chipIndex, unsigned char channel, unsigned short *waveform)
{
   return DecodeWave(fWaveforms, chipIndex, channel, waveform);
}

/*------------------------------------------------------------------*/

int DRSBoard::DecodeWave(unsigned char *waveforms, unsigned int chipIndex, unsigned char channel,
                         unsigned short *waveform)
{
  //printf("DecodeWave(...)\n");
  
   // Get waveform
   int i, offset=0 /*, ind, n_bins */;

   /* check valid parameters */
   assert((int)channel < fNumberOfChannels);
   assert((int)chipIndex < fNumberOfChips);

   /* remap channel */
   if (fBoardType == 1) {
      if (channel < 8)
         channel = 7 - channel;
      else
         channel = 16 - channel;
   } else if (fBoardType == 6) {
      if (fReadoutChannelConfig == 7) {
         if (channel < 8)
            channel = 7-channel;
      } else if (fReadoutChannelConfig == 4) {
         if (channel == 8)
            channel = 4;
         else
            channel = 3 - channel/2;
      } else {
         channel = channel / 2;
         if (channel != 4)
           channel = 3-channel;
      }
   } /* else
      channel = channel; */

   // Read channel
   if (fTransport == TR_USB) {
      offset = kNumberOfBins * 2 * (chipIndex * 16 + channel);
      for (i = 0; i < kNumberOfBins; i++) {
         // 12-bit data
         waveform[i] = ((waveforms[i * 2 + 1 + offset] & 0x0f) << 8) + waveforms[i * 2 + offset];
      }
   } else if (fTransport == TR_USB2) {

     if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
         // see dpram_map_eval1.xls
         offset = kNumberOfBins * 2 * (chipIndex * 16 + channel);

	 //printf("DecodeWave(): kNumberOfBins=%i, chipIndex=%i, channel=%i, offset=%i\n",kNumberOfBins,chipIndex,channel,offset);

     } else if (fBoardType == 6) {
         // see dpram_map_mezz1.xls mode 0-3
         offset = (kNumberOfBins * 4) * (channel % 9) + 2 * (chipIndex/2);
      }

      for (i = 0; i < kNumberOfBins; i++) {
         // 16-bit data
	if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
        
	  waveform[i] = ((waveforms[i * 2 + 1 + offset] & 0xff) << 8) + waveforms[i * 2 + offset];
	  /*
	  if( fFirmwareVersion == 11030 ) { // NA61
	    waveform[i] = ((waveforms[i*2+1+offset]&0xff)<<4) + ((waveforms[i*2+  offset]&0xff)>>4);
	  } else { // PSI
	    waveform[i] = ((waveforms[i * 2 + 1 + offset] & 0xff) << 8) + waveforms[i * 2 + offset];
	  }
	  */
	  
	  /*
	  if(channel==0) {
	    //printf("DecodeWave(): %3i  %4x %4x %4x\n",i, *(waveform+i), *(waveforms+i*2+1+offset), *(waveforms+i*2+offset) );
	    
	    std::cout << "DecodeWave(): " << setw(4) << i
		      << "  " << setw(6) << waveform[i]  
		      << "  " << (bitset<16>) waveform[i]
		      << "  " << (bitset<8>) waveforms[i * 2 + 1]
		      << "  " << (bitset<8>) waveforms[i * 2 ]
	      //<< "  " << (bitset<12>) waveform[i]
		      << endl;
	  	    
	    //std::cout << (bitset<16>) (*(waveform+i));
	  }
	  */
	  
	} else if (fBoardType == 6)
            waveform[i] = ((waveforms[i * 4 + 1 + offset] & 0xff) << 8) + waveforms[i * 4 + offset];
      }

   } 
  else {
      printf("Error: invalid transport %d\n", fTransport);
      return kInvalidTransport;
   }
   return kSuccess;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetWave(unsigned int chipIndex, unsigned char channel, float *waveform)
{
   return GetWave(chipIndex, channel, waveform, true, fStopCell[chipIndex], -1, false, 0, true);
}

/*------------------------------------------------------------------*/

int DRSBoard::GetWave(unsigned int chipIndex, unsigned char channel, short *waveform, bool responseCalib,
                      int triggerCell, int wsr, bool adjustToClock, float threshold, bool offsetCalib)
{
   return GetWave(fWaveforms, chipIndex, channel, waveform, responseCalib, triggerCell, wsr, adjustToClock,
                  threshold, offsetCalib);
}

/*------------------------------------------------------------------*/

int DRSBoard::GetWave(unsigned int chipIndex, unsigned char channel, float *waveform, bool responseCalib,
                      int triggerCell, int wsr, bool adjustToClock, float threshold, bool offsetCalib)
{
   return GetWave(fWaveforms, chipIndex, channel, waveform, responseCalib, triggerCell, wsr, adjustToClock, threshold,
               offsetCalib);
}

/*------------------------------------------------------------------*/

int DRSBoard::GetWave(unsigned char *waveforms, unsigned int chipIndex, unsigned char channel,
                      float *waveform, bool responseCalib, int triggerCell, int wsr, bool adjustToClock,
                      float threshold, bool offsetCalib)
{
   int ret, i;
   short waveS[2*kNumberOfBins];
   ret =
       GetWave(waveforms, chipIndex, channel, waveS, responseCalib, triggerCell, wsr, adjustToClock, threshold,
               offsetCalib);
   if (responseCalib)
      for (i = 0; i < fChannelDepth ; i++)
         waveform[i] = static_cast < float >(static_cast <short> (waveS[i]) * GetPrecision());
   else {
      for (i = 0; i < fChannelDepth ; i++) {
         if (fBoardType == 4 || fBoardType == 5 || fBoardType == 6 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
            waveform[i] = static_cast < float >(waveS[i] * GetPrecision());
         } else
            waveform[i] = static_cast < float >(waveS[i]);
      }
   }
   return ret;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetWave(unsigned char *waveforms, unsigned int chipIndex, unsigned char channel,
                      short *waveform, bool responseCalib, int triggerCell, int wsr, bool adjustToClock,
                      float threshold, bool offsetCalib)
{
  //printf("GetWave()\n");
  //printf("GetWave(): fChannelCascading=%i\n",fChannelCascading);
  
   unsigned short adcWaveform[kNumberOfBins];
   int i, ret;

   if (fChannelCascading == 1 || channel == 8) {
     
     /* single channel configuration */
      ret = DecodeWave(waveforms, chipIndex, channel, adcWaveform);
      if (ret != kSuccess)
         return ret;

      ret = CalibrateWaveform(chipIndex, channel, adcWaveform, waveform, responseCalib,
                              triggerCell, adjustToClock, threshold, offsetCalib);

      return ret;

   } else if (fChannelCascading == 2) {
     
      /* double channel configuration */
      short wf1[kNumberOfBins];
      short wf2[kNumberOfBins];

      // first half
      ret = DecodeWave(waveforms, chipIndex, 2*channel, adcWaveform);
      if (ret != kSuccess)
         return ret;

      ret = CalibrateWaveform(chipIndex, 2*channel, adcWaveform, wf1, responseCalib,
                              triggerCell, adjustToClock, threshold, offsetCalib);

      // second half
      ret = DecodeWave(waveforms, chipIndex, 2*channel+1, adcWaveform);
      if (ret != kSuccess)
         return ret;

      ret = CalibrateWaveform(chipIndex, 2*channel+1, adcWaveform, wf2, responseCalib,
                              triggerCell, adjustToClock, threshold, offsetCalib);


      // combine two halfs correctly, see 2048_mode.ppt
      if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
         if ((wsr == 0 && triggerCell < 767) ||
             (wsr == 1 && triggerCell >= 767)) {
            for (i=0 ; i<kNumberOfBins-triggerCell ; i++)
               waveform[i] = wf1[i];
            for (; i<kNumberOfBins; i++)
               waveform[i] = wf2[i];
            for (i=0 ; i<kNumberOfBins-triggerCell ; i++)
               waveform[i+kNumberOfBins] = wf2[i];
            for (; i<kNumberOfBins; i++)
               waveform[i+kNumberOfBins] = wf1[i];
         } else {
            for (i=0 ; i<kNumberOfBins-triggerCell ; i++)
               waveform[i] = wf2[i];
            for (; i<kNumberOfBins; i++)
               waveform[i] = wf1[i];
            for (i=0 ; i<kNumberOfBins-triggerCell ; i++)
               waveform[i+kNumberOfBins] = wf1[i];
            for (; i<kNumberOfBins; i++)
               waveform[i+kNumberOfBins] = wf2[i];
         }
      } else {
         if (wsr == 1) {
            if (fDecimation) {
               for (i=0 ; i<kNumberOfBins/2-triggerCell/2 ; i++)
                  waveform[i] = wf1[i];
               for (; i<kNumberOfBins/2; i++)
                  waveform[i] = wf2[i];
               for (i=0 ; i<kNumberOfBins/2-triggerCell/2 ; i++)
                  waveform[i+kNumberOfBins/2] = wf2[i];
               for (; i<kNumberOfBins/2; i++)
                  waveform[i+kNumberOfBins/2] = wf1[i];
            } else {
               for (i=0 ; i<kNumberOfBins-triggerCell ; i++)
                  waveform[i] = wf1[i];
               for (; i<kNumberOfBins; i++)
                  waveform[i] = wf2[i];
               for (i=0 ; i<kNumberOfBins-triggerCell ; i++)
                  waveform[i+kNumberOfBins] = wf2[i];
               for (; i<kNumberOfBins; i++)
                  waveform[i+kNumberOfBins] = wf1[i];
            }
         } else {
            if (fDecimation) {
               for (i=0 ; i<kNumberOfBins/2-triggerCell/2 ; i++)
                  waveform[i] = wf2[i];
               for (; i<kNumberOfBins/2; i++)
                  waveform[i] = wf1[i];
               for (i=0 ; i<kNumberOfBins/2-triggerCell/2 ; i++)
                  waveform[i+kNumberOfBins/2] = wf1[i];
               for (; i<kNumberOfBins/2; i++)
                  waveform[i+kNumberOfBins/2] = wf2[i];
            } else {
               for (i=0 ; i<kNumberOfBins-triggerCell ; i++)
                  waveform[i] = wf2[i];
               for (; i<kNumberOfBins; i++)
                  waveform[i] = wf1[i];
               for (i=0 ; i<kNumberOfBins-triggerCell ; i++)
                  waveform[i+kNumberOfBins] = wf1[i];
               for (; i<kNumberOfBins; i++)
                  waveform[i+kNumberOfBins] = wf2[i];
            }
         }
      }

      return ret;
   } else
      assert(!"Not implemented");

   return 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetRawWave(unsigned int chipIndex, unsigned char channel, unsigned short *waveform,
                         bool adjustToClock)
{
   return GetRawWave(fWaveforms, chipIndex, channel, waveform, adjustToClock);
}

/*------------------------------------------------------------------*/

int DRSBoard::GetRawWave(unsigned char *waveforms, unsigned int chipIndex, unsigned char channel, 
                         unsigned short *waveform, bool adjustToClock)
{
   int i, status, tc;
   unsigned short wf[kNumberOfBins];

   status = DecodeWave(waveforms, chipIndex, channel,  wf);

   if (adjustToClock) {
      tc = GetTriggerCell(chipIndex);
      for (i = 0 ; i < kNumberOfBins; i++)
         waveform[(i + tc) % kNumberOfBins] = wf[i];
   } else {
      for (i = 0 ; i < kNumberOfBins; i++)
         waveform[i] = wf[i];
   }

   return status;
}

/*------------------------------------------------------------------*/

int DRSBoard::CalibrateWaveform(unsigned int chipIndex, unsigned char channel, unsigned short *adcWaveform,
                                short *waveform, bool responseCalib,
                                int triggerCell, bool adjustToClock, float threshold, bool offsetCalib)
{
  //printf("CalibrateWaveform(...)\n");

   int j, n_bins, skip;
   double value;
   short left, right;

   //printf("CalibrateWaveform(): responseCalib=%i, fVoltageCalibrationValid=%i\n",(int)responseCalib,(int)fVoltageCalibrationValid);

   // calibrate waveform
   if (responseCalib && fVoltageCalibrationValid) {
      if (GetDRSType() == 4) {
         // if Mezz though USB2 -> select correct calibration channel
         if (fBoardType == 6 && (fReadoutChannelConfig == 0 || fReadoutChannelConfig == 2) &&
             channel != 8)
            channel++;

         // Channel readout mode #4 -> select correct calibration channel
         if (fBoardType == 6 && fReadoutChannelConfig == 4 && channel % 2 == 0 && channel != 8)
            channel++;

         n_bins = fDecimation ? kNumberOfBins/2 : kNumberOfBins;
         skip = fDecimation ? 2 : 1;
	 
         for (j = 0; j < n_bins; j++) {
	   
            value = adcWaveform[j] - fCellOffset[channel+chipIndex*9][(j*skip + triggerCell) % kNumberOfBins];
            value = value / fCellGain[channel+chipIndex*9][(j*skip + triggerCell) % kNumberOfBins];

            if (offsetCalib && channel != 8)
               value = value - fCellOffset2[channel+chipIndex*9][j*skip] + 32768;

            /* convert to units of 0.1 mV */
            value = value / 65536.0 * 1000 * 10; 

            /* apply clipping */
            if (channel != 8) {
               if (adcWaveform[j] >= 0xFFF0 || value > (fRange * 1000 + 500) * 10)
                  value = (fRange * 1000 + 500) * 10;
               if (adcWaveform[j] <  0x0010 || value < (fRange * 1000 - 500) * 10)
                  value = (fRange * 1000 - 500) * 10;
            }

            if (adjustToClock)          
               waveform[(j + triggerCell) % kNumberOfBins] = (short) (value + 0.5);
            else
               waveform[j] = (short) (value + 0.5); 
         }

         // check for stuck pixels and replace by average of neighbors
         for (j = 0 ; j < n_bins; j++) {
            if (adjustToClock) {
               if (fCellOffset[channel+chipIndex*9][j*skip] == 0) {
                  left = waveform[(j-1+kNumberOfBins) % kNumberOfBins];
                  right = waveform[(j+1) % kNumberOfBins];
                  waveform[j] = (short) ((left+right)/2);
               }
            } else {
               if (fCellOffset[channel+chipIndex*9][(j*skip + triggerCell) % kNumberOfBins] == 0) {
                  left = waveform[(j-1+kNumberOfBins) % kNumberOfBins];
                  right = waveform[(j+1) % kNumberOfBins];
                  waveform[j] = (short) ((left+right)/2);
               }
            }
         }

      } else {
         if (!fResponseCalibration->
             Calibrate(chipIndex, channel % 10, adcWaveform, waveform, triggerCell, threshold, offsetCalib))
            return kZeroSuppression;       // return immediately if below threshold
      }
      
   } else { // no calib
     
     //printf("----------- \n");

      if (GetDRSType() == 4) {
         // if Mezz though USB2 -> select correct calibration channel
         if (fBoardType == 6 && (fReadoutChannelConfig == 0 || fReadoutChannelConfig == 2) &&
             channel != 8)
            channel++;

	 //printf(" fRange=%f\n",fRange);
	 
         for (j = 0 ; j < kNumberOfBins; j++) {
	   
	    value = 0;
	    //printf("%i %f %f\n",j,fRange,value);
	    
	    if( fFirmwareVersion == 11030 && false ) { // NA61
	      
	      short a;
	      memcpy(&a,&adcWaveform[j],sizeof(a));
	      value = a;
	      
	      /*
	      float value2 = 0;
	      unsigned short b = adcWaveform[j];
	      if( b & 0x8000 ) { // -
		b = (~b) + 1;
		value2 = -b;
	      } else { // +
		value2 = b;
	      }
	      value2 /= 65536.; value2 *= (1000 * 10);
	      */
	      
	      value /= 65536.; // 2^16

	      value *= (1000 * 10);
	      
	      /*
	      std::cout << setw(4) << j 
		        << "  " << setw(7) << (bitset<16>) adcWaveform[j]
		      //<< " " << setw(7) << a
			<< " " << (bitset<16>) a
			<< " " << setw(9) << value << endl;
	      //<< " " << setw(9) << value2 << endl;
	      */
	      
	    } else { // PSI

	      value = adcWaveform[j];

	      /* convert to units of 0.1 mV */
	      value = (value - 32768) / 65536.0 * 1000 * 10; 

	      /*
	      std::cout << setw(4) << j 
		        << "  " << setw(7) << (bitset<16>) adcWaveform[j]
			<< " " << setw(9) << value << endl;
	      */

	      /* correct for range */
	      value += fRange * 1000 * 10;
	      
	    }
	    
	    //printf(" adjustToClock=%i\n",adjustToClock);

            if (adjustToClock)          
               waveform[(j + triggerCell) % kNumberOfBins] = (short) (value + 0.5);
            else
               waveform[j] = (short) (value + 0.5); 
         }
	 
	 
	 
      } else {
         for (j = 0; j < kNumberOfBins; j++) {
            if (adjustToClock) {
               // rotate waveform such that waveform[0] corresponds to bin #0 on the chip
               waveform[j] = adcWaveform[(kNumberOfBins-triggerCell+j) % kNumberOfBins];
            } else {
               waveform[j] = adcWaveform[j];
            }
         }
      }
   }
   
   // fix bad cells for single turn mode
   if (GetDRSType() == 2) {
      if (fDominoMode == 0 && triggerCell == -1) {
         waveform[0] = 2 * waveform[1] - waveform[2];
         short m1 = (waveform[kNumberOfBins - 5] + waveform[kNumberOfBins - 6]) / 2;
         short m2 = (waveform[kNumberOfBins - 6] + waveform[kNumberOfBins - 7]) / 2;
         waveform[kNumberOfBins - 4] = m1 - 1 * (m2 - m1);
         waveform[kNumberOfBins - 3] = m1 - 2 * (m2 - m1);
         waveform[kNumberOfBins - 2] = m1 - 3 * (m2 - m1);
         waveform[kNumberOfBins - 1] = m1 - 4 * (m2 - m1);
      }
   }
   
   /*
   printf("\n-----------------------------\n");
   
   for (j = 0 ; j < kNumberOfBins; j++) {
     printf("%4i %4d %4d \n",j,waveform[j],waveform[j]>>4);
   }
   */
   
   return kSuccess;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetStretchedTime(float *time, float *measurement, int numberOfMeasurements, float period)
{
   int j;
   if (*time >= measurement[numberOfMeasurements - 1]) {
      *time -= measurement[numberOfMeasurements - 1];
      return 1;
   }
   if (*time < measurement[0]) {
      *time = *time - measurement[0] - (numberOfMeasurements - 1) * period / 2;
      return 1;
   }
   for (j = 0; j < numberOfMeasurements - 1; j++) {
      if (*time > measurement[j] && *time <= measurement[j + 1]) {
         *time =
             (period / 2) / (measurement[j + 1] - measurement[j]) * (*time - measurement[j + 1]) -
             (numberOfMeasurements - 2 - j) * period / 2;
         return 1;
      }
   }
   return 0;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetTriggerCell(unsigned int chipIndex)
{
   if (fDRSType == 4)
      return GetStopCell(chipIndex);

   return GetTriggerCell(fWaveforms, chipIndex);
}

/*------------------------------------------------------------------*/

int DRSBoard::GetTriggerCell(unsigned char *waveforms, unsigned int chipIndex)
{
   int j, triggerCell;
   bool calib;
   unsigned short baseLevel = 1000;
   unsigned short triggerChannel[1024];

   if (fDRSType == 4)
      return GetStopCell(chipIndex);

   GetRawWave(waveforms, chipIndex, 8, triggerChannel);
   calib = fResponseCalibration->SubtractADCOffset(chipIndex, 8, triggerChannel, triggerChannel, baseLevel);

   triggerCell = -1;
   for (j = 0; j < kNumberOfBins; j++) {
      if (calib) {
         if (triggerChannel[j] <= baseLevel + 200
             && triggerChannel[(j + 1) % kNumberOfBins] > baseLevel + 200) {
            triggerCell = j;
            break;
         }
      } else {
         if (fDRSType == 3) {
            if (triggerChannel[j] <= 2000 && triggerChannel[(j + 1) % kNumberOfBins] > 2000) {
               triggerCell = j;
               break;
            }
         } else {
            if (triggerChannel[j] >= 2000 && triggerChannel[(j + 1) % kNumberOfBins] < 2000) {
               triggerCell = j;
               break;
            }
         }
      }
   }
   if (triggerCell == -1) {
      return kInvalidTriggerSignal;
   }
   fStopCell[0] = triggerCell;
   return triggerCell;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetStopCell(unsigned int chipIndex)
{
   return fStopCell[chipIndex];
}

/*------------------------------------------------------------------*/

unsigned char DRSBoard::GetStopWSR(unsigned int chipIndex)
{
   return fStopWSR[chipIndex];
}

/*------------------------------------------------------------------*/

void DRSBoard::TestDAC(int channel)
{
   // Test DAC
   int status;

   do {
      status = SetDAC(channel, 0);
      Sleep(1000);
      status = SetDAC(channel, 0.5);
      Sleep(1000);
      status = SetDAC(channel, 1);
      Sleep(1000);
      status = SetDAC(channel, 1.5);
      Sleep(1000);
      status = SetDAC(channel, 2);
      Sleep(1000);
      status = SetDAC(channel, 2.5);
      Sleep(1000);
   } while (status);
}

/*------------------------------------------------------------------*/

void DRSBoard::MeasureSpeed()
{
   // Measure domino sampling speed
   FILE *f;
   double vdr, vds, freq;

   f = fopen("speed.txt", "wt");
   fprintf(f, "\t");
   printf("\t");
   for (vdr = 0.5; vdr <= 2.501; vdr += 0.05) {
      fprintf(f, "%1.2lf\t", vdr);
      printf("%1.2lf\t", vdr);
   }
   fprintf(f, "\n");
   printf("\n");

   for (vds = 0.5; vds <= 2.501; vds += 0.05) {
      fprintf(f, "%1.2lf\t", vds);
      printf("%1.2lf\t", vds);

      SetDAC(fDAC_DSA, vds);
      StartDomino();
      Sleep(1000);
      ReadFrequency(0, &freq);

      fprintf(f, "%1.3lf\t", freq);
      printf("%1.3lf\t", freq);

      fprintf(f, "\n");
      printf("\n");
      fflush(f);
   }
}

/*------------------------------------------------------------------*/

void DRSBoard::InteractSpeed()
{
   int status, i;
   double freq, vds;

   do {
      printf("DS: ");
      scanf("%lf", &vds);
      if (vds == 0)
         break;

      SetDAC(fDAC_DSA, vds);
      SetDAC(fDAC_DSB, vds);

      StartDomino();
      for (i = 0; i < 4; i++) {
         Sleep(1000);

         status = ReadFrequency(0, &freq);
         if (!status)
            break;
         printf("%1.6lf GHz\n", freq);
      }

      /* turn BOARD_LED off */
      SetLED(0);

   } while (1);
}

/*------------------------------------------------------------------*/

void DRSBoard::MonitorFrequency()
{
   // Monitor domino sampling frequency
   int status;
   unsigned int data;
   double freq, dac;
   FILE *f;
   time_t now;
   char str[256];

   f = fopen("DRSBoard.log", "w");

   do {
      Sleep(1000);

      status = ReadFrequency(0, &freq);
      if (!status)
         break;

      data = 0;
      if (fBoardType == 1)
         Read(T_STATUS, &data, REG_RDAC3, 2);
      else if (fBoardType == 2 || fBoardType == 3)
         Read(T_STATUS, &data, REG_RDAC1, 2);

      dac = data / 65536.0 * 2.5;
      printf("%1.6lf GHz, %1.4lf V\n", freq, dac);
      time(&now);
      strcpy(str, ctime(&now) + 11);
      str[8] = 0;

      fprintf(f, "%s %1.6lf GHz, %1.4lf V\n", str, freq, dac);
      fflush(f);

   } while (!drs_kbhit());

   fclose(f);
}

/*------------------------------------------------------------------*/

int DRSBoard::TestShift(int n)
{
   // Test shift register
   unsigned char buffer[3];

   memset(buffer, 0, sizeof(buffer));

#if 0
   buffer[0] = CMD_TESTSHIFT;
   buffer[1] = n;

   status = msend_usb(buffer, 2);
   if (status != 2)
      return status;

   status = mrecv_usb(buffer, sizeof(buffer));
   if (status != 1)
      return status;
#endif

   if (buffer[0] == 1)
      printf("Shift register %c works correctly\n", 'A' + n);
   else if (buffer[0] == 2)
      printf("SROUT%c does hot go high after reset\n", 'A' + n);
   else if (buffer[0] == 3)
      printf("SROUT%c does hot go low after 1024 clocks\n", 'A' + n);

   return 1;
}

/*------------------------------------------------------------------*/

unsigned int DRSBoard::GetCtrlReg()
{
   unsigned int status;

   Read(T_CTRL, &status, REG_CTRL, 4);
   return status;
}

/*------------------------------------------------------------------*/

unsigned short DRSBoard::GetConfigReg()
{
   unsigned short status;

   Read(T_CTRL, &status, REG_CONFIG, 2);
   return status;
}

/*------------------------------------------------------------------*/

unsigned int DRSBoard::GetStatusReg()
{
   unsigned int status;

   Read(T_STATUS, &status, REG_STATUS, 4);
   return status;
}

/*------------------------------------------------------------------*/

int DRSBoard::EnableTcal(int freq, int level, int phase)
{
  //printf("EnableTcal(freq=%i,level=%i,phase=%i)\n",freq,level,phase);
  
   fTcalFreq = freq;
   fTcalLevel = level;
   fTcalPhase = phase;

   if (fBoardType == 6) {
      ConfigureLMK(fNominalFrequency, false, freq, phase);
   } else {
      if (fBoardType == 9) {
         // Enable clock a switch channel multiplexers
         if (freq) {
            fCtrlBits |= (BIT_TCAL_EN | BIT_ACAL_EN);
         } else
            fCtrlBits &= ~(BIT_TCAL_EN | BIT_ACAL_EN);
         
      } else {
         // Enable clock channel
         if (freq)
            fCtrlBits |= BIT_TCAL_EN;
         else
            fCtrlBits &= ~(BIT_TCAL_EN | BIT_TCAL_SOURCE);
         
         // Set output level, needed for gain calibration
         if (fDRSType == 4) {
            if (level)
               fCtrlBits |= BIT_NEG_TRIGGER;
            else
               fCtrlBits &= ~BIT_NEG_TRIGGER;
         }
      }

      Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
   }

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SelectClockSource(int source)
{
  printf("SelectClockSource(source=%i)\n",source);

   fTcalSource = source;

   // Select clock source:
   // EVAL1: synchronous (0) or asynchronous (1) (2nd quartz)
   if (fBoardType <= 8) {
      if (source)
         fCtrlBits |= BIT_TCAL_SOURCE;
      else
         fCtrlBits &= ~BIT_TCAL_SOURCE;
      
      Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
   }

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetRefclk(int source)
{
  printf("SetRefclk(%i (=0 FPGA) )\n",source);
  
   // Select reference clock source to internal FPGA (0) or external P2 (1)
   if (fBoardType == 6) {
      if (source) 
         fCtrlBits |= BIT_REFCLK_SOURCE;
      else
         fCtrlBits &= ~BIT_REFCLK_SOURCE;
   } else if (fBoardType == 8 || fBoardType == 9) {
      if (source) 
         fCtrlBits |= BIT_REFCLK_EXT;
      else
         fCtrlBits &= ~BIT_REFCLK_EXT;
   }

   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
   fRefclk = source;

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::EnableAcal(int mode, double voltage)
{
  printf("EnableAcal(mode=%i,voltage=%.3f V)\n",mode,voltage);
  
   double t1, t2;

   fAcalMode = mode;
   fAcalVolt = voltage;

   if (mode == 0) {
      /* turn calibration off */
      SetCalibTiming(0, 0);
      if (fBoardType == 5 || fBoardType == 6) {
         /* turn voltages off (50 Ohm analog switch!) */
         SetDAC(fDAC_CALP, 0);
         SetDAC(fDAC_CALN, 0);
      }
      if (fBoardType == 7 || fBoardType == 8 || fBoardType == 9)
         SetCalibVoltage(0);

      fCtrlBits &= ~BIT_ACAL_EN;
      Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   } else if (mode == 1) {

      /* static calibration */
      SetCalibVoltage(voltage);
      SetCalibTiming(0, 0);
      fCtrlBits |= BIT_ACAL_EN;
      Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

   } else if (mode == 2) {
      /* first part calibration:
         stop domino wave after 1.2 revolutions
         turn on calibration voltage after 0.1 revolutions */

      /* ensure circulating domino wave */
      SetDominoMode(1);

      /* set calibration voltage but do not turn it on now */
      SetCalibVoltage(voltage);
      fCtrlBits &= ~BIT_ACAL_EN;
      Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

      /* calculate duration of DENABLE signal as 1.2 revolutions */
      t1 = 1 / fNominalFrequency * 1024 * 1.2; // ns
      t1 = static_cast < int >((t1 - 30) / 30 + 1);     // 30 ns offset, 30 ns units, rounded up
      t2 = 1 / fNominalFrequency * 1024 * 0.1; // ns
      t2 = static_cast < int >((t2 - 30) / 30 + 1);     // 30 ns offset, 30 ns units, rounded up
      SetCalibTiming(static_cast < int >(t1), static_cast < int >(t2));

   } else if (mode == 3) {
      /* second part calibration:
         stop domino wave after 1.05 revolutions */

      /* ensure circulating domino wave */
      SetDominoMode(1);

      /* turn on and let settle calibration voltage */
      SetCalibVoltage(voltage);
      fCtrlBits |= BIT_ACAL_EN;
      Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);

      /* calculate duration of DENABLE signal as 1.1 revolutions */
      t1 = 1 / fNominalFrequency * 1024 * 1.05;        // ns
      t1 = static_cast < int >((t1 - 30) / 30 + 1);     // 30 ns offset, 30 ns units, rounded up
      SetCalibTiming(static_cast < int >(t1), 0);
   }

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetCalibTiming(int t_enable, int t_cal)
{
   unsigned short d;

   if (fDRSType == 2) {
      d = t_cal | (t_enable << 8);
      Write(T_CTRL, REG_CALIB_TIMING, &d, 2);
   }

   if (fDRSType == 3) {
      d = t_cal;
      Write(T_CTRL, REG_CALIB_TIMING, &d, 2);
   }

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::SetCalibVoltage(double value)
{
  printf("SetCalibVoltage(%.3f)\n",value);
  
   // Set Calibration Voltage
   if (fBoardType == 5 || fBoardType == 6 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
     
     if( fFirmwareVersion == 11030 ) { // NA61
       
       SetDAC(fDAC_VCAL, value );
       
     } else { // PSI
       
       if (fBoardType == 5)
         value = value * (1+fNominalFrequency/65); // rough correction factor for input current
       if (fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
	 value = value * (1+fNominalFrequency/47); // rough correction factor for input current
       }
       printf(" SetCalibVoltage(): fCommonMode=%.3f, value=%.3f\n",fCommonMode,value);
       
       SetDAC(fDAC_CALP, fCommonMode + value / 2);
       SetDAC(fDAC_CALN, fCommonMode - value / 2);
       
     }
     
   } else
      SetDAC(fDAC_ACALIB, value);
   
   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::StartClearCycle()
{
  printf("StartClearCycle()\n");

   /* clear cycle is necessary for DRS4 to reduce noise */

   fbkAcalVolt  = fAcalVolt;
   fbkAcalMode  = fAcalMode;
   fbkTcalFreq  = fTcalFreq;
   fbkTcalLevel = fTcalLevel;

   /* switch all inputs to zero */
   EnableAcal(1, 0);

   /* start, stop and readout of zero */
   StartDomino();
   SoftTrigger();

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::FinishClearCycle()
{
   while (IsBusy());

   /* restore old values */
   EnableAcal(fbkAcalMode, fbkAcalVolt);

   return 1;
}

/*------------------------------------------------------------------*/

double DRSBoard::GetTemperature()
{
   // Read Out Temperature Sensor
   unsigned char buffer[2];
   unsigned short d;
   double temperature;

   Read(T_STATUS, buffer, REG_TEMPERATURE, 2);

   d = (static_cast < unsigned int >(buffer[1]) << 8) +buffer[0];
   temperature = ((d >> 3) & 0x0FFF) * 0.0625;

   return temperature;
}

/*------------------------------------------------------------------*/

int DRSBoard::Is2048ModeCapable()
{
   unsigned int status;
  
   if (fFirmwareVersion < 21305)
      return 0;
   
   // Read pin J44 and return 1 if 2048 mode has been soldered
   Read(T_STATUS, &status, REG_STATUS, 4);
   if ((status & BIT_2048_MODE))
      return 0;
   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetTriggerBus()
{
   /* unsigned size, d; */

   Read(T_STATUS, &fTriggerBus, REG_TRIGGER_BUS, 2);
   return static_cast < int >(fTriggerBus);
}


/*------------------------------------------------------------------*/

unsigned int DRSBoard::GetScaler(int channel)
{
   int reg=0;
   unsigned d;
   
   if (fBoardType < 9 || fFirmwareVersion < 21000 || fTransport != TR_USB2)
      return 0;
   
   switch (channel ) {
      case 0: reg = REG_SCALER0; break;
      case 1: reg = REG_SCALER1; break;
      case 2: reg = REG_SCALER2; break;
      case 3: reg = REG_SCALER3; break;
      case 4: reg = REG_SCALER4; break;
      case 5: reg = REG_SCALER5; break;
   }
   
   Read(T_STATUS, &d, reg, 4);

   return static_cast < unsigned int >(d * 10); // measurement clock is 10 Hz
}


/*------------------------------------------------------------------*/

int DRSBoard::SetBoardSerialNumber(unsigned short serialNumber)
{
   unsigned char buf[32768];

/* merge serial number into eeprom page #0 */
ReadEEPROM(0, buf, sizeof(buf));
buf[0] = serialNumber & 0xFF;
buf[1] = serialNumber >> 8;
WriteEEPROM(0, buf, sizeof(buf));

/* erase DPRAM */
memset(buf, 0, sizeof(buf));
Write(T_RAM, 0, buf, sizeof(buf));

/* read back EEPROM */
ReadEEPROM(0, buf, sizeof(buf));

/* check if correctly set */
if (((buf[1] << 8) | buf[0]) != serialNumber) return 0;

fBoardSerialNumber = serialNumber;

return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::ReadEEPROM(unsigned short page, void *buffer, int size)
{
  printf(" ReadEEPROM(page=%i, ,size=%i)\n",page,size);
   assert(fDRSType == 4);
   assert(fBoardType == 9);

   int i;
   unsigned int d;
   unsigned long status;
   // write eeprom page number
   if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
//  Quick and Dirty
//    Write(T_CTRL, REG_EEPROM_PAGE_EVAL, &page, 2);
      assert(Read(T_CTRL, &d, (REG_EEPROM_PAGE_EVAL & 0xfffffffc), sizeof(int))==sizeof(int));
//      d = (d & 0xFFFF0000) | page;
      d = uint32_set_bitfield(d,15,0,page);
      fprintf(stderr,"debug  d = %08x\n",d);
      assert(Write(T_CTRL, (REG_EEPROM_PAGE_EVAL & 0xfffffffc), &d, sizeof(int))==sizeof(int));
      }
   else if (fBoardType == 6)
      Write(T_CTRL, REG_EEPROM_PAGE_MEZZ, &page, 2);
   else 
      return -1;

   // execute eeprom read
   fCtrlBits |= BIT_EEPROM_READ_TRIG;
   assert(Write(T_CTRL, REG_CTRL, &fCtrlBits, 4)==4);
   fCtrlBits &= ~BIT_EEPROM_READ_TRIG;

   // poll on serial_busy flag
   for (i=0 ; i<100 ; i++) {
      assert(Read(T_STATUS, &status, REG_STATUS, 4)==4);
      if ((status & BIT_SERIAL_BUSY) == 0)
         break;
      Sleep(10); // ms
   }
   
   int iread = Read(T_RAM, buffer, 0, size);
//   printf(" iread=%i\n",iread);
   
   return iread;
}

/*------------------------------------------------------------------*/

int DRSBoard::WriteEEPROM(unsigned short page, void *buffer, int size)
{
  printf("WriteEEPROM(page=%i, ,size=%i)\n",page,size);

   int i;
   unsigned long status;
   unsigned char buf[32768];

   // read previous page
   ReadEEPROM(page, buf, sizeof(buf));
   
   // combine with new page
   memcpy(buf, buffer, size);
   
   // write eeprom page number
   if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9)
      Write(T_CTRL, REG_EEPROM_PAGE_EVAL, &page, 2);
   else if (fBoardType == 6)
      Write(T_CTRL, REG_EEPROM_PAGE_MEZZ, &page, 2);
   else 
      return -1;

   // write eeprom page to RAM
   Write(T_RAM, 0, buf, size);

   // execute eeprom write
   fCtrlBits |= BIT_EEPROM_WRITE_TRIG;
   Write(T_CTRL, REG_CTRL, &fCtrlBits, 4);
   fCtrlBits &= ~BIT_EEPROM_WRITE_TRIG;

   // poll on serail_busy flag
   for (i=0 ; i<500 ; i++) {
      Read(T_STATUS, &status, REG_STATUS, 4);
      if ((status & BIT_SERIAL_BUSY) == 0)
         break;
      Sleep(10);
   }

   return 1;
}

/*------------------------------------------------------------------*/

bool DRSBoard::IsTimingCalibrationValid()
{
   return fabs(fNominalFrequency - fTimingCalibratedFrequency) < 0.001;
}

double DRSBoard::GetTrueFrequency()
{
   int i;
   double f;
   
   if (IsTimingCalibrationValid()) {
      /* calculate true frequency */
      for (i=0,f=0 ; i<1024 ; i++)
         f += fCellDT[0][0][i];
      f = 1024.0 / f;
   } else
      f = fNominalFrequency;
   
   return f;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetTime(unsigned int chipIndex, int channelIndex, int tc, float *time, bool tcalibrated, bool rotated)
{
   int i, scale, iend;
   double gt0, gt;

   /* for DRS2, please use function below */
   if (fDRSType < 4)
      return GetTime(chipIndex, channelIndex, fNominalFrequency, tc, time, tcalibrated, rotated);

   scale = fDecimation ? 2 : 1;

   if (!IsTimingCalibrationValid() || !tcalibrated) {
      double t0 = tc / fNominalFrequency;
      for (i = 0; i < fChannelDepth; i++) {
         if (rotated)
            time[i] = static_cast < float >(((i*scale+tc) % kNumberOfBins) / fNominalFrequency - t0);
         else
            time[i] = static_cast < float >((i*scale) / fNominalFrequency);
         if (time[i] < 0)
            time[i] += static_cast < float > (kNumberOfBins / fNominalFrequency);
         if (i*scale >= kNumberOfBins)
            time[i] += static_cast < float > (kNumberOfBins / fNominalFrequency);
      }
      return 1;
   }

   time[0] = 0;
   for (i=1 ; i<fChannelDepth ; i++) {
      if (rotated)
         time[i] = time[i-1] + (float)fCellDT[chipIndex][channelIndex][(i-1+tc) % kNumberOfBins];
      else
         time[i] = time[i-1] + (float)fCellDT[chipIndex][channelIndex][(i-1) % kNumberOfADCBins];
   }

   if (channelIndex > 0) {
      // correct all channels to channel 0 (Daniel's method)
      iend = tc >= 700 ? 700+1024 : 700;
      for (i=tc,gt0=0 ; i<iend ; i++)
         gt0 += fCellDT[chipIndex][0][i % 1024];
      
      for (i=tc,gt=0 ; i<iend ; i++)
         gt += fCellDT[chipIndex][channelIndex][i % 1024];
      
      for (i=0 ; i<fChannelDepth ; i++)
         time[i] += (float)(gt0 - gt);
   }
      
   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetTimeCalibration(unsigned int chipIndex, int channelIndex, int mode, float *time, bool force)
{
  printf("GetTimeCalibration(%i,%i,%i,)\n",chipIndex,channelIndex,mode);

   int i;
   float tint;

   /* not implemented for DRS2 */
   if (fDRSType < 4)
      return -1;

   if (!force && !IsTimingCalibrationValid()) {
      for (i = 0; i < kNumberOfBins; i++)
         time[i] = (float) (1/fNominalFrequency);
      return 1;
   }

   if (mode == 0) {
      /* differential nonlinearity */
      for (i=0 ; i<kNumberOfBins ; i++)
         time[i] = static_cast < float > (fCellDT[chipIndex][channelIndex][i]);
   } else {
      /* integral nonlinearity */
      for (i=0,tint=0; i<kNumberOfBins ; i++) {
         time[i] = static_cast < float > (tint - i/fNominalFrequency);
         tint += (float)fCellDT[chipIndex][channelIndex][i];
      }
   }

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::GetTime(unsigned int chipIndex, int channelIndex, double freqGHz, int tc, float *time, bool tcalibrated, bool rotated)
{
   /* for DRS4, use function above */
   if (fDRSType == 4)
      return GetTime(chipIndex, channelIndex, tc, time, tcalibrated, rotated);

   int i, irot;
   DRSBoard::TimeData * init;
   DRSBoard::TimeData::FrequencyData * freq;
   int frequencyMHz = (int)(freqGHz*1000);

   init = GetTimeCalibration(chipIndex);

   if (init == NULL) {
      for (i = 0; i < kNumberOfBins; i++)
         time[i] = static_cast < float >(i / fNominalFrequency);
      return 1;
   }
   freq = NULL;
   for (i = 0; i < init->fNumberOfFrequencies; i++) {
      if (init->fFrequency[i]->fFrequency == frequencyMHz) {
         freq = init->fFrequency[i];
         break;
      }
   }
   if (freq == NULL) {
      for (i = 0; i < kNumberOfBins; i++)
         time[i] = static_cast < float >(i / fNominalFrequency);
      return 1;
   }
   for (i = 0; i < kNumberOfBins; i++) {
      irot = (fStopCell[chipIndex] + i) % kNumberOfBins;
      if (fStopCell[chipIndex] + i < kNumberOfBins)
         time[i] = static_cast < float >((freq->fBin[irot] - freq->fBin[fStopCell[chipIndex]]) / fNominalFrequency);
      else
      time[i] =
          static_cast <
          float
          >((freq->fBin[irot] - freq->fBin[fStopCell[chipIndex]] + freq->fBin[kNumberOfBins - 1] - 2 * freq->fBin[0] +
             freq->fBin[1]) / fNominalFrequency);
   }
   return 1;
}

/*------------------------------------------------------------------*/

bool DRSBoard::InitTimeCalibration(unsigned int chipIndex)
{
   return GetTimeCalibration(chipIndex, true) != NULL;
}

/*------------------------------------------------------------------*/

DRSBoard::TimeData * DRSBoard::GetTimeCalibration(unsigned int chipIndex, bool reinit)
{
   int i, l, index;
   char *cstop;
   char fileName[500];
   char error[240];
   PMXML_NODE node, rootNode, mainNode;

   index = fNumberOfTimeData;
   for (i = 0; i < fNumberOfTimeData; i++) {
      if (fTimeData[i]->fChip == static_cast < int >(chipIndex)) {
         if (!reinit)
            return fTimeData[i];
         else {
            index = i;
            break;
         }
      }
   }

   fTimeData[index] = new DRSBoard::TimeData();
   DRSBoard::TimeData * init = fTimeData[index];

   init->fChip = chipIndex;

   for (i = 0; i < init->kMaxNumberOfFrequencies; i++) {
      if (i <= 499 || (i >= 501 && i <= 999) || (i >= 1001 && i <= 1499) || (i >= 1501 && i <= 1999) || 
          (i >= 2001 && i <= 2499) || i >= 2501)
         continue;
      sprintf(fileName, "%s/board%d/TimeCalib_board%d_chip%d_%dMHz.xml", fCalibDirectory, fBoardSerialNumber,
              fBoardSerialNumber, chipIndex, i);
      rootNode = mxml_parse_file(fileName, error, sizeof(error), NULL);
      if (rootNode == NULL)
         continue;

      init->fFrequency[init->fNumberOfFrequencies] = new DRSBoard::TimeData::FrequencyData();
      init->fFrequency[init->fNumberOfFrequencies]->fFrequency = i;

      mainNode = mxml_find_node(rootNode, "/DRSTimeCalibration");

      for (l = 0; l < kNumberOfBins; l++) {
         node = mxml_subnode(mainNode, l + 2);
         init->fFrequency[init->fNumberOfFrequencies]->fBin[l] = strtod(mxml_get_value(node), &cstop);
      }
      mxml_free_tree(rootNode);
      init->fNumberOfFrequencies++;
   }
   if (init->fNumberOfFrequencies == 0) {
      printf("Board %d --> Could not find time calibration file\n", GetBoardSerialNumber());
   }

   if (index == fNumberOfTimeData)
      fNumberOfTimeData++;

   return fTimeData[index];
}

/*------------------------------------------------------------------*/

void DRSBoard::SetCalibrationDirectory(const char *calibrationDirectoryPath)
{
   strncpy(fCalibDirectory, calibrationDirectoryPath, strlen(calibrationDirectoryPath));
   fCalibDirectory[strlen(calibrationDirectoryPath)] = 0;
};

/*------------------------------------------------------------------*/

void DRSBoard::GetCalibrationDirectory(char *calibrationDirectoryPath)
{
   strncpy(calibrationDirectoryPath, fCalibDirectory, strlen(fCalibDirectory));
   calibrationDirectoryPath[strlen(fCalibDirectory)] = 0;
};

/*------------------------------------------------------------------*/

void DRSBoard::LinearRegression(double *x, double *y, int n, double *a, double *b)
{
   int i;
   double sx, sxx, sy, sxy;

   sx = sxx = sy = sxy = 0;
   for (i = 0; i < n; i++) {
      sx += x[i];
      sxx += x[i] * x[i];
      sy += y[i];
      sxy += x[i] * y[i];
   }

   *a = (n * sxy - sx * sy) / (n * sxx - sx * sx);
   *b = (sy - *a * sx) / n;
}

/*------------------------------------------------------------------*/

void DRSBoard::ReadSingleWaveform(int nChip, int nChan, 
                                  unsigned short wf[kNumberOfChipsMax][kNumberOfChannelsMax][kNumberOfBins],
                                  bool rotated)
{
   int i, j, k, tc;

   StartDomino();
   SoftTrigger();
   while (IsBusy());
   TransferWaves();

   for (i=0 ; i<nChip ; i++) {
      tc = GetTriggerCell(i);

      for (j=0 ; j<nChan ; j++) {
         GetRawWave(i, j, wf[i][j], rotated);
         if (!rotated) {
            for (k=0 ; k<kNumberOfBins ; k++) {
               /* do primary offset calibration */
               wf[i][j][k] = wf[i][j][k] - fCellOffset[j+i*9][(k + tc) % kNumberOfBins] + 32768;
            }
         }
	 //printf("%i %i %u\n",i,j,wf[i][j][100]);
      }
      //exit(0);
   }
}
      
static unsigned short swf[kNumberOfChipsMax][kNumberOfChannelsMax][kNumberOfBins];
static float          center[kNumberOfChipsMax][kNumberOfChannelsMax][kNumberOfBins];

int DRSBoard::AverageWaveforms(DRSCallback *pcb, int nChip, int nChan, 
                               int prog1, int prog2, unsigned short *awf, int n, bool rotated)
{
   int i, j, k, l, prog, old_prog = 0;
   float cm;

   if (pcb != NULL)
      pcb->Progress(prog1);

   memset(center, 0, sizeof(center));

   for (i=0 ; i<n; i++) {
      ReadSingleWaveform(nChip, nChan, swf, rotated);

      for (j=0 ; j<nChip ; j++) {
         for (k=0 ; k<nChan ; k++) {
            if (i > 5) {
               /* calculate and subtract common mode */
               for (l=0,cm=0 ; l<kNumberOfBins ; l++)
                  cm += swf[j][k][l] - 32768;
               cm /= kNumberOfBins;
               for (l=0 ; l<kNumberOfBins ; l++)
                  center[j][k][l] += swf[j][k][l]- cm;
            }
         }
      }

      prog = (int)(((double)i/n)*(prog2-prog1)+prog1);
      if (prog > old_prog) {
         old_prog = prog;
         if (pcb != NULL)
            pcb->Progress(prog);
      }
   }

   for (i=0 ; i<nChip ; i++)
      for (j=0 ; j<nChan ; j++)
         for (k=0 ; k<kNumberOfBins ; k++)
            awf[(i*nChan+j)*kNumberOfBins+k] = (unsigned short)(center[i][j][k]/(n-6) + 0.5);
   
   return 1;
}

int DRSBoard::RobustAverageWaveforms(DRSCallback *pcb, int nChip, int nChan, 
                               int prog1, int prog2, unsigned short *awf, int n, bool rotated)
{
   int i, j, k, l, prog, old_prog = 0;

   if (pcb != NULL)
      pcb->Progress(prog1);

   Averager *ave = new Averager(nChip, nChan, kNumberOfBins, 200);
                                
   /* fill histograms */
   for (i=0 ; i<n ; i++) {
      ReadSingleWaveform(nChip, nChan, swf, rotated);
      for (j=0 ; j<nChip ; j++)
         for (k=0 ; k<nChan ; k++)
            for (l=0 ; l<kNumberOfBins ; l++)
               ave->Add(j, k, l, swf[j][k][l]);
               
      /* update progress bar */
      prog = (int)(((double)(i+10)/(n+10))*(prog2-prog1)+prog1);
      if (prog > old_prog) {
         old_prog = prog;
         if (pcb != NULL)
            pcb->Progress(prog);
      }
   }

   /*
   FILE *fh = fopen("calib.csv", "wt");
   for (i=40 ; i<60 ; i++) {
      for (j=0 ; j<WFH_SIZE ; j++)
         fprintf(fh, "%d;", wfh[0][0][i][j]);
      fprintf(fh, "\n");
   }
   fclose(fh);
   */

   for (i=0 ; i<nChip ; i++)
      for (j=0 ; j<nChan ; j++)
         for (k=0 ; k<kNumberOfBins ; k++)
            awf[(i*nChan+j)*kNumberOfBins+k] = (unsigned short)ave->RobustAverage(100, i, j, k);

   ave->SaveNormalizedDistribution("wv.csv", 0, 100);
   
   /*
   FILE *fh = fopen("calib.csv", "wt");
   for (i=40 ; i<60 ; i++) {
      fprintf(fh, "%d;", icenter[0][0][0] + (i - WFH_SIZE/2)*16);
      fprintf(fh, "%d;", wfh[0][0][0][i]);
      if (i == 50)
         fprintf(fh, "%d;", awf[0]);
      fprintf(fh, "\n");
   }
   fclose(fh);
   */

   if (pcb != NULL)
      pcb->Progress(prog2);
   
   delete ave;

   return 1;
}

/*------------------------------------------------------------------*/

int idx[4][10] = {
   {  0,  2,  4,  6,  8, 18, 20, 22, 24, 26 },
   {  1,  3,  5,  7, 39, 19, 21, 23, 25, 39 },
   {  9, 11, 13, 15, 17, 27, 29, 31, 33, 35 },
   { 10, 12, 14, 16, 39, 28, 30, 32, 34, 39 },
};

#define F1(x) ((int) (84.0/24 * (x)))
#define F2(x) ((int) (92.0/8 * (x)))

static unsigned short wft[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024], // 
                      wf1[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024], // 0 V
                      wf2[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024], // fR+0.4
                      wf3[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024]; // 
// my
static unsigned short wf01[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024], 
                      wf02[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024], 
                      wf03[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024],
                      wf049[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024],
                      wf050[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024];
static unsigned short wfm01[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024], 
                      wfm02[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024], 
                      wfm03[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024],
                      wfm04[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024],
                      wfm0403[kNumberOfChipsMax*kNumberOfChannelsMax*kNumberOfChipsMax][1024];

int DRSBoard::CalibrateVolt(DRSCallback *pcb)
{
  printf("\n");
  printf("CalibrateVolt()\n");
  
int    i, j, nChan, timingChan, chip, config, p, /* clkon, */ refclk, trg1, trg2, n_stuck, readchn, casc;
double f, r;
unsigned short buf[1024*16]; // 32 kB
   
   f       = fNominalFrequency;
   r       = fRange;
//   clkon   = (GetCtrlReg() & BIT_TCAL_EN) > 0;
   refclk  = (GetCtrlReg() & BIT_REFCLK_SOURCE) > 0;
   trg1    = fTriggerEnable1;
   trg2    = fTriggerEnable2;
   readchn = fNumberOfReadoutChannels;
   casc    = fChannelCascading;
   
   
   Init();
   fNominalFrequency = f;
   SetRefclk(refclk);
   SetFrequency(fNominalFrequency, true);
   SetDominoMode(1);
   SetDominoActive(1);
   SetReadoutMode(1);
   SetInputRange(r);
   if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9)
      SelectClockSource(0);
   else if (fBoardType == 6)
      SetRefclk(refclk);
   EnableTrigger(0, 0);
   if (readchn == 5)
      SetChannelConfig(4, 8, 8); // even channel readout mode
   
   // WSROUT toggling causes some noise, so calibrate that out
   if (casc == 2) {
      if (fTransport == TR_USB2)
	SetChannelConfig(0, 8, 8); 
	//SetChannelConfig(0, 8, 4); 
      else
         SetChannelConfig(7, 8, 4); 
   }

   StartDomino();

   nChan = 0;
   timingChan = 0;

   if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
      if (fBoardType == 9) {
         nChan = 8;
         timingChan = -1;
      } else {
         nChan = 9;
         timingChan = 8;
      }

      printf(" ------- input for calib is 0 V ------\n");
      
      int NIT = 200; // PSI
      if( fFirmwareVersion == 11030 ) NIT = 1000; // NA61
      
      /* measure offset */
      if (fBoardType == 5)
         EnableAcal(0, 0); // no inputs signal is allowed during calibration!
      else
         EnableAcal(1, 0); // disconnect analog inputs, 0 V
      EnableTcal(0, 0); 
      Sleep(100);
      if( fFirmwareVersion != 11030 ) // PSI
	RobustAverageWaveforms(pcb, 1, nChan, 0, 25, wf1[0], 200, true);
      
      //printf("CalibrateVolt(): fRange = %lf\n",fRange);
      
      printf(" ------- input for calib is %.2f V ------\n",fRange+0.4);
      
      /* measure gain at upper range */
      EnableAcal(1, fRange+0.4); // fRange+0.4 V
      EnableTcal(0, 1);
      Sleep(100);
      if( fFirmwareVersion != 11030 ) // PSI
	RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wf2[0], 200, true);
      
      //for(int ic=0; ic<1024; ic++) printf("%4i  fRange = %.2f   wf1 = %5u  wf2 = %5u\n", ic, fRange, wf1[0][ic], wf2[0][ic] );
      //exit(0);
      


      ///////// my /////////////////////////////////////////////////////
      /*
      EnableAcal(1, fRange+0.1);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wf01[0], NIT, true);
            
      EnableAcal(1, fRange+0.2);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wf02[0], NIT, true);

      EnableAcal(1, fRange+0.3);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wf03[0], NIT, true);

      EnableAcal(1, fRange+0.49);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wf049[0], NIT, true);
      */
      
      EnableAcal(1, fRange+0.50);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wf050[0], NIT, true);
      
      ///////// - ////////////////////////
      /*
      EnableAcal(1, fRange-0.1);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wfm01[0], NIT, true);
      
      EnableAcal(1, fRange-0.2);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wfm02[0], NIT, true);
      */
      
      EnableAcal(1, fRange-0.3);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wfm03[0], NIT, true);
      
      /*
      EnableAcal(1, fRange-0.4);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wfm04[0], NIT, true);
      
      EnableAcal(1, fRange-0.403);
      EnableTcal(0, 1);
      Sleep(100);
      RobustAverageWaveforms(pcb, 1, nChan, 25, 50, wfm0403[0], NIT, true);
      */
      

      //////////////////////////////////////////////////////////////
      FILE *fh = fopen("calib_volt_wf12.txt", "wt");
      printf("Write offsetss to calib_volt_wf12.txt\n");
      for (j=0 ; j<kNumberOfBins; j++) {
	fprintf(fh, "%5d   ", j);
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wf1[i][j] ); // 0 V
	fprintf(fh, "    ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wf2[i][j] ); // 0.4 V
	//fprintf(fh, "    ");
	//for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", fCellOffset3[i][j]-32768 );
	fprintf(fh, "\n");
      }
      fclose(fh);
      //////////////////////////////////////////////////////////////
      fh = fopen("calib_volt_wf_0-49.txt", "wt");
      printf("Write offsetss to calib_volt_wf_0-49.txt\n");
      //fprintf(fh, "%i   %.2f %.2f %.2f %.2f %.2f %.2f\n", 6, 0.,0.1,0.2,0.3,0.4,0.49);
      fprintf(fh, "%i   %.3f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n", 12, 
	      -0.403,-0.4,-0.3,-0.2,-0.1, 0., 0.1,0.2,0.3,0.4,0.49,0.50);
      for (j=0 ; j<kNumberOfBins; j++) {
	fprintf(fh, "%5d   ", j);
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wfm0403[i][j] ); // -0.403
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wfm04[i][j] ); // -0.4
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wfm03[i][j] ); // -0.3
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wfm02[i][j] ); // -0.2
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wfm01[i][j] ); // -0.1
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wf1 [i][j] ); // 0
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wf01[i][j] ); // 0.1
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wf02[i][j] ); // 0.2
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wf03[i][j] ); // 0.3
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wf2 [i][j] ); // 0.4
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wf049[i][j]); // 0.49
	fprintf(fh, "   ");
	for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", wf050[i][j]); // 0.50
	fprintf(fh, "\n");
      }
      fclose(fh);
      //////////////////////////////////////////////////////////////
      


      
   } else if (fBoardType == 6) {
      if (fTransport == TR_USB2) {
         nChan = 36;
         timingChan = 8;
         memset(wf1, 0, sizeof(wf1));
         memset(wf2, 0, sizeof(wf2));
         memset(wf3, 0, sizeof(wf3));
         for (config=p=0 ; config<4 ; config++) {
            SetChannelConfig(config, 8, 8);

            /* measure offset */
            EnableAcal(1, 0);
            EnableTcal(0, 0); // 0 V
            Sleep(100);
            RobustAverageWaveforms(pcb, 0, 10, F1(p), F1(p+1), wft[0], 500, true); p++;
            for (i=0 ; i<5 ; i++)
               memcpy(wf1[idx[config][i]], wft[i*2], sizeof(float)*kNumberOfBins);
            RobustAverageWaveforms(pcb, 2, 10, F1(p), F1(p+1), wft[0], 500, true); p++;
            for (i=0 ; i<5 ; i++)
               memcpy(wf1[idx[config][i+5]], wft[i*2], sizeof(float)*kNumberOfBins);

            /* measure gain at +400 mV */
            EnableAcal(1, 0.4); // 0.4 V
            EnableTcal(0, 0);
            Sleep(100);
            RobustAverageWaveforms(pcb, 0, 8, F1(p), F1(p+1), wft[0], 500, true); p++;
            for (i=0 ; i<4 ; i++)
               memcpy(wf2[idx[config][i]], wft[i*2], sizeof(float)*kNumberOfBins);
            RobustAverageWaveforms(pcb, 2, 8, F1(p), F1(p+1), wft[0], 500, true); p++;
            for (i=0 ; i<4 ; i++)
               memcpy(wf2[idx[config][i+5]], wft[i*2], sizeof(float)*kNumberOfBins);

            /* measure gain at -400 mV */
            EnableAcal(1, -0.4);
            EnableTcal(0, 1);
            Sleep(100);
            RobustAverageWaveforms(pcb, 0, 8, F1(p), F1(p+1), wft[0], 500, true); p++;
            for (i=0 ; i<4 ; i++)
               memcpy(wf3[idx[config][i]], wft[i], sizeof(float)*kNumberOfBins);
            RobustAverageWaveforms(pcb, 2, 8, F1(p), F1(p+1), wft[0], 500, true); p++;
            for (i=0 ; i<4 ; i++)
               memcpy(wf3[idx[config][i+5]], wft[i], sizeof(float)*kNumberOfBins);
         }
      } else {
         nChan = 36;
         timingChan = 8;
        
         /* measure offset */
         EnableAcal(0, 0); // no inputs signal is allowed during calibration!
         EnableTcal(0, 0);
         Sleep(100);
         RobustAverageWaveforms(pcb, 4, 9, 0, 25, wf1[0], 500, true);

         /* measure gain at upper range */
         EnableAcal(1, fRange+0.4);
         EnableTcal(0, 0);
         Sleep(100);
         RobustAverageWaveforms(pcb, 4, 9, 25, 50, wf2[0], 500, true);
      }
   }
   
   
   
   /* convert offsets and gains to 16-bit values */
   memset(fCellOffset, 0, sizeof(fCellOffset));
   n_stuck = 0;
   for (i=0 ; i<nChan ; i++) {
      for (j=0 ; j<kNumberOfBins; j++) {
         if (i % 9 == timingChan) {
	    /* calculate offset and gain for timing channel */
            if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
               /* we have a +325mV and a -325mV value */
               fCellOffset[i][j] = (unsigned short) ((wf1[i][j]+wf2[i][j])/2+0.5);
               fCellGain[i][j]   = (wf2[i][j] - wf1[i][j])/65536.0*1000 / 650.0;
            } else {
               /* only have offset */
               fCellOffset[i][j] = wf1[i][j];
               fCellGain[i][j]   = 1;
            }
         } else {
	  
	    /* calculate offset and gain for data channel */

	   if( fFirmwareVersion == 11030 ) { // NA61
	     
	     /*
	     // PSI
	     fCellOffset[i][j] = wf1[i][j]+3500;
	     //
	     if (fCellOffset[i][j] < 100) {
               // mark stuck pixel
               n_stuck ++;
               fCellOffset[i][j] = 0;
               fCellGain[i][j] = 1;
	     } else {
               fCellGain[i][j] = (wf2[i][j] - fCellOffset[i][j])/65536.0 / (0.4+fRange);
	     }
	     */
	     
	     fCellGain[i][j] = ( wf050[i][j] - wfm03[i][j] ) / 65536. / (0.5+0.3);
	     fCellOffset[i][j] = wfm03[i][j] - (fRange-0.3) * fCellGain[i][j] * 65536.;
	     
	     //
	     //printf("  %d %d\n",wf2[i][j],fCellOffset[i][j]);
	     //printf("%i %i PSI  %4u %9f\n",i,j,fCellOffset[i][j],fCellGain[i][j]);
	     
	     /*
	     // NA61
	     double gain = (wf2[i][j] - wfm04[i][j]) /65536.0 / (0.4 + 0.4);
	     fCellOffset[i][j] = (wf2[i][j] - gain*(0.4+fRange)* 65536 ) ;
	     fCellGain[i][j] = gain;
	     //
	     //printf("%i %i NA61 %4u %9f\n",i,j,fCellOffset[i][j],fCellGain[i][j]);
	     */
	     
	     //fCellOffset[i][j] = 0;
	     //fCellGain[i][j] = 1;
	     
	   } else { // PSI
	     
	     fCellOffset[i][j] = wf1[i][j];
	     if (fCellOffset[i][j] < 100) {
               // mark stuck pixel
               n_stuck ++;
               fCellOffset[i][j] = 0;
               fCellGain[i][j] = 1;
	     } else {
               fCellGain[i][j] = (wf2[i][j] - fCellOffset[i][j])/65536.0*1000 / ((0.4+fRange)*1000);
	     }

	   }

         }
	 
	 //printf("%i %4i  offset = %5u   gain = %10lf\n", i,j, fCellOffset[i][j], fCellGain[i][j] );
	 
	 
         // check gain !
         if (fCellGain[i][j] < 0.5 || fCellGain[i][j] > 1.1) {
	   /*
            if ((fBoardType == 7 || fBoardType == 8 || fBoardType == 9) && i % 2 == 1) {
	      // channels are not connected, so don't print error 
	      //printf("channels are not connected, so don't print error\n");
            } else {
               printf("Gain of %6.3lf for channel %2d, cell %4d out of range 0.5 ... 1.1\n",
                  fCellGain[i][j], i, j);
            }
	   */
	   
	   if( fFirmwareVersion == 11030 ) { // NA61
	     fCellGain[i][j] = 0.83; 
	   } else {   // PSI
	     fCellGain[i][j] = 1;
	   }
	   
         }
	 
	 

      }
   }
   
   /*
   for (i=0 ; i<1 ; i++) {
     for (j=0 ; j<kNumberOfBins; j++) {
       printf("%i %4i  offset = %5u   gain = %10lf\n", i,j, fCellOffset[i][j], fCellGain[i][j] );
     }
   }
   */
   
   /*
   FILE *fh = fopen("calib.txt", "wt");
   for (i=0 ; i<nChan ; i++) {
      fprintf(fh, "CH%02d:", i);
      for (j=0 ; j<20 ; j++)
         fprintf(fh, " %5d", fCellOffset[i][j]-32768);
      fprintf(fh, "\n");
   }
   fclose(fh);
   */
   
   /* perform secondary calibration */
   if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
      nChan = 9;
      timingChan = 8;
     
      printf(" ------- input for calib is 0. V  (for offset) ------\n");
      
      /* measure offset */
      if (fBoardType == 5)
         EnableAcal(0, 0); // no inputs signal is allowed during calibration!
      else
         EnableAcal(1, 0); // disconnect analog inputs
      EnableTcal(0, 0);
      Sleep(100);
      AverageWaveforms(pcb, 1, 9, 50, 90, wf1[0], 500, false);
   } 
   
   /* convert offset to 16-bit values */
   memset(fCellOffset2, 0, sizeof(fCellOffset2));
   for (i=0 ; i<nChan ; i++)
      for (j=0 ; j<kNumberOfBins; j++)
	 if (i % 9 != timingChan) {
	   fCellOffset2[i][j] = wf1[i][j]; // PSI
	   
	   //printf("%i %i PSI2 %4u\n",i,j,fCellOffset2[i][j]);
	   //fCellOffset2[i][j] = 65536/2; // my
	 }
   

   //////////////////// my
   /*
   // perform tertiary calibration 
   if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8 || fBoardType == 9) {
      nChan = 9;
      timingChan = 8;
      
      // measure offset
      if (fBoardType == 5)
         EnableAcal(0, 0); // no inputs signal is allowed during calibration!
      else
         EnableAcal(1, 0); // disconnect analog inputs
      EnableTcal(0, 0);
      Sleep(100);
      AverageWaveforms(pcb, 1, 9, 50, 90, wf1[0], 500, false);
   }
   
   unsigned short fCellOffset3[kNumberOfChipsMax * kNumberOfChannelsMax][kNumberOfBins];
   // convert offset to 16-bit values
   memset(fCellOffset3, 0, sizeof(fCellOffset3));
   for (i=0 ; i<nChan ; i++)
      for (j=0 ; j<kNumberOfBins; j++)
         if (i % 9 != timingChan)
            fCellOffset3[i][j] = wf1[i][j];
   */

   
   ////////////////// write offsets /////////////////////////////
   FILE *fh = fopen("calib_volt_offset.txt", "wt");
   printf("Write offsetss to calib_volt_offset.txt\n");
   for (j=0 ; j<kNumberOfBins; j++) {
     fprintf(fh, "%5d   ", j);
     for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", fCellOffset[i][j]-32768 );
     fprintf(fh, "    ");
     for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", fCellOffset2[i][j]-32768 );
     //fprintf(fh, "    ");
     //for (i=0 ; i<8; i+=2) fprintf(fh, " %7d", fCellOffset3[i][j]-32768 );
     fprintf(fh, "\n");
   }
   fclose(fh);
   ////////////////// write gain ////////////////////////////////
   fh = fopen("calib_volt_gain.txt", "wt");
   printf("Write offsetss to calib_volt_gain.txt\n");
   for (j=0 ; j<kNumberOfBins; j++) {
     fprintf(fh, "%5d   ", j);
     for (i=0 ; i<8; i+=2) fprintf(fh, " %lf", fCellGain[i][j] );
     fprintf(fh, "    ");
     for (i=0 ; i<8; i+=2) fprintf(fh, " %u", (unsigned short)((fCellGain[i][j] - 0.7) / 0.4 * 65535 ) );
     fprintf(fh, "\n");
   }
   fclose(fh);
   //////////////////////////////////////////////////////////////
   
   


   /*
   for (i=0 ; i<nChan ; i++) {
     for (j=0 ; j<kNumberOfBins; j++) {
       printf("%i %5d:   %5d %5d   %5d %5d\n",i, j, 
	      fCellOffset [0][j]-32768, fCellOffset [2][j]-32768, 
	      fCellOffset2[0][j]-32768, fCellOffset2[2][j]-32768);
       //printf("%i %4i  offset = %5u   gain = %10lf\n", i,j, fCellOffset[i][j]-32768, fCellGain[i][j] );
     }
   }
   */
   
   
   if (fBoardType == 9) {

     printf("\n ------- write calibration to EEPROM ------\n");
     
      /* write calibration CH0-CH7 to EEPROM page 1 */
      for (i=0 ; i<8 ; i++)
         for (j=0 ; j<1024; j++) {
            buf[(i*1024+j)*2]   = fCellOffset[i][j];
	    //printf("%i %4i  %lf\n",i,j,fCellGain[i][j]);
            buf[(i*1024+j)*2+1] = (unsigned short) ((fCellGain[i][j] - 0.7) / 0.4 * 65535);
         }
      WriteEEPROM(1, buf, 1024*32);
      if (pcb != NULL)
         pcb->Progress(93);

      
      /* write secondary calibration to EEPROM page 2 */
      ReadEEPROM(2, buf, 1024*32);
      for (i=0 ; i<8 ; i++)
         for (j=0 ; j<1024; j++)
            buf[(i*1024+j)*2] = fCellOffset2[i][j];
      WriteEEPROM(2, buf, 1024*32);
      if (pcb != NULL)
         pcb->Progress(96);

      /* update page # 0 */
      ReadEEPROM(0, buf, 4096); // 0-0x0FFF
      /* write calibration method */
      buf[2]  = (buf[2] & 0xFF00) | VCALIB_METHOD;
      /* write temperature and range */
      buf[10] = ((unsigned short) (GetTemperature() * 2 + 0.5) << 8) | ((signed char)(fRange * 100));
      buf[11] = 1; // EEPROM page #1+2
      WriteEEPROM(0, buf, 4096);
      fCellCalibratedRange = fRange;
      if (pcb != NULL)
         pcb->Progress(100);

      
   } else if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8) {
      /* write calibration CH0-CH7 to EEPROM page 1 */
      for (i=0 ; i<8 ; i++)
         for (j=0 ; j<1024; j++) {
            buf[(i*1024+j)*2]   = fCellOffset[i][j];
            buf[(i*1024+j)*2+1] = (unsigned short) ((fCellGain[i][j] - 0.7) / 0.4 * 65535);
         }
      WriteEEPROM(1, buf, 1024*32);

      /* write calibration CH8 and secondary calibration to EEPROM page 2 */
      for (j=0 ; j<1024; j++) {
         buf[j*2]   = fCellOffset[8][j];
         buf[j*2+1] = (unsigned short) ((fCellGain[8][j] - 0.7) / 0.4 * 65535);
      }
      for (i=0 ; i<4 ; i++)
         for (j=0 ; j<1024; j++) {
            buf[2*1024+(i*1024+j)*2]   = fCellOffset2[i*2][j];
            buf[2*1024+(i*1024+j)*2+1] = fCellOffset2[i*2+1][j];
         }
      WriteEEPROM(2, buf, 1024*5*4);

      /* write calibration method and range */
      ReadEEPROM(0, buf, 2048); // 0-0x0FFF
      buf[2] = VCALIB_METHOD_V4 | ((signed char)(fRange * 100)) << 8;
      WriteEEPROM(0, buf, 2048);
      fCellCalibratedRange = fRange;

   } else if (fBoardType == 6) {
      for (chip=0 ; chip<4 ; chip++) {
         /* write calibration of A0 to A7 to EEPROM page 1 
                                 B0 to B7 to EEPROM page 2 and so on */
         for (i=0 ; i<8 ; i++)
            for (j=0 ; j<1024; j++) {
               buf[(i*1024+j)*2]   = fCellOffset[i+chip*9][j];
               buf[(i*1024+j)*2+1] = (unsigned short) ((fCellGain[i+chip*9][j] - 0.7) / 0.4 * 65535);
            }
         WriteEEPROM(1+chip, buf, 1024*32);
         if (pcb != NULL)
            pcb->Progress(75+chip*4);
       }

      /* write calibration A/B/C/D/CLK to EEPROM page 5 */
      ReadEEPROM(5, buf, 1024*4*4);
      for (chip=0 ; chip<4 ; chip++) {
         for (j=0 ; j<1024; j++) {
            buf[j*2+chip*0x0800]   = fCellOffset[8+chip*9][j];
            buf[j*2+1+chip*0x0800] = (unsigned short) ((fCellGain[8+chip*9][j] - 0.7) / 0.4 * 65535);
         }
      }
      WriteEEPROM(5, buf, 1024*4*4);
      if (pcb != NULL)
         pcb->Progress(90);

      /* write secondary calibration to EEPROM page 7 and 8 */
      for (i=0 ; i<8 ; i++) {
         for (j=0 ; j<1024; j++) {
            buf[i*0x800 + j*2]   = fCellOffset2[i][j];
            buf[i*0x800 + j*2+1] = fCellOffset2[i+9][j];
         }
      }
      WriteEEPROM(7, buf, 1024*32);
      if (pcb != NULL)
         pcb->Progress(94);

      for (i=0 ; i<8 ; i++) {
         for (j=0 ; j<1024; j++) {
            buf[i*0x800 + j*2]   = fCellOffset2[i+18][j];
            buf[i*0x800 + j*2+1] = fCellOffset2[i+27][j];
         }
      }
      WriteEEPROM(8, buf, 1024*32);
      if (pcb != NULL)
         pcb->Progress(98);

      /* write calibration method and range */
      ReadEEPROM(0, buf, 2048); // 0-0x0FFF
      buf[2] = VCALIB_METHOD | ((signed char)(fRange * 100)) << 8;
      WriteEEPROM(0, buf, 2048);
      fCellCalibratedRange = fRange;
      if (pcb != NULL)
         pcb->Progress(100);
   }

   if (n_stuck)
      printf("\nFound %d stuck pixels on this board\n\n", n_stuck);

   fVoltageCalibrationValid = true;

   /* remove calibration voltage */
   EnableAcal(0, 0);
   EnableTcal(0, 0);
   EnableTrigger(trg1, trg2);
   
   //SetCalibVoltage(0.2);
   printf("------------- end of CalibrateVolt()\n\n");

   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::AnalyzeSlope(Averager *ave, int iIter, int nIter, int channel, float wf[kNumberOfBins], int tCell,
                           double cellDV[kNumberOfBins], double cellDT[kNumberOfBins])
{
   int i;
   float dv, llim, ulim;
   double sum, dtCell /*, dtWindow */;
   
   //printf("AnalyzeSlop: %i %i %i\n",iIter,nIter,channel);
   
   if (fNominalFrequency > 3) {
      llim = -100;
      ulim =  100;
   } else {
      llim = -300;
      ulim =  300;
   }
   
   //printf("%f %f %f\n",fNominalFrequency,llim,ulim);

   /////////// my //////////////
   int iSkip = 5;
   if( fFirmwareVersion == 11030 ) { // NA61
     iSkip = 30;
   } else { // PSI
     iSkip = 5;
   }

   

   // rising edges ----

   // skip first cells after trigger cell
   for (i=tCell+iSkip ; i<tCell+kNumberOfBins-iSkip ; i++) {
      // test slope between previous and next cell to allow for negative cell width
      if (wf[(i+kNumberOfBins-1) % kNumberOfBins] < wf[(i+2) % kNumberOfBins] &&
          wf[i % kNumberOfBins] > llim &&
          wf[(i+1) % kNumberOfBins] < ulim) {
         
         // calculate delta_v
         dv = wf[(i+1) % kNumberOfBins] - wf[i % kNumberOfBins];
    
         // average delta_v
         ave->Add(0, channel, i % kNumberOfBins, dv);
      }
   }
   
   // falling edges ----
   
   // skip first cells after trigger cell
   for (i=tCell+iSkip ; i<tCell+kNumberOfBins-iSkip ; i++) {
      // test slope between previous and next cell to allow for negative cell width
      if (wf[(i+kNumberOfBins-1) % kNumberOfBins] > wf[(i+2) % kNumberOfBins] &&
          wf[i % kNumberOfBins] < ulim &&
          wf[(i+1) % kNumberOfBins] > llim) {
         
         // calcualte delta_v
         dv = wf[(i+1) % kNumberOfBins] - wf[i % kNumberOfBins];
         
         ave->Add(0, channel, i % kNumberOfBins, -dv);
      }
   }
   
   // calculate calibration every 100 events
   if ((iIter + 1) % 100 == 0) {
      // average over all 1024 dU
      sum = 0;
      for (i=0 ; i<kNumberOfBins ; i++) {
         
         if (fBoardType == 8)
            cellDV[i] = ave->Median(0, channel, i);
         else {
            // use empirically found limits for averaging
            if (fNominalFrequency >= 4)
               cellDV[i] = ave->RobustAverage(3, 0, channel, i);
            else if (fNominalFrequency >= 3)
               cellDV[i] = ave->RobustAverage(6, 0, channel, i);
            else
               cellDV[i] = ave->Median(0, channel, i);
         }
         
         sum += cellDV[i];
      }
      
      sum /= kNumberOfBins;
      dtCell = (float)1/fNominalFrequency;
      //dtWindow = dtCell * kNumberOfBins;
      
      // here comes the central calculation, dT = dV/average * dt_cell
      for (i=0 ; i<kNumberOfBins ; i++)
         cellDT[i] = cellDV[i] / sum * dtCell;
   }
   
   return 1;
}

/*------------------------------------------------------------------*/

int DRSBoard::AnalyzePeriod(Averager *ave, int iIter, int nIter, int channel, float wf[kNumberOfBins], int tCell,
                            double cellDV[kNumberOfBins], double cellDT[kNumberOfBins])
{
int    i, i1, i2, j, nzx, zeroXing[1000], edge, n_correct, nest;
double damping, zeroLevel, tPeriod, corr, dv, dv_limit, corr_limit;
 
//printf("AnalyzePeriod: %i %i %i\n",iIter,nIter,channel);

   /* calculate zero level */
   for (i=0,zeroLevel=0 ; i<1024 ; i++)
      zeroLevel += wf[i];
   zeroLevel /= 1024;

   /* correct for zero common mode */
   for (i=0 ; i<1024 ; i++)
      wf[i] -= (float)zeroLevel;

   /* estimate damping factor */
   if (fBoardType == 9)
      damping = 0.01;
   else
      damping = fNominalFrequency / nIter * 2   ;

   //fTCALFrequency = 1000/9.8;
   printf("AnalyzePeriod(): fNominalFrequency=%f, fTCALFrequency=%f\n",fNominalFrequency,fTCALFrequency);
   //exit(0);
   
   /* estimate number of zero crossings */
   nest = (int) (1/fNominalFrequency*1024 / (1/fTCALFrequency*1000));
   
   if (fNominalFrequency >= 4)
      dv_limit = 4;
   else if (fNominalFrequency >= 3)
      dv_limit = 6;
   else
      dv_limit = 10000;

   for (edge = 0 ; edge < 2 ; edge ++) {

      /* find edge zero crossing with wrap-around */
      for (i=tCell+5,nzx=0 ; i<tCell+1023-5 && nzx < (int)(sizeof(zeroXing)/sizeof(int)) ; i++) {
         dv = fabs(wf[(i+1) % 1024] - wf[i % 1024]);
         
         if (edge == 0) {
            if (wf[(i+1) % 1024] < 0 && wf[i % 1024] > 0) { // falling edge
               if (fBoardType != 9 || fabs(dv-cellDV[i % 1024]) < dv_limit) // only if DV is not more the dv_limit away from average
                  zeroXing[nzx++] = i % 1024;
            }
         } else {
            if (wf[(i+1) % 1024] > 0 && wf[i % 1024] < 0) { // rising edge
               if (fBoardType != 9 || fabs(dv-cellDV[i % 1024]) < dv_limit)
                  zeroXing[nzx++] = i % 1024;
            }
         }
      }

      /* abort if uncorrect number of edges is found */
      if (abs(nest - nzx) > nest / 3)
         return 0;

      for (i=n_correct=0 ; i<nzx-1 ; i++) {
         i1 = zeroXing[i];
         i2 = zeroXing[i+1];
         if (i1 == 1023 || i2 == 1023)
            continue;
         
         if (wf[(i1 + 1) % 1024] == 0 || wf[i2 % 1024] == 0)
            continue;
         
         /* first partial cell */
         tPeriod = cellDT[i1]*(1/(1-wf[i1]/wf[(i1 + 1) % 1024]));

         /* full cells between i1 and i2 */
         if (i2 < i1)
            i2 += 1024;
         
         for (j=i1+1 ; j<i2 ; j++)
            tPeriod += cellDT[j % 1024];
         
         /* second partial cell */
         tPeriod += cellDT[i2 % 1024]*(1/(1-wf[(i2+1) % 1024]/wf[i2 % 1024]));

         /* calculate correction to nominal period as a fraction */
         corr = (1/fTCALFrequency*1000) / tPeriod;

         /* skip very large corrections (noise) */
         if (fBoardType == 9 && fNominalFrequency >= 2)
            corr_limit = 0.001;
         else if (fBoardType == 9)
            corr_limit = 0.004;
         else
            corr_limit = 0.01;

         if (fBoardType == 9 && fabs(1-corr) > corr_limit)
            continue;

         /* remeber number of valid corrections */
         n_correct++;

         /* apply damping factor */
         corr = (corr-1)*damping + 1;

         /* apply from i1 to i2-1 inclusive */
         if (i1 == i2)
            continue;

         /* distribute correciton equally into bins inside the region ... */
         for (j=i1 ; j<=i2 ; j++)
            cellDT[j % 1024] *= corr;
            
         /* test correction */
         tPeriod = cellDT[i1]*(1/(1-wf[i1]/wf[(i1 + 1) % 1024]));
         for (j=i1+1 ; j<i2 ; j++)
            tPeriod += cellDT[j % 1024];
         tPeriod += cellDT[i2]*(1/(1-wf[(i2+1) % 1024]/wf[i2]));
      }

      if (n_correct < nzx/3)
         return 0;
   }

   return 1;
}

/*------------------------------------------------------------------*/


int DRSBoard::CalibrateTiming(DRSCallback *pcb)
{
  printf("\nCalibrateTiming()\n");

   int    index, status, error, tCell, i, j, c, chip, mode, nIterPeriod, nIterSlope, clkon, phase, refclk, trg1, trg2, n_error, channel;
   double f, range, tTrue, tRounded, dT, t1[8], t2[8], cellDV[kNumberOfChipsMax*kNumberOfChannelsMax][kNumberOfBins];
   unsigned short buf[1024*16]; // 32 kB
   float  wf[1024];
   Averager *ave = NULL;
   
   nIterPeriod = 5000;
   nIterSlope  = 5000;
   n_error = 0;
   refclk  = 0;
   f       = fNominalFrequency;
   range   = fRange;
   clkon   = (GetCtrlReg() & BIT_TCAL_EN) > 0;
   if (fBoardType == 6)
      refclk  = (GetCtrlReg() & BIT_REFCLK_SOURCE) > 0;
   trg1    = fTriggerEnable1;
   trg2    = fTriggerEnable2;

   
   /////////////// my ///////////////
   unsigned short mCellOffset[kNumberOfChipsMax * kNumberOfChannelsMax][kNumberOfBins];
   unsigned short mCellOffset2[kNumberOfChipsMax * kNumberOfChannelsMax][kNumberOfBins];
   double            mCellGain[kNumberOfChipsMax * kNumberOfChannelsMax][kNumberOfBins];
   bool bVC = false;
   if( fFirmwareVersion == 11030 && fVoltageCalibrationValid ) {
     memcpy(mCellOffset ,fCellOffset ,sizeof(fCellOffset ));
     memcpy(mCellOffset2,fCellOffset2,sizeof(fCellOffset2));
     memcpy(mCellGain   ,fCellGain   ,sizeof(fCellGain   ));
     //printf("++ %u\n",fCellOffset[0][10]);
     bVC = true;
   }
   
   Init();
   //usleep(100000);
   
   if( fFirmwareVersion == 11030 && bVC ) { // my
     memcpy(fCellOffset ,mCellOffset ,sizeof(fCellOffset ));
     memcpy(fCellOffset2,mCellOffset2,sizeof(fCellOffset2));
     memcpy(fCellGain   ,mCellGain   ,sizeof(fCellGain   ));
     //printf("++ %u\n",fCellOffset[0][10]);
     fVoltageCalibrationValid = true;
     //exit(0);
   }
   
   
   fNominalFrequency = f;
   fTimingCalibratedFrequency = 0;
   if (fBoardType == 6) // don't set refclk for evaluation boards
      SetRefclk(refclk);
   SetFrequency(fNominalFrequency, true);
   if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8)
     fTCALFrequency = 132; // 132 MHz for EVAL1, for MEZZ this is set by ConfigureLMK
   else if (fBoardType == 9) {
     //if(fFirmwareVersion == 11030 ) fTCALFrequency = 1000/9.827; // NA61
     //else                           fTCALFrequency = 100; //  PSI
     fTCALFrequency = 100;
   }
   SetDominoMode(1);
   SetDominoActive(1);
   SetReadoutMode(1);
   EnableTrigger(0, 0);
   if (fBoardType == 5 || fBoardType == 7) {
      EnableTcal(1, 0, 0);
      SelectClockSource(1); // 2nd quartz
   } else if (fBoardType == 8) {
      nIterSlope  = 0;
      nIterPeriod = 1500;
      EnableTcal(1, 0, 0);
      SelectClockSource(1); // 2nd quartz
      ave = new Averager(1, 1, 1024, 500); // one chip, 1 channel @ 1024 bins
   } else if (fBoardType == 9) {
      EnableTcal(1);
      nIterSlope  = 500; // PSI
      nIterPeriod = 500;

      nIterSlope  = 1000; // NA61
      nIterPeriod = 1000;
      
      //nIterPeriod = 0;
      //ave = new Averager(1, 9, 1024, 500); // one chip, 9 channels @ 1024 bins
      ave = new Averager(1, 9, 1024, 1000);
   }
   StartDomino();
   
   /* initialize time array */
   for (i=0 ; i<1024 ; i++)
      for (chip=0 ; chip<4 ; chip++)
         for (channel = 0 ; channel < 8 ; channel++) {
            fCellDT[chip][channel][i] = (float)1/fNominalFrequency;  // [ns]
         }

   error = 0;
   
   
   printf("CalibrateTiming(): nIterSlope=%i, nIterPeriod=%i\n", nIterSlope, nIterPeriod );
   
   /// my
   FILE *file = fopen("calibT_wf.bin", "w");
   //open("calibT_wf.bin", O_RDONLY, 0666);
   printf("Writing to file: calibT_wf.bin\n");
   //////

   time_t now;
   time(&now);
   struct tm *lt = localtime(&now);
   short int Year         = lt->tm_year+1900;
   short int Month        = lt->tm_mon+1;
   short int Day          = lt->tm_mday;
   short int Hour         = lt->tm_hour;
   short int Minute       = lt->tm_min;
   
   double tfr = this->GetTrueFrequency();
   double temp = this->GetTemperature();
   
   fwrite(&Year,  sizeof(Year),  1, file); // my
   fwrite(&Month, sizeof(Month), 1, file); // my
   fwrite(&Day,   sizeof(Day),   1, file); // my
   fwrite(&Hour,  sizeof(Hour),  1, file); // my
   fwrite(&Minute,sizeof(Minute),1, file); // my
   fwrite(&tfr,   sizeof(tfr),   1, file); // my
   fwrite(&temp,  sizeof(temp),  1, file); // my
   
   //printf("Nominal frequency:    %lf GHz\n", this->GetNominalFrequency());
   printf(" Time: %i %i %i %i %i\n",Year,Month,Day,Hour,Minute);
   printf(" True frequency:       %lf GHz\n", tfr);
   printf(" Temperature:          %lf C\n", temp);
   //printf("External Clock freq.: %lf MHz\n", this->GetExternalClockFrequency());
   //printf("Calibrated frequency: %lf GHz\n", this->GetTcalFreq());

   printf("CalibrateTiming(): fVoltageCalibrationValid=%i, fTimingCalibratedFrequency=%.3f\n",(int)fVoltageCalibrationValid,fTimingCalibratedFrequency);
   //usleep(10000);

   
   
   ///////////////////// loop ////////////////////////

   for (index = 0 ; index < nIterSlope+nIterPeriod ; index++) {
      if (index % 10 == 0)
         if (pcb)
            pcb->Progress(100*index/(nIterSlope+nIterPeriod));
      
      {
         if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8) { // DRS4 Evaluation board: 1 Chip
            SoftTrigger();
            while (IsBusy());
	    
            StartDomino();
            TransferWaves();

            tCell = GetStopCell(0);
            GetWave(0, 8, wf, true, tCell, 0, true);
	    
	    
            if (index < nIterSlope)
               status = AnalyzeSlope(ave, index, nIterSlope, 0, wf, tCell, cellDV[0], fCellDT[0][0]);
            else
	      status = AnalyzePeriod(ave, index, nIterPeriod, 0, wf, tCell, cellDV[0], fCellDT[0][0]);
	    
            if (!status)
               n_error++;

            if (n_error > nIterPeriod / 10) {
               error = 1;
               break;
            }
         } else if (fBoardType == 9) { // DRS4 Evaluation board V5: all channels from one chip
	   //printf("\n--- index = %i ------\n",index); 
	   
	   //usleep(10000); // my

	    SoftTrigger();
            while (IsBusy());
            
            StartDomino();
	    //usleep(1000);
            TransferWaves();
            
	    
	    
            // calibrate all channels individually
            for (channel = 0 ; channel < 8 ; channel+=2) {
               tCell = GetStopCell(0);
               GetWave(0, channel, wf, true, tCell, 0, true);
	       
	       
	       ////////// my 
	       fwrite(&tCell, sizeof(tCell), 1, file); // my
	       fwrite(&channel, sizeof(channel), 1, file); // my
	       fwrite(wf, sizeof(wf), 1, file); // my
	       //////////////
	       
	       
	       if( fFirmwareVersion == 11030 ) { // NA61

		 //printf("========================\n");
		 int iFIR = 100;
		 int iLAS = 50;

		 double meanWF = 0, ampWF = 0;
		 double maxWF=-99999, minWF=99999;
	       
		 for( int i=iFIR; i<1024-iLAS; i++ ) {
		   if( wf[i] > maxWF ) maxWF = wf[i];
		   if( wf[i] < minWF ) minWF = wf[i];
		 }
		 meanWF = (maxWF+minWF) / 2;
		 ampWF  = (maxWF-minWF) / 2;
		 //printf("%2i, ich=%i --- meanWF = %.1f, ampWF = %.1f,  minWF = %.1f, maxWF = %.1f\n", index,channel,meanWF, ampWF, minWF,maxWF );
	       
		 /*
		   double meanWF = 0, rmsWF = 0;	       
		   meanWF = 0;
		   for( int i=iFIR; i<1024-iLAS; i++) {
		   //double v = cellDV[0][i];
		   //double v = cellDV[0][i] / 65536.;
		   //printf("%4i %.1f %.3f\n",i,wf[i],fCellDT[0][0][i]);
		   meanWF += wf[i];
		   }
		   meanWF /= 1024-iFIR-iLAS;
	       
		   rmsWF = 0;
		   for( int i=iFIR; i<1024-iLAS; i++) {
		   rmsWF += pow(wf[i]-meanWF,2);
		   }
		   rmsWF /= 1024-iFIR-iLAS;
		   rmsWF = sqrt(rmsWF);
		   printf("%.1f %.1f\n", meanWF, rmsWF*sqrt(2) );
		 */
	       
		 //printf("------------------------\n");
		 //exit(0);
	       
		 //for( int j=0; j<1024; j++) {
		 //printf("%i %u %f\n",j,fCellOffset[0][j],fCellGain[0][j]);
		 //}
		 
		 
		 meanWF = 465.6; 
		 if(channel<4) ampWF = 346;
		 else          ampWF = 248;
		 
		 for( int i=0; i<1024; i++) {
		   wf[i] -= meanWF;
		   wf[i] *= (490./ampWF);
		   //if(channel==0)
		   //printf("%4i %7.1f\n", i, wf[i] );
		 }
		 //exit(0);
	       }
	       
	       //////////////////////////////////////

               if (index < nIterSlope)
                  status = AnalyzeSlope(ave, index, nIterSlope, channel, wf, tCell, cellDV[channel], fCellDT[0][channel]);
               else
                  status = AnalyzePeriod(ave, index, nIterPeriod, channel, wf, tCell, cellDV[channel], fCellDT[0][channel]);
	       
	       
	       
	       ////////// my 
	       float fcellDV[1024]={0};
	       for(int jj = 0; jj < 1024; jj++) fcellDV[jj] = fCellDT[0][channel][jj];
	       fwrite(fcellDV, sizeof(fcellDV), 1, file); // my
	       //////////////
	       
	       


               if (!status)
                  n_error++;
               
               if (n_error > nIterPeriod / 2) {
                  error = 1;
                  break;
               }
            }
	    
	    
            if (!status)
               break;

         } else {               // DRS4 Mezzanine board: 4 Chips
            for (mode=0 ; mode<2 ; mode++) {
               SetChannelConfig(mode*2, 8, 8);
               SoftTrigger();
               while (IsBusy());

               /* select random phase */
               phase = (rand() % 30) - 15;
               if (phase == 0)
                  phase = 15;
               EnableTcal(1, 0, phase);

               StartDomino();
               TransferWaves();

               for (chip=0 ; chip<4 ; chip+=2) {
                  tCell = GetStopCell(chip+mode);
                  GetWave(chip+mode, 8, wf, true, tCell, 0, true);
                  status = AnalyzePeriod(ave, index, nIterPeriod, 0, wf, tCell, cellDV[chip+mode], fCellDT[chip+mode][0]);

                  if (!status) {
                     error = 1;
                     break;
                  }
               }
               if (!status)
                  break;

            }
         }
      }
   } // end of loop over index
   
   fclose(file); // my

   if (pcb)
      pcb->Progress(100);
   
   /*
   /////// My
   if (fBoardType == 9) {
     for (channel = 0 ; channel < 8 ; channel++) {
       for (int ib = 0 ; ib < 1024; ib++) {
	 fCellDT[0][channel][ib] = 1;
	 //printf("%f ", (float)fCellDT[0][channel][ib] );
       }
     }
     //printf("\n");
   } // end of my
   */

   // DRS4 Evaluation board V5: copy even channels to odd channels (usually not connected)
   if (fBoardType == 9) {
      for (channel = 0 ; channel < 8 ; channel+=2)
         memcpy(fCellDT[0][channel+1], fCellDT[0][channel], sizeof(unsigned short)*1024);
   }


   // use following lines to save calibration into an ASCII file
 #if 0
   FILE *fh;

   fh = fopen("cellt.csv", "wt");
   if (!fh)
      printf("Cannot open file \"cellt.csv\"\n");
   else {
      printf("Store the time-calibration file cellt.csv\n");
      fprintf(fh, "index dt_ch1 dt_ch2 dt_ch3 dt_ch4\n");
      for (i=0 ; i<1024 ; i++)
         fprintf(fh, "%4d %5.3lf %5.3lf %5.3lf %5.3lf\n", i, fCellDT[0][0][i], fCellDT[0][2][i], fCellDT[0][4][i], fCellDT[0][6][i]);
      fclose(fh);
   }
 #endif
   
   if (fBoardType == 5 || fBoardType == 7 || fBoardType == 8) {
      /* write timing calibration to EEPROM page 0 */
      ReadEEPROM(0, buf, sizeof(buf));
      for (i=0,t1[0]=0 ; i<1024; i++)
         buf[i*2+1] = (unsigned short) (fCellDT[0][0][i] * 10000 + 0.5);

      /* write calibration method and frequency */
      buf[4] = TCALIB_METHOD_V4;
      buf[6] = (unsigned short)(fNominalFrequency*1000+0.5);
      
      fTimingCalibratedFrequency = fNominalFrequency;
      WriteEEPROM(0, buf, sizeof(buf));
      
      // copy calibration to all channels
      for (i=1 ; i<8 ; i++)
         for (j=0 ; j<1024; j++)
            fCellDT[0][i][j] = fCellDT[0][0][j];

   } else if (fBoardType == 9) {
  
      /* write timing calibration to EEPROM page 2 */
      ReadEEPROM(2, buf, sizeof(buf));
      for (i=0 ; i<8 ; i++) {
         tTrue = 0;    // true cellT
         tRounded = 0; // rounded cellT
         for (j=0 ; j<1024; j++) {
            tTrue += fCellDT[0][i][j];
            dT = tTrue - tRounded;
            // shift by 1 ns to allow negative widths
            dT = (unsigned short) (dT*10000+1000+0.5);
            tRounded += (dT - 1000) / 10000.0;
            buf[(i*1024+j)*2+1] = (unsigned short) dT;
         }
      }
      WriteEEPROM(2, buf, sizeof(buf));
      
      /* write calibration method and frequency to EEPROM page 0 */
      ReadEEPROM(0, buf, sizeof(buf));
      buf[4] = 1; // number of calibrations
      buf[2] = (TCALIB_METHOD << 8) | (buf[2] & 0xFF); // calibration method
      float fl = (float) fNominalFrequency;
      memcpy(&buf[8], &fl, sizeof(float)); // exact freqeuncy
      
      fTimingCalibratedFrequency = fNominalFrequency;
      WriteEEPROM(0, buf, sizeof(buf));
   
   } else {
   
      /* write timing calibration to EEPROM page 6 */
      ReadEEPROM(6, buf, sizeof(buf));
      for (c=0 ; c<4 ; c++)
         t1[c] = 0;
      for (i=0 ; i<1024; i++) {
         for (c=0 ; c<4 ; c++) {
            t2[c] = fCellDT[0][c][i] - t1[c];
            t2[c] = (unsigned short) (t2[c] * 10000 + 0.5);
            t1[c] += t2[c] / 10000.0;
         }
         buf[i*2]         = (unsigned short) t2[0];
         buf[i*2+1]       = (unsigned short) t2[1];
         buf[i*2+0x800]   = (unsigned short) t2[2];
         buf[i*2+0x800+1] = (unsigned short) t2[3];
      }
      WriteEEPROM(6, buf, sizeof(buf));

      /* write calibration method and frequency */
      ReadEEPROM(0, buf, 16);
      buf[4] = TCALIB_METHOD;
      buf[6] = (unsigned short) (fNominalFrequency * 1000 + 0.5);
      fTimingCalibratedFrequency = buf[6] / 1000.0;
      WriteEEPROM(0, buf, 16);
   }

   if (ave)
      delete ave;

   /* remove calibration voltage */
   EnableAcal(0, 0);
   EnableTcal(clkon, 0);
   SetInputRange(range);
   EnableTrigger(trg1, trg2);

   //printf("+++++++++++++\n");
   //EnableTcal(1);
   //StartDomino();
   
   if (error)
      return 0;

   return 1;
}


/*------------------------------------------------------------------*/


void DRSBoard::RemoveSymmetricSpikes(short **wf, int nwf,
                                     short diffThreshold, int spikeWidth,
                                     short maxPeakToPeak, short spikeVoltage,
                                     int nTimeRegionThreshold)
{
  printf("---------------RemoveSymmetricSpikes(...)\n");

   // Remove a specific kind of spike on DRS4.
   // This spike has some features,
   //  - Common on all the channels on a chip
   //  - Constant heigh and width
   //  - Two spikes per channel
   //  - Symmetric to cell #0.
   //
   // This is not general purpose spike-removing function.
   // 
   // wf                   : Waveform data. cell#0 must be at bin0,
   //                        and number of bins must be kNumberOfBins.
   // nwf                  : Number of channels which "wf" holds.
   // diffThreshold        : Amplitude threshold to find peak
   // spikeWidth           : Width of spike
   // maxPeakToPeak        : When peak-to-peak is larger than this, the channel
   //                        is not used to find spikes.
   // spikeVoltage         : Amplitude of spikes. When it is 0, it is calculated in this function
   //                        from voltage difference from neighboring bins.
   // nTimeRegionThreshold : Requirement of number of time regions having spike at common position.
   //                        Total number of time regions is 2*"nwf".

   if (!wf || !nwf || !diffThreshold || !spikeWidth) {
      return;
   }

   int          ibin, jbin, kbin;
   double       v;
   int          nbin;
   int          iwf;
   short        maximum, minimum;
   int          spikeCount[kNumberOfBins / 2];
   int          spikeCountSum[kNumberOfBins / 2] = {0};
   bool         largePulse[kNumberOfChannelsMax * 2] = {0};
   const short  diffThreshold2 = diffThreshold + diffThreshold;

   const short  maxShort = 0xFFFF>>1;
   const short  minShort = -maxShort - 1;

   // search spike
   for (iwf = 0; iwf < nwf; iwf++) {
      // first half
      memset(spikeCount, 0, sizeof(spikeCount));
      maximum = minShort;
      minimum = maxShort;
      for (ibin = 0; ibin < kNumberOfBins / 2; ibin++) {
         jbin = ibin;
         maximum = max(maximum, wf[iwf][jbin]);
         minimum = min(minimum, wf[iwf][jbin]);
         if (jbin - 1 >= 0 && jbin + spikeWidth < kNumberOfBins) {
            v = 0;
            nbin = 0;
            for (kbin = 0; kbin < spikeWidth; kbin++) {
               v += wf[iwf][jbin + kbin];
               nbin++;
            }
            if ((nbin == 2 && v - (wf[iwf][jbin - 1] + wf[iwf][jbin + spikeWidth]) > diffThreshold2) ||
                (nbin != 2 && nbin && v / nbin - (wf[iwf][jbin - 1] + wf[iwf][jbin + spikeWidth]) / 2 > diffThreshold)) {
               spikeCount[ibin]++;
            }
         }
      }
      if (maximum != minShort && minimum != maxShort &&
          (!maxPeakToPeak || maximum - minimum < maxPeakToPeak)) {
         for (ibin = 0; ibin < kNumberOfBins / 2; ibin++) {
            spikeCountSum[ibin] += spikeCount[ibin];
         }
         largePulse[iwf] = false;
#if 0 /* this part can be enabled to skip checking other channels */
         if (maximum != minShort && minimum != maxShort &&
             maximum - minimum < diffThreshold) {
            return;
         }
#endif
      } else {
         largePulse[iwf] = true;
      }

      // second half
      memset(spikeCount, 0, sizeof(spikeCount));
      maximum = minShort;
      minimum = maxShort;
      for (ibin = 0; ibin < kNumberOfBins / 2; ibin++) {
         jbin = kNumberOfBins - 1 - ibin;
         maximum = max(maximum, wf[iwf][jbin]);
         minimum = min(minimum, wf[iwf][jbin]);
         if (jbin + 1 < kNumberOfBins && jbin - spikeWidth >= 0) {
            v = 0;
            nbin = 0;
            for (kbin = 0; kbin < spikeWidth; kbin++) {
               v += wf[iwf][jbin - kbin];
               nbin++;
            }
            if ((nbin == 2 && v - (wf[iwf][jbin + 1] + wf[iwf][jbin - spikeWidth]) > diffThreshold2) ||
                (nbin != 2 && nbin && v / nbin - (wf[iwf][jbin + 1] + wf[iwf][jbin - spikeWidth]) / 2 > diffThreshold)) {
               spikeCount[ibin]++;
            }
         }
      }
      if (maximum != minShort && minimum != maxShort &&
          maximum - minimum < maxPeakToPeak) {
         for (ibin = 0; ibin < kNumberOfBins / 2; ibin++) {
            spikeCountSum[ibin] += spikeCount[ibin];
         }
         largePulse[iwf + nwf] = false;
#if 0 /* this part can be enabled to skip checking other channels */
         if (maximum != minShort && minimum != maxShort &&
             maximum - minimum < diffThreshold) {
            return;
         }
#endif
      } else {
         largePulse[iwf + nwf] = true;
      }
   }

   // Find common spike
   int commonSpikeBin = -1;
   int commonSpikeMax = -1;
   for (ibin = 0; ibin < kNumberOfBins / 2; ibin++) {
      if (commonSpikeMax < spikeCountSum[ibin]) {
         commonSpikeMax = spikeCountSum[ibin];
         commonSpikeBin = ibin;
      }
   } 

   if (spikeCountSum[commonSpikeBin] >= nTimeRegionThreshold) {
      if (spikeVoltage == 0) {
         // Estimate spike amplitude
         double  baseline      = 0;
         int    nBaseline      = 0;
         double  peakAmplitude = 0;
         int    nPeakAmplitude = 0;
         for (iwf = 0; iwf < nwf; iwf++) {
            // first half
            if (!largePulse[iwf]) {
               // baseline
               if ((jbin = commonSpikeBin - 1) >= 0 && jbin < kNumberOfBins) {
                  baseline += wf[iwf][jbin];
                  nBaseline++;
               }
               if ((jbin = commonSpikeBin + spikeWidth + 1) >= 0 && jbin < kNumberOfBins) {
                  baseline += wf[iwf][jbin];
                  nBaseline++;
               }
               // spike
               for (ibin = 0; ibin < spikeWidth; ibin++) {
                  if ((jbin = commonSpikeBin + ibin) >= 0 && jbin < kNumberOfBins) {
                     peakAmplitude += wf[iwf][jbin];
                     nPeakAmplitude++;
                  }
               }
            }

            // second half
            if (!largePulse[iwf + nwf]) {
               // baseline
               if ((jbin = kNumberOfBins - 1 - commonSpikeBin + 1) >= 0 && jbin < kNumberOfBins) {
                  baseline += wf[iwf][jbin];
                  nBaseline++;
               }
               if ((jbin = kNumberOfBins - 1 - commonSpikeBin - spikeWidth - 1) >= 0 && jbin < kNumberOfBins) {
                  baseline += wf[iwf][jbin];
                  nBaseline++;
               }
               // spike
               for (ibin = 0; ibin < spikeWidth; ibin++) {
                  if ((jbin = kNumberOfBins - 1 - commonSpikeBin - ibin) >= 0 && jbin < kNumberOfBins) {
                     peakAmplitude += wf[iwf][jbin];
                     nPeakAmplitude++;
                  }
               }
            }
         }
         if (nBaseline && nPeakAmplitude) {
            baseline /= nBaseline;
            peakAmplitude /= nPeakAmplitude;
            spikeVoltage = static_cast<short>(peakAmplitude - baseline);
         } else {
            spikeVoltage = 0;
         }
      }

      // Remove spike
      if (spikeVoltage > 0) {
         for (iwf = 0; iwf < nwf; iwf++) {
            for (ibin = 0; ibin < spikeWidth; ibin++) {
               if ((jbin = commonSpikeBin + ibin) >= 0 && jbin < kNumberOfBins) {
                  wf[iwf][jbin] -= spikeVoltage; 
               }
               if ((jbin = kNumberOfBins - 1 - commonSpikeBin - ibin) >= 0 && jbin < kNumberOfBins) {
                  wf[iwf][jbin] -= spikeVoltage; 
               }
            }
         }
      }
   }
}


#include "ResponseCalibration.cpp"
