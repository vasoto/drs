/********************************************************************\

  Name:         musbstd.c
  Created by:   Konstantin Olchanski, Stefan Ritt

  Contents:     Midas USB access

  $Id$

\********************************************************************/

#include <stdio.h>
#include <assert.h>
#include <musbstd.h>

#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <errno.h>
#include <libusb-1.0/libusb.h>

int musb_open(MUSB_INTERFACE **musb_interface, int vendor, int product, int instance, int configuration, int usbinterface)
{
   static int first_call = 1;

   libusb_device **dev_list;
   libusb_device_handle *dev;
   struct libusb_device_descriptor desc;
   
   int status, i, n;
   int count = 0;
   
   if (first_call) {
      first_call = 0;
      libusb_init(NULL);
      // libusb_set_debug(NULL, 3);
   }
      
   n = libusb_get_device_list(NULL, &dev_list);
   
   for (i=0 ; i<n ; i++) {
      status = libusb_get_device_descriptor(dev_list[i], &desc);
      if (desc.idVendor == vendor && desc.idProduct == product) {
         if (count == instance) {
            status = libusb_open(dev_list[i], &dev);
            if (status < 0) {
               fprintf(stderr, "musb_open: libusb_open() error %d\n", status);
               return MUSB_ACCESS_ERROR;
            }

            status = libusb_set_configuration(dev, configuration);
            if (status < 0) {
               fprintf(stderr, "musb_open: usb_set_configuration() error %d\n", status);
               fprintf(stderr,
                       "musb_open: Found USB device 0x%04x:0x%04x instance %d, but cannot initialize it: please check permissions on \"/proc/bus/usb/%d/%d\" and \"/dev/bus/usb/%d/%d\"\n",
                       vendor, product, instance, libusb_get_bus_number(dev_list[i]), libusb_get_device_address(dev_list[i]), libusb_get_bus_number(dev_list[i]), libusb_get_device_address(dev_list[i]));
               return MUSB_ACCESS_ERROR;
            }
            
            /* see if we have write access */
            status = libusb_claim_interface(dev, usbinterface);
            if (status < 0) {
               fprintf(stderr, "musb_open: libusb_claim_interface() error %d\n", status);
               
               fprintf(stderr,
                       "musb_open: Found USB device 0x%04x:0x%04x instance %d, but cannot initialize it: please check permissions on \"/proc/bus/usb/%d/%d\"\n",
                       vendor, product, instance, libusb_get_bus_number(dev_list[i]), libusb_get_device_address(dev_list[i]));
               
               return MUSB_ACCESS_ERROR;
            }

            *musb_interface = (MUSB_INTERFACE*)calloc(1, sizeof(MUSB_INTERFACE));
            (*musb_interface)->dev = dev;
            (*musb_interface)->usb_configuration = configuration;
            (*musb_interface)->usb_interface     = usbinterface;
            return MUSB_SUCCESS;

         }
         count++;
      }
   }
   
   libusb_free_device_list(dev_list, 1);
   
   return MUSB_NOT_FOUND;
}

int musb_set_altinterface(MUSB_INTERFACE *musb_interface, int index)
{
   return -1;
}

int musb_close(MUSB_INTERFACE *musb_interface)
{

   int status;
   status = libusb_release_interface(musb_interface->dev, musb_interface->usb_interface);
   if (status < 0)
      fprintf(stderr, "musb_close: libusb_release_interface() error %d\n", status);
   musb_reset(musb_interface);
   libusb_close(musb_interface->dev);

   /* free memory allocated in musb_open() */
   free(musb_interface);
   return 0;
}

int musb_write(MUSB_INTERFACE *musb_interface, int endpoint, const void *buf, int count, int timeout)
{
   int n_written;

   
   int status = libusb_bulk_transfer(musb_interface->dev, endpoint, (unsigned char*)buf, count, &n_written, timeout);
   
   //printf("musb_write(): endpoint=%i, timeout=%i\n",endpoint,timeout);
   //printf(" musb_write(): requested count=%d, wrote n_written=%d, errno status=%d (%s)\n", count, n_written, status, strerror(status));
   
   if (n_written != count) {
      fprintf(stderr, "musb_write: requested %d, wrote %d, errno %d (%s)\n", count, n_written, status, strerror(status));
   }

   //fprintf(stderr, "musb_write(ep %d, %d bytes) (%s) returns %d\n", endpoint, count, buf, n_written); 

   return n_written;
}

int musb_read(MUSB_INTERFACE *musb_interface, int endpoint, void *buf, int count, int timeout)
{
   int n_read = 0;

   
   int status = libusb_bulk_transfer(musb_interface->dev, endpoint | 0x80, (unsigned char*)buf, count, &n_read, timeout);
   
   //printf("musb_read(): requested %d, read %d, errno %d (%s)\n", count, n_read, status, strerror(status));

   /* errors should be handled in upper layer ....
    if (n_read <= 0) {
    fprintf(stderr, "musb_read: requested %d, read %d, errno %d (%s)\n", count, n_read, status, strerror(status));
    }
    */
   //fprintf(stderr, "musb_read(ep %d, %d bytes) returns %d (%s)\n", endpoint, count, n_read, buf); 

   return n_read;
}

int musb_reset(MUSB_INTERFACE *musb_interface)
{
   int status;
   status = libusb_reset_device(musb_interface->dev);
   if (status < 0)
      fprintf(stderr, "musb_reset: usb_reset() status %d\n", status);
   
   return 0;
}

int musb_get_device(MUSB_INTERFACE *usb_interface)
{
   struct libusb_device_descriptor d;
   libusb_get_descriptor(usb_interface->dev, LIBUSB_DT_DEVICE, 0, (unsigned char *)&d, sizeof(d));
   return d.bcdDevice;
}

/* end */
