#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <error.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <libusb-1.0/libusb.h>
#include <time.h>
#include "mystuff.h"

#define TIMEOUT_RW      1000 /* ms */

char* ui2b(unsigned int val,char *buf,int digits){
/* arguments NOT checked */
/* should work with ASCII */
	
buf[digits]=0; // end of string
do {
   buf[--digits] = '0'+(val&1);
   val >>= 1;
   } while(digits);
return buf;
}

size_t getFilesize(const char* filename) {
    struct stat st;
    if(stat(filename, &st) != 0) {
        return 0;
    }
    return st.st_size;
}

#define BYTES_PER_LINE 16
void display_le(FILE *f,unsigned char *d,unsigned int l,unsigned int addr_off) {
        int i,j;
        for(i=BYTES_PER_LINE;i;i--) fprintf(f," %02x",i-1);
        fprintf(f,"\n");
        for(i=0;i<l;i+=BYTES_PER_LINE){
                for(j=i+BYTES_PER_LINE;j>l;j--) fprintf(f,"   ");
                for(;j>i;j--) fprintf(f," %02x",d[j-1]);
                fprintf(f," : %08x\n",addr_off+i);
        }
}
int in(libusb_device_handle *handle,
       unsigned char* data,
       int bufsize /* bytes */,
       int *transferred /* bytes */) {
int r;

// for bufsize == 0 (from request for ZLP) we adjust bufsize to 1 to avoid errors
// was necessary for windows, we keep it here because it won't hurt
if (!bufsize) bufsize++;

if((r=libusb_bulk_transfer(handle,
                           EP_IN,
                           data,
                           bufsize,
                           transferred,
                           TIMEOUT_RW))) {
   fprintf(stderr, "libusb_bulk_transfer EP 0x%02x: %s\n",EP_IN,libusb_error_name(r));
   return -1;
   }
return r;
}

int out(libusb_device_handle *handle,
unsigned char* data,
int bufsize /* bytes */,
int *transferred /* bytes */) {
int r;
if((r=libusb_bulk_transfer(handle,
                           EP_OUT,
                           data,
                           bufsize,
                           transferred,
                           TIMEOUT_RW))) {
   fprintf(stderr, "libusb_bulk_transfer EP 0x%02x: %s\n",EP_OUT,libusb_error_name(r));
   return -1;
   }
return r;
}

int get_control_registers(libusb_device_handle *hUsb, t_controlregs *pctrl){

t_usbcmd mycmd; 
int transferred;
mycmd.command  = e_usbcmd_read;
mycmd.address  = BASE_CTRL_REGS;
mycmd.bytesize = sizeof(t_controlregs);

if(out(hUsb, (unsigned char *)&mycmd, sizeof(t_usbcmd), &transferred )<0) {
   fprintf(stderr,"function out() failed\n");
   return -1;
   }
if(transferred!=sizeof(t_usbcmd)) {
   fprintf(stderr,"get_control_registers out() transferred only %d of %d bytes\n",
           transferred,(int)sizeof(t_usbcmd));
   return -1 ;
   }
if(in(hUsb, (unsigned char *)pctrl, sizeof(t_controlregs), &transferred /* byte */)) {
   fprintf(stderr,"function in() failed\n");
   return -1;
   }
if(transferred!=sizeof(t_controlregs)) {
   fprintf(stderr,"get_control_registers in() transferred only %d of %d bytes\n",
           transferred,(int)sizeof(t_controlregs));
   return -1 ;
   }
return 0;
}

int set_control_registers(libusb_device_handle *hUsb, t_controlregs *pctrl){

unsigned char buffer[sizeof(t_usbcmd)+sizeof(t_controlregs)];
int transferred;

((t_usbcmd *)buffer)->command  = e_usbcmd_write;
((t_usbcmd *)buffer)->address  = BASE_CTRL_REGS;
((t_usbcmd *)buffer)->bytesize = sizeof(t_controlregs);
memcpy(buffer+sizeof(t_usbcmd),pctrl,sizeof(t_controlregs));

if(out(hUsb, (unsigned char *)buffer, 
             sizeof(t_usbcmd)+sizeof(t_controlregs), 
             &transferred )) {
   fprintf(stderr,"function out() failed\n");
   return -1;
   }
if(transferred!=sizeof(t_usbcmd)+sizeof(t_controlregs)) {
   fprintf(stderr,"set_control_registers out() transferred only %d of %d bytes\n",
           transferred,(int)sizeof(t_usbcmd)+(int)sizeof(t_controlregs));
   return -1 ;
   }
return 0;
}

int set_control_register_0(libusb_device_handle *hUsb, unsigned int reg0){

unsigned char buffer[sizeof(t_usbcmd)+sizeof(unsigned int)];
int transferred;

((t_usbcmd *)buffer)->command  = e_usbcmd_write;
((t_usbcmd *)buffer)->address  = BASE_CTRL_REGS;
((t_usbcmd *)buffer)->bytesize = sizeof(unsigned int);
((unsigned int *)(buffer+sizeof(t_usbcmd)))[0]=reg0;

if(out(hUsb, (unsigned char *)buffer, 
             sizeof(t_usbcmd)+sizeof(unsigned int), 
             &transferred )) {
   fprintf(stderr,"function out() failed\n");
   return -1;
   }
if(transferred!=sizeof(t_usbcmd)+sizeof(unsigned int)) {
   fprintf(stderr,"set_control_registers out() transferred only %d of %d bytes\n",
           transferred,(int)sizeof(t_usbcmd)+(int)sizeof(unsigned int));
   return -1 ;
   }
return 0;
}

int get_status_registers(libusb_device_handle *hUsb, t_statusregs *pstat){

t_usbcmd mycmd; 
int transferred;

mycmd.command  = e_usbcmd_read;
mycmd.address  = BASE_STAT_REGS;
mycmd.bytesize = sizeof(t_statusregs);

if(out(hUsb, (unsigned char *)&mycmd, sizeof(t_usbcmd), &transferred )) {
   fprintf(stderr,"function out() failed\n");
   return -1;
   }
if(transferred!=sizeof(t_usbcmd)) {
   fprintf(stderr,"get_status_registers out() transferred only %d of %d bytes\n",
           transferred,(int)sizeof(t_usbcmd));
   return -1 ;
   }
if(in(hUsb, (unsigned char *)pstat, sizeof(t_statusregs), &transferred /* byte */)) {
   fprintf(stderr,"function in() failed\n");
   return -1;
   }
if(transferred!=sizeof(t_statusregs)) {
   fprintf(stderr,"get_status_registers in() transferred only %d of %d bytes\n",
           transferred,(int)sizeof(t_statusregs));
   return -1 ;
   }
return 0;
}

int display_control_registers(FILE *f,t_controlregs *p) {
int i=0;
char sbuf[(sizeof(int)*8)];

if(p==NULL) goto fin;

fprintf(f,"----- Control Registers (base 0x%08x) -----\n",BASE_CTRL_REGS);

i=0;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.1s\n","start_trig",0,0,ui2b(p->start_trig,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %31.1s\n","reinit_trig",1,1,ui2b(p->reinit_trig,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %30.1s\n","soft_trig",2,2,ui2b(p->soft_trig,sbuf,1));

i=1;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.1s\n","autostart",0,0,ui2b(p->autostart,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %31.1s\n","adc_active",1,1,ui2b(p->adc_active,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %29.1s\n","tca_ctrl",3,3,ui2b(p->tca_ctrl,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %26.1s\n","enable_trigger",6,6,ui2b(p->enable_trigger,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %25.1s\n","readout_mode",7,7,ui2b(p->readout_mode,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %24.1s\n","neg_trigger",8,8,ui2b(p->neg_trigger,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %23.1s\n","acalib",9,9,ui2b(p->acalib,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %22.1s\n","refclk_source",10,10,ui2b(p->refclk_source,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %21.1s\n","dactive",11,11,ui2b(p->dactive,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %20.1s\n","standby",12,12,ui2b(p->standby,sbuf,1));

i=2;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","DAC0",15,0,ui2b(p->dac[0],sbuf,16));

i=3;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","DAC1",15,0,ui2b(p->dac[1],sbuf,16));

i=4;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","DAC2",15,0,ui2b(p->dac[2],sbuf,16));

i=5;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","DAC3",15,0,ui2b(p->dac[3],sbuf,16));

i=6;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","DAC4",15,0,ui2b(p->dac[4],sbuf,16));

i=7;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));

i=8;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));

i=9;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));

i=10;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));

fprintf(f,"  %-36s%02d:%02d = %32.4s\n","drs_ctl_last_chn",3,0,ui2b(p->drs_ctl_last_chn,sbuf,4));
fprintf(f,"  %-36s%02d:%02d = %28.4s\n","drs_ctl_first_chn",7,4,ui2b(p->drs_ctl_first_chn,sbuf,4));
fprintf(f,"  %-36s%02d:%02d = %16.8s\n","drs_ctl_chn_config",23,16,ui2b(p->drs_ctl_chn_config,sbuf,8));
fprintf(f,"  %-36s%02d:%02d = %8.1s\n","dmode",24,24,ui2b(p->dmode,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %7.1s\n","pllen",25,25,ui2b(p->pllen,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %6.1s\n","wsrloop",26,26,ui2b(p->wsrloop,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %5.5s\n","drs_ctl_config[7:3]",31,27,ui2b(p->drs_ctl_config_7_3,sbuf,5));

i=11;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","sampling_freq",15,0,ui2b(p->sampling_freq,sbuf,16));
fprintf(f,"  %-36s%02d:%02d = %16.8s\n","trigger_delay",23,16,ui2b(p->trigger_delay,sbuf,8));

i=12;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %16.16s\n","trigger_config",31,16,ui2b(p->trigger_config,sbuf,16));

i=13;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","eeprom_page",15,0,ui2b(p->eeprom_page,sbuf,16));
fprintf(f,"  %-36s%02d:%02d = %2.1s\n","eeprom_write_trig",30,30,ui2b(p->eeprom_write_trig,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %1.1s\n","eeprom_read_trig",31,31,ui2b(p->eeprom_read_trig,sbuf,1));

i=14;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.8s\n","adcclk_phase_shift",7,0,ui2b(p->adcclk_phase_shift,sbuf,8));
fprintf(f,"  %-36s%02d:%02d = %2.1s\n","adcclk_phase_updn",30,30,ui2b(p->adcclk_phase_updn,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %1.1s\n","adcclk_phase_trig",31,31,ui2b(p->adcclk_phase_trig,sbuf,1));

fin:
return (0);
}

int display_status_registers(FILE *f,t_statusregs *p) {
char sbuf[(sizeof(int)*8)];
int i=0;

if(p==NULL) goto fin;

fprintf(f,"----- Status Registers (base 0x%08x) -----\n",BASE_STAT_REGS);
i=0;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.8s\n","drs_type",7,0,ui2b(p->drs_type,sbuf,8));
fprintf(f,"  %-36s%02d:%02d = %24.8s\n","board_type",16,8,ui2b(p->board_type,sbuf,8));
fprintf(f,"  %-36s%02d:%02d = %16.16s\n","board_magic",31,16,ui2b(p->board_magic,sbuf,16));

i=1;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.1s\n","plllck_0",0,0,ui2b(p->plllck_0,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %31.1s\n","plllck_1",1,1,ui2b(p->plllck_1,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %30.1s\n","plllck_2",2,2,ui2b(p->plllck_2,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %29.1s\n","plllck_3",3,3,ui2b(p->plllck_3,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %28.1s\n","denable",4,4,ui2b(p->denable,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %27.1s\n","dwrite",5,5,ui2b(p->dwrite,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %26.1s\n","arm_trig",6,6,ui2b(p->arm_trig,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %25.1s\n","trig_ff",7,7,ui2b(p->trig_ff,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %24.1s\n","soft_trig",8,8,ui2b(p->soft_trig,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %23.1s\n","stat_busy",9,9,ui2b(p->stat_busy,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %22.1s\n","stat_running",10,10,ui2b(p->stat_running,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %21.1s\n","stat_idle",11,11,ui2b(p->stat_idle,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %3.1s\n","serdes_busy",29,29,ui2b(p->serdes_busy,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %2.1s\n","serial_busy",30,30,ui2b(p->serial_busy,sbuf,1));
fprintf(f,"  %-36s%02d:%02d = %1.1s\n","eeprom_busy",31,31,ui2b(p->eeprom_busy,sbuf,1));

i=2;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.10s\n","stop_cell_0",9,0,ui2b(p->stop_cell_0,sbuf,10));
fprintf(f,"  %-36s%02d:%02d = %16.8s\n","stop_wsr_0",23,16,ui2b(p->stop_wsr_0,sbuf,8));
fprintf(f,"  %-36s%02d:%02d = %8.8s\n","adcclk_phase_out",31,24,ui2b(p->adcclk_phase_out,sbuf,8));

i=3;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.10s\n","stop_cell_1",9,0,ui2b(p->stop_cell_1,sbuf,10));
fprintf(f,"  %-36s%02d:%02d = %16.8s\n","stop_wsr_1",23,16,ui2b(p->stop_wsr_1,sbuf,8));

i=4;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.10s\n","stop_cell_2",9,0,ui2b(p->stop_cell_2,sbuf,10));
fprintf(f,"  %-36s%02d:%02d = %16.8s\n","stop_wsr_2",23,16,ui2b(p->stop_wsr_2,sbuf,8));

i=5;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.10s\n","stop_cell_3",9,0,ui2b(p->stop_cell_3,sbuf,10));
fprintf(f,"  %-36s%02d:%02d = %16.8s\n","stop_wsr_3",23,16,ui2b(p->stop_wsr_3,sbuf,8));

i=6;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %16.16s\n","trigger bus",31,16,ui2b(p->trigger_bus,sbuf,16));

i=7;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","temperature_0",15,0,ui2b(p->temperature[0],sbuf,16));
fprintf(f,"  %-36s%02d:%02d = %16.16s\n","temperature_1",31,16,ui2b(p->temperature[1],sbuf,16));

i=8;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","temperature_2",15,0,ui2b(p->temperature[2],sbuf,16));
fprintf(f,"  %-36s%02d:%02d = %16.16s\n","temperature_3",31,16,ui2b(p->temperature[3],sbuf,16));

i=9;
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
fprintf(f,"  %-36s%02d:%02d = %32.16s\n","version_fw",15,0,ui2b(p->version_fw,sbuf,16));
fprintf(f,"  %-36s%02d:%02d = %16.16s\n","serial_number",31,16,ui2b(p->serial_number,sbuf,16));
for(i=10;i<16;i++) {
fprintf(f,"Register 0x%02x, Byte Offset 0x%02x: 0x%08x = %32s\n",
        i, i*(int)sizeof(int),
        ((unsigned int *)p)[i],ui2b(((unsigned int *)p)[i],sbuf,32));
   }

fin:
return (0);
}

/*
 * Because bit field numbering is implementation dependent we do this:
 * "little endian" bit numbering msb=31, lsb=0
 * 31>=posh>=0, 31>=posl>=0, posh>=posl
 * arguments NOT checked 
 * return copy of src with src[posh:posl] replaced by field[(posh-posl):0]
 */
unsigned int uint32_set_bitfield(unsigned int src,
                                 unsigned int posh,
                                 unsigned int posl,
                                 unsigned int field){
signed int mask;
mask = ((1<<(posh-posl+1))-1); // min 0x00000001, max 0xffffffff
return ((src & ~(mask<<posl)) | ((field & mask)<<posl));
}

