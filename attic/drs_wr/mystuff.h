/*
 *  Wolfgang.Rauch@cern.ch 20160819
 */
#define GET_FW_VERSION  0x20
#define GET_BUFF_SIZE   0x21
#define VENDOR_ID       0x04b4
#define PRODUCT_ID      0x00f1
#define EP_OUT          0x01
#define EP_IN           0x81

#define BASE_CTRL_REGS  0x00000000
#define BASE_STAT_REGS  0x00010000
#define BASE_DPRAM      0x00040000

/*
 * enums
 */
enum e_usbcmd {e_usbcmd_read=1,e_usbcmd_write=2,e_usbcmd_readzlp=5,e_usbcmds};
/*
 *  typedefs
 */
typedef struct __attribute__((__packed__)) s_usbcmd {
unsigned int command;
unsigned int address;
unsigned int bytesize;
} t_usbcmd, *t_pusbcmd;

/*
 *  to ease data retrieval from DPNC342
 *  two structures: t_controlregs and t_statusregs
 *  data show up in structures as seen in FPGA
 */

typedef struct __attribute__((__packed__)) s_controlregs {

// reg  0, byte offset 0x00
unsigned char  start_trig         :  1;  //  0
unsigned char  reinit_trig        :  1;  //  1
unsigned char  soft_trig          :  1;  //  2
unsigned int                      : 29; //  31 - 2

// reg  1, byte offset 0x04
unsigned char  autostart          :  1;  //  0
unsigned char  adc_active         :  1;  //  1
unsigned char                     :  1;  //  2
unsigned char  tca_ctrl           :  1;  //  3 
unsigned char                     :  2;  //  5 -  4
unsigned char  enable_trigger     :  1;  //  6
unsigned char  readout_mode       :  1;  //  7
unsigned char  neg_trigger        :  1;  //  8
unsigned char  acalib             :  1;  //  9
unsigned char  refclk_source      :  1;  // 10 
unsigned char  dactive            :  1;  // 11
unsigned char  standby            :  1;  // 12
unsigned int                      : 19;  // 31 - 12

// reg  2, byte offset 0x04
unsigned int  dac[5];
 
// reg  7, byte offset 0x1c
unsigned int                      : 32;

// reg  8, byte offset 0x20
unsigned int                      : 32;

// reg  9, byte offset 0x24
unsigned int                      : 32;

// reg 10, byte offset 0x28
unsigned char drs_ctl_last_chn    :  4;  //  3 -  0
unsigned char drs_ctl_first_chn   :  4;  //  7 -  4
unsigned char                     :  8;  // 15 -  8
unsigned char drs_ctl_chn_config  :  8;  // 23 - 16
unsigned char dmode               :  1;  // 24
unsigned char pllen               :  1;  // 25
unsigned char wsrloop             :  1;  // 26
unsigned char drs_ctl_config_7_3  :  5;  // 31 - 27

// reg 11, byte offset 0x2c
unsigned short sampling_freq;            // 15 -  0
unsigned char  trigger_delay;            // 23 - 16
unsigned char                     :  8;  // 31 - 24

// reg 12, byte offset 0x30
unsigned short                    : 16;  // 15 -  0
unsigned short trigger_config;           // 31 - 16

// reg 13, byte offset 0x34
unsigned short eeprom_page;              // 15 -  0
unsigned short                    : 14;  // 29 - 16
unsigned char eeprom_write_trig   :  1;  // 30
unsigned char eeprom_read_trig    :  1;  // 31

// reg 14, byte offset 0x38
unsigned char adcclk_phase_shift;        //  7 -  0
unsigned int                      : 22;  // 29 -  8
unsigned char adcclk_phase_updn   :  1;  // 30
unsigned char adcclk_phase_trig   :  1;  // 31
} t_controlregs, *t_pcontrolregs;

typedef struct __attribute__((__packed__)) s_statusregs {
// reg  0, byte offset 0x00
unsigned char  drs_type;              //  7 -  0
unsigned char  board_type;            // 15 -  8
unsigned short board_magic;           // 31 - 16
// reg  1, byte offset 0x04
unsigned char  plllck_0         :  1;    //  0
unsigned char  plllck_1         :  1;    //  1
unsigned char  plllck_2         :  1;    //  2
unsigned char  plllck_3         :  1;    //  3
unsigned char  denable          :  1;    //  4
unsigned char  dwrite           :  1;    //  5
unsigned char  arm_trig         :  1;    //  6
unsigned char  trig_ff          :  1;    //  7
unsigned char  soft_trig        :  1;    //  8 
unsigned char  stat_busy        :  1;    //  9
unsigned char  stat_running     :  1;    // 10
unsigned char  stat_idle        :  1;    // 11
unsigned int                    : 17;    // 28 - 12 
unsigned char  serdes_busy      :  1;    // 29
unsigned char  serial_busy      :  1;    // 30
unsigned char  eeprom_busy      :  1;    // 31
// reg  2, byte offset 0x08
unsigned short stop_cell_0      : 10;    //  9 -  0
unsigned char                   :  6;    // 15 - 10
unsigned short stop_wsr_0       :  8;    // 23 - 16
unsigned char  adcclk_phase_out :  8;    // 31 - 24
// reg  3, byte offset 0x0c
unsigned short stop_cell_1      : 10;    //  9 -  0
unsigned char                   :  6;    // 15 - 10
unsigned short stop_wsr_1       :  8;    // 23 - 16
unsigned char                   :  8;    // 31 - 24
// reg  4, byte offset 0x10
unsigned short stop_cell_2      : 10;    //  9 -  0
unsigned char                   :  6;    // 15 - 10
unsigned short stop_wsr_2       :  8;    // 23 - 16
unsigned char                   :  8;    // 31 - 24
// reg  5, byte offset 0x14
unsigned short stop_cell_3      : 10;    //  9 -  0
unsigned char                   :  6;    // 15 - 10
unsigned short stop_wsr_3       :  8;    // 23 - 16
unsigned char                   :  8;    // 31 - 24
// reg  6, byte offset 0x18
unsigned short                  : 16;    // 15 -  0 
unsigned short trigger_bus      : 16;    // 31 - 16
// reg  7, byte offset 0x1c
unsigned short  temperature[4];
// reg  9, byte offset 0x24
unsigned short version_fw;
unsigned short serial_number;
// reg 10, byte offset 0x28
unsigned int scaler[6];
// byte offset 0x40
} t_statusregs, *t_pstatusregs;

/*
 * Function Prototypes
 */
size_t getFilesize(const char* filename);
void display_le(FILE *f,unsigned char *d,unsigned int l,unsigned int addr_off);
#ifdef __cplusplus
extern "C" {
#endif
int in(libusb_device_handle *handle,
       unsigned char* data,
       int bufsize /* bytes */,
       int *transferred /* bytes */);
int out(libusb_device_handle *handle,
       unsigned char* data,
       int bufsize /* bytes */,
       int *transferred /* bytes */);
int get_control_registers(libusb_device_handle *hUsb, t_controlregs *pctrl);
int set_control_registers(libusb_device_handle *hUsb, t_controlregs *pctrl);
int set_control_register_0(libusb_device_handle *hUsb, unsigned int reg0);
int get_status_registers(libusb_device_handle *hUsb, t_statusregs *pstat);
int display_control_registers(FILE *f,t_controlregs *p);
int display_status_registers(FILE *f,t_statusregs *p);
#ifdef __cplusplus
}
#endif
unsigned int uint32_set_bitfield(unsigned int src,
                                 unsigned int posh,
                                 unsigned int posl,
                                 unsigned int field);
