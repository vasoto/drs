#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <error.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <libusb-1.0/libusb.h>
#include <time.h>
#include "mystuff.h"

#define VERBOSE
#define MAXFX3BULKOUT   64
#define MAXFX3BULKIN    1024
#define BLOCKSIZE       (64*MAXFX3BULKIN) // byte
#define TIMEOUT_C       1000 /* ms */
#define TIMEOUT_I       1000 /* ms */
#define TIMEOUT_O       1000 /* ms */

#ifdef NOTNOW
#endif /* NOTNOW */

int main( int argc, char *argv[ ], char *envp[ ] )
{
libusb_device **devs=NULL,**dev_current_p=NULL; 
libusb_context *ctx = NULL;
struct libusb_device_descriptor dev_desc;
struct libusb_config_descriptor *config_desc=NULL;
//libusb_interface *interface;
//int bConfigurationValue;
//
libusb_device_handle *hUsb=NULL;
unsigned char data[BLOCKSIZE];
int transferred=0;
ssize_t cnt;
int hFile;
int r,transfers,somesize,restsize /* bytes */ ,bufsize /* bytes */,j;
int someaddress /* byte address */;
char somestr[200];
struct stat st;
t_usbcmd mycmd;
t_controlregs my_ctrlregs;
t_statusregs  my_statregs;

fprintf(stdout,"HiHo\n");

if((r=libusb_init(&ctx))) {
   fprintf(stderr,"libusb_init: %s\n",libusb_error_name(r));
   goto fin;
   }


libusb_set_debug(ctx, LIBUSB_LOG_LEVEL_DEBUG);

if((cnt = libusb_get_device_list(ctx, &devs))<0) { 
   fprintf(stderr,"libusb_get_device_list(ctx, &devs): %s\n",libusb_error_name(cnt));
   goto fin;
   }
/*
** We have a list of devices
*/
#ifdef VERBOSE
fprintf(stdout,"%d devices in list\n",(int)cnt);
#endif /* VERBOSE */

for(dev_current_p=devs;dev_current_p<devs+cnt;dev_current_p++) {
   if((r=libusb_get_device_descriptor(*dev_current_p,&dev_desc))) {
      fprintf(stderr,"libusb_get_device_descriptor(*dev_current_p,&dev_desc) %s\n",
              libusb_error_name(r));
      goto fin;
      }
   if((dev_desc.idVendor==VENDOR_ID)&&(dev_desc.idProduct==PRODUCT_ID)) {
/*
** We have a device to look at ...
*/ 
#ifdef VERBOSE
       fprintf(stdout,"Found device ID %04x:%04x with %d configuration(s)\n",
                       dev_desc.idVendor,dev_desc.idProduct,dev_desc.bNumConfigurations);
       fprintf(stdout,"Any other devices disregarded...\n");
#endif /* VERBOSE */
      break;
      } // if((dev_desc.idVendor==VENDOR_ID)&&(dev_desc.idProduct==PRODUCT_ID))
   } // for(dev_current_p=devs;dev_current_p<devs+cnt;dev_current_p++)

if(dev_current_p>=devs+cnt) goto fin;

if((r=libusb_open(*dev_current_p,&hUsb))) {
      fprintf(stderr,"libusb_open(*dev_current_p,&hUsb) %s\n",
              libusb_error_name(r));
      goto fin;
      }


/*******
 * We have a handle: hUsb
 * Action starts here
*******/
//fprintf(stdout,"We finally got hUsb=%d\n",(int)hUsb);

/*** Get transfer size EP_OUT ***/

fprintf(stdout,"Speed %d\n",libusb_get_device_speed(*dev_current_p));
fprintf(stdout,"EP: 0x%02x, max packet size = %d\n",EP_OUT,
                libusb_get_max_packet_size(*dev_current_p,EP_OUT));
fprintf(stdout,"EP: 0x%02x, max packet size = %d\n",EP_IN,
                libusb_get_max_packet_size(*dev_current_p,EP_IN));

/*** User class request ***/

fprintf(stdout,"User class request GET_FW_VERSION %02x ... ",GET_FW_VERSION);
if((r=libusb_control_transfer(
           hUsb,
           LIBUSB_ENDPOINT_IN         |
           LIBUSB_REQUEST_TYPE_CLASS  |
           LIBUSB_RECIPIENT_DEVICE,       /* bmRequestType */
           GET_FW_VERSION,                /* bRequest      */
           0,            /* wValue: */
           0,            /* wIndex: Endpoint num*/
           data,         /* buffer to receive data */
           32,           /* wLength*/
           TIMEOUT_C     /* timeout millis*/))<0)  {
    fprintf(stderr, "libusb_control_transfer : %s\n", libusb_error_name(r));
    goto fin;
    }
fprintf(stdout,"returns %d bytes: \n",r);
fprintf(stdout,"Firmware string: %s\n",data);

/*** User class request ***/

fprintf(stdout,"User class request GET_BUFF_SIZE %02x ... ",GET_BUFF_SIZE);
if((r=libusb_control_transfer(
           hUsb,
           LIBUSB_ENDPOINT_IN         |
           LIBUSB_REQUEST_TYPE_CLASS  |
           LIBUSB_RECIPIENT_DEVICE,       /* bmRequestType */
           GET_BUFF_SIZE,                 /* bRequest      */
           0,            /* wValue: */
           0,            /* wIndex: Endpoint num*/
           data,         /* buffer to receive data */
           8,            /* wLength*/
           TIMEOUT_C     /* timeout millis*/))<0)  {
    fprintf(stderr, "libusb_control_transfer : %s\n", libusb_error_name(r));
    return 1;
    }

fprintf(stdout,"returns %d bytes: \n",r);
fprintf(stdout,"Host to FX3 (OUT), dma buffer size = 0x%04x, dma buffer count = 0x%04x\n",
               *((unsigned short *) (data+0)),*((unsigned short *) (data+2)));
fprintf(stdout,"FX3 to host (IN) , dma buffer size = 0x%04x, dma buffer count = 0x%04x\n",
               *((unsigned short *) (data+4)),*((unsigned short *) (data+6)));
for(transfers=1;;transfers++) {
   fprintf(stdout,"\n-------------------------------------\n");
   fprintf(stdout,"{i[n],o[ut],e[xit]} size (hex, 32-bit words) or\n");
   fprintf(stdout,"{re[ad],rz[lp],wr[ite]} address (hex, byte) size (hex, byte) or\n");
   fprintf(stdout,"{rf[ile],wf[ile]} address (hex, byte) size (hex, byte) filename or\n");
   fprintf(stdout,"{rr[egisters]},c01,start,soft>");
   if((fscanf(stdin,"%s",somestr) != 1)) { 
      fprintf(stderr,"read error\n");
      continue;
      }
   if(somestr[0]=='i' || somestr[0]=='I') { 
/***
 * in command   
***/
      if((fscanf(stdin,"%x",&somesize) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }

      bufsize = somesize * sizeof(int);
      if(bufsize>BLOCKSIZE) {
         fprintf(stderr,"Warning: size (bytes) = %08x > %08x\n",bufsize,BLOCKSIZE);
         }
      if(in(hUsb, data, bufsize /* byte */, &transferred /* byte */)) {
         fprintf(stderr,"function in() failed\n"); 
         goto fin; 
         }
      fprintf(stdout,"transfer %d: 0x%08x of 0x%08x bytes read\n",
              transfers,transferred,bufsize); 
      if(transferred) display_le(stdout,data,transferred,0);
      }
   else if(somestr[0]=='o' || somestr[0]=='O') {
/***
 * out command   
***/
      if((fscanf(stdin,"%x",&somesize) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      bufsize = somesize * sizeof(int);
      if(bufsize>MAXFX3BULKOUT) {
         fprintf(stderr,"Warning: size (bytes) = %08x > %08x \n",bufsize,MAXFX3BULKOUT);
         }
//      fprintf(stdout,"buffer size will always be 0x%02x (bytes)\n",MAXFX3BULKOUT);
      for(j=0;
            j<somesize;
//          j<((somesize<MAXFX3BULKOUT/sizeof(int)) ?
//                      somesize : (MAXFX3BULKOUT/sizeof(int)));
          j++) {
         fprintf(stdout," word 0x%02x>",j);
         fscanf(stdin,"%x",((int *)data)+j);
         }
//      somesize=MAXFX3BULKOUT/(sizeof (int)); // force to EP out packet size
//      for(;j<somesize;j++) ((int *)data)[j]=0;
      if(somesize) display_le(stdout,data,somesize*sizeof(int),0);
      if(out(hUsb, data, bufsize /* byte */, &transferred /* byte */)) {
         fprintf(stderr,"function out() failed\n"); 
         goto fin; 
         }
      fprintf(stdout,"transfer %d: 0x%08x of 0x%08lx bytes written\n",
              transfers,transferred,somesize*sizeof(unsigned int)); 
      } 
   else if(!strncasecmp(somestr,"re",2)) {
/***
 * re command   
***/
      mycmd.command  = e_usbcmd_read;
      if((fscanf(stdin,"%x",&someaddress) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      mycmd.address  = someaddress;
      if((fscanf(stdin,"%x",&bufsize) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      if((bufsize/sizeof(int))*sizeof(int)<bufsize) bufsize=((bufsize/sizeof(int))+1)*sizeof(int);
      if(bufsize > BLOCKSIZE) {
         fprintf(stderr,"bufsize 0x%08x > max=0x%08x\n",bufsize,BLOCKSIZE);
         goto fin;
         }
      mycmd.bytesize  = bufsize;
// send out command
      if(out(hUsb, (unsigned char *)&mycmd, sizeof(t_usbcmd), &transferred )) {
         fprintf(stderr,"function out() failed\n");
         goto fin; 
         }
      if(transferred!=sizeof(t_usbcmd)) {
         fprintf(stderr,"out() transferred only %d of %d bytes\n",
                 transferred,(int)sizeof(t_usbcmd));
         goto fin; 
         }
      transfers++;
// send in command
      if(in(hUsb, data, bufsize /* byte */, &transferred /* byte */)) {
         fprintf(stderr,"function in() failed\n"); 
         goto fin; 
         }
      fprintf(stdout,"0x%08x of 0x%08x bytes read\n",transferred,bufsize); 
      if(transferred) display_le(stdout,data,transferred,someaddress);
      }
   else if(!strncasecmp(somestr,"rz",2)) {
/***
 * rz command   
***/
      mycmd.command  = e_usbcmd_readzlp;
      if((fscanf(stdin,"%x",&someaddress) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      mycmd.address  = someaddress;
      if((fscanf(stdin,"%x",&bufsize) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      if((bufsize/sizeof(int))*sizeof(int)<bufsize) bufsize=((bufsize/sizeof(int))+1)*sizeof(int);
      if(bufsize > BLOCKSIZE) {
         fprintf(stderr,"bufsize 0x%08x > max=0x%08x\n",bufsize,BLOCKSIZE);
         goto fin;
         }
      mycmd.bytesize  = bufsize;
// send out command
      if(out(hUsb, (unsigned char *)&mycmd, sizeof(t_usbcmd), &transferred )) {
         fprintf(stderr,"function out() failed\n");
         goto fin; 
         }
      if(transferred!=sizeof(t_usbcmd)) {
         fprintf(stderr,"out() transferred only %d of %d bytes\n",
                 transferred,(int)sizeof(t_usbcmd));
         goto fin; 
         }
      transfers++;
// send in command
      if(in(hUsb, data, bufsize /* byte */, &transferred /* byte */)) {
         fprintf(stderr,"function in() failed\n"); 
         goto fin; 
         }
      fprintf(stdout,"0x%08x of 0x%08x bytes read\n",transferred,bufsize); 
      if(transferred) display_le(stdout,data,transferred,someaddress);
      }
   else if(!strncasecmp(somestr,"wr",2)) {
/***
 * wr command   
***/
      ((int *)data)[0]=e_usbcmd_write;
      if((fscanf(stdin,"%x",&someaddress) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      ((int *)data)[1]=someaddress;
      if((fscanf(stdin,"%x",&bufsize) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      if((bufsize/sizeof(int))*sizeof(int)<bufsize) bufsize=((bufsize/sizeof(int))+1)*sizeof(int);
      if(bufsize > (BLOCKSIZE-3*sizeof(int))) {//  adjust for command words
         fprintf(stderr,"bufsize 0x%08x > max=0x%08lx\n",bufsize,BLOCKSIZE-3*sizeof(int));
         goto fin;
         }
      ((int *)data)[2]=bufsize;
      for(j=3;
            j<(bufsize/sizeof(int)+3);
//          j<((somesize<MAXFX3BULKOUT/sizeof(int)) ?
//                      somesize : (MAXFX3BULKOUT/sizeof(int)));
         j++) {
         fprintf(stdout," word 0x%02x>",j-3);
         fscanf(stdin,"%x",((int *)data)+j);
         }
// send out command
      restsize =  bufsize+3*sizeof(int);
      while(restsize>MAXFX3BULKOUT) {
         if(out(hUsb, data+bufsize+3*sizeof(int)-restsize, MAXFX3BULKOUT /* byte */, &transferred /* byte */)) {
            fprintf(stderr,"function out() failed\n"); 
            goto fin; 
            }
         restsize -= transferred;
         }
      fprintf(stdout,"0x%08x of 0x%08lx bytes written (including 12 byte for command words)\n",
              transferred,bufsize+3*sizeof(int)); 
      if(out(hUsb, data+bufsize+3*sizeof(int)-restsize, restsize /* byte */, &transferred /* byte */)) {
         fprintf(stderr,"function out() failed\n"); 
         goto fin; 
            }
      fprintf(stdout,"0x%08x of 0x%08lx bytes written (including 12 byte for command words)\n",
              transferred,bufsize+3*sizeof(int)); 
//      if(transferred) 
      display_le(stdout,data+3*sizeof(int),bufsize,0);
      }
   else if(!strncasecmp(somestr,"rf",2)) {
/***
 * rf command: read from USB, write to file   
***/
      mycmd.command  = e_usbcmd_read;
      if((fscanf(stdin,"%x",&someaddress) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      mycmd.address  = someaddress;
      if((fscanf(stdin,"%x",&bufsize) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      if((bufsize/sizeof(int))*sizeof(int)<bufsize) bufsize=((bufsize/sizeof(int))+1)*sizeof(int);
      if(bufsize > BLOCKSIZE) {
         fprintf(stderr,"bufsize 0x%08x > max=0x%08x\n",bufsize,BLOCKSIZE);
         goto fin;
         }
      mycmd.bytesize  = bufsize;
      if((fscanf(stdin,"%s",somestr) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      if((hFile = open(somestr,O_WRONLY|O_CREAT|O_EXCL,S_IRUSR|S_IWUSR))==(-1)){
        error(0,errno,"open(%s,O_WRONLY|O_CREAT|O_EXCL)",somestr);
        goto fin;
        }

// send out command
      if(out(hUsb, (unsigned char *)&mycmd, sizeof(t_usbcmd), &transferred )) {
         fprintf(stderr,"function out() failed\n");
         goto fin; 
         }
      if(transferred!=sizeof(t_usbcmd)) {
         fprintf(stderr,"out() transferred only %d of %d bytes\n",
                 transferred,(int)sizeof(t_usbcmd));
         goto fin; 
         }
      transfers++;
// send in command
      if(in(hUsb, data, bufsize /* byte */, &transferred /* byte */)) {
         fprintf(stderr,"function in() failed\n"); 
         goto fin; 
         }
      fprintf(stdout,"0x%08x of 0x%08x bytes read\n",transferred,bufsize); 
      somesize = transferred;
      for(restsize=transferred,transferred=0;restsize>0;restsize-=transferred){
         if((transferred=write(hFile,data+(somesize-restsize),restsize))<0) {
            error(0,errno,"write(hFile,data+(somesize-restsize),restsize)");
            goto fin;
            }
#ifdef VERBOSE
         fprintf(stdout,"Transferred %d bytes to \"%s\"\n",transferred,somestr);
#endif /* VERBOSE */
         }
      fprintf(stdout,"Transfer of %d bytes to \"%s\" completed.\n",somesize,somestr);
      if((r=close(hFile))) {
         error(0,errno,"close(hFile)");
         }
      }
   else if(!strncasecmp(somestr,"wf",2)) {
/***
 * wf command: write to USB, read from file     
***/
      ((int *)data)[0]=e_usbcmd_write;
      if((fscanf(stdin,"%x",&someaddress) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      ((int *)data)[1]=someaddress;
      if((fscanf(stdin,"%x",&bufsize) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }
      if((bufsize/sizeof(int))*sizeof(int)<bufsize) bufsize=((bufsize/sizeof(int))+1)*sizeof(int);
      if(bufsize > (BLOCKSIZE-3*sizeof(int))) {//  adjust for command words
         fprintf(stderr,"bufsize 0x%08x > max=0x%08lx\n",bufsize,BLOCKSIZE-3*sizeof(int));
         goto fin;
         }
      ((int *)data)[2]=bufsize;
      if((fscanf(stdin,"%s",somestr) != 1)) {
         fprintf(stderr,"read error\n");
         continue;
         }

      if((hFile = open(somestr,O_RDONLY))==(-1)){
        error(0,errno,"open(%s,O_RDONLY)",somestr);
        goto fin;
        }
      if(stat(somestr, &st) != 0) { // get file size
        error(0,errno,"stat(%s,&st)",somestr);
        goto fin;
        }
      somesize = st.st_size;
      if(somesize<bufsize) {
         fprintf(stderr,"\"%s\": only 0x%08x bytes\n",somestr,somesize);
         goto fin;
         }
      else if(somesize>bufsize) {
         fprintf(stderr,"WARNING \"%s\": 0x%08x bytes in file, only 0x%08x bytes requested.\n",
                 somestr,somesize,bufsize);
         }
      for(restsize=bufsize,transferred=0;restsize>0;restsize-=transferred){
         if((transferred=read(hFile,data+(3*sizeof(int))+(bufsize-restsize),restsize))<0) {
            error(0,errno,"read(hFile,data+(somesize-restsize),restsize)");
            goto fin;
            }
#ifdef VERBOSE
         fprintf(stdout,"Transferred %d bytes from \"%s\"\n",transferred,somestr);
#endif /* VERBOSE */
         }
      fprintf(stdout,"Transfer of %d bytes from \"%s\" completed.\n",bufsize,somestr);
// send out command
      restsize =  bufsize+3*sizeof(unsigned int);
      while(restsize>MAXFX3BULKOUT) {
         if(out(hUsb, data+bufsize+3*sizeof(int)-restsize, MAXFX3BULKOUT /* byte */, &transferred /* byte */)) {
            fprintf(stderr,"function out() failed\n"); 
            goto fin; 
            }
         restsize -= transferred;
         }
      if(out(hUsb, data+bufsize+3*sizeof(int)-restsize, restsize /* byte */, &transferred /* byte */)) {
         fprintf(stderr,"function out() failed\n"); 
         goto fin; 
            }
//      fprintf(stdout,"0x%08x of 0x%08x bytes written (including 12 byte for command words)\n",
//              transferred,bufsize+3*sizeof(int)); 
//      if(transferred) 
//      display_le(stdout,data+3*sizeof(int),bufsize,0);
      if((r=close(hFile))) {
         error(0,errno,"close(hFile)");
         }
      }
   else if(!strncasecmp(somestr,"rr",2)) {
/***
 * rr command: read control and status registers
***/
      fprintf(stderr,"\n");
      if((r=get_control_registers(hUsb,&my_ctrlregs))) {
         fprintf(stderr,"function get_control_registers() failed\n"); 
         goto fin; 
         }
      display_control_registers(stdout,&my_ctrlregs);
      fprintf(stderr,"\n");
      if((r=get_status_registers(hUsb,&my_statregs))) {
         fprintf(stderr,"function get_status_registers() failed\n"); 
         goto fin; 
         }
      display_status_registers(stdout,&my_statregs);
      fprintf(stderr,"\n");
      }
   else if(!strncasecmp(somestr,"c01",2)) {
/***
 * c01 command: configuration 01 
***/
      if((r=get_control_registers(hUsb,&my_ctrlregs))) {
         fprintf(stderr,"function get_control_registers() failed\n"); 
         goto fin; 
         }
      my_ctrlregs.start_trig=0;
      my_ctrlregs.reinit_trig=1;
      my_ctrlregs.soft_trig=0;
      if((r=set_control_registers(hUsb,&my_ctrlregs))) {
         fprintf(stderr,"function set_control_registers() failed\n"); 
         goto fin; 
         }
      my_ctrlregs.reinit_trig=0;  // next set_registers() we don't want it again

      my_ctrlregs.dactive         = 1;  // 0: stop domino wave during readout, 1: keep it running
//      my_ctrlregs.dactive         = 0;  // 0: stop domino wave during readout, 1: keep it running
      my_ctrlregs.readout_mode    = 1;  // 0: start from first bin, 1: start from domino stop
//      my_ctrlregs.readout_mode    = 0;  // 0: start from first bin, 1: start from domino stop
      my_ctrlregs.neg_trigger     = 0;  // 0: trigger low to high, 1: trigger high to low
//      my_ctrlregs.trigger_config  = 1;  // DRS 1
//      my_ctrlregs.trigger_config  = 0x1F; // all trigger sources
      my_ctrlregs.trigger_config  = 0x10; // front panel trigger (NIM) 
      my_ctrlregs.tca_ctrl        = 0;  // 0: 100 MHz clock off/on
      my_ctrlregs.enable_trigger  = 1;  // 0/1: disable/enable trigger
      my_ctrlregs.acalib          = 1;  // 0/1: disable/enable amplitude calibration

/* VCAL DAC */
#define MAXVCAL      2.048 // Volt, see schematics
#define MAXTRIGLEVEL 2.048 // Volt, see schematics
#define DACBITS 16    // in reality 12 bits, LSB 3 to LSB 0 constant 0
// a write to a DAC will trigger an action
      double value;
      unsigned short local_d;
// TrigLevel0
      value = 0.5;
      if      (value <= 0.)      local_d = 0;
      else if (value >= MAXTRIGLEVEL) local_d = (1<<DACBITS)-1;
      else                       local_d = ((value / MAXTRIGLEVEL) * (1<<DACBITS)) + 0.5;
      my_ctrlregs.dac[0] = local_d; 
// TrigLevel1
      value = 0.00;
      if      (value <= 0.)      local_d = 0;
      else if (value >= MAXTRIGLEVEL) local_d = (1<<DACBITS)-1;
      else                       local_d = ((value / MAXTRIGLEVEL) * (1<<DACBITS)) + 0.5;
      my_ctrlregs.dac[1] = local_d; 
// TrigLevel2
      value = 0.00;
      if      (value <= 0.)      local_d = 0;
      else if (value >= MAXTRIGLEVEL) local_d = (1<<DACBITS)-1;
      else                       local_d = ((value / MAXTRIGLEVEL) * (1<<DACBITS)) + 0.5;
      my_ctrlregs.dac[2] = local_d; 
// TrigLevel3
      value = 0.00;
      if      (value <= 0.)      local_d = 0;
      else if (value >= MAXTRIGLEVEL) local_d = (1<<DACBITS)-1;
      else                       local_d = ((value / MAXTRIGLEVEL) * (1<<DACBITS)) + 0.5;
      my_ctrlregs.dac[3] = local_d; 
//  VCAL
      value = 0.00;
      if      (value <= 0.)      local_d = 0;
      else if (value >= MAXVCAL) local_d = (1<<DACBITS)-1;
      else                       local_d = ((value / MAXVCAL) * (1<<DACBITS)) + 0.5;
      my_ctrlregs.dac[4] = local_d; 
//
      my_ctrlregs.drs_ctl_first_chn   = 0;    // first readout channel DRS
      my_ctrlregs.drs_ctl_last_chn    = 7;    // last readout channel  DRS
//
//    a write to the following byte triggers an action (see VHDL)
      my_ctrlregs.drs_ctl_chn_config  = 0xff; // 8 channels, no cascading
//
//    a write to the following byte triggers an action (see VHDL)
      my_ctrlregs.dmode              = 1;        // 0: one shot, 1: continuous cycling
      my_ctrlregs.pllen              = 1;        // enable PLL
//      my_ctrlregs.wsrloop            = 1;        // enable wsrloop
      my_ctrlregs.wsrloop            = 0;        // enable wsrloop
      my_ctrlregs.drs_ctl_config_7_3 = 0x1f;     // five MSB reserved, must be all ones

//      my_ctrlregs.tca_ctrl           = 1;        // enable time calibration
//      my_ctrlregs.transp_mode        = 1;        // enable transparent mode
//
      if((r=set_control_registers(hUsb,&my_ctrlregs))) {
         fprintf(stderr,"function set_control_registers() failed\n"); 
         goto fin; 
         }
      }
   else if(!strncasecmp(somestr,"start",5)) {
/***
 *  command: start domino wave
***/
      if((r=get_control_registers(hUsb,&my_ctrlregs))) {
         fprintf(stderr,"function get_control_registers() failed\n"); 
         goto fin; 
         }
      my_ctrlregs.start_trig  = 1;                  // start the domino wave
      my_ctrlregs.reinit_trig = 0;                  // 
      my_ctrlregs.soft_trig   = 0;                  // 
      if((r=set_control_register_0(hUsb,*((unsigned int *)&my_ctrlregs)))) {
         fprintf(stderr,"function set_control_register_0) failed\n"); 
         goto fin; 
         }
      }
   else if(!strncasecmp(somestr,"soft",4)) {
/***
 *  command: soft trigger
***/
      if((r=get_control_registers(hUsb,&my_ctrlregs))) {
         fprintf(stderr,"function get_control_registers() failed\n"); 
         goto fin; 
         }
      my_ctrlregs.start_trig = 0;                  // do not start the domino wave
      my_ctrlregs.soft_trig  = 1;                  // soft trigger
      if((r=set_control_register_0(hUsb,*((unsigned int *)&my_ctrlregs)))) {
         fprintf(stderr,"function set_control_register_0) failed\n"); 
         goto fin; 
         }
      }
   else if(somestr[0]=='e' || somestr[0]=='E') goto fin;
/***
 * exit command   
***/
   else {
/***
 * unknown command   
***/
      fprintf(stderr,"Command %s not implemented\n",somestr);
      } 
   } // for(transfers=1;;transfers++) 
/*******
 * Action ends here
*******/
//fprintf(stdout,"Type return to get out of here...");
//fscanf(stdin,"%1s",stmp);
//fscanf(stdin,"%*[^\n]");

fin:
/*
***** Cleanup *****
*/
//if(hUsb) if((r=libusb_release_interface(hUsb,0))) error(r,errno,"Release Interface 0");
/*
** Freeing our configuration descriptor
*/
if(hUsb) libusb_free_config_descriptor(config_desc);
/*
** Closing current device
*/
if(hUsb) libusb_close(hUsb);
/*
** Unreferencing and freeing list of devices
*/
if(devs) libusb_free_device_list(devs, 1);
/*
** Deinitializing context
*/
if(ctx) libusb_exit(ctx); //close the session
#ifdef NOTNOW
#endif /* NOTNOW */
return 0;
}
