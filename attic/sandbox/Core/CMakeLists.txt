include_directories(${CATCH_INCLUDE_DIR})

file( GLOB TEST_SOURCES test_*.cxx )
foreach( test_src_file ${TEST_SOURCES} )
    # I used a simple string replace, to cut off .cpp.
    ###string( REPLACE ".cxx" "" testname ${test_src_file} )
    get_filename_component(test_name ${test_src_file} NAME_WE)
    message(STATUS "Found test ${test_name} --> ${test_src_file}")
    add_executable( ${test_name} ${test_src_file} )
    add_dependencies(${test_name} catch)
    
    # Make sure YourLib is linked to each app
#    target_link_libraries( ${testname} YourLib )
endforeach( test_src_file ${TEST_SOURCES} )

#add_executable(test_catch test_catch.cxx)
#add_dependencies(test_catch catch)
target_link_libraries(test_usb_async usb)
target_link_libraries(test_usb_read usb)  
