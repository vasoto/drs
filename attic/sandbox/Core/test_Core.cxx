/*
 * Core_test.cxx
 *
 *  Created on: Jul 23, 2016
 *      Author: Vassil Z. Verguilov
 */
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "Core.h"

//TODO: Add Core tests to CMake

namespace drs {
namespace core {
	TEST_CASE("Core class default constructor"){
		Core core(1);
	}
} /* namespace core */
} /* namespace drs */
