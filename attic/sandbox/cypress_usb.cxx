#include <libusb-1.0/libusb.h>
#include <IO/USB/policy.h>
#include <IO/USB.h>
#include <Utils/cout_tools.h>

#include <Core/drs_command.h>

#include <IO/IOInterface.h>

#include <IO/Module/DRSModule.h>
#include <DAQ/Modules/DPNC342.h>

//typedef struct libusb_device               cyusb_device;
//typedef struct libusb_device_handle        cyusb_handle;

/*
class USBDevice{
  libusb_device _device;
  libusb_device_handle _device_handle;
  unsigned _vendor_id;
  unsigned _product_id;
public:
  USBDevice() = delete;
  USBDevice(unsigned int vendor_id, unsigned product_id):_vendor_id(
  virtual ~USBDevice() = default;
};
*/
//const unsigned VENDOR_ID = 0x10C4;
//const unsigned PRODUCT_ID = 0x1175;
//const unsigned VENDOR_ID = 0x04b4;
//const unsigned PRODUCT_ID = 0x00f1;

int main(){

	drs::io::usb::USBSession::initialize();
	//auto devices = drs::utils::usb::enumerate_devices();

	//std::cout << "Number of devices: " << devices.size() << std::endl;
//
//	std::for_each(devices.begin(), devices.end(),
//				[](drs::utils::usb::USBDevice::Pointer& dev){
//					auto desc = *dev->impl.get_descriptor();
//					std::cout << "Descr:" << desc << std::endl;
//				});
	auto devs = drs::io::usb::find_devices(0x0b95,
										   0x1790);

	std::cout << "Found devices: " << devs.size() << std::endl;
	if(devs.size() > 0){
//		std::cout << *devs[0]->impl.get_descriptor() << std::endl;
		daq::modules::DPNC342Module module(*devs[0]);
		module.open();
		std::cout << "Found device " << std::hex << std::setfill('0') << std::setw(4)
				  << module.impl.get_descriptor()->idVendor << ":"
				  << std::hex << std::setfill('0') << std::setw(4)
				  << module.impl.get_descriptor()->idProduct
				  << std::dec
				  << " Handle: " << module.impl._handle
				  << std::endl;
//		module.info();
//		auto eps = module.impl.get_endpoints();
//		std::cout << "Endpoints (" << eps.size() << "): " << std::endl;
//		drs::io::usb::Endpoint::Pointer ep_in(nullptr), ep_out(nullptr);
//		for( auto& ep : eps ){
//			std::cout << *ep->_endpoint.get() << std::endl;
//
//			//auto EP = drs::io::usb::Endpoint::create(ep);
//			std::cout << "id: " << (int)ep->id()
//					  << " direction: " << ep->direction() << std::endl
//					  << "Transfer type: " << ep->transfer_type()
//					  << " Shared count: " << ep.use_count()
//					  << std::endl;
//		}
//		auto buf_size = module.get_buffer_size();
//		std::cout << "Firmware version: " << module.get_firmware_version() << std::endl;
//		std::cout << "Buffers sizes: " << std::endl << module.get_buffer_size() << std::endl;
		if (module.is_open()){
			std::cout << "Closing device..." << std::endl;
			module.close();
		}
	}

//	devices = drs::utils::usb::enumerate_devices();
//	std::for_each(devs.begin(), devs.end(),
//					[](drs::utils::usb::USBDevice::Pointer& dev){
//			auto desc = *dev->impl.get_descriptor();
//			std::cout << dev << " : "
//					  << TOHEX(4)
//					  << desc.idVendor << ":"
//					  << TOHEX(4)
//					  << desc.idProduct
//					  << TODEC()
//					  << std::endl; });
//



	static auto buffer = drs::core::detail::Buffer(new unsigned char [1024*1024]);


//	drs::core::add_command("start_trig",  0x00000000, 0, 1, buffer);
//	drs::core::add_command("reinit_trig", 0x00000000, 1, 1, buffer);
//	drs::core::add_command("soft_trig",   0x00000000, 2, 1, buffer);
//
//	drs::core::add_command("autostart",  0x00000004, 0, 1, buffer);
//	drs::core::add_command("adc_active", 0x00000004, 1, 1, buffer);
//	drs::core::add_command("reserved",   0x00000004, 2, 1, buffer);
//	drs::core::add_command("tca_ctrl",   0x00000004, 3, 1, buffer);
//
//
//	for(auto cmd_pair : drs::core::commands){
//		std::cout << cmd_pair.first << "::" << cmd_pair.second.get() << std::endl;
//	}
//
//	auto iface = drs::core::IOInterface();
//	auto trig = drs::core::commands["start_trig"]->read(iface);


	return(0);
}
