/*
 * dpnc342_1.cxx
 *
 *  Created on: Sep 22, 2016
 *      Author: vasoto
 */

//#include <libusb-1.0/libusb.h>
#include <IO/USB/policy.h>
#include <IO/USB.h>
//#include <Utils/cout_tools.h>

//#include <Core/drs_command.h>

//#include <IO/IOInterface.h>

//#include <IO/Module/DRSModule.h>
//#include <DAQ/Modules/DPNC342.h>

struct DPNC342ModuleSignature{
	// This class contains all device specific methods and variables;
	static const unsigned VENDOR_ID = 0x0b95;//0x04b4;
	static const unsigned PRODUCT_ID = 0x1790;//0x00f1;

	void enable_trigger(){
		std::cout << "Trigger enabled" << std::endl;
	}
	void set_trigger_source(uint16_t channel){
		std::cout << "Trigger source is channel " << channel << " now." << std::endl;
	}

	void set_trigger_level(double level, bool edge){
		std::cout << "Trigger level is set to " <<  level << ", "
				<< ((edge)? "with" : "no") << " edge."  << std::endl;
	}

	void set_trigger_delay(unsigned delay){
		std::cout << "Trigger delay is " << delay << " [ns]" << std::endl;
	}
};

// Generic policies would allow the use of alternative IO modules (VME or USB) for example;
typedef Module<DPNC342ModuleSignature,
			   drs::io::usb::USBDevice,
			   IOPolicyMock> DPNC342Module;

int main(){
	drs::io::usb::USBSession::initialize(); // Open and enumerate USB devices
	DPNC342Module module;
	module.load_device(0);
	module._device->info();
	module.enable_trigger();
	module.set_trigger_source(7);
	module.set_trigger_level(0.5, true);
	module.set_trigger_delay(0);
	return (0);
}
