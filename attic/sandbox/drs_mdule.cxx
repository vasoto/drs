/*
 * drs_mdule.cxx
 *
 *  Created on: Aug 25, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <iostream>
#include <memory>
#include <vector>

#include <Utils/cout_tools.h>
#include <Utils/type_tools.h>


enum struct Registers : long {
	Ctrl = 0,
	DacOfs = 1 << 2,
	Dac0 = (1 << 2) | 0,
	Dac1 = (1 << 2) | (1 << 1),
	Dac2 = (1 << 3),
	Dac3 = (1 << 2) | (1 << 3),
};

enum struct DacRegisters : char {
	Dac0 = 0,
	Dac1 = (1 << 1),
	Dac2 = (1 << 2),
	Dac3 = (1 << 3),
};




enum struct DRSBoardConstants : int {
   kNumberOfChannelsMax         =   10,
   kNumberOfCalibChannelsV3     =   10,
   kNumberOfCalibChannelsV4     =    8,
   kNumberOfBins                = 1024,
   kNumberOfChipsMax            =    4,
   kFrequencyCacheSize          =   10,
   kBSplineOrder                =    4,
   kPreCaliculatedBSplines      = 1000,
   kPreCaliculatedBSplineGroups =    5,
   kNumberOfADCBins             = 4096,
   kBSplineXMinOffset           =   20,
   kMaxNumberOfClockCycles      =  100,
};

enum struct DRSErrorCodes : int {
   kSuccess                     =  0,
   kInvalidTriggerSignal        = -1,
   kWrongChannelOrChip          = -2,
   kInvalidTransport            = -3,
   kZeroSuppression             = -4,
   kWaveNotAvailable            = -5
};

/*---- Status register bit definitions -----------------------------*/
enum struct StatusRegisterBits{
	RUNNING           = (1<<0),    // one if domino wave running or readout in progress
	NEW_FREQ1         = (1<<1),    // one if new frequency measurement available
	NEW_FREQ2         = (1<<2),
	PLL_LOCKED0       = (1<<1),    // 1 if PLL has locked (DRS4 evaluation board only)
	PLL_LOCKED1       = (1<<2),    // 1 if PLL DRS4 B has locked (DRS4 mezzanine board only)
	PLL_LOCKED2       = (1<<3),    // 1 if PLL DRS4 C has locked (DRS4 mezzanine board only)
	PLL_LOCKED3       = (1<<4),    // 1 if PLL DRS4 D has locked (DRS4 mezzanine board only)
	SERIAL_BUSY       = (1<<5),    // 1 if EEPROM operation in progress
	LMK_LOCKED        = (1<<6),    // 1 if PLL of LMK chip has locked (DRS4 mezzanine board only)
	MODE_2048         = (1<<7)    // 1 if 2048-bin mode has been soldered
};

struct int12{
	unsigned short data : 12;
	int12(int v):data(v){};
};
//struct int12_alloc : public std::allocator<int12>{
//	typedef int12 value_type;
//};

typedef std::vector<int12> data_type;

struct CalibrationData{
	struct Channel{
		std::vector<unsigned char> _limit_group;
		std::vector<float> _min;
		std::vector<float> _range;
		std::vector<int12> _offset;
		std::vector<int12> _gain;
		std::vector<unsigned short> _offset_adc;
		std::vector<int12> _data;

		Channel(const Channel &c) = delete;
		Channel &operator=(const Channel &rhs) = delete;
		Channel(int num_of_grid_points):
			_limit_group(enum_to_int(DRSBoardConstants::kNumberOfBins), 0),
			_min(enum_to_int(DRSBoardConstants::kNumberOfBins), 0.0),
			_range(enum_to_int(DRSBoardConstants::kNumberOfBins), 0.0),
			_offset(enum_to_int(DRSBoardConstants::kNumberOfBins), 0),
			_gain(enum_to_int(DRSBoardConstants::kNumberOfBins), 0),
			_offset_adc(enum_to_int(DRSBoardConstants::kNumberOfBins), 0),
			_data(enum_to_int(DRSBoardConstants::kNumberOfBins), 0)
		{}

	};
};
int main(){

	std::cout << DUMP_ENUM(Registers::Ctrl)
			  << DUMP_ENUM(Registers::DacOfs)
			  << DUMP_ENUM(DacRegisters::Dac0)
			  << DUMP_ENUM(DacRegisters::Dac1)
			  << DUMP_ENUM(DacRegisters::Dac2)
			  << DUMP_ENUM(DacRegisters::Dac3)
			  << TOHEX() << (enum_to_int(Registers::DacOfs) | enum_to_int(DacRegisters::Dac0))<< std::endl
			  << TOHEX() << (enum_to_int(Registers::DacOfs) | enum_to_int(DacRegisters::Dac1)) << std::endl
			  << TOHEX() << (enum_to_int(Registers::DacOfs) | enum_to_int(DacRegisters::Dac2)) << std::endl
			  << TODEC() << std::endl;

//	CalibrationData data;
	CalibrationData::Channel ch(10);
//	std::cout << "ch._data[3]: " << ch._data[3] << std::endl;

	return (0);
};



