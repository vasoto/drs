/*
 * io_operations.cxx
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */
#include <iostream>
#include <memory>
#include <vector>
#include <cassert>
#include <iomanip>
#include <iostream>


#define TOHEX(length) std::hex << std::showbase << std::internal << std::setfill('0') << std::setw(length+2)
#define DEBUG(msg) std::cout << "[debug] " << msg << "\t" << "[" << __FUNCTION__ << "] " << __FILE__ << ":" << __LINE__ << std::endl;
#define TODEC() std::dec << std::setw(0)

struct IOBase{
	// IO Interface class
	typedef IOBase * Pointer;
	IOBase(){std::cout << "IOBase: " << this << std::endl; }
	virtual ~IOBase(){};

	virtual void read() = 0;
	virtual void write() = 0;
	virtual void read_block(size_t address, unsigned * buffer, size_t length) = 0;
	virtual void write_block(size_t address, unsigned * buffer, size_t length) = 0;
};

struct IOEnabled{
	IOEnabled(IOBase::Pointer t_io):io(t_io){};
	virtual ~IOEnabled() = default;

	IOEnabled() = delete;
	IOEnabled(const IOEnabled& other) = delete;
	IOEnabled operator=(const IOEnabled& other) = delete;

	IOBase::Pointer io;
};


struct ModuleSignature : public IOEnabled{
	// Holds definition of variables and methods specific for a given module
	typedef ModuleSignature* Pointer;

	using IOEnabled::IOEnabled;

	virtual ~ModuleSignature() = default;


	virtual void load() = 0;
};


struct ModuleConstituent{
	// Part of the module
	ModuleConstituent(ModuleSignature::Pointer t_owner):module(t_owner){};
	ModuleSignature::Pointer module;
};

//struct MemoryMap:public ModuleConstituent{
//	typedef MemoryMap * Pointer;
//	MemoryMap() = delete;
//	MemoryMap(size_t t_base_address, size_t t_size, ModuleSignature::Pointer t_module):
//		ModuleConstituent(t_module), size(t_size), base_address(t_base_address){};
////	using ModuleConstituent::ModuleConstituent;
//
//	size_t base_address;
//	size_t size;
//};
struct MemoryFragment : public ModuleConstituent{
	MemoryFragment(size_t t_address, size_t t_size, ModuleSignature::Pointer t_module):address(t_address),
			size(t_size),
			ModuleConstituent(t_module){
		std::cout << "Base Address: " << TOHEX(8) << address
				  << " Size: " << TOHEX(8) << size
				  << " Highest address: " << TOHEX(8) << address + size - 1 << TODEC() << std::endl;
	}

	MemoryFragment(size_t t_address, size_t t_size, MemoryFragment& parent):
		address(parent.address + t_address),
				size(t_size),
				ModuleConstituent(parent.module){
					std::cout << "Base Address: " << TOHEX(8) << address
						  << " Size: " << TOHEX(8) << size
						  << " Highest address: " << TOHEX(8) << address + size - 1 << TODEC() << std::endl;
					assert(address + size <= parent.size );
				}

	void read(unsigned * buffer, size_t length = 0, size_t offset = 0){
		if (length == 0)
			length = size;
		std::cout << "Data start: " << TOHEX(8) << address+offset
				  << " Data end: " << TOHEX(8) << address + offset + length
				  << " Limit: " << TOHEX(8) << address + size << TODEC() << std::endl;
		assert(address + offset + length <= address + size); // Check boundaries
		module->io->read_block(address + offset, buffer, length);
	}

	void write(unsigned * buffer, size_t length = 0, size_t offset = 0){
		if (length == 0)
				length = size;
		assert(address + offset + length <= address + size); // Check boundaries
		module->io->write_block(address + offset, buffer, length);
	}

	const size_t address;
	const size_t size;
//	std::vector<unsigned> data;
//	MemoryMap::Pointer owner;
};

#define DEF_MEM(name, base_addr, size) MemoryFragment<base_addr, size> name {this};

struct Register: public ModuleConstituent{
	typedef Register * Pointer;
	Register(size_t t_base_address,
			 size_t t_size,
			ModuleSignature::Pointer owner):
		data(t_size, 0),
		base_address(t_base_address),
		ModuleConstituent(owner){}
	//using ModuleConstituent::ModuleConstituent; // Propagate ModuleConstituent constructors

	void load(){ module->io->read_block(base_address, data.data(), data.size());} // Example use of the IO read
	void commit(){ module->io->write_block(base_address, data.data(), data.size()); } // Example use the IO write

	std::vector<unsigned> data;
	size_t base_address;
};

template
<size_t Index, size_t Length, size_t Offset>
struct RegisterAttribute{
	RegisterAttribute(Register::Pointer t_owner):owner(t_owner){};

	int operator=(int i){ return (i); } // Attribute set, should read from owner->data[Index][Offset+Size]
	operator int(){ return (0); } // Attribute get, should write to owner->data[Index][Offset+Size]

	Register * owner;
};

#define DEF_REG(name, index, length, offset) RegisterAttribute<index, length, offset> name {this};

struct Register1 : public Register{
	Register1(ModuleSignature::Pointer owner):Register(0x00000000, 14, owner){}
	//using Register::Register; // Propagate Register constructors
	DEF_REG(attr1, 0, 1, 0)
	DEF_REG(attr2, 0, 1, 1)
	DEF_REG(attr3, 1, 32, 0)
};

struct Register2 : public Register{
	Register2(ModuleSignature::Pointer owner):Register(0x00010000, 16, owner){}
	//using Register::Register; // Propagate Register constructors

	DEF_REG(attr1, 0, 1, 0)
	DEF_REG(attr2, 0, 1, 1)
	DEF_REG(attr3, 1, 32, 0)
};

struct MyMemoryMap: public MemoryFragment{
	MyMemoryMap(ModuleSignature::Pointer t_module):MemoryFragment(0x00000000, 36 * 16 * 1024, t_module){};
	MemoryFragment EEPROM {0x00020000, 64 * 1024, *this};

	MemoryFragment Waveform_DRS1 {0x00040000, 16 * 1024, *this};
	MemoryFragment Waveform_DRS2 {0x00044000, 16 * 1024, *this};
	MemoryFragment Waveform_DRS3 {0x00048000, 16 * 1024, *this};
	MemoryFragment Waveform_DRS4 {0x0004C000, 16 * 1024, *this};

	MemoryFragment Pedestal_DRS1 {0x00080000, 16 * 1024, *this};
	MemoryFragment Pedestal_DRS2 {0x00084000, 16 * 1024, *this};
	MemoryFragment Pedestal_DRS3 {0x00088000, 16 * 1024, *this};
	MemoryFragment Pedestal_DRS4 {0x0008C000, 16 * 1024, *this};
};

struct Device : public IOBase{
	Device(){ std::cout << "Device: " << this << std::endl; }
	virtual void read(){ std::cout << "Read!" << std::endl; }
	virtual void write(){ std::cout << "Write!" << std::endl; }
	virtual void read_block(size_t address, unsigned * buffer, size_t length){ std::cout << "Read Block!" << std::endl; }
	virtual void write_block(size_t address, unsigned * buffer, size_t length){ std::cout << "Write Block!" << std::endl; }
};

struct USBDevice: public Device{
	USBDevice(){}
};

struct MyModuleSignature : public ModuleSignature{
	MyModuleSignature(IOBase::Pointer t_io):reg1(this),
										    reg2(this),
											memory(this),
											ModuleSignature(t_io){}
	MyModuleSignature() = delete;
	virtual ~MyModuleSignature(){};


	virtual void load(){
		std::cout << "Loading..." << std::endl;
		reg1.load();
		reg2.load();
	}

	Register1 reg1;
	Register2 reg2;
	MyMemoryMap memory;
};

template <class DevicePolicy, class ModuleSignature>
struct Module : public DevicePolicy, public ModuleSignature{
	Module():ModuleSignature(this){};
};

struct MyModule: public Module<USBDevice, MyModuleSignature>{
	//MyModule():Module(){ std::cout << "MyModule: " << this << std::endl; }
};

int main(){
	MyModule module;
	module.load();
	std::cout << "Size: " << sizeof(module) << std::endl
			<< "Size reg1: " << sizeof(module.reg1) << " data: " << module.reg1.data.size() * sizeof(unsigned) << std::endl
			<< "Size reg2: " << sizeof(module.reg2) << " data: " << module.reg2.data.size() * sizeof(unsigned)
			<< std::endl;
	USBDevice * dev = new USBDevice();
	MyModuleSignature mod(dev);
	std::vector<unsigned> buffer(16384, 0);
	mod.load();
	mod.memory.EEPROM.read(buffer.data());
	delete dev;
	return 0;
};




