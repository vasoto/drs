#include <iostream>
#include <string>
#include <map>
#include <functional>

namespace drs {
namespace meta{
namespace detail{

using CallMember = std::function<void (const void *, const void *, const void **)>;
using GetMember = std::function<void (const void *, const void *)>;
using SetMember = std::function<void (const void *, const void *)>;

using Name = std::function<const char*()>;
using Create = std::function<void(void**)>;
using Construct = std::function<void(void**)>;
using Destroy = std::function<void(void**)>;
using Destruct = std::function<void(void**)>;
using Clone = std::function<void(void* const*, void**)>;
using Move = std::function<void(void* const*, void**)>;

struct type_table{
	Name name;
	Create create;
	Construct construct;
	Destroy destroy;
	Destruct destruct;
	Clone clone;
	Move move;
	unsigned size;
	bool is_small;
};

struct enumerator_table{
	const char * name;
	const unsigned int value;
};

};
};
};

int main(){
	return 0;
}
