/*
 * registers.cxx
 *
 *  Created on: Sep 24, 2016
 *      Author: vasoto
 */

#include <iostream>
#include <iomanip>
#include <bitset>

#include "new/core/bits.h"
#include "new/daq/core/register.h"
#include "new/daq/module/dpnc342/registers.h"


typedef struct __attribute__((__packed__))
s_controlregs {

// reg  0, byte offset 0x00
unsigned char  start_trig         :  1;  //  0
unsigned char  reinit_trig        :  1;  //  1
unsigned char  soft_trig          :  1;  //  2
unsigned int                      : 29; //  31 - 2

// reg  1, byte offset 0x04
unsigned char  autostart          :  1;  //  0
unsigned char  adc_active         :  1;  //  1
unsigned char                     :  1;  //  2
unsigned char  tca_ctrl           :  1;  //  3
unsigned char                     :  2;  //  5 -  4
unsigned char  enable_trigger     :  1;  //  6
unsigned char  readout_mode       :  1;  //  7
unsigned char  neg_trigger        :  1;  //  8
unsigned char  acalib             :  1;  //  9
unsigned char  refclk_source      :  1;  // 10
unsigned char  dactive            :  1;  // 11
unsigned char  standby            :  1;  // 12
unsigned int                      : 19;  // 31 - 12

// reg  2, byte offset 0x04
unsigned int  dac[5];

// reg  7, byte offset 0x1C
unsigned int                      : 32;

// reg  8, byte offset 0x20
unsigned int                      : 32;

// reg  9, byte offset 0x24
unsigned int                      : 32;

// reg 10, byte offset 0x2C
unsigned char drs_ctl_last_chn    :  4;  //  3 -  0
unsigned char drs_ctl_first_chn   :  4;  //  7 -  4
unsigned char adcclk_phase        :  8;  // 15 -  8
unsigned char drs_ctl_chn_config  :  8;  // 23 - 16
unsigned char dmode               :  1;  // 24
unsigned char pllen               :  1;  // 25
unsigned char wsrloop             :  1;  // 26
unsigned char drs_ctl_config_7_3  :  5;  // 31 - 27

// reg 11, byte offset 0x30
unsigned short sampling_freq;            // 15 -  0
unsigned char  trigger_delay;            // 23 - 16
unsigned char                     :  8;  // 31 - 24

// reg 12, byte offset 0x1c
unsigned short                    : 16;  // 15 -  0
unsigned short trigger_config;           // 31 - 16

// reg 13, byte offset 0x1c
unsigned short eeprom_page;              // 15 -  0
unsigned short                    : 14;  // 29 - 16
unsigned char eeprom_write_trig   :  1;  // 30
unsigned char eeprom_read_trig    :  1;  // 31
} t_controlregs, *t_pcontrolregs;


using drs4::core::read_region;
using drs4::core::write_region;

using drs4::daq::core::detail::RegisterData;
using drs4::daq::core::Register;

using drs4::daq::module::dpnc342::ControlRegister;


//unsigned ControlRegister::data[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};


inline std::ostream& operator<<(std::ostream& os, const ControlRegister& reg){
	os << "####       |       |       |       |" << std::endl;
	for(int i=0; i<14; ++i)
		os << std::setw(2)
		   << std::setfill(' ')
		   << i << ": " << std::bitset<32>(reg.data[i]) << std::endl;
	return (os);
};


int main(){
	std::cout << "Size of t_controlregs: " << sizeof(t_controlregs) << std::endl
			<< "Size of ControlRegister: " << sizeof(ControlRegister) << std::endl;
	// Try casting
	t_controlregs cr1;
	cr1.start_trig = 1;
	cr1.reinit_trig = 1;
	cr1.soft_trig = 1;
	cr1.autostart = 1;
	cr1.adc_active = 1;
	cr1.tca_ctrl = 1;
	cr1.dac[0] = 0;
	cr1.dac[1] = 0;
	cr1.dac[2] = 0;
	cr1.dac[3] = 0;
	cr1.dac[4] = 0;
	cr1.drs_ctl_last_chn = 0;//0xF;
	cr1.drs_ctl_first_chn = 0;//0xF;
	cr1.adcclk_phase = 0; //0xFF;
	cr1.drs_ctl_chn_config = 0;//0xFF;
	cr1.dmode = 1;
	cr1.pllen = 1;
	cr1.wsrloop = 1;
	cr1.drs_ctl_config_7_3 = 1;
	cr1.sampling_freq = 0xaaaa;
	cr1.trigger_delay = 0xaa;
	cr1.trigger_config = 0xffff;
	cr1.eeprom_page = 0xffff;
	cr1.eeprom_write_trig = 1;
	cr1.eeprom_read_trig = 1;
	//ControlRegister cr2;
	//unsigned (*r)[14] = &cr2.data;
	//cr2.set_data( (static_cast<RegisterData<14>*>(static_cast<void* >(&cr1)))->data );//(static_cast<unsigned(*)[14] >(static_cast<void* >(&cr1)));
//	std::cout << "cr2: " << std::endl
//			<< cr2 << std::endl;
////	cr2.eeprom_read_trig._parent = &cr2;
////	std::cout << cr2.eeprom_read_trig._parent << " : " << &cr2 << std::endl;
////	std::cout << "eeprom_write_trig: " << std::bitset<1>(cr2.read_setting(13, 1, 30 )) << std::endl;
////	std::cout << "eeprom_read_trig:  " << std::bitset<1>(cr2.read_setting(13, 1, 31 )) << std::endl;
//	cr2.eeprom_read_trig = 0;
//	std::cout << "eeprom_read_trig:  " << cr2.eeprom_read_trig << std::endl;
//	std::cout << "eeprom_write_trig:  " << cr2.eeprom_write_trig << std::endl;
//	//cr2.eeprom_read_trig = 0;
//	cr2.sampling_freq = 0xffff;
////	cr2.trigger_delay = 0xffff-1;
//	std::cout << "trigger_delay:     " << cr2.trigger_delay<< std::endl;
//
//	std::cout << "sampling_freq:     " << cr2.sampling_freq << std::endl;
////	cr2.sampling_freq = 0x5555;
//	std::cout << "sampling_freq:     " << cr2.sampling_freq << std::endl;
//	cr2.trigger_config = 0x0;
//	std::cout << "trigger_config:    " << cr2.trigger_config << std::endl;
//
////	cr2.sampling_freq = 0xffff;
//
//	//	int a = 17;
////	std::cout << "a = " << std::bitset<8>(a) << std::endl;
////	a = write_region(a, 3, 2, 5 );
////	std::cout << "a = " << std::bitset<8>(a) << std::endl;
////	a = write_region(a, 0, 1, 4 );
////	std::cout << "a = " << std::bitset<8>(a) << std::endl;
//	std::cout << cr2 << std::endl;
////	std::cout << std::bitset<32>(0xFFFF) << std::endl
////			  << std::bitset<32>(0xFFFF & (1 << 8) - 1) << std::endl
////			  << std::bitset<32>(0xFFFF << 7) << std::endl << std::bitset<32>((0xFFFF << 7) >> 7) << std::endl;
	return (0);
};
