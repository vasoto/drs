/*
 * std.h
 *
 *  Created on: Aug 29, 2016
 *      Author: Vassil Z. Verguilov
 */

#pragma once

// Include all needed modules

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <thread>
#include <memory>
/*
#include <sys/time.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <stdlib.h>        // random()  RAND_MAX
#include <stdio.h>
#include <stdarg.h>
#include <signal.h>
*/
