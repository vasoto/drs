/*
Copyright (c) 2014 Aerys

Permission is hereby granted, free of charge, to variant person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF variant KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT innerS BE LIABLE FOR variant CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//
//  Created by Warren Seine on Jul 14, 2012.
//  Copyright (c) 2012 Aerys. All rights reserved.
//
//
//  Copyright Kevlin Henney, 2000, 2001, 2002. All rights reserved.
//
//  Distributed under the Boost Software License, Version 1.0. (See
//  accompvarianting file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt)

//#pragma once

#include <algorithm>
#include <iostream>
#include <memory>
#include <type_traits>
#include <typeinfo>


#define DBG(comment) std::cout << "Line: " << __LINE__ << " Function: " << __FUNCTION__ << ": " << comment << std::endl


namespace drs
{
  namespace core{
  //template<class T> //using RemoveReference = typename std::decay<T>::type;
  //template<typename T>
  //RemoveReference = std::remove_reference<T>::type;
//  template <typename T>
//    struct RemoveReference
//    {
//        typedef T Type;
//    };
//
//    template <typename T>
//    struct RemoveReference<T&>
//    {
//        typedef T Type;
//    };

    class variant final{
    public:
        variant() : _content(nullptr){ DBG("default constructor"); }

        template <typename T>
        variant(const T & value) : _content(new inner<T>(value)){ DBG("type erasure constructor"); } //TODO(vasoto): Copy or Move?

        // Copy constructor
        variant(const variant& other) :
            _content(other._content ? other._content->clone() : nullptr) { DBG("copy constructor"); }

        // Move constructor
        variant(variant&& other) :
            _content(other._content ? std::move(other._content) : nullptr) { DBG("move constructor"); }

        // Destructor
        ~variant() = default;

        variant&
        swap(variant& rhs) {
        	DBG("");
            std::swap(_content, rhs._content);
            return *this;
        }

        template <typename T>
        variant&
        operator=(const T& rhs) {
        	DBG("const assign operator");
        	variant(rhs).swap(*this);
            return *this;
        }

        variant& operator=(variant& rhs){
        	DBG("non-const assign operator");
        	///rhs.swap(*this); // ORIGINAL CODE
        	auto tmp(rhs);
        	this->swap(tmp);
            return *this;
        }

        // Move
        variant& operator=(variant&& rhs){
        	DBG("move operator");
        	_content = std::move(rhs._content);
            return *this;
        }

        bool empty() const noexcept { DBG("");
        	return !_content; }

        const std::type_info&
        type() const noexcept {
        	DBG("");
            return (_content)? _content->type() : typeid(void);
        }

        template <typename T>
        static T*
        cast(variant* any) noexcept {
        	DBG("variant* --> T*");
        	return (any && (any->type() == typeid(T))) ?
                &static_cast<variant::inner<T> *>(any->_content.get())->_held :
				nullptr;
        }

        template <typename T>
        static T*
        unsafe_cast(variant* any) noexcept {
        	DBG("variant* --> T*");
        	T * result = nullptr;
            return (any) ?
            	&static_cast<variant::inner<T> *>(any->_content.get())->_held :
            	nullptr;
        }

        template <typename T>
        static inline const T*
        cast(const variant* any){
        	DBG("const variant* --> const T*");
        	return variant::cast<T>(const_cast<variant*>(any));
        }

        template <typename T>
        static inline const T*
        unsafe_cast(const variant* any){
        	DBG("const variant* --> const T*");
        	return variant::unsafe_cast<T>(const_cast<variant*>(any));
        }

        template <typename T>
        static T
        cast(variant& any){
        	DBG("variant& --> T");
        	typedef typename  std::remove_reference<T>::type non_ref;
            non_ref* result = cast<non_ref>(&any);
            if (!result)
                throw std::bad_cast();
            return *result;
        }

        template <typename T>
        static T
        unsafe_cast(variant& any){
        	DBG("variant& --> T");
        	typedef typename  std::remove_reference<T>::type non_ref;
        	auto a = &any;
            non_ref* result = unsafe_cast<non_ref>(a);
            if (!result)
                throw std::bad_cast();
            return *result;
        }

        template <typename T>
        static inline T
        cast(const variant& any){
        	DBG("const variant& --> T");
        	typedef typename  std::remove_reference<T>::type non_ref;
            return cast<const non_ref&>(const_cast<variant&>(any));
        }

        template <typename T>
        static inline T
        unsafe_cast(const variant& any){
        	DBG("const variant& --> T");
        	typedef typename std::remove_reference<T>::type non_ref;
            return unsafe_cast<const non_ref&>(const_cast<variant&>(any));
        }

        class inner_base
        {
        public:
        	inner_base() = default;
            virtual ~inner_base() = default;

        public:
            virtual const std::type_info&
            type() const = 0;

            virtual inner_base*
            clone() const = 0;
        };

        template <typename T>
        class inner : public inner_base{
        public:
            inner(const T & value) : _held(value) { DBG(""); }
            virtual const std::type_info&
            type() const {
            	DBG("");
                return typeid(T);
            }

            virtual inner_base* clone() const {
            	DBG("");
            	return new inner(_held);
            }
            inner& operator=(const inner &) = delete;
        public:
            T _held;

        };

        std::unique_ptr<inner_base> _content;
    };
  }
}



int main(){
  std::cout << "Test" << std::endl;
  int i = 5;

  drs::core::variant nil;
  drs::core::variant a(i);
  drs::core::variant b(a);
  drs::core::variant c(std::move(b)); // b should be empty afterwards
  drs::core::variant d;
  drs::core::variant e;

  const float f = 42.5;
  d = a; // a is valid afterwards
  e = std::move(c); // c is empty afterwards
  e = f;//"test string"; // should be able to take new value of random type

  std::cout << "Is empty? nil = " << nil.empty() << " b = " << b.empty() << " a = " << a.empty() << " c = " << c.empty() << std::endl;
  std::cout << "Unsafe cast a: " << drs::core::variant::unsafe_cast<long long>(a) << std::endl;
  std::cout << "Safe cast a: "   << drs::core::variant::cast<int>(a) << std::endl;
  std::cout << "Safe cast e: "   << drs::core::variant::cast<float>(e) << std::endl;
  std::cout << "Unsafe cast e: " << drs::core::variant::unsafe_cast<double>(e) << std::endl; // doesn't work
  std::cout << "Safe cast e: "   << drs::core::variant::cast<float>(e) << std::endl;
  auto j = drs::core::variant::unsafe_cast<unsigned long long>(d);
  double db = static_cast<std::remove_reference<double>::type>(f);
  double * t = &db;
  std::cout << "Test: " << *t << " " << j << std::endl;
  return 0;
}
