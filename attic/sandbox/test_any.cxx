/*
 * test_any.cxx
 *
 *  Created on: Aug 20, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <memory>
#include <typeinfo>
#include <string>
#include <iostream>

namespace drs{
namespace core{

template<typename T>
struct remove_reference{
	typedef T type;
};


template<typename T>
struct remove_reference<T&>{
	typedef T type;
};

class any{
public:
	any():_content(nullptr){};

	template <typename T>
		any(T value): _content(new content<T>(std::move(value))){}
//	template <typename T>
//		any(const T& value): _content(new content<T>(std::move(value))){}

	// Copy c-tor
	any(const any& other): _content(other._content ? other._content->clone() : nullptr){};

	// Move c-tor
	any(any&& other): _content(other._content ? std::move(other._content) : nullptr){};

	// Empty destructor - smart pointer is taking care of deletion
	~any() = default;

	// Swap
	any& swap(any& other){std::swap(_content, other._content); return *this;}

	// Safe assignment of custom data. If tmp fails to be created, this is not changed;
	template<typename T>
	any& operator=(const T& value){any tmp(value); *this = std::move(tmp); return *this;}

	// Copy is actually move
	any& operator=(any& other){*this = std::move(other); return *this;}
	// Move
	any& operator=(any&& other){*this = std::move(other); return *this;}

	bool is_empty(){return _content==nullptr;}

	const std::type_info& type() const {return _content? _content->type():typeid(void);}

	template<typename T>
	static T* cast(any* operand){
		return operand && operand->type() == typeid(T) ?
				&static_cast<any::content<T> *>(operand->_content)->_wrapped_data:
				nullptr;
	}

	template<typename T>
	static T* unsafe_cast(any* operand){
		return operand ? &static_cast<any::content<T> *>(operand->_content)->_wrapped_data:
				nullptr;
	}

	template<typename T>
	static inline const T* cast(any* operand){
		return any::cast<T>(const_cast<any*>(operand));
	}

	template<typename T>
	static inline const T* unsafe_cast(any* operand){
		return any::unsafe_cast<T>(const_cast<any*>(operand));
	}

	template<typename T>
	static T cast(any* operand){
		typedef typename remove_reference<T>::type non_ref;
		non_ref* result = cast<non_ref>(&operand);
		if (!result)
			throw std::bad_cast();
		return *result;
	}

	template<typename T>
	static T unsafe_cast(any* operand){
		typedef typename remove_reference<T>::type non_ref;
		non_ref* result = unsafe_cast<non_ref>(&operand);
		if (!result)
			throw std::bad_cast();
		return *result;
	}

	template<typename T>
	static inline T cast(any& operand){
		typedef typename remove_reference<T>::type non_ref;
		return cast<const non_ref&>(const_cast<any&>(operand));
	}

	template<typename T>
	static inline T unsafe_cast(any& operand){
		typedef typename remove_reference<T>::type non_ref;
		return cast<const non_ref&>(const_cast<any&>(operand));
	}


private:
	struct abstract_content{
		virtual ~abstract_content(){};
		virtual const std::type_info& type() const = 0; // TODO(vasoto): is needed?
		virtual abstract_content* clone() const = 0; // TODO(vasoto) make shared_ptr?
	};// struct abstract_content

	template <typename ValueType>
	struct content: public abstract_content{
//		content(const ValueType & value): _wrapped_data(value){}
		content(ValueType value) : _wrapped_data(std::move(value)){}
		virtual const std::type_info& type() const{
			return typeid(ValueType);
		}
		virtual abstract_content* clone() const{ return new content(_wrapped_data);}
		// Remove assign operator created by default
		content& operator=(const content& ) = delete;
		ValueType _wrapped_data;
	}; // struct content
	std::unique_ptr<const abstract_content> _content;
}; // class any


//std::ostream operator<<(std::ostream& out, const drs::core::any& value){
//	using drs::core::any;
//	out << any::unsafe_cast<std::string>(any);
//	return out;
//}

};

};

using drs::core::any;

int main(){
	std::cout << "Test:" << std::endl;
	any five(5);
	int * f(any::cast<int*>(five));
	std::cout << "Five = " << f << std::endl;

//	std::cout<< "Five = " << any::unsafe_cast<std::string>(five) << std::endl;
//	typedef decltype five.type() five_type;
//	std::cout<< "Five = " << any::cast<five_type >(five) << std::endl;
	std::cout << "End of Test" << std::endl;

	return 0;
};

