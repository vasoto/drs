/*
 * test_any2.cxx
 *
 *  Created on: Aug 22, 2016
 *      Author: Vassil Z. Verguilov
 */


#include <type_traits>
#include <utility>
#include <typeinfo>
#include <string>
#include <cassert>
#include <iostream>
#include <memory>

using namespace std;

template<class T>
using DerefType = typename decay<T>::type;

struct Any{
    bool is_null() const { return !ptr; }
    bool not_null() const { return ptr != nullptr; }

    template<typename U> Any(U&& value)
        : ptr(new Derived<DerefType<U>>(std::move(value))) {}

    template<class U> bool is() const{
        using T = DerefType<U>;
        // template SFINAE?
//        return std::is_convertible<Derived<T>, decltype ptr>::value;
        //auto derived = dynamic_cast<Derived<T>>(ptr); // huh?
        //return derived;
    }

//    template<class U>
//    DerefType<U>& as(){
//    	using T = DerefType<U>;
//        auto derived = dynamic_cast<Derived<T>*> (ptr);
//        if (!derived)
//            throw bad_cast();
//        return derived->value;
//    }

//    template<class U>
//    operator U(){
//        return as<DerefType<U>>();
//    }

    Any(): ptr(nullptr){}

    Any(Any& that): ptr(std::move(that.ptr->clone())){}

    Any(Any&& that): ptr(std::move(that.ptr)){
//        that.ptr = nullptr;
    }

    Any(const Any& that): ptr(that.ptr->clone()){}

    Any(const Any&& that): ptr(that.ptr->clone()){}

    Any& operator=(const Any& a){
        if (ptr == a.ptr)
            return *this;
        Any tmp(a);
        *this = std::move(tmp);
        //any tmp(a);
//        auto old_ptr = ptr;
        //ptr = a.ptr->clone();

        //if (old_ptr)
//        delete old_ptr;
        return *this;
    }

    Any& operator=(Any&& a){
        if (ptr == a.ptr)
            return *this;

        swap(ptr, a.ptr);

        return *this;
    }

    ~Any(){}

private: // if private why is struct and not class?
    struct Base{
        virtual ~Base() {}
        virtual Base* clone() const = 0;
    };

    template<typename T>
    struct Derived : Base
    {
        template<typename U> Derived(U&& value) : value(forward<U>(value)) { }
        T value;
        Base* clone() const { return new Derived<T>(value); }
    };

//    std::unique_ptr<Base> clone() const{
//        return ptr ? ptr->clone() : nullptr;
//    }

    //Base* ptr;
    std::unique_ptr<const Base> ptr;
};


int main()
{
    Any n;
    assert(n.is_null());

    string s1 = "foo";

    Any a1 = s1;

    assert(a1.not_null());
    assert(a1.is<string>());
    assert(!a1.is<int>());

    Any a2(a1);

    assert(a2.not_null());
    assert(a2.is<string>());
    assert(!a2.is<int>());

//    string s2 = a2;

//    assert(s1 == s2);
    std::cout << "Done.\n";
    return 0;
}



