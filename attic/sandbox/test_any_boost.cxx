#define BOOST_LOG_DYN_LINK 1
/*
 * test_any_boost.cxx
 *
 *  Created on: Aug 23, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <iostream>
#include <string>
#include <vector>
#include <memory>

#include <boost/any.hpp>
#include <boost/log/trivial.hpp>

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}


class Event{
public:
	Event() = default;
	~Event() = default;
	template<typename T>
	void add_payload(T& value){_payload.emplace_back(make_unique<boost::any>(value));}

	template<typename T>
	void add_payload(T&& value){_payload.emplace_back(make_unique<boost::any>(std::move(value)));}
	boost::any& operator[](const int index){ return *_payload[index]; }
private:
	std::vector<std::unique_ptr<boost::any>> _payload;
};

//int operator=(const boost::any a){
//	return boost::any_cast<int>(a);
//}

int main(){
	boost::any a = 1;
	boost::any f = 42.5f;
//	int i = boost::any_cast<decltype a.type()>(a);
	std::cout << boost::any_cast<int>(a) << std::endl;
	a = true;
	BOOST_LOG_TRIVIAL(error) << boost::any_cast<bool>(a) << std::endl;
	const std::type_info &ti = a.type();
	std::cout << ti.name() << '\n';
	Event ev;
	ev.add_payload(1);
	ev.add_payload(std::string{"Test String"});
	BOOST_LOG_TRIVIAL(debug) << boost::any_cast<int>(ev[0]) << std::endl;
	BOOST_LOG_TRIVIAL(debug) << boost::any_cast<float>(f) << std::endl;

	BOOST_LOG_TRIVIAL(debug) << "Unsafe cast: "<< *boost::unsafe_any_cast<long long>(&a) << std::endl;
	BOOST_LOG_TRIVIAL(debug) << "Unsafe cast: "<< *boost::unsafe_any_cast<double>(&f) << std::endl;
	BOOST_LOG_TRIVIAL(info) << boost::any_cast<std::string>(ev[1]) << std::endl;

	Event ev1;
	ev1.add_payload(&ev);
	//std::cout << boost::any_cast<int>(ev1[0]->operator[0]) << std::endl;
	//std::cout << boost::any_cast<std::string>(ev1[0]->operator[1]) << std::endl;

	return 0;
}
