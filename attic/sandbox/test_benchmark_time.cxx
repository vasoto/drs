/*
 * test_benchmark_time.cxx
 *
 *  Created on: Aug 23, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <random>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <vector>
#include <unordered_map>

#include <Utils/benchmark_time.h>

const unsigned long SIZE = 10000000;
const unsigned DEPTH = 3;


std::vector<uint64_t> vec(SIZE);
//boost::mt19937 rng;
//boost::uniform_int<uint64_t> dist(std::numeric_limits<uint64_t>::min(),
//                                  std::numeric_limits<uint64_t>::max());
std::unordered_map<int, long double> map(SIZE/DEPTH);

void test_insert () {
    for (unsigned i = 0; i < SIZE; ++i)
        map[vec[i]] = 0.0;
}

void test_get () {
    long double val;
    for (unsigned i = 0; i < SIZE; ++i)
        val = map[vec[i]];
}


int main () {
	std::random_device rd;
	std::mt19937 g(rd());

	std::uniform_int_distribution<uint64_t> dist(std::numeric_limits<uint64_t>::min(),
			                                std::numeric_limits<uint64_t>::max());

    for (unsigned i = 0; i < SIZE; ++i) {
        uint64_t val = 0;
        while (val == 0) {
            val = dist(rd);
        }
        vec[i] = val;
    }
    time_test(test_insert, "inserts");
    std::random_shuffle(vec.begin(), vec.end());
    time_test(test_insert, "get");
    return 0;
}



