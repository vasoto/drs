/*
 * test_config.cxx
 *
 *  Created on: Aug 10, 2016
 *      Author: Vassil Z. Verguilov
 */

#include <iostream>
#include <string>
#include <vector>

using std::string;

struct attribute{
	virtual ~attribute(){};
};


template<typename T>
struct configuration_attr : public attribute{
	typedef T value_type;
	typedef configuration_attr<T> self;
	value_type value;
};

std::string test_config = "\
[TestCategory]\n\
test_str_attr = \"test\"\n\
test_int_attr = 42\n\
";

int main(){
	configuration_attr<std::string> str_attr;
	str_attr.value = "Test";
	std::cout << "configuration ... " << str_attr.value << std::endl;
	std::cout << test_config << std::endl;
	return 0;
};




