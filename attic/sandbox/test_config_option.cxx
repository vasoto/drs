#include <iostream>
#include <memory>
#include <string>
#include <boost/any.hpp>

class AbstractOption{};


struct Option{
  typedef std::shared_ptr<boost::any> value_type;

  std::string name;
  value_type value;

  Option() = delete;
  
  Option(std::string name):name(name){};
  Option(std::string name, value_type val):name(name),value(val){};
  template<typename T>
  Option(std::string name, const T& val):name(name),value(std::make_shared<boost::any>(new boost::any(val))){}; 
                                                            /*  template<typename T>
  void add_option(const std::string opt_name,
                  T type,
                  const std::string opt_short="")*/
  
};


int main(){
  Option opt1("test name only");
  std::cout << "Test\n";
  int val = 55;
  auto a = boost::any(val);
  std::cout << "a = " << boost::any_cast<int>(a) << "\n";

  Option opt2("test name and value",
              std::make_shared<boost::any>(new boost::any(5)));
  auto value2 = opt2.value;
  auto value2prime = value2;
  boost::any v2 = *value2;
  std::cout << "v2 = " << boost::any_cast<int>(v2) << "\n";
  std::cout << "Opt2: name = "<< opt2.name
            << ", value = " << boost::any_cast<int>(opt2.value.get())
            << ", use count = " << opt2.value.use_count() << std::endl;
  Option opt3("test name and custom type", 42);
  std::cout << "Opt3: name = "<< opt3.name
            << ", value = " << boost::any_cast<int>(opt3.value.get())
            << ", use count = " << opt3.value.use_count() << std::endl; 
  std::cout << "End\n";
  return 0;
}

