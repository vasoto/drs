/*
 * skeleton.cxx

 *
 *  Created on: Aug 23, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <thread>
//#include <DAQ/DAQ.h>

namespace daq{
	class CommandLineOptions{
	public:
		CommandLineOptions() = default;
		~CommandLineOptions() = default;

		void parse_option(std::string option){
			// Mock
			_options[option] = option;
		}
	private:
		std::map<std::string, std::string> _options; // TODO(vasoto): map{string-->any}
	};

	class AbstractModule{
	protected:
		std::string _name;
	};



	class AbstractApplication{
	public:
		virtual ~AbstractApplication(){};
		virtual int execute() = 0;
	protected:
		std::thread _main_thread;
	};


	class DAQApplication:
			public AbstractApplication,
			public std::enable_shared_from_this<DAQApplication>{
	public:
		typedef std::shared_ptr<DAQApplication> pointer;
		DAQApplication() = default;
//
//		DAQApplication(int& argc, char** argv):_options(argc, ""){
//			for(int arg=0; arg < argc; ++arg)
//				_options[arg] = std::move(argv[arg]);
//		}
		virtual ~DAQApplication(){}
		static pointer create(){
			pointer application = std::shared_ptr<DAQApplication>(new DAQApplication());
			return application;
		};

		int execute(){
			std::cerr << "Executing..." << std::endl;
			return 0;
		}


		static pointer create(int argc, char** argv){
			// Factory function to create an application
			pointer application = create();
			// Add options to application
			for(int arg=0; arg < argc; ++arg)
				application->_options.parse_option(argv[arg]);
			return application;
		}

		pointer
		add_module(std::shared_ptr<AbstractModule> module){ // sink?
			if(!module)
				throw std::invalid_argument("module");
			return shared_from_this();
		}
	private:
		CommandLineOptions _options;
		std::vector<std::unique_ptr<AbstractModule>> _modules;
	};
	namespace modules{
		template<typename T>
		class Module : public AbstractModule{
		public:
			typedef std::shared_ptr<T> pointer;

			inline static pointer create(std::string name){
				auto mod = pointer(new T());
				mod->_name = name;
				return mod;
			}

		};
		class TestModule1:public Module<TestModule1>{};
		class TestModule2:public Module<TestModule2>{};
	};

};

using namespace daq;
using namespace daq::modules;


int main(int argc, char** argv){
	std::cout << "Begin...\n";
	auto mod1 = TestModule1::create("test mod");
	// Create application and parse command line options
	//TestModule1()::create("test_module")
	auto app = DAQApplication::create(argc, argv)->add_module(mod1);
		//->add_module(std::make_shared<AbstractModule>(new TestModule2()));
	auto result = app->execute();
	std::cout << "End." << std::endl;
	return result;
}


