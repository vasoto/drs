/*
 * test_flyweight.cxx
 *
 *  Created on: Aug 20, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <memory>
#include <unordered_set>

namespace drs{
namespace core{

template<typename T>
class Flyweight{
private:
	std::unique_ptr<const T*> _value;

};

};

};


int main(){
	drs::core::Flyweight<int> fwght;

	return 0;
}


