/*
 * test_inheritance.cxx
 *
 *  Created on: Aug 11, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <cassert>
#include <utility>

template<typename T>
void draw(const T& x, std::ostream& out, size_t position){
	out << std::string(position, ' ') << x << std::endl;
}


class object_t{
private:
	struct concept_t{
		virtual ~concept_t() = default;
		virtual concept_t* copy_() const = 0;
		virtual void draw_(std::ostream&, size_t) const = 0;

	};

	template <typename T>
	struct model : concept_t{
	    model(T x) : data_(std::move(x)){}
	    concept_t * copy_() const {return new model(*this);}
	    void draw_(std::ostream& out, size_t position) const {
	    	draw(data_, out, position);
	    }

	    T data_;
	};
    std::unique_ptr<const concept_t> self_;
public:
   template<typename T>
   object_t(const T& x) : self_(new model<T>(std::move(x))){}
   object_t(const object_t& x) : self_(x.self_->copy_()){ }
   object_t(object_t&&) noexcept = default;

   object_t& operator=(const object_t& x){
	  object_t tmp(x); // Create a copy of x
      *this = std::move(tmp); // Move the copy to *this
      // Ensures that if copy fails, the original object is intact
      return *this;
   }

   object_t& operator=(object_t&&) noexcept = default;

    friend void draw(const object_t& x, std::ostream& out, size_t position){
    	x.self_->draw_(out, position);
    }
};


using document_t = std::vector<object_t>;

void draw(const document_t& x, std::ostream& out, size_t position)
{
    out << std::string(position, ' ') << "<document>" << std::endl;
    for (auto& e : x)
    	draw(e, out, position + 2);
    out << std::string(position, ' ') << "</document>" << std::endl;
}

using history_t = std::vector<document_t>;

void commit(history_t& x) {
	assert(x.size());
	x.push_back(x.back());
}

void undo(history_t& x) {
	assert(x.size());
	x.pop_back();
}

document_t& current(history_t& x) {
	assert(x.size());
	return x.back();
}


class my_class{};


void draw(const my_class&, std::ostream& out, size_t position){
	out << std::string(position, ' ') << "my_class" << std::endl;
}


int main(){
    history_t h(1);

    current(h).emplace_back(0);
    current(h).emplace_back(std::string("Hello!"));

    draw(current(h), std::cout, 0);
    std::cout << "--------------------------" << std::endl;

    commit(h);

    current(h).emplace_back(current(h));
    current(h).emplace_back(my_class());
    current(h)[1] = std::string("World");

    draw(current(h), std::cout, 0);
    std::cout << "--------------------------" << std::endl;

    undo(h);

    draw(current(h), std::cout, 0);
}




