#include <iostream>
#include <vector>
#include <string>

// CRTP
// Provides common interface for all IOInterface derrived classes
class IOInterfaceDeleter{
public:
	virtual ~IOInterfaceDeleter(){}
	std::string name;
};

template<typename T>
class IOInterface : public IOInterfaceDeleter {
public:
  void ioInit(){ static_cast<T*>(this)->init(); };
  void ioRead(){ static_cast<T*>(this)->read(); };
  void ioWrite(){ static_cast<T*>(this)->write(); };
  void ioClose(){ static_cast<T*>(this)->close(); };
};

class VMEInterface : public IOInterface<VMEInterface>{
public:
  void init(){};
  void read(){};
  void write(){};
  void close(){};
};

class USBInterface : public IOInterface<USBInterface>{
  void init(){};
  void read(){};
  void write(){};
  void close(){};
};


int main(){
  std::vector<IOInterfaceDeleter *> v;
  VMEInterface * vme = new VMEInterface();
  USBInterface * usb = new USBInterface();
  usb->name = "test";
  v.push_back(vme);
  v.push_back(usb);
//  v[0]->ioInit();
  return 0;
}
