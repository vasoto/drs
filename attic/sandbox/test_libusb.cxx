/*
 * test_libusb.cxx
 *
 *  Created on: Aug 16, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <iostream>
#include <memory>
#include <sstream>
#include <iomanip>
#include <string>
//#include "libusb-1.0/libusb.h"
#include <errno.h>
#include <libusb-1.0/libusb.h>

using std::cout;
using std::cerr;
using std::endl;

struct USBDevice{
	// Constructs a UsbDevice object by taking a raw pointer to a libusb_device
	  // struct as |device|. The ownership of |device| is not transferred, but its
	  // reference count is increased by one during the lifetime of this object.
	explicit USBDevice(libusb_device* device);

	std::string GetStringDescriptorAscii(uint8_t index);
	libusb_device* device_;
	libusb_device_handle* device_handle_;
	std::unique_ptr<libusb_device_descriptor> _device_descriptor;
};

std::ostream& set_hex( std::ostream& in, int width=6){
	in << std::hex << std::showbase << std::internal
	   << std::setfill('0') << std::setw(width);
	return in;
}

std::ostream& reset_hex( std::ostream& in, int width=0){
	in << std::setw(width) << std::dec;
	return in;
}

class USBDeviceDescriptor {
 public:
  // Constructs a UsbDeviceDescriptor object by taking a weak pointer to a
  // UsbDevice object as |device| and a raw pointer to a
  // libusb_device_descriptor struct as |device_descriptor|. |device| is
  // used for getting USB string descriptors related to this object. The
  // ownership of |device_descriptor| is not transferred, and thus it should
  // outlive this object.
  USBDeviceDescriptor(const std::weak_ptr<USBDevice>& device,
                      const libusb_device_descriptor* device_descriptor)
					    : _device(device),
					      _device_descriptor(device_descriptor) {}
  ~USBDeviceDescriptor() = default;

  // Getters for retrieving fields of the libusb_device_descriptor struct.
  inline uint8_t length() const {return _device_descriptor->bLength;};
  inline uint8_t descriptorType() const {return _device_descriptor->bDescriptorType;};
  uint8_t deviceClass() const { return _device_descriptor->bDeviceClass; };
  uint8_t deviceSubclass() const { return _device_descriptor->bDeviceSubClass; };
  uint8_t protocol() const { return _device_descriptor->bDeviceProtocol; };
  uint8_t maxPacketSize0() const { return _device_descriptor->bMaxPacketSize0; };
  uint16_t vendorId() const { return _device_descriptor->idVendor;};
  uint16_t productId() const { return _device_descriptor->idProduct; };
  std::string manufacturer() const { return getTextInfo( _device_descriptor->iManufacturer); };
  std::string product() const { return getTextInfo(_device_descriptor->iProduct);};
  std::string serialNumber() const { return getTextInfo(_device_descriptor->iSerialNumber);};
  uint8_t numConfigurations() const { return _device_descriptor->bNumConfigurations; };

  // Returns a string describing the properties of this object for logging
  // purpose.
  std::string toString() const{
	  std::stringstream ss;
	  ss <<"Device (Length=" << length() << ", "
	            << "DescriptorType=" << descriptorType()  << ", "
	            << "DeviceClass=" << deviceClass()  << ", "
	            << "DeviceSubclass=" << deviceSubclass()  << ", "
	            << "DeviceProtocol=" << protocol()  << ", "
	            << "MaxPacketSize0=" << maxPacketSize0()  << ", "
	            //<< "VendorId=" << set_hex( std::cout ) << vendorId()  << ", "
	            //<< "ProductId=" << productId() << reset_hex( std::cout ) << ", "
	            << "Manufacturer='"<< manufacturer() << "', "
	            << "Product='"<< product() <<"', "
	            << "SerialNumber='"<< serialNumber() <<"', "
	            << "NumConfigurations=" << numConfigurations()
	            << ")";
	 return ss.str();
  };

private:
  USBDevice* getDevice() const{
	  if( auto dev = _device.lock() )
		  return dev.get();
	  else return nullptr;
  }
  std::string getTextInfo(uint8_t index) const{
	  USBDevice* device = getDevice();
	  return device ? device->GetStringDescriptorAscii(index) : "N/A";
  };

  std::weak_ptr<USBDevice> _device;
  const libusb_device_descriptor* const _device_descriptor;
  USBDeviceDescriptor(const USBDeviceDescriptor&) = delete;
  void operator=(const USBDeviceDescriptor&) = delete;
//  DISALLOW_COPY_AND_ASSIGN(UsbDeviceDescriptor);
};




std::string USBDevice::GetStringDescriptorAscii(uint8_t index) {
//  if (!VerifyOpen())
//    return string();

  // libusb_get_string_descriptor_ascii uses an internal buffer that can only
  // hold up to 128 ASCII characters.
  int length = 128;
  /*
  unique_ptr<uint8_t[]> data(new uint8_t[length]);
  int result = libusb_get_string_descriptor_ascii(
      device_handle_, index, data.get(), length);
  if (result < 0) {
    error_.SetFromLibUsbError(static_cast<libusb_error>(result));
    return string();
  }

  error_.Clear();
  return string(reinterpret_cast<const char*>(data.get()), result);
  */
  return ("");
}

class USBReader final{ //Inherit from Module or Interface?
public:
	enum class Status : char {
		Undefined = 0,
		Success  = 1,
		NotFound = 2,
		InvalidParameter = 3,
		NoMemory = 4,
		AccessError = 5
	};

	void open();
	int close();
	int read();
	int write();
	int reset();
private:
//	std::unique_ptr<libusb_device_handle> _dev;
};


int main(){
	cout << "C++ version: " << __cplusplus << endl;
	cout <<"Test USB readout...\n";
	USBReader reader;//(0x0781, 0x5571);
//	reader.open();
	cout << "Done." << endl;
	return 0;
}

