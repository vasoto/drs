/*
 * test_log.cxx
 *
 *  Created on: Aug 17, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <iostream>
#include <string>
#include <sstream>


//typedef int LogSeverity;
//const LogSeverity LOG_VERBOSE = -1;  // This is level 1 verbosity
//// Note: the log severities are used to index into the array of names,
//// see log_severity_names.
//const LogSeverity LOG_INFO = 0;
//const LogSeverity LOG_WARNING = 1;
//const LogSeverity LOG_ERROR = 2;
//const LogSeverity LOG_FATAL = 3;
//const LogSeverity LOG_NUM_SEVERITIES = 4;


class LogMessage{
public:
	enum class LogSeverity{
		Debug = 0,
		Info = 1,
		Warning = 2,
		Error = 3,
		Fatal = 4
	};
	LogMessage(const char * file_name, int line, LogSeverity severity);
	~LogMessage();

	std::ostream& stream() { return _stream; }
private:
	void init(const char* file, int line);
	LogSeverity _severity;
	std::ostringstream _stream;
};

int main(){

	return 0;
}

