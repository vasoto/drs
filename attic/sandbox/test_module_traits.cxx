/*
 * test_module_traits.cxx
 *
 *  Created on: Aug 13, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <type_traits>
#include <iostream>
#include <typeinfo>

struct buffer{

};

class CanRead{
public:
	buffer read();
};

class CanRead2: public CanRead{

};

class CanWrite{
	void write(buffer buf);
};

//struct sfinae_types{
//	typedef char one;
//	typedef  struct { char __arr[2]; } two;
//};
//
//template<typename T>
//struct can_read__helper : public sfinae_types{
//	template <typename C> static constexpr one test( decltype(&C::read));
//	template <typename C> static constexpr two test(...);
//public:
//	static const bool __value = sizeof(test<T>(0)) == sizeof(one);
////	enum { value = };
//};

//template<typename T,
//struct has_method


template<typename, typename T>
struct has_read {
    static_assert(
        std::integral_constant<T, false>::value,
        "Second template parameter needs to be of function type.");
};

template<typename C, typename Ret, typename... Args>
struct has_read<C, Ret(Args...)> {
private:
    template<typename T>
    static constexpr auto check(T*)
    -> typename
        std::is_same<
            decltype( std::declval<T>().read( std::declval<Args>()... ) ),
            Ret    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        >::type;  // attempt to call it and see if the return type is correct
    template<typename> static constexpr std::false_type check(...);
    typedef decltype(check<C>(0)) type;

public:
    static constexpr bool value = type::value;
};

//template<typename T>
//struct can_read: public std::integral_constant<bool, can_read__helper<T>::__value>{};

template<typename T>
struct can_read: public has_read<T, buffer()>{};


int main(){
	std::cout << std::boolalpha
			<< "can_read (CanRead): " << can_read<CanRead>::value << " "
			<< has_read<CanRead, buffer(void)>::value << " "
			<< has_read<CanRead, buffer()>::value << " "
			//<< has_read<CanRead, int>::value << " "
			<< has_read<CanRead, buffer(long)>::value << std::endl
			<< "can_read (CanRead2): " << can_read<CanRead2>::value << " "
			<< has_read<CanRead2, buffer(void)>::value << " "
			<< has_read<CanRead2, buffer(long)>::value << std::endl
			<< "can_read (CanWrite): " << can_read<CanWrite>::value << " "
			<< has_read<CanWrite, buffer(void)>::value << std::endl;
	return 0;
}


