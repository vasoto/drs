/*
 * test_newcpp.cxx
 *
 *  Created on: Jul 22, 2016
 *      Author: Vassil Z. Verguilov
 */


// Test Modern C++

#include <list>
#include <vector>
#include <iostream>

using std::vector;
using std::cout;

int main(){
	vector<int> a(10, 5);
	for(auto& el : a){
		cout << el << "\n";
	}
	return 0;
}


