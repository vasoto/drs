/*
    Copyright 2013 Adobe Systems Incorporated
    Distributed under the MIT License (see license at
    http://stlab.adobe.com/licenses.html)

    This file is intended as example code and is not production quality.
*/

#include <cassert>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <chrono>

using namespace std;
using namespace std::chrono;

milliseconds ms = duration_cast< milliseconds >(
    system_clock::now().time_since_epoch()
);

/******************************************************************************/
// Library

template <typename T>
void draw(const T& x, ostream& out, size_t position)
{ out << string(position, ' ') << x << endl; }

class object_t {
  public:
    template <typename T>
    object_t(T x) : self_(new model<T>(move(x)))
    { }

    object_t(const object_t& x) : self_(x.self_->copy_())
    { }
    object_t(object_t&&) noexcept = default;

    object_t& operator=(const object_t& x)
    { object_t tmp(x); *this = move(tmp); return *this; }
    object_t& operator=(object_t&&) noexcept = default;

    friend void draw(const object_t& x, ostream& out, size_t position)
    { x.self_->draw_(out, position); }

  private:
    struct concept_t {
        virtual ~concept_t() = default;
        virtual concept_t* copy_() const = 0;
        virtual void draw_(ostream&, size_t) const = 0;
    };
    template <typename T>
    struct model : concept_t {
        model(T x) : data_(move(x)) { }
        concept_t* copy_() const { return new model(*this); }
        void draw_(ostream& out, size_t position) const
        { draw(data_, out, position); }

        T data_;
    };

   unique_ptr<const concept_t> self_;
};

using payload_t = vector<object_t>;

void draw(const payload_t& x, ostream& out, size_t position)
{
    out << string(position, ' ') << "<payload>" << endl;
    for (auto& e : x) draw(e, out, position + 2);
    out << string(position, ' ') << "</payload>" << endl;
}

using event_t = vector<payload_t>;

void commit(event_t& x) {
	assert(x.size());
	x.push_back(x.back());
}

void undo(event_t& x) {
	assert(x.size());
	x.pop_back();
}

payload_t& current(event_t& x) { assert(x.size()); return x.back(); }

/******************************************************************************/


// Client
class my_class_t {
    /* ... */
};





struct EventHeader{
	EventHeader():timestamp(duration_cast< milliseconds >(
    system_clock::now().time_since_epoch())){};
	std::chrono::milliseconds timestamp;
};

class EventFooter{
	EventFooter():timestamp(duration_cast< milliseconds >(
			system_clock::now().time_since_epoch())){};

	friend std::ostream& operator<<(std::ostream& out, const EventFooter& footer ){
		out << "Event footer(" << footer.timestamp.count()
			<< ")";
		return out;
	}

	std::chrono::milliseconds timestamp;

};


std::ostream& operator<<(std::ostream& out, const EventHeader& header ){
	out << "Event header(" << header.timestamp.count()
		<< ")";
	return out;
}

void draw(const EventHeader& header, ostream& out, size_t position){
	out << string(position, ' ') << header << std::endl;
}

void draw(const my_class_t&, ostream& out, size_t position)
{ out << string(position, ' ') << "my_class_t" << endl; }

typedef std::pair<std::string, object_t> key_value_pair;

void draw(const key_value_pair& kv, ostream& out, size_t position ){
	out <<  string(position, ' ')
		<< std::get<0>(kv) << ": "
		//<< std::get<1>(kv)
		<< std::endl;
}
int main()
{
//	Payload pld;
	object_t one(1);
    event_t h(1);

    current(h).emplace_back(0);
    current(h).emplace_back(string("Hello!"));

    draw(current(h), cout, 0);
    cout << "--------------------------" << endl;

    commit(h);

    current(h).emplace_back(current(h));
    current(h).emplace_back(my_class_t());
    current(h)[1] = string("World");
    current(h).emplace_back(EventHeader());
    draw(current(h), cout, 0);
    cout << "--------------------------" << endl;

    undo(h);

    draw(current(h), cout, 0);
}
