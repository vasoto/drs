#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <memory>

// based on https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Type_Erasure

template<class T> using DerefType = typename std::decay<T>::type;

class variant{
public:
  // Default constructor
  variant():_inner(nullptr){};

  // Type erasure constructor
  template<typename T>
  variant(T value):_inner(new inner<T>(std::forward<T>(value))){};

  template<typename T>
  variant& operator=(T& value){
    variant tmp(value);
    *this = std::move(tmp);
    return *this;
  }


  template <typename T>
  static T*
  cast(variant* operand) noexcept {
    return operand && operand->type() == typeid(T) ?
      &static_cast<variant::inner<T> *>(operand->_inner)->_value : nullptr;
  }
  
  template <typename T>
  static T*
  unsafe_cast(variant* operand) noexcept {
    return operand ? &static_cast<variant::inner<T> *>(operand->_inner)->_value : nullptr;
  }

  template <typename T>
  static inline const T*
  cast(const variant* operand) {
    return variant::cast<T>(const_cast<variant*>(operand));
  }

  template <typename T>
  static inline const T*
  unsafe_cast(const variant* operand) {
    return variant::unsafe_cast<T>(const_cast<variant*>(operand));
  }

  template <typename T>
  static T
  cast(variant& operand){
    //    typedef typename RemoveReference<ValueType>::Type NonRef;
    DerefType<T>* result = cast<DerefType<T>>(&operand);
    if (!result)
      throw std::bad_cast();
    return *result;
  }

  template <typename T>
  static T
  unsafe_cast(variant& operand){
    //    typedef typename RemoveReference<ValueType>::Type NonRef;
    DerefType<T>* result = unsafe_cast<DerefType<T>>(&operand);
    if (!result)
      throw std::bad_cast();
    return *result;
  }

  template <typename T>
  static inline T
  cast(const variant& operand){
    //    typedef typename RemoveReference<ValueType>::Type NonRef;
    return cast<const DerefType<T>&>(const_cast<variant&>(operand));
  }

  template <typename T>
  static inline T
  unsafe_cast(const variant& operand){
    //typedef typename RemoveReference<ValueType>::Type NonRef;
    return unsafe_cast<const DerefType<T>&>(const_cast<variant&>(operand));
  }

  const std::type_info& type() const noexcept { return _inner->type();}
  
private:

  class inner_base{
  public:
    using pointer = std::unique_ptr<const inner_base>;
    inner_base() = default;
    virtual ~inner_base() = default;
    virtual const std::type_info& type() const = 0;
  };
  
  template <typename ValueType>
  class inner : public inner_base{
  public:
    inner() = delete;
    inner(ValueType value): _value(value){};
    virtual const std::type_info& type() const override { return typeid(ValueType); }
  private:
    ValueType _value;
  };
  typename inner_base::pointer _inner;
  
};


int main(){
  std::cout << "Test\n";
  variant v = 5;
  //  int a = *variant::unsafe_cast<int>(v);
  //  std::cout << v.cast(&v) << std::endl;
  std::cout << "Done.\n";
  return 0;
};
