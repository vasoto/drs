#include <iostream>
#include <bitset>

struct Device{
  bool waitForData(){ return false; }
};
struct Buffer{
  void add(Buffer& buffer){}
  Buffer& pop(int start, int end){}
};
struct EventQueue;

Buffer readRawData(Device dev){

}

int findHeader(Buffer buffer){

}

void unpackDataStream(EventQueue& ev_queue, const Device& dev){
  Buffer raw_data;
  Buffer event_buffer;
  raw_data = readRawData(dev);
  int data_ptr = -1;
  do{
    data_ptr = findHeader(raw_data);
    event_buffer.add(raw_data.pop(0, data_ptr));
    
  }while( true );
}

template<typename T>
T create_mask(const short length, const short start){
	return (T)((1 << length) - 1) << start;
}

struct buffer{
	union {
		uint64_t _dword;
		uint8_t  _byte[8];
		uint16_t _short[4];
		uint32_t _word[2];
	};
	size_t length;
};

std::ostream& operator<<(std::ostream& os, const buffer& buf){
	auto line = std::string(20, '-');
	os << "_byte[0]: " << (int)buf._byte[0] << std::endl
		<< "_byte[1]: " << (int)buf._byte[1] << std::endl
		<< "_byte[2]: " << (int)buf._byte[2] << std::endl
		<< "_byte[3]: " << (int)buf._byte[3] << std::endl
		<< "_byte[4]: " << (int)buf._byte[4] << std::endl
		<< "_byte[5]: " << (int)buf._byte[5] << std::endl
		<< "_byte[6]: " << (int)buf._byte[6] << std::endl
		<< "_byte[7]: " << (int)buf._byte[7] << std::endl
		<< line << std::endl
		<< "_short[0]: " << buf._short[0] << std::endl
		<< "_short[1]: " << buf._short[1] << std::endl
		<< "_short[2]: " << buf._short[2] << std::endl
		<< "_short[3]: " << buf._short[3] << std::endl
		<< line << std::endl
		<< "_word[0]: " << buf._word[0] << std::endl
		<< "_word[1]: " << buf._word[1] << std::endl
		<< line << std::endl
		<< "_dword[0]: " << buf._dword;
	return (os);
}

int main(){
	uint16_t a = 101;
	unsigned int mask = ~0;
	uint16_t m = create_mask<uint16_t>(3, 2);
	auto i = (a & m);
	auto b = create_mask<uint8_t>(4, 0);
	uint8_t c = 32 + 4 + 8;
	buffer b1 {18446744073709551615, 64};
	//b1._dword = 18446744073709551615;
	//b1.length = 64;
	std::cout << std::bitset<16>(a) << std::endl
			<< std::bitset<16>(m) << std::endl
			<< std::bitset<16>(i) << " = " << i << std::endl
			<< std::bitset<16>((a & 8)) << std::endl
			<< std::bitset<8>(0xF) << " " << b << std::endl
			<< std::bitset<8>(b & 0x80) << std::endl
			<< std::bitset<8>( c ) << std::endl
			<< std::bitset<8>(c >> 2) << " " << ((c >> 2) & 0x3) << std::endl
			<< "Size of char: " << sizeof(char) << std::endl
			<< "Size of uint8_t: " << sizeof(uint8_t) << std::endl
			<< "Size of short: " << sizeof(short) << std::endl
			<< "Size of uint16_t: " << sizeof(uint16_t) << std::endl
			<< "Size of int: " << sizeof(int) << std::endl
			<< "Size of long: " << sizeof(long) << std::endl
			<< "b1: " << std::endl << b1
			<< std::endl;

  return 0;
}
