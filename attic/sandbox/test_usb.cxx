/*
 * test_usb.cxx
 *
 *  Created on: Aug 15, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <algorithm>
#include <memory>
#include <iomanip>
#include <iostream>
#include <string.h>
#include "usb.h"
//#include "libusb-1.0/libusb.h"

using std::cout;
using std::endl;

#ifdef __cplusplus <= 201103
// Define make_unique for versions below C++14
namespace std{
template<typename T, typename... Args>
unique_ptr<T> make_unique(Args&&... args) {
    return unique_ptr<T>(new T(forward<Args>(args)...));
}
};
#endif


template<typename USBType>
struct USBIterator : std::iterator< std::forward_iterator_tag, USBType>{
  typedef USBIterator Self;
  //typedef std::forward_iterator_tag iterator_category;
  typedef USBType value_type;
  typedef std::shared_ptr<value_type> pointer;
  typedef value_type& reference;

  USBIterator():node(){}; // default constructor
  explicit USBIterator(value_type * other):node(std::shared_ptr<value_type>(other)){}; // 'Copy' constructor

  Self& operator++(){
    std::shared_ptr<value_type> tmp(this->node->next);
    this->node = std::move(tmp);
    return *this;
  }

  Self operator++(int){
    Self current = *this; // copy
    std::shared_ptr<value_type> tmp(this->node->next);
    this->node = std::move(tmp); // node->next;
    return current;
  }

  reference operator*() const{
    return *(this->node.get());
  }

  pointer operator->() const {
    return node;
  }

  bool operator==(const Self& other) const{
    return node == other.node;
  }

  bool operator!=(const Self& other) const{
    return node != other.node;
  }

  //value_type *
  pointer node;
};


struct USBBusses{
public:
  typedef USBIterator<struct usb_bus> iterator;
  typedef usb_bus value_type;

  USBBusses():node(usb_get_busses()){}
  iterator begin(){
    return iterator(usb_get_busses());
  }

  iterator end(){
    return iterator(nullptr);
  }
  value_type * node;
};

struct USBDevices{
public:
  typedef USBIterator<struct usb_device> iterator;
  typedef struct usb_device value_type;

  USBDevices() : bus(nullptr), node(nullptr){};
  USBDevices(struct usb_bus * _bus):bus(_bus),node(_bus->devices){}

  iterator begin(){
    return iterator(bus->devices);
  }

  iterator end(){
    return iterator(nullptr);
  }

  value_type * node;
  usb_bus * bus;
};


class USBReader final{ //Inherit from Module or Interface?
public:
	enum class Status : char {
		Undefined = 0,
		Success  = 1,
		NotFound = 2,
		InvalidParameter = 3,
		NoMemory = 4,
		AccessError = 5
	};
	USBReader() = delete; // Remove default constructor
	explicit USBReader(int vendor, int product, int instance=1):_vendor(vendor),
			_product(product), _instance(instance),
			_dev(nullptr), _status(Status::Undefined),
			_usb_configuration(0),_usb_interface(0),
			_usb_type(0), _initialized(false){};
	~USBReader(){}; // Class is final

	void open();
	int close();
	int read();
	int write();
	int reset();
	//int setAltInterface(); // ???
	//int getDevice();
	Status getStatus(){ return _status;}
private:
	void initialize_usb(){
		if (not _initialized){
			usb_init();
			usb_set_debug(100);
			// Find all busses on the system
			int bus_count = usb_find_busses();
			int dev_count = usb_find_devices();
			std::cout << "Found " << bus_count << " busses and "
					<< dev_count << " devices." << std::endl;
			_initialized = true;
		}
	}

	void setStatus(Status stat){_status = stat;}
	void setStatus(int stat){_status = static_cast<Status>(stat);}

	std::unique_ptr<struct usb_device> findDevice();
	bool _initialized;
	int _vendor;
	int _product;
	int _instance;
	usb_dev_handle * _dev;
	Status _status;
	int _usb_configuration;
	int _usb_interface;
	int _usb_type;
};

void USBReader::open(){
	initialize_usb();
	std::unique_ptr<struct usb_device> device = USBReader::findDevice();
	if( device != nullptr){
		std::cout << "Device found:\n"
				<< device->bus->dirname
				<< std::endl;
		_dev = usb_open(device.get());
		if(!_dev){
			_status = Status::AccessError;
			std::cerr << "Error! Cannot open device" << std::endl;
			return;
		}
		int status = usb_set_configuration(_dev, _usb_configuration);
		if (status < 0) {
			std::cerr << "Error(open): usb_set_configuration() error " << status
					<< " ("<< strerror(-status) << ")\n"
					<< "Found USB device " << std::hex << std::showbase << std::internal
					<< std::setfill('0') << std::setw(6) << _vendor
					<< ":" << std::setw(6) << _product << std::dec << std::setw(0)
					<< " instance " << _instance << ", but cannot initialize it "
					<< "please check permissions on \"/proc/bus/usb/"<< device->bus->dirname << "/"
					<< device->filename << "\" and \"/dev/bus/usb/" << device->bus->dirname << "/" << device->filename << "\"\n";
		    _status = Status::AccessError;
		    return;
		}
		status = usb_claim_interface(_dev, _usb_interface);
		if (status < 0) {
			std::cerr << "Error(open): usb_claim_interface() error " << status
					<< " ("<< strerror(-status) << ")\n";
			_status = Status::AccessError;
			return;
		}
	}else{
		_status = Status::NotFound;
		std::cerr << "Error! Could not find instance " << _instance
				  << " of device " << std::hex << std::showbase << std::internal
				  << std::setfill('0') << std::setw(6) << _vendor
				  << ":" << std::setw(6) << _product << std::dec << std::setw(0) << std::endl;
		return;
	}
	_status = Status::Success;
};

std::unique_ptr<struct usb_device> USBReader::findDevice(){
	int count(0);
	USBBusses busses;
	for(auto bus : busses){
		USBDevices devices(&bus);
//		USBDevices::iterator dev_iter = devices.begin();
		for(auto device : devices ){
			std::cout << std::hex << device.descriptor.idVendor << ":"
					  << device.descriptor.idProduct << std::dec << std::endl;
			if ((device.descriptor.idVendor == _vendor) &&
				(device.descriptor.idProduct == _product)){
				++count;
				if (count == _instance)
					return std::make_unique<struct usb_device>(device);
			}

		}
	}
	return nullptr;
}



int main(){
	cout << "C++ version: " << __cplusplus << endl;
	cout <<"Test USB readout...\n";
	USBReader reader(0x0781, 0x5571);
	reader.open();
	cout << "Done." << endl;
	return 0;
}
