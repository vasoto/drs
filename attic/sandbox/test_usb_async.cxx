#include <iostream>
#include <algorithm>
#include <iterator>
#include <memory>
#include <vector>

#include <usb.h>

//void cb(struct libusb_transfer *transfer){
  //  int *completed = transfer->user_data;
  //  *completed = 1;
//}
template<typename USBType>
struct USBIterator{
  typedef USBIterator Self;
  typedef std::forward_iterator_tag iterator_category;
  typedef USBType value_type;
  //  typedef usb_bus value_type;
  typedef std::shared_ptr<value_type> pointer;
  typedef value_type& reference;

  USBIterator():node(){}; // default constructor
  explicit USBIterator(value_type * other):node(std::shared_ptr<value_type>(other)){}; // 'Copy' constructor

  Self& operator++(){
    std::shared_ptr<value_type> tmp(this->node->next);
    this->node = std::move(tmp);
    return *this;
  }
  
  Self operator++(int){
    Self current = *this; // copy
    std::shared_ptr<value_type> tmp(this->node->next);
    this->node = std::move(tmp); // node->next;
    return current;
  }
  
  reference operator*() const{
    return *(this->node.get());
  }

  pointer operator->() const {
    return node;
  }
  
  bool operator==(const Self& other) const{
    return node == other.node;
  }

  bool operator!=(const Self& other) const{
    return node != other.node;
  }

  //value_type *
  pointer node;
};

std::ostream& operator<<(std::ostream& out, const struct usb_device_descriptor& descr){
  out << "Length: " << (int)descr.bLength << std::endl
      << "Descriptor type: " << (int)descr.bDescriptorType << std::endl
      << "bcdUSB: " << (int)descr.bcdUSB << std::endl
      << "Device class: " << (int)descr.bDeviceClass << std::endl
      << "Device subclass: " << (int)descr.bDeviceSubClass << std::endl
      << "Device protocol: " << (int)descr.bDeviceProtocol << "\n"
      << "Max packet size0: " << (int)descr.bMaxPacketSize0 << "\n"
      << "Vendor ID: " << (int)descr.idVendor << "\n"
      << "Product ID: " << (int)descr.idProduct << "\n"
      << "bcdDevice: " << (int)descr.bcdDevice << "\n"
      << "Manifacturer: " << (int)descr.iManufacturer << "\n"
      << "Product: " << (int)descr.iProduct << "\n"
      << "Serial Number: " << (int)descr.iSerialNumber << "\n"
      << "Num. of configurations: " << (int)descr.bNumConfigurations << "\n";
} 

std::ostream& operator<<(std::ostream& out, const struct usb_device& device){
  out << "Filename: " << device.filename << "\n"
      << "Devnum: " << (int)device.devnum << "\n"
      << "Num. of children: " << (int)device.num_children << "\n"
      << "Descriptor: " << std::endl
      << "#########################################" << std::endl
      << device.descriptor << std::endl;
  return out;
}

struct USBBusses{
public:
  typedef USBIterator<struct usb_bus> iterator;
  typedef usb_bus value_type;

  USBBusses():node(usb_get_busses()){}
  iterator begin(){
    return iterator(usb_get_busses());
  }

  iterator end(){
    return iterator(nullptr);
  }
  value_type * node;
};

struct USBDevices{
public:
  typedef USBIterator<struct usb_device> iterator;
  typedef struct usb_device value_type;

  USBDevices() : bus(nullptr), node(nullptr){};
  USBDevices(struct usb_bus * _bus):bus(_bus),node(_bus->devices){}

  iterator begin(){
    return iterator(bus->devices);
  }
  
  iterator end(){
    return iterator(nullptr);
  }

  value_type * node;
  usb_bus * bus;
};




int enumerate_devices(){
  int devices = 0;
  // Find all busses on the system
  int bus_count = usb_find_busses();
  int dev_count = usb_find_devices();
  std::cout << "Found " << bus_count << " busses and "
            << dev_count << " devices." << std::endl;
  USBBusses busses;
  for(auto bus : busses){
    USBDevices devices(&bus);
    std::cout << "Dirname: " << bus.dirname << std::endl;
    for(auto device : devices){
      std::cout << device << std::endl;
    }
  }
  std::cout << "Passed." << std::endl;
  /*for(auto bus : usb_get_busses()){
    std::cout << "Bus: " << bus << std::endl;
  }
  */
}


int main(){
  usb_init();

  int devices = enumerate_devices();

  //usb_exit();
  
  return 0;
}
