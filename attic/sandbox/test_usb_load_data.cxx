#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <functional>
#include <ctype.h>
#include <time.h>
#include <limits.h>
#include <algorithm>

#include <boost/algorithm/string/trim.hpp>

const std::string USB_IDS_FILE = "/usr/share/misc/usb.ids";

class not_isalpha {
  bool table[UCHAR_MAX];
public:
  not_isalpha() {
    for (int i=0; i<UCHAR_MAX; i++)
      table[i] = !isalpha(i);
  }

  inline bool operator()(char input){
    return table[(unsigned char)input];
  }
};


std::string strip_string(std::string const &input){
  std::string result;
  result.reserve(input.size());
  std::remove_copy_if(input.begin(),
                      input.end(),
                      std::back_inserter(result),
                      not_isalpha());
  return result;
}



static inline bool is_commented(const std::string& line){
  return line[0] == '#';
}


static inline bool is_idented(const std::string& line){
  return line[0] == '\t';
}

/*
static inline bool is_empty(const std::string& line){
  
}
*/

typedef unsigned int ID;

typedef struct{
  int id;
  std::string name;
  std::map<ID, std::string> products;
} Vendor;


std::istream& operator >>(std::istream &is, Vendor& vendor){
  ID id;
  std::string name;
  is >> vendor.id >> vendor.name;
  return (is);
}

std::map<ID, Vendor> Vendors;

int main(){
  std::cout << "Read from " << USB_IDS_FILE << std::endl;
  std::ifstream ids_file(USB_IDS_FILE);
  if(ids_file.is_open()){
    std::string line;
    Vendor vendor;
    while(getline(ids_file, line)){
      //boost::trim(line);
      // ignore comments and empty lines
      if(is_commented(line) || line.empty())
        continue;
      // check ident
      if(not is_idented(line))
        
        std::cout << "IDENT! ";
      std::cout << line << '\n';
    }
  }
}
