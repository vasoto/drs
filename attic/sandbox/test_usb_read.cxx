#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string>
#include <usb.h>
#include <iostream>


const unsigned BULK_EP_OUT = 0x82;
const unsigned BULK_EP_IN = 0x08;

int main(){
  usb_init();
  usb_find_busses();
  usb_find_devices();
  usb_set_debug(3);

  struct usb_bus *bus;
  struct usb_device *dev;
  int count = 0;

  for (bus = usb_get_busses(); bus; bus = bus->next)
    for (dev = bus->devices; dev; dev = dev->next){
      int status;
      usb_dev_handle *udev;
      std::cout << dev->descriptor.idVendor << " : " << dev->descriptor.idProduct << std::endl;
      udev = usb_open(dev);
      if (!udev) {
        fprintf(stderr, "musb_open: usb_open() error\n");
        return 1;
      }
    }
}
