/*
 * Core.h
 *
 *  Created on: Jul 23, 2016
 *      Author: Vassil Z. Verguilov
 */

#ifndef SRC_CORE_H_
#define SRC_CORE_H_

namespace drs {
namespace core {

class Core {
public:
	virtual ~Core() = delete;
};

} /* namespace core */
} /* namespace drs */

#endif /* SRC_CORE_H_ */
