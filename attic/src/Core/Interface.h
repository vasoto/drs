/*
 * Interface.h
 *
 *  Created on: Aug 10, 2016
 *      Author: Vassil Z. Verguilov
 */

#ifndef SRC_CORE_INTERFACE_H_
#define SRC_CORE_INTERFACE_H_

namespace drs {
namespace core {
/*
 * Abstract common ancestor for all interface classes.
 */
class Interface {

public:
	Interface();
	virtual ~Interface();
	virtual void initialize()=0;
	virtual void error()=0;
	virtual void close()=0;
};

} /* namespace core */
} /* namespace drs */

#endif /* SRC_CORE_INTERFACE_H_ */
