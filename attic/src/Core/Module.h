/*
 * Module.h
 *
 *  Created on: Aug 13, 2016
 *      Author: Vassil Z. Verguilov
 */

#ifndef SRC_CORE_MODULE_H_
#define SRC_CORE_MODULE_H_
#include <string>


namespace drs {
namespace core {

class Module {
public:
	typedef long HWAddress;
	Module(HWAddress Address, std::string Name="Module"):
		address(Address),name(Name){}
	inline HWAddress getAddress() const { return address; }
	inline std::string getName() const {return name; }
private:
	Module() = delete;
	HWAddress address;
	std::string name;
};


} /* namespace core */
} /* namespace drs */

#endif /* SRC_CORE_MODULE_H_ */
