/*
 * drs_command.h
 *
 *  Created on: Sep 5, 2016
 *      Author: vasoto
 */
#pragma once

#include <string>
#include <memory>
#include <map>

namespace drs {
namespace core {
namespace detail{

typedef std::shared_ptr<unsigned char> Buffer;



class command{
public:
	typedef std::shared_ptr<command> Pointer;

	command() = delete;
	command(std::string name):
	_read_only(false),
	_write_only(false),
			_name(name),
			_offset(0),
			_bit_offset(0),
			_length(0),
			_payload(nullptr)

			{};
	virtual ~command() = default;

	static Pointer create(const std::string name,
						  const unsigned address,
						  unsigned bit_offset,
						  short length,
						  Buffer payload = nullptr,
						  bool read_only = false,
						  bool write_only = false){
		command * cmd = new command(name);
		cmd->_bit_offset = bit_offset;
		cmd->_offset = address;
		cmd->_read_only = read_only;
		cmd->_write_only = write_only;
		cmd->_length = length;
		cmd->_payload = payload;
		return(Pointer(std::move(cmd)));
	}

	template<typename Interface>
	unsigned read(Interface& iface){
		auto result = iface.read(_offset, 4);
		return(0);
	};

	template<typename Interface>
	Buffer read_blk(Interface& iface){return(nullptr);}

	template<typename Interface>
	void write(Interface& iface){
		if (_read_only)
			//std::exception("Command " + _name + " is read only");
			std::cerr << "Command " << _name << " is read only!";
	}

	Buffer get_payload(){
		return _payload;
	}

	void set_payload(Buffer payload){
		_payload = payload;
	}


private:
	bool _read_only;
	bool _write_only;
	std::string _name;
	unsigned _offset;
	unsigned _bit_offset;
	short _length;
	Buffer _payload; // TODO: Buffer object
};

}

typedef std::map<std::string, detail::command::Pointer> CommandMap;
static CommandMap commands;

static void add_command(const std::string name,
						  const unsigned address,
						  unsigned bit_offset,
						  short length,
						  detail::Buffer payload = nullptr,
						  bool read_only = false,
						  bool write_only = false){
	commands[name] = detail::command::create(name,
											 address,
											 bit_offset,
											 length,
											 payload,
											 read_only,
											 write_only);
}

}
}
