/*
 * variant.h
 *
 *  Created on: Aug 25, 2016
 *      Author: Vassil Z. Verguilov
 */
#pragma once

#include <algorithm>
#include <memory>
#include <type_traits>
#include <typeinfo>

#include <Utils/debug.h>

namespace drs
{
  namespace core{
    class variant final{
    public:
        variant() : _content(nullptr){ DBG("default constructor"); }

        template <typename T>
        variant(const T & value) : _content(new inner<T>(value)){ DBG("type erasure constructor"); } //TODO(vasoto): Copy or Move?

        // Copy constructor
        variant(const variant& other) :
            _content(other._content ? other._content->clone() : nullptr) { DBG("copy constructor"); }

        // Move constructor
        variant(variant&& other) :
            _content(other._content ? std::move(other._content) : nullptr) { DBG("move constructor"); }

        // Destructor
        ~variant() = default;

        variant&
        swap(variant& rhs) {
        	DBG("");
            std::swap(_content, rhs._content);
            return *this;
        }

        template <typename T>
        variant&
        operator=(const T& rhs) {
        	DBG("const assign operator");
        	variant(rhs).swap(*this);
            return *this;
        }

        variant& operator=(variant& rhs){
        	DBG("non-const assign operator");
        	///rhs.swap(*this); // ORIGINAL CODE
        	auto tmp(rhs);
        	this->swap(tmp);
            return *this;
        }

        // Move
        variant& operator=(variant&& rhs){
        	DBG("move operator");
        	_content = std::move(rhs._content);
            return *this;
        }

        bool empty() const noexcept { DBG("");
        	return !_content; }

        const std::type_info&
        type() const noexcept {
        	DBG("");
            return (_content)? _content->type() : typeid(void);
        }

        template <typename T>
        static T*
        cast(variant* any) noexcept {
        	DBG("variant* --> T*");
        	return (any && (any->type() == typeid(T))) ?
                &static_cast<variant::inner<T> *>(any->_content.get())->_held :
				nullptr;
        }

        template <typename T>
        static T*
        unsafe_cast(variant* any) noexcept {
#warning "Does not work!"
        	DBG("variant* --> T*");
        	T * result = nullptr;
            return (any) ?
            	&static_cast<variant::inner<T> *>(any->_content.get())->_held :
            	nullptr;
        }

        template <typename T>
        static inline const T*
        cast(const variant* any){
        	DBG("const variant* --> const T*");
        	return variant::cast<T>(const_cast<variant*>(any));
        }

        template <typename T>
        static inline const T*
        unsafe_cast(const variant* any){
#warning "Does not work!"
        	DBG("const variant* --> const T*");
        	return variant::unsafe_cast<T>(const_cast<variant*>(any));
        }

        template <typename T>
        static T
        cast(variant& any){
        	DBG("variant& --> T");
        	typedef typename  std::remove_reference<T>::type non_ref;
            non_ref* result = cast<non_ref>(&any);
            if (!result)
                throw std::bad_cast();
            return *result;
        }

        template <typename T>
        static T
        unsafe_cast(variant& any){
#warning "Does not work!"
        	DBG("variant& --> T");
        	typedef typename  std::remove_reference<T>::type non_ref;
            non_ref* result = unsafe_cast<non_ref>(&any);
            if (!result)
                throw std::bad_cast();
            return *result;
        }

        template <typename T>
        static inline T
        cast(const variant& any){
        	DBG("const variant& --> T");
        	typedef typename  std::remove_reference<T>::type non_ref;
            return cast<const non_ref&>(const_cast<variant&>(any));
        }

        template <typename T>
        static inline T
        unsafe_cast(const variant& any){
#warning "Does not work!"
        	DBG("const variant& --> T");
        	typedef typename std::remove_reference<T>::type non_ref;
            return unsafe_cast<const non_ref&>(const_cast<variant&>(any));
        }

        class inner_base
        {
        public:
        	inner_base() = default;
            virtual ~inner_base() = default;

        public:
            virtual const std::type_info&
            type() const = 0;

            virtual inner_base*
            clone() const = 0;
        };

        template <typename T>
        class inner : public inner_base{
        public:
            inner(const T & value) : _held(value) { DBG(""); }
            virtual const std::type_info&
            type() const {
            	DBG("");
                return typeid(T);
            }

            virtual inner_base* clone() const {
            	DBG("");
            	return new inner(_held);
            }
            inner& operator=(const inner &) = delete;
        public:
            T _held;
        };

        std::unique_ptr<inner_base> _content;
    };
  }
}
