/*
 * DPNC342.h
 *
 * See memory map: https://wrauch.web.cern.ch/wrauch/prod_02/cern_only/unige/dpnc342_memory_map.html
 *
 *  Created on: Sep 21, 2016
 *      Author: vasoto
 */

#pragma once

#include <libusb-1.0/libusb.h>
#include <IO/USB.h>

namespace daq{
namespace modules{

struct USBDevice;

//template<typename SignaturePolicy>
struct DPNC342Module : public drs::io::usb::USBDevice {
	DPNC342Module(USBDevice& dev):USBDevice(dev){};


};

}

}
