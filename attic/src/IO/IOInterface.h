/*
 * IOInterface.h
 *
 *  Created on: Aug 10, 2016
 *      Author: Vassil Z. Verguilov
 */
#pragma once
#include <stdint.h>
#include <stddef.h>
#include <memory>

#include <Core/Interface.h>

namespace drs {
namespace core {

class IOInterface: public Interface {
public:
	typedef unsigned char * Buffer;
	typedef std::shared_ptr<IOInterface> Pointer;

	IOInterface();
	virtual ~IOInterface();

	virtual void initialize(){}
	virtual void close(){}
	virtual void error(){}

	Buffer read(uint32_t address, size_t size){

//		return nullptr;
	}

	void write(uint32_t address, Buffer buffer, size_t size){

	}


	static Pointer create(){
		IOInterface * tmp = new IOInterface();
		return(Pointer(std::move(tmp)));
	}
};

} /* namespace core */
} /* namespace drs */

