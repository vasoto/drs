/*
 * DRS.h
 *
 *  Created on: Sep 8, 2016
 *      Author: vasoto
 */
#pragma once
#include <iostream>
#include "../USB/USBModule.h"
#include "consts.h"


struct buffer_size{
	unsigned short out_buffer_size;
	unsigned short out_buffer_count;
	unsigned short in_buffer_size;
	unsigned short in_buffer_count;
};

std::ostream& operator<<(std::ostream& os, const buffer_size& bs){
	os << "\tOUT DMA Buffer Size: " << bs.out_buffer_size << std::endl
	   << "\tOUT DMA Buffer Count: " << bs.out_buffer_count << std::endl
	   << "\tIN DMA Buffer Size: " << bs.in_buffer_size << std::endl
	   << "\tIN DMA Buffer Count: " << bs.in_buffer_count << std::endl;
	return (os);
}


struct DRSModule : public drs::io::usb::USBDevice {
	DRSModule(USBDevice& dev):USBDevice(dev){};

	std::string get_firmware_version(){
		auto result = read(FIRMWARE_VERSION);
		if(!result)
			return std::string("");
		return (std::string((char*)(result)));
	}

	buffer_size get_buffer_size(){
		auto result = read(BUFFER_SIZE);
		std::cout << "Buffer size: " << result << std::endl;
		if(!result)
			return (buffer_size({0,0,0,0}));
		return (*((buffer_size*) (result)));
	}

};
