/*
 * consts.h
 *
 *  Created on: Sep 8, 2016
 *      Author: vasoto
 */
#pragma once
//General constants

static constexpr int FIRMWARE_VERSION  = 0x20;
static constexpr int BUFFER_SIZE 	   = 0x21;
static constexpr int VENDOR_ID         = 0x04b4;
static constexpr int PRODUCT_ID        = 0x00f1;

static constexpr int ENDPOINT_OUT      = 0x01;
static constexpr int ENDPOINT_IN       = 0x81;
