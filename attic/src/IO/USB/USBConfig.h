/*
 * USBConfig.h
 *
 *  Created on: Sep 20, 2016
 *      Author: vasoto
 */

#pragma once

#include <libusb-1.0/libusb.h>
#include "./utils.h"
#include "./types.h"

namespace drs{
namespace io{
namespace usb{


struct USBConfigDeleter{
	void operator()(libusb_config_descriptor *config) { libusb_free_config_descriptor(config); };

};

using drs::io::usb::detail::DevicePointer;

struct USBConfig{
	typedef std::shared_ptr<libusb_config_descriptor> ConfigPointer;
	typedef std::shared_ptr<USBConfig> Pointer;

	USBConfig() = default;

	USBConfig(const DevicePointer& device):_config(nullptr){
		libusb_config_descriptor *config;
		auto err = libusb_get_config_descriptor(device.get(), 0, &config);
		if(!is_error(err, "Cannot get device configuration.")){
			_config.reset(config, USBConfigDeleter());
		}
	}

	ConfigPointer _config;


};

}
}
}
