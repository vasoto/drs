/*
 * USBModule.h
 *
 *  Created on: Sep 7, 2016
 *      Author: vasoto
 */
#pragma once

// STD
#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>
// Third party
#include <libusb-1.0/libusb.h>

#include <Utils/cout_tools.h>
#include <Utils/type_tools.h>

#include "USBModuleImpl.h"
#include "utils.h"
#include "endpoint.h"


using drs::io::usb::detail::USBDeviceImpl;
using drs::io::usb::detail::usb_device_deleter;
using drs::io::usb::detail::usb_handle_deleter;

typedef uint32_t word;

const int USB_DEBUG_LEVEL = LIBUSB_LOG_LEVEL_DEBUG;
static const int CONTROL_REQUEST_TYPE_IN = LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE;
static const int CONTROL_REQUEST_TYPE_OUT = LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE;
static const int MAX_FX3_BULKOUT = 64;
static const int MAX_FX3_BULKIN  = 1024;
static const int BLOCKSIZE     = 64 * MAX_FX3_BULKIN; // byte
static const int TIMEOUT_C     = 1000; /* ms */
static const int TIMEOUT_I     = 1000; /* ms */
static const int TIMEOUT_O     = 1000; /* ms */
static const int TIMEOUT_RW    = 1000; /* ms */



namespace drs{
namespace io{
namespace usb{

typedef struct buffer{
	size_t size;
	union {
		void * void_p;
		unsigned char * uchar_p;
		uint8_t *  u8_p;
		uint16_t * u1_p;
		uint32_t * u32_p;
		uint64_t * u64_p;
	};
} Buffer;

struct USBDevice;
std::string get_usb_string(USBDevice*, const int);
//bool is_error(int, const std::string );


struct USBDevice {
	typedef std::shared_ptr<USBDevice> Pointer;
	USBDevice() = delete;
	USBDevice(libusb_device* dev):impl(dev){};

	~USBDevice() = default;

	USBDevice(USBDevice& dev):impl(dev.impl){}

	void close(){
		// Invalidate device handle and related structures
		impl._descriptor.reset();
		impl._handle.reset();
	};

	void open(){
		if(!is_open()){
			libusb_device_handle * handle;// = new libusb_device_handle();
			auto result = libusb_open(impl._device.get(), &handle);
			if(is_error(result, "Error: libusb_open failed") )
				return;
			impl._handle = detail::HandlePointer(std::move(handle),
												 usb_handle_deleter());
			select_endpoints();
		} else
			std::cerr << "Warning: Device is already opened!" << std::endl;

	};


	void select_endpoints(){
		if( is_open() ){
			for( auto& ep : impl.get_endpoints() ){
				if((ep->direction() == LIBUSB_ENDPOINT_IN) && (ep->transfer_type() == 2)){
					_ep_in = ep;
				} else if((ep->direction() == LIBUSB_ENDPOINT_OUT) && (ep->transfer_type() == 2)){
					_ep_out = ep;
				}
			}
		} else {
			std::cerr << "Error: Cannot select endpoints on closed device. Please call open device first." << std::endl;
		}
	}

	static Pointer create(libusb_device* dev){
		USBDevice::Pointer dev_ptr = Pointer(std::move(new USBDevice(dev)));
		return (dev_ptr);
	}


	unsigned char * read(int request){
		unsigned char data[BLOCKSIZE];
		auto result = libusb_control_transfer(impl._handle.get(),
											  CONTROL_REQUEST_TYPE_IN,
											  request,
											  0,            /* wValue: */
									          _ep_in->_endpoint->bEndpointAddress, /* wIndex: Endpoint num*/
									          data,         /* buffer to receive data */
									          32,           /* wLength*/
									          TIMEOUT_C     /* timeout millis*/
											  );
		if(is_error(result, "Error while reading using control_transfer"))
			return (nullptr);
		std::clog << "Read size: " << result << " bytes." << std::endl;
		return (std::move(data));

	}

	int read_block(Buffer& buffer) {
		// for bufsize == 0 (from request for ZLP) we adjust bufsize to 1 to avoid errors
		// was necessary for windows, we keep it here because it won't hurt
		if (!buffer.size) buffer.size++;
		int transferred = -1;
		auto result = libusb_bulk_transfer( impl._handle.get(),
											_ep_in->_endpoint->bEndpointAddress,
											buffer.uchar_p,
											buffer.size,
											&transferred,
											TIMEOUT_RW);
		if(is_error(result, "Error bulk transfer (read_block)"))
			return(0);
		return (transferred);
	}

	int write_block(Buffer& buffer){
		if (!buffer.size) buffer.size++;
		int transferred = -1;
		auto result = libusb_bulk_transfer( impl._handle.get(),
											_ep_out->_endpoint->bEndpointAddress,
											buffer.uchar_p,
											buffer.size,
											&transferred,
											TIMEOUT_RW);
		if(is_error(result, "Error bulk transfer (write_block)"))
			return(0);
		return (transferred);
	}



	void info(){
		auto descr = *impl.get_descriptor();
		std::cout << "Descriptor: " << descr << std::endl;
		auto config = impl._config->_config;
		std::cout << std::setw(8) << "Config: " << std::endl << *config << std::endl;
		std::cout<<"Interfaces: "<<(int)config->bNumInterfaces<< std::endl;

	//	const libusb_interface *inter;
//		const libusb_interface_descriptor *interdesc;
		const libusb_endpoint_descriptor *epdesc;

		for(auto& inter : impl.enumerate_interfaces()){
//		for(int i=0; i<(int)config->bNumInterfaces; i++) {
//			inter = &config->interface[i];
			std::cout << "Number of alternate settings: "
					  << inter->num_altsetting << std::endl;
			for(auto & interdesc : impl.enumerate_interface_descriptors(inter)){
//			for(int j=0; j<inter->num_altsetting; j++) {
//				interdesc = &inter->altsetting[j];

				std::cout<<"\tInterface: " << std::endl << *interdesc << std::endl;
				for(int k=0; k < (int) interdesc->bNumEndpoints; k++) {
					epdesc = &interdesc->endpoint[ k ];
					std::cout << "\tEndpoint["<<k<<"]: " << std::endl << *epdesc
					   << "\t\tEndpoint max. packet size: "
					   << libusb_get_max_packet_size(impl._device.get(), epdesc->bEndpointAddress)
					   << std::endl;

				}
			}
		}

	//			  << "Speed: "
//				  	  << libusb_get_device_speed(impl._device.get()) << std::endl
//				  << "Output endpoint max. packet size: "
//				  	  << libusb_get_max_packet_size(impl._device.get(), EP_OUT) << std::endl
//				  << "Input endpoint max. packet size: "
//				  	  << libusb_get_max_packet_size(impl._device.get(), EP_IN) << std::endl;
	//	libusb_config_descriptor * config;
//		libusb_get_active_config_descriptor(impl._device.get(), &config);
		std::cout << "Manifacturer: " << get_usb_string(this, descr.iManufacturer) << std::endl;
		std::cout << "Product: " << get_usb_string(this, descr.iProduct) << std::endl;
		std::cout << "Serial Number: " << get_usb_string(this, descr.iSerialNumber) << std::endl;

//		libusb_free_config_descriptor(config);

	}

	bool is_open(){ return (!!impl._handle);};

	detail::USBDeviceImpl impl;
	Endpoint::Pointer _ep_out, _ep_in;
};

typedef std::vector<USBDevice::Pointer> DeviceList;







}
}
}
