/*
 * USBModuleImpl.h
 *
 *  Created on: Sep 7, 2016
 *      Author: vasoto
 */
#pragma once
#include <memory>
#include "utils.h"
#include "types.h"
#include "endpoint.h"

#include "USBConfig.h"

namespace drs{
namespace io{
namespace usb{
namespace detail{

using drs::io::usb::detail::HandlePointer;
using drs::io::usb::detail::DevicePointer;
using drs::io::usb::detail::DescriptorPointer;
using drs::io::usb::detail::usb_device_deleter;
using drs::io::usb::detail::usb_handle_deleter;
using drs::io::usb::USBConfig;


//typedef std::shared_ptr<libusb_device_handle> HandlePointer;
//typedef std::shared_ptr<libusb_device> DevicePointer;
//typedef std::shared_ptr<libusb_device_descriptor> DescriptorPointer;
//

struct USBDeviceImpl{
	USBDeviceImpl(libusb_device* device):_device(libusb_ref_device(device),
												 usb_device_deleter()),
			_handle(nullptr),
			_descriptor(nullptr),
			_config(nullptr)
	{
		_config.reset( new USBConfig(_device) );
	};

	USBDeviceImpl(USBDeviceImpl& impl):_device(impl._device),
			_handle(impl._handle),
			_descriptor(impl._descriptor),
			_config(new USBConfig(impl._device))
	{};

	DevicePointer get_device(){return( _device);}
	HandlePointer get_handle(){return( _handle);}
	DescriptorPointer& get_descriptor(){
		// Lazy load - loads only once, usually when searching by VID+PID
		if(!_descriptor){
			//DEBUG("Loading libusb_device_descriptor.")
			libusb_device_descriptor* tmp = new libusb_device_descriptor();
			auto dev = _device.get();
			auto result = libusb_get_device_descriptor(dev, tmp);
			if(!is_error(result, "Error: libusb_get_device_descriptor() failed" ))
				_descriptor = DescriptorPointer(std::move(tmp));
		}
		return (_descriptor);
	}

//	libusb_config_descriptor * get_config(){
//		libusb_config_descriptor *config;
//		auto err = libusb_get_config_descriptor(impl._device.get(), 0, &config);
//		if(is_error(err, "Cannot get device configuration.")){
//			return (nullptr);
//		}
//		return (std::move(config));
	//	return impl._config.get()->_config.get();
//	}

	std::vector<libusb_interface *> enumerate_interfaces(){
		std::vector<libusb_interface *> result;
		libusb_interface *inter;
		auto config = _config.get()->_config;
		for(int i=0; i<(int)config->bNumInterfaces; i++) {
			inter = const_cast<libusb_interface *>(&config->interface[i]);
			result.emplace_back(std::move(inter));
		}
		return (std::move(result));
	}

	std::vector<libusb_interface_descriptor * > enumerate_interface_descriptors(const libusb_interface * interface){
		std::vector<libusb_interface_descriptor * > result;
		for(int j=0; j<interface->num_altsetting; j++) {
			libusb_interface_descriptor * interdesc = const_cast<libusb_interface_descriptor *>(&interface->altsetting[j]);
			result.emplace_back(std::move(interdesc));
		}
		return (std::move(result));
	}

	Endpoints get_endpoints(){
		// Lazy load
		if(!_endpoints.size()){
			for(auto& inter : enumerate_interfaces()){
				for(auto & interdesc : enumerate_interface_descriptors(inter)){
					for(int k=0; k < (int) interdesc->bNumEndpoints; k++) {
						auto ep_desc = const_cast<libusb_endpoint_descriptor *>(&interdesc->endpoint[ k ]);
					_endpoints.emplace_back(new Endpoint(ep_desc));
					}
				}
			}
		}
		return (_endpoints);
	}

	DevicePointer _device;
	HandlePointer _handle;
	DescriptorPointer _descriptor;
	USBConfig::Pointer _config;
	Endpoints _endpoints;
};

}
}
}
}
