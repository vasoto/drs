/*
 * USBSession.h
 *
 *  Created on: Sep 8, 2016
 *      Author: vasoto
 */
#pragma once
#include "./utils.h"

namespace drs{
namespace io{
namespace usb{


struct SessionDeleter{
	void operator()(libusb_context* context) { libusb_exit(context); };

};

class USBSession : public std::enable_shared_from_this<USBSession> {
public:
	typedef std::shared_ptr<libusb_context> ContextPointer;
	typedef std::shared_ptr<USBSession> Pointer;


	USBSession(){}

	static void initialize(){
		if(!_context){
			libusb_context* context = nullptr;
			auto result = libusb_init(&context);
			if (result != libusb_error::LIBUSB_SUCCESS){
				std::cerr << "Error while initializing context: "
						  << libusb_strerror(int_to_enum<libusb_error>(result)) << std::endl;
				return; // exit function and leave _context = nullptr;
			}
			libusb_set_debug(context, USB_DEBUG_LEVEL);
			// Store in a shared_ptr
			_context.reset(context, SessionDeleter());
		}
	}

	static std::shared_ptr<USBSession> create(){
		USBSession * session = new USBSession();
		if(!_context){
			session->initialize();
		}
		return(std::move(Pointer(session)));
	}

	static ContextPointer get_context(){return (_context);}
private:
	static ContextPointer _context;
};

USBSession::ContextPointer USBSession::_context;

}
}
}
