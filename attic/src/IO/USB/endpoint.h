#pragma once
#include "types.h"
#include "utils.h"
#include <memory>



namespace drs{
namespace io{
namespace usb{


using drs::io::usb::detail::EndpointPointer;

struct endpoint_deleter{
	void operator()(libusb_endpoint_descriptor* dev_handle){
		// Empty. Endpoints are managed by the libusb.
	}
};

struct Endpoint{

	typedef std::shared_ptr<Endpoint> Pointer;

	Endpoint() = default;

	Endpoint(libusb_endpoint_descriptor * endpoint):
		_endpoint(endpoint, endpoint_deleter()){}

	static Pointer create(libusb_endpoint_descriptor * endpoint){
		Endpoint * ep = new Endpoint(endpoint);
		return (std::move(Pointer(ep)));
	}

	short id(){
		return (_endpoint.get()->bEndpointAddress & 0xF); //bits 0-3
	}

	libusb_endpoint_direction direction(){
		return ((libusb_endpoint_direction)(_endpoint.get()->bEndpointAddress & 0x80)); // bit 7
	}

	short transfer_type(){
		/* Transfer Type
		    00 (0) = Control
		    01 (1) = Isochronous
		    10 (2) = Bulk
		    11 (3) = Interrupt
		*/
		return (_endpoint->bmAttributes & 0x3);
	}

	short synchronisation_type(){
		/*Synchronisation Type (Iso Mode)
    	 00 = No Synchonisation
    	 01 = Asynchronous
    	 10 = Adaptive
    	 11 = Synchronous
		 */
		return ((_endpoint->bmAttributes >> 2) & 0x3);
	}

	short usage_type(){
		/*Usage Type (Iso Mode)

		    00 = Data Endpoint
		    01 = Feedback Endpoint
		    10 = Explicit Feedback Data Endpoint
		    11 = Reserved
			*/
		return ((_endpoint->bmAttributes >> 4) & 0x3);
	}

	uint16_t maximum_packet_size(){
		return (_endpoint->wMaxPacketSize);
	}

	uint8_t poll_interval(){
		return (_endpoint->bInterval);
	}

	EndpointPointer _endpoint;
};

typedef std::vector<Endpoint::Pointer> Endpoints;

}
}
}
