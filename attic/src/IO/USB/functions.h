/*
 * functions.h
 *
 *  Created on: Sep 8, 2016
 *      Author: vasoto
 */
#pragma once
#include <algorithm>
#include <libusb-1.0/libusb.h>
#include "USBModule.h"
#include "USBSession.h"

namespace drs{
namespace io{
namespace usb{


DeviceList enumerate_devices(){
	USBSession::initialize();
	libusb_device **device_list = nullptr;
	auto dev_count = libusb_get_device_list(USBSession::get_context().get(),
											&device_list);
	// static?
	DeviceList result(dev_count, nullptr);
	std::transform(device_list,
				   device_list + dev_count,
				   result.begin(),
				   [](libusb_device* dev){
						//std::cout << "dev: " << dev << std::endl;
						return(std::move(USBDevice::create(std::move(dev))));});
	libusb_free_device_list(device_list, 0);
	return (result);
}

DeviceList find_devices(int vendor_id, int product_id){
	auto devices = enumerate_devices();
	DeviceList result;
	//TODO(vasoto): Do it with copy_if or another function
	for(auto& dev : devices){
		auto descr = *dev->impl.get_descriptor(); // loads
		if ((descr.idVendor == vendor_id) &&
			(descr.idProduct == product_id)){
			result.push_back(dev);
		}
	}
	return(result);
}

std::string get_usb_string(USBDevice* dev, const int index){
	unsigned char data[512];
	auto result = libusb_get_string_descriptor_ascii(dev->impl._handle.get(),
													 index,
													 data,
													 512);
	// Result returns the size of the chars in the string, if successful
	if( is_error(result, "Error while reading string descriptor"))
		return (std::string());

	std::string s((char*)data);
	return(s);
}

}
}
}
