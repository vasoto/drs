/*
 * policy.h
 *
 *  Created on: Sep 22, 2016
 *      Author: vasoto
 */
#pragma once

#include "USBModule.h"
#include "endpoint.h"
#include "functions.h"
#include "utils.h"
#include <exception>
#include <sstream>
#include <Utils/cout_tools.h>


class StringFormatter{
public:
	StringFormatter() = default;
    StringFormatter(const StringFormatter&) = delete;
    StringFormatter & operator= (StringFormatter &) = delete;

    ~StringFormatter() = default;

    template <typename Type>
    StringFormatter & operator << (const Type & value){
        _stream << value;
        return (*this);
    }

    std::string str() const         { return (_stream.str()); }
    operator std::string () const   { return (_stream.str()); }

    enum ConvertToString{ to_str };

    std::string operator >> (ConvertToString) { return (_stream.str()); }
private:
    std::stringstream _stream {};
};


class ModuleNotFound : public std::runtime_error{
public:
	ModuleNotFound():std::runtime_error( "Cannot find module!" ){};
	ModuleNotFound(const int pid, const int vid) : std::runtime_error( StringFormatter() << "Cannot find module "
			   << TOHEX(4) << vid << ":"
			   << TOHEX(4) << pid << TODEC() << ";" >> StringFormatter::to_str){};
};


class ModuleInstanceNotFound : public std::runtime_error{
public:
	ModuleInstanceNotFound():std::runtime_error( "Cannot find module instance!" ){};
	ModuleInstanceNotFound(const int pid, const int vid, const int instance) :
		std::runtime_error( StringFormatter() << "Cannot find instance " << instance << " of module "
			   << TOHEX(4) << vid << ":"
			   << TOHEX(4) << pid << TODEC() << ";" >> StringFormatter::to_str){};
};



template<class Device>
struct IOPolicyMock{
	int read_bulk(int where, size_t size){ return (0); };
	int write_bulk(int where, size_t size){ return (0); };
	unsigned char * buffer {nullptr};
};


template<typename SignaturePolicy,
		 class DevicePolicy, template <class> class IOPolicy>
struct Module : public SignaturePolicy, private IOPolicy<DevicePolicy> {
	using IOPolicy<DevicePolicy>::read_bulk;
	using IOPolicy<DevicePolicy>::write_bulk;
	typedef typename DevicePolicy::Pointer DevicePointer;

	Module():_device(nullptr){};

	void load_device(size_t instance = 0){
		// TODO(vasoto): Move to Device.open
		auto devices = drs::io::usb::find_devices(SignaturePolicy::VENDOR_ID, SignaturePolicy::PRODUCT_ID);
		if (!devices.size()){
			throw ModuleNotFound(SignaturePolicy::VENDOR_ID,
								 SignaturePolicy::PRODUCT_ID);
		}
		std::cout << "Found devices: " << devices.size() << std::endl;
		if(devices.size() <= instance){
			throw ModuleInstanceNotFound(SignaturePolicy::VENDOR_ID,
										 SignaturePolicy::PRODUCT_ID,
										 instance);
		}
		_device = devices[instance];
		_device->open();
//		std::cout << "Device " << _device.get() << " : " <<_device->is_open() << std::endl
//				<< "Dev: " << _device->impl._device
//				<< " Handle: " << _device->impl._handle
//				<<  std::endl
//				<< "Descriptor: " << std::endl << *_device->impl._descriptor
//				<< "Config: " << std::endl << *_device->impl._config->_config << std::endl;
//		auto ifaces = _device->impl.enumerate_interfaces();
//		drs::io::usb::Endpoints _endpoints;
//		std::cout << "Found " << ifaces.size() << " interface(s)." << std::endl;
//		for( auto& iface : ifaces ){
//			auto iface_descr = _device->impl.enumerate_interface_descriptors(iface);
//			for(auto & interdesc : iface_descr){
//				std::cout << *interdesc << std::endl;
//				for(int k=0; k < interdesc->bNumEndpoints; k++) {
//					auto ep = &interdesc->endpoint[ k ];
//					std::cout << *ep << std::endl;
//					auto ep_desc = const_cast<libusb_endpoint_descriptor *>(&interdesc->endpoint[ k ]);
//					auto EP = drs::io::usb::Endpoint::create(ep_desc);
//					std::cout << "Direction: " << EP->id() << std::endl;
//				_endpoints.emplace_back(std::move(EP));
//				}
//			}
//		}
//		auto eps = _device->impl.get_endpoints();
		//_device->select_endpoints();
	}

	DevicePointer _device;
};
