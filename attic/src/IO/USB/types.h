/*
 * types.h
 *
 *  Created on: Sep 20, 2016
 *      Author: vasoto
 */

#pragma once

namespace drs{
namespace io{
namespace usb{
namespace detail{

typedef std::shared_ptr<libusb_device_handle> HandlePointer;
typedef std::shared_ptr<libusb_device> DevicePointer;
typedef std::shared_ptr<libusb_device_descriptor> DescriptorPointer;
typedef std::shared_ptr<libusb_endpoint_descriptor> EndpointPointer;


// Deleter functor
struct usb_device_deleter{
	void operator()(libusb_device* dev) { libusb_unref_device(dev); };
};

struct usb_handle_deleter{
	void operator()(libusb_device_handle* dev_handle){ libusb_close(dev_handle); }
};


}
}
}
}

