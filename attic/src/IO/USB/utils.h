/*
 * utils.h
 *
 *  Created on: Sep 7, 2016
 *      Author: vasoto
 */
#pragma once
// STD
#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>
// Third party
#include <libusb-1.0/libusb.h>
#include <bitset>
#include <iterator>

#include <Utils/cout_tools.h>

std::ostream& operator<<(std::ostream& os, const libusb_device_descriptor& descr){
	os << "\tVendor ID: " << TOHEX(4) << descr.idVendor << TODEC() << std::endl
	   << "\tProduct ID: " << TOHEX(4) << descr.idProduct << TODEC() << std::endl
	   << "\tSize of the Descriptor in Bytes: " << int(descr.bLength) << std::endl
	   << "\tDevice Descriptor: " << int(descr.bDescriptorType) << std::endl
	   << "\tUSB Specification Number: " << int(descr.bcdUSB) << std::endl
	   << "\tClass Code: " << int(descr.bDeviceClass) << std::endl
	   << "\tSubclass Code: " << int(descr.bDeviceSubClass) << std::endl
	   << "\tProtocol Code: " << int(descr.bDeviceProtocol) << std::endl
	   << "\tMaximum Packet Size for Zero Endpoint: " << int(descr.bMaxPacketSize0) << std::endl
	   << "\tDevice Release Number: " << int(descr.bcdDevice) << std::endl
	   << "\tIndex of Manufacturer String Descriptor: " << int(descr.iManufacturer) << std::endl
	   << "\tIndex of Product String Descriptor: " << int(descr.iProduct) << std::endl
	   << "\tIndex of Serial Number String Descriptor: " << int(descr.iSerialNumber) << std::endl
	   << "\tNumber of Possible Configurations: " << int(descr.bNumConfigurations) << std::endl;
	return(os);
}

std::ostream& operator<<(std::ostream& os, const libusb_config_descriptor& config){
//	os << os << "\nInterface Descriptors: " <<  std::endl;
    os << "\tNumber of Interfaces : " << int(config.bNumInterfaces) <<  std::endl;
    os << "\tLength : " << int(config.bLength) <<  std::endl;
    os << "\tDesc_Type : " << int(config.bDescriptorType) <<  std::endl;
    os << "\tConfig_index : " << int(config.iConfiguration) <<  std::endl;
    os << "\tTotal length : " << int(config.wTotalLength) <<  std::endl;
    os << "\tConfiguration Value  : " << int(config.bConfigurationValue) <<  std::endl;
    os << "\tConfiguration Attributes : " << TOHEX(2) << int(config.bmAttributes) << TODEC() <<  std::endl;
    os << "\tMaxPower(mA) : " << int(config.MaxPower) <<  std::endl;
	return (os);
}


std::ostream& operator<<(std::ostream& os, const libusb_interface_descriptor& intd){
	os << "\tLength: " << (int)intd.bLength << std::endl
	   << "\tDescriptor Type: " << (int)intd.bDescriptorType << std::endl
	   << "\tClass: " << (int)intd.bInterfaceClass << std::endl
	   << "\tSubclass: " << (int)intd.bInterfaceSubClass << std::endl
	   << "\tNumber: " << (int)intd.bInterfaceNumber << std::endl
	   << "\tProtocol: " << (int)intd.bInterfaceProtocol << std::endl
	   << "\tEndpoints: " << (int)intd.bNumEndpoints << std::endl;
	return (os);
}


std::ostream& operator<<(std::ostream& os, const libusb_endpoint_descriptor& ep){
	std::bitset<8> attr(ep.bmAttributes);

	os << "\t\tLength: " << (int)ep.bLength << std::endl
	   << "\t\tDescriptor type: " << std::bitset<8>(ep.bDescriptorType) << std::endl
	   << "\t\tAddress: " << TOHEX(2) << (int)ep.bEndpointAddress << TODEC()
	   << " (" << std::bitset<8>(ep.bEndpointAddress) << ")" << std::endl
 	   << "\t\tInterval: " << (int)ep.bInterval << std::endl
	   << "\t\tAttributes: " << std::bitset<8>(ep.bmAttributes)
	   << " (" << TOHEX(2) << (int)ep.bmAttributes << ")" << TODEC() << std::endl;
	  // << "\t\tRefresh (for audio only): " << (int)ep.bRefresh << std::endl
	  // << "\t\tSync Address (for audio only): " << TOHEX(2) << (int)ep.bSynchAddress << TODEC() << std::endl
	  // << "\t\tExtra: " << std::string((char*)ep.extra) << std::endl;
	return (os);
}


inline std::ostream& operator<<(std::ostream& os, const libusb_endpoint_direction& dir){
	auto to_str = ( dir == LIBUSB_ENDPOINT_IN ) ? "LIBUSB_ENDPOINT_IN" : "LIBUSB_ENDPOINT_OUT";
	os << to_str;
	return (os);
}


bool is_error(int result, const std::string error_str = "Error:"){
	if(result<0){
		std::cerr << error_str << ": "
				  <<  libusb_strerror(int_to_enum<libusb_error>(result))
				  << std::endl;
		return (true);
	}
	return (false);
}


template<typename T>
inline
T create_mask(const short length, const short offset){
	return ((T)((1 << length) - 1) << offset);
}

class USBDevice;


