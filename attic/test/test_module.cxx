/*
 * test_module.cxx
 *
 *  Created on: Aug 13, 2016
 *      Author: Vassil Z. Verguilov
 */
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <type_traits>
#include "catch.hpp"
#include<Core/Module.h>

using drs::core::Module;

TEST_CASE("Test Module class", "[Module]"){
	Module test1(1);

	REQUIRE(not std::is_default_constructible<Module>::value);
	REQUIRE(test1.getAddress() == 1);
	REQUIRE(test1.getName() == "Module");

}
