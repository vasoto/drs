/*
 * test_variant.cxx
 *
 *  Created on: Aug 25, 2016
 *      Author: Vassil Z. Verguilov
 */
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include <iostream>

#include <Core/variant.h>

TEST_CASE( "Variant creation", "[variant]" ) {
	auto i = 5;
	drs::core::variant nil;
	drs::core::variant a(i);
	drs::core::variant b(a);
	drs::core::variant c(std::move(b)); // b should be empty afterwards

	REQUIRE( nil.empty() );
    REQUIRE_FALSE( a.empty() );
    REQUIRE( b.empty() );
    REQUIRE_FALSE( c.empty() );
}

TEST_CASE( "Variant assigment", "[variant]" ) {
	int i = 5;
	drs::core::variant nil;
	drs::core::variant a(i);
	drs::core::variant b(a);
	drs::core::variant c(std::move(b)); // b should be empty afterwards
	drs::core::variant d;
	drs::core::variant e;

	const float f = 42.5;
	d = a;
	e = std::move(c);
	e = f;

	REQUIRE( (!d.empty() && !a.empty()) );
    REQUIRE_FALSE( a.empty() );
    REQUIRE( c.empty() );
    REQUIRE( drs::core::variant::cast<int>(a) == 5 );
    REQUIRE( drs::core::variant::cast<float>(e) == 42.5f );
    REQUIRE( a.type() == typeid(int));
    REQUIRE( e.type() == typeid(float));
    REQUIRE( e.type() != typeid(double));
    auto j = drs::core::variant::unsafe_cast<unsigned long long>(d);
    REQUIRE(typeid(j) == typeid(unsigned long long));
    REQUIRE(j==5);
    auto k = drs::core::variant::unsafe_cast<unsigned long long>(a);
    REQUIRE(typeid(k) == typeid(unsigned long long));
//    REQUIRE(k==5);
//    auto l = drs::core::variant::unsafe_cast<double>(e);
//    REQUIRE( drs::core::variant::cast<float>(e) == 42.5f );
//    REQUIRE(typeid(l) == typeid(double));
//    REQUIRE(l==42.5);
//    b = std::string("Test string");
//    std::cout << drs::core::variant::cast<std::string>(b) << std::endl;
//    CHECK_THROWS_WITH( drs::core::variant::cast<int>(a) );

}
