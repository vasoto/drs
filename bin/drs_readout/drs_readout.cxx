/*
 * drs_readout.cxx
 *
 *  Created on: Nov 17, 2016
 *      Author: vasoto
 */
#include <iostream>
#include <string>
#include <memory>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <iostream>
#include <locale>
#include <map>
#include <vector>
#include <thread>

#include "drs/daq/core/usb/session.h"
#include "drs/daq/core/usb/usb_device.h"
#include "drs/daq/module/dpnc342/module.h"
#include "drs/daq/module/dpnc342/registers.h"

#include "drs_readout_module.h"
#include "drs_readout_cmdopt.h"
#include "drs_readout_run_manager.h"

int main(int ac, char* av[]){
	DRSReadoutCommandOptions cmd_opts;
	cmd_opts.parse(ac, av);

	// Find all devices matching the Vendor ID and Product ID
	auto devices = find_devices(cmd_opts.module_opt.vid,
								cmd_opts.module_opt.pid);
	if(devices.size()){
		std::cout << "Found " << devices.size() << " devices.\n";
		if(cmd_opts.module_opt.instance > devices.size()){
			std::cerr << "Error: instance index (" << cmd_opts.module_opt.instance
					  << ") is bigger than the device count (" << devices.size() << ")\n";
			exit(EXIT_FAILURE);
		}
		// Select specific device instance
		// TODO(vasoto): select by device ID
		USBDevice::Pointer device = devices[cmd_opts.module_opt.instance];

		DPNC342 module(device);
		module.open();
		cmd_opts.setup(module);
		module.control_reg.commit(); //commit and clean control register
		module.status_reg.load(); // load status register
		module.control_reg.load(); // load control register

		std::cout << "Iterations: " << cmd_opts.iterations << '\n';
		RunManager run_manager(cmd_opts.iterations);
		run_manager.initialize(cmd_opts.output_file);
		run_manager.run(module);
		std::cout << "Readout done.\n";
	} else {
		std::cerr << "Error: No devices ("
				  << cmd_opts.module_opt.vid << ':'
				  << cmd_opts.module_opt.pid << ") found.\n";
	}
	return (0);
}

