/*
 * drs_readout_module.h
 *
 *  Created on: Nov 17, 2016
 *      Author: vasoto
 */

#pragma once

#include "drs/daq/core/usb/session.h"
#include "drs/daq/core/usb/usb_device.h"
#include "drs/daq/module/dpnc342/module.h"
#include "drs/daq/module/dpnc342/registers.h"


using drs4::daq::module::dpnc342::DPNC342Module;
using drs4::daq::module::dpnc342::ControlRegister;
using drs4::daq::module::dpnc342::StatusRegister;
using drs4::daq::core::usb::USBDevice;
using drs4::daq::core::usb::find_devices;

template<class MSignature, class DevicePolicy>
struct Module: public MSignature, public DevicePolicy{
	typedef typename DevicePolicy::Pointer DevPtr;
	Module(DevPtr dev ):
		MSignature(this),
		DevicePolicy(*dev)
	{}
};

typedef Module<DPNC342Module, USBDevice> DPNC342;

// TODO(vasoto): Find module


//EOF
