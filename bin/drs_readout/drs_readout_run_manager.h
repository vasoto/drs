/*
 * drs_readout_run_manager.h
 *
 *  Created on: Nov 21, 2016
 *      Author: vasoto
 */

#pragma once

#include <TTree.h>
#include <TFile.h>

#include "drs_readout_module.h"
typedef Module<DPNC342Module, USBDevice> DPNC342;


struct TFileDeleter{
	void operator()(TFile * file) {
		std::string name("");
		std::cout << "Closing file " << name << '\n';
		file->Close();
		delete file;
		std::cout << "TFile object " << name << " deleted...\n";
	};
};

struct TTreeDeleter{
	void operator()(TTree * tree){
		std::string name("");
		std::cout << "Writing tree \""<< name <<"\".\n";
//		tree->Write();
		tree->AutoSave();
		delete tree;
		std::cout << name << "tree deleted.\n";
	}
};


struct RunManager{
	RunManager() = delete;
	explicit RunManager(size_t t_iterations):
						iterations(t_iterations)
	{
		for(unsigned i = 0; i < 32; ++i)
			cells[i] = new unsigned short[32 * 1024];
	}

	void initialize(const std::string file_name){
          std::cout << "File name is '" << file_name << "';\n";
          data_file.reset(new TFile(file_name.c_str(), "RECREATE"), TFileDeleter());
	}

	void exec_step(DPNC342& module){
		// Create new tree
		init_dataset(module);
//		std::cout << "Taking data with " << step_voltage << "V (" <<  module.control_reg.DAC4.get() <<")\n";
		size_t iteration {0};
		while(iteration < iterations){
			exec_iteration(module, iteration++);
		}
	}

	void init_dataset(DPNC342& module){
		dataset.reset(new TTree("DPNC342_Data", "Calibration data"), TTreeDeleter());
		// Create the branches
		std::stringstream ss;
		std::string name;
		std::string leaf_type;
		for(unsigned i = 0; i < module.MaxChannels; ++i){
			//TODO: Ugly! do it better!!!
			get_branch_name(name, leaf_type, "Channel", i, "[1024]/s");
			dataset->Branch(name.c_str(),
							cells[i],
							leaf_type.c_str());
		}

		get_branch_name(name, leaf_type, "Temperature", -1, "[4]/I");
		std::cout << name << " " << leaf_type << '\n';
		dataset->Branch(name.c_str(), &temperature, leaf_type.c_str());
		get_branch_name(name, leaf_type, "StopCell", -1, "[4]/I");
		dataset->Branch(name.c_str(), &stop_cell, leaf_type.c_str());
		get_branch_name(name, leaf_type, "StopWRS", -1, "[4]/I");
		dataset->Branch(name.c_str(), &stop_wrs, leaf_type.c_str());
		get_branch_name(name, leaf_type, "PLLLock", -1, "[4]/I");
		dataset->Branch(name.c_str(), &pll_lock, leaf_type.c_str());
		//
//		for(unsigned b=0; b < module.BoardCount; ++b){
//			get_branch_name(name, leaf_type, "Temperature", b, "/I");
//			std::cout << name << " " << leaf_type << '\n';
//			dataset->Branch(name.c_str(), temperature[b], leaf_type.c_str());
//			get_branch_name(name, leaf_type, "StopCell", b, "/I");
//			dataset->Branch(name.c_str(), stop_cell[b], leaf_type.c_str());
//			get_branch_name(name, leaf_type, "StopWRS", b, "/I");
//			dataset->Branch(name.c_str(), stop_wrs[b], leaf_type.c_str());
//			get_branch_name(name, leaf_type, "PLLLock", b, "/I");
//			dataset->Branch(name.c_str(), pll_lock[b], leaf_type.c_str());
//		}
		dataset->Branch("ADCPhase", &adc_phase, "ADCPhase/I");
	}

	void get_branch_name(std::string& name,
			std::string& leaf_type,
			const std::string prefix,
			const int index,
			const std::string type_name){
		std::stringstream ss;
		ss << prefix;
		if(index >= 0)
			ss << index;
		name = ss.str();
		ss << type_name;
		leaf_type = ss.str();
	}

	bool wait_for_data(DPNC342& module){
		const unsigned max_laps = 200000; // 10 seconds timeout
		const unsigned wait_time_ns = 50;
		unsigned laps(0);
		module.status_reg.load();
	//	std::cout << "Module is " << ((module.status_reg.stat_idle.get())?"idle":"not idle") << '\n';
		while( !module.status_reg.stat_idle && (laps < max_laps) ){
	//		std::cout << "Module is " << ((module.status_reg.stat_idle.get())?"idle":"not idle");
	//		std::cout << " laps: " << laps << " Max laps: " << max_laps << '\n';
			std::this_thread::sleep_for(std::chrono::nanoseconds(wait_time_ns));
			module.status_reg.load(); // Load status register from the device;
			++laps;
		}
		//std::cout << "Waited for " << laps * wait_time_ns << " ns.\n";
		return (laps < max_laps);
	}

	void transfer_data(DPNC342& module){
		module.memory.read_all();
		for(unsigned i = 0; i < 32; ++i)
			std::copy(module.memory.buffer.begin() + i * 1024,
					  module.memory.buffer.end(),
					  cells[i]);

		module.status_reg.load();
		module.control_reg.load();
		temperature[0] = module.status_reg.temperature_0.get();
		temperature[1] = module.status_reg.temperature_1.get();
		temperature[2] = module.status_reg.temperature_2.get();
		temperature[3] = module.status_reg.temperature_3.get();

		stop_cell[0] = module.status_reg.stop_cell_0.get();
		stop_cell[1] = module.status_reg.stop_cell_1.get();
		stop_cell[2] = module.status_reg.stop_cell_2.get();
		stop_cell[3] = module.status_reg.stop_cell_3.get();

		stop_wrs[0] = module.status_reg.stop_wrs_0.get();
		stop_wrs[1] = module.status_reg.stop_wrs_1.get();
		stop_wrs[2] = module.status_reg.stop_wrs_2.get();
		stop_wrs[3] = module.status_reg.stop_wrs_3.get();

		pll_lock[0] = module.status_reg.plllck0.get();
		pll_lock[1] = module.status_reg.plllck1.get();
		pll_lock[2] = module.status_reg.plllck2.get();
		pll_lock[3] = module.status_reg.plllck3.get();

		adc_phase = module.control_reg.adcclk_phase.get();
	}




	void read_data(DPNC342& module){
		module.control_reg.start_trig = 1;
		module.control_reg.commit();
		module.control_reg.start_trig = 0;
		// Send a software trigger
		module.control_reg.soft_trig = 1;
		module.control_reg.commit();
		module.control_reg.soft_trig = 0;
		wait_for_data(module);
		transfer_data(module);

	}

	void exec_iteration(DPNC342& module, const size_t iteration_num){
//		if( (iterations % iteration_num) == 0)
//			std::cout << '.';
		read_data(module);
		dataset->Fill();
	}

	void run(DPNC342& module){
		exec_step(module);
	}

	size_t iterations;
//	double min_voltage, max_voltage, step;
	std::shared_ptr<TFile> data_file {nullptr}; // This should be declared before all trees as the objects are deleted in reverse order of declaration
	std::shared_ptr<TTree> dataset {nullptr};
	std::shared_ptr<TTree> conditions {nullptr};
	unsigned short * cells[32] {nullptr};//[32 * 1024];
	int temperature[4] {0, 0, 0, 0};
	int stop_cell[4] {0, 0, 0, 0};
	int stop_wrs[4] {0, 0, 0, 0};
	int pll_lock[4] {0, 0, 0, 0};
	int adc_phase {0};
};

//void init_module(DPNC342& module){
//	//Disconnect analog inputs
//	module.control_reg.load();
//	module.control_reg.start_trig = 0;
//	module.control_reg.reinit_trig = 1;
//	module.control_reg.soft_trig = 0;
//	module.control_reg.commit();
//
//	module.control_reg.clear();
//	// Begin configuration
//	module.control_reg.dactive = 1;
//	module.control_reg.trigger_config = 0x1F; // 0x1F --> all trigger sources 0x10 --> front panel trigger only
////	module.control_reg.tca_ctrl = 0;
//	module.control_reg.enable_trigger = 1;
//	module.control_reg.acalib = 1;
//	module.control_reg.DAC4 = 0x0000;
////	module.control_reg.drs_ctl_first_chn = 0;
//	module.control_reg.drs_ctl_last_chn = 7;
//	module.control_reg.channel_config = 0xFF;
//	module.control_reg.dmode = 1;
//	module.control_reg.pllen = 1;
////	module.control_reg.wrsloop = 0;
//	module.control_reg.drs_ctl_config_7_3 = 0x1F; // five MSB reserved, must be all ones
//	module.control_reg.commit();
//	std::cout << module.control_reg << std::endl;
//	// End of configuration
//}


//EOF
