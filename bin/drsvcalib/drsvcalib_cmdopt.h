/*
 * drs_readout_cmdopt.h
 *
 *  Created on: Nov 17, 2016
 *      Author: vasoto
 */

#pragma once
#include <iostream>
#include <string>

#include <boost/config.hpp>
#include <boost/program_options/detail/config_file.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options.hpp>

#include "drs_readout_module.h"

namespace pod = boost::program_options::detail;
namespace po = boost::program_options;

using std::string;

/*
 * 1.) Define command line options and their references to control register values
 * 2.) Parse command line options
 * 3.) Apply control register values
 */

struct DPNC342ModuleOptions{
	po::options_description options;
	unsigned pid {DPNC342::PRODUCT_ID};
	unsigned vid {DPNC342::VENDOR_ID};
	unsigned instance {0};
	bool readout_mode;
	DPNC342ModuleOptions():options("Module Options"){
		options.add_options()
		("product-id",
				po::value<unsigned>(&pid)->default_value(pid),
				"DPNC342 Product id (should not change)" )
		("vendor-id",
				po::value<unsigned>(&vid)->default_value(vid),
				"DPNC342 vendor id (should not change)")
		("instance",
			po::value<unsigned>(&instance)->default_value(0),
			"Module instance (if more than one module is connected)")
		//TODO(vasoto): support for multiple modules
		("readout-mode,r",
			po::value<bool>(&readout_mode)->default_value(false),
			"Readout mode (0) start from first cell or (1) start from domino stop")
		;
	}
};

struct DPNC342VoltageOptions{
	po::options_description options;
	double voltage;
	bool enable_vcal;
	DPNC342VoltageOptions():options("Voltage Options"){
		options.add_options()
		("calibration-voltage-value",
				po::value<double>(&voltage)->default_value(0.0),
				"Set calibration voltage (DAC4)" )
		("voltage-calibration",
				po::value<bool>(&enable_vcal)->default_value(false),
				"Disable(0)/enable(1) voltage calibration")
		;
	}
};


struct DPNC342TriggerOptions{
	po::options_description options;
	bool domino_active;
	int trigger_config;
	double trigger_level;
	bool ext_trigger;
	DPNC342TriggerOptions():options("Trigger Options"){
		options.add_options()
		("trigger-level",
				po::value<double>(&trigger_level)->default_value(0.0),
				 "Set trigger level voltage (DAC0-3)" )
		("domino-active",
				po::value<bool>(&domino_active)->default_value(true),
				"Domino wave active(1)/non-active(0) during readout")
		("trigger-config",
				po::value<int>(&trigger_config)->default_value(0x10),
				"Trigger configuration")
		("enable-ext-trigger",
				po::value<bool>(&ext_trigger)->default_value(true),
				"Disable(0)/enable(1) external trigger")
		;
	}
};


struct DRSReadoutCommandOptions{

//	static po::options_description generic;

	po::options_description options;

	DPNC342VoltageOptions voltage_opt {};
	DPNC342TriggerOptions trigger_opt {};
	DPNC342ModuleOptions  module_opt {};
	std::string output_file {""};
	size_t iterations {0};

	explicit DRSReadoutCommandOptions(): options("Configuration") {
		initialize();
	}

	void initialize(){
		po::options_description generic("Generic options");
		po::options_description config("Configuration");

		generic.add_options()
		            ("version,v",
					po::value<std::string>()
							->implicit_value("")
							->notifier(
								[=](const std::string& version) {
									std::cout << "Version 0.0.0\n";
								}
							),
		            		"print version string")
		            ("config,c", po::value<string>(&config_file)->default_value("drs_config.cfg"),
		                  "name of a file of a configuration.")
				    ("help,h",
				    		po::value<std::string>()
								->implicit_value("")
								->notifier(
							[=](const std::string& topic) {
								show_help(generic, topic);
							}
						),
					"Show help. If given, show help on the specified topic."
				    )
		;

		config.add_options()
				("iterations,i",
						po::value<size_t>(&iterations)->default_value(1000),
						"Number of iterations")
				("output-file,o",
						po::value<string>(&output_file)->default_value("output.root"),
								                  "Name of a file for data output.")

		;
		config.add(voltage_opt.options).add(trigger_opt.options).add(module_opt.options);
		options.add(generic).add(config);
	}


	void parse(int ac, char* av[]){
		po::variables_map args;

		if (ac == 1) {
			show_help(options); // does not return
		}

		try {
			po::store(
				po::parse_command_line(ac, av, options),
				args
			);
		}
		catch (po::error const& e) {
			std::cerr << e.what() << '\n';
			exit( EXIT_FAILURE );
		}
		po::notify(args);
		return;
	}

	void setup(DPNC342& module){
		// Set the same trigger level for all 4 DRS4 chips
		module.set_voltage(trigger_opt.trigger_level, 0);
		module.set_voltage(trigger_opt.trigger_level, 1);
		module.set_voltage(trigger_opt.trigger_level, 2);
		module.set_voltage(trigger_opt.trigger_level, 3);
		// Set Voltage calibration value
		module.set_voltage(voltage_opt.voltage, 4);
		// Enable or disable voltage calibration
		module.control_reg.acalib = voltage_opt.enable_vcal;

		module.control_reg.trigger_config = trigger_opt.trigger_config;
		module.control_reg.readout_mode = module_opt.readout_mode;
		module.control_reg.enable_trigger = trigger_opt.ext_trigger;
		module.control_reg.drs_ctl_last_chn = 7;
		module.control_reg.channel_config = 0xFF;
		module.control_reg.dmode = 1;
		module.control_reg.pllen = 1;
	//	module.control_reg.wrsloop = 0;
		module.control_reg.drs_ctl_config_7_3 = 0x1F; // five MSB reserved, must be all ones
	}

private:
	void show_help(const po::options_description& desc, const std::string& topic = "") {
	    std::cout << desc << '\n';
	    if (topic != "") {
	        std::cout << "You asked for help on: " << topic << '\n';
	    }
	    exit( EXIT_SUCCESS );
	}
	// This is internal setting
	std::string config_file {""};
};


//void show_help(const po::options_description& desc, const std::string& topic = "") {
//    std::cout << desc << '\n';
//    if (topic != "") {
//        std::cout << "You asked for help on: " << topic << '\n';
//    }
//    exit( EXIT_SUCCESS );
//}
//
//
//void define_generic(po::options_description& config){
////	config.add_options()
////		   ("optimization", po::value<int>(&opt)->default_value(10),
////		  "optimization level")
//}
//
//
//
//void process_program_options(const int argc, const char *const argv[])
//{
//	po::options_description desc("Usage");
//	    desc.add_options()
//	        (
//	            "help,h",
//	            po::value<std::string>()
//	                ->implicit_value("")
//	                ->notifier(
//	                    [&desc](const std::string& topic) {
//	                        show_help(desc, topic);
//	                    }
//	                ),
//	            "Show help. If given, show help on the specified topic."
//	        )
//	    ;
//
//	    if (argc == 1) {
//	        show_help(desc); // does not return
//	    }
//
//	    po::variables_map args;
//
//	    try {
//	        po::store(
//	            po::parse_command_line(argc, argv, desc),
//	            args
//	        );
//	    }
//	    catch (po::error const& e) {
//	        std::cerr << e.what() << '\n';
//	        exit( EXIT_FAILURE );
//	    }
//	    po::notify(args);
//	    return;
//}



//EOF
