#include "drs_calibrate.h"

DrsCalibrate::DrsCalibrate() : fNumEntries(0), fNumSamples(0), fNumChannels(0){
	fSum.reserve(1024*1440);
	fSum2.reserve(1024*1440);
}


void DrsCalibrate::Reset(){
    fNumEntries  = 0;
    fNumSamples  = 0;
    fNumChannels = 0;

    fSum.clear();
    fSum2.clear();
}

void DrsCalibrate::InitSize(uint16_t channels, uint16_t samples){
    fNumChannels = channels;
    fNumSamples  = samples;

    fSum.assign(samples*channels, 0);
    fSum2.assign(samples*channels, 0);
}

void DrsCalibrate::AddRel(const int16_t *val, const int16_t *start)
{
    /*
    for (size_t ch=0; ch<fNumChannels; ch++)
    {
        const int16_t &spos = start[ch];
        if (spos<0)
            continue;

        const size_t pos = ch*1024;
        for (size_t i=0; i<1024; i++)
        {
            // Value is relative to trigger
            // Abs is corresponding index relative to DRS pipeline
            const size_t rel = pos +  i;
            const size_t abs = pos + (spos+i)%1024;

            const int64_t v = val[rel];

            fSum[abs]  += v;
            fSum2[abs] += v*v;
        }
    }*/

    // This version is 2.5 times faster because the compilers optimization
    // is not biased by the evaluation of %1024
    for (size_t ch=0; ch<fNumChannels; ch++)
    {
        const int16_t &spos = start[ch];
        if (spos<0)
            continue;

        const size_t pos = ch*1024;

        const int16_t *beg_val  = val          + pos;
        int64_t       *beg_sum  = fSum.data()  + pos;
        int64_t       *beg_sum2 = fSum2.data() + pos;

        const int16_t *pval  = beg_val;          // val[rel]
        int64_t       *psum  = beg_sum  + spos;  // fSum[abs]
        int64_t       *psum2 = beg_sum2 + spos;  // fSum2[abs]

        while (psum<beg_sum+1024)
        {
            const int64_t v = *pval++;

            *psum++  += v;
            *psum2++ += v*v;
        }

        psum  = beg_sum;
        psum2 = beg_sum2;

        while (pval<beg_val+1024){
            const int64_t v = *pval++;

            *psum++  += v;
            *psum2++ += v*v;
        }
    }
    fNumEntries++;
}



void DrsCalibrate::AddRel(const int16_t *val,    const int16_t *start,
            const int32_t *offset, const int64_t scale)
{
    /*
    for (size_t ch=0; ch<fNumChannels; ch++)
    {
        const int16_t spos = start[ch];
        if (spos<0)
            continue;

        const size_t pos = ch*1024;

        for (size_t i=0; i<fNumSamples; i++)
        {
            // Value is relative to trigger
            // Offset is relative to DRS pipeline
            // Abs is corresponding index relative to DRS pipeline
            const size_t rel = pos +  i;
            const size_t abs = pos + (spos+i)%1024;

            const int64_t v = int64_t(val[rel])*scale-offset[abs];

            fSum[abs]  += v;
            fSum2[abs] += v*v;
        }
    }*/

    // This version is 2.5 times faster because the compilers optimization
    // is not biased by the evaluation of %1024
    for (size_t ch=0; ch<fNumChannels; ch++)
    {
        const int16_t &spos = start[ch];
        if (spos<0)
            continue;

        const size_t pos = ch*1024;

        const int16_t *beg_val    = val          + pos;
        const int32_t *beg_offset = offset       + pos;
        int64_t       *beg_sum    = fSum.data()  + pos;
        int64_t       *beg_sum2   = fSum2.data() + pos;


        const int16_t *pval    = beg_val;            // val[rel]
        const int32_t *poffset = beg_offset + spos;  // offset[abs]
        int64_t       *psum    = beg_sum    + spos;  // fSum[abs]
        int64_t       *psum2   = beg_sum2   + spos;  // fSum2[abs]

        while (psum<beg_sum+1024)
        {
            const int64_t v = int64_t(*pval++)*scale - *poffset++;

            *psum++  += v;
            *psum2++ += v*v;
        }

        psum    = beg_sum;
        psum2   = beg_sum2;
        poffset = beg_offset;

        while (pval<beg_val+1024)
        {
            const int64_t v = int64_t(*pval++)*scale - *poffset++;

            *psum++  += v;
            *psum2++ += v*v;
        }
    }
    fNumEntries++;
}



void DrsCalibrate::AddAbs(const int16_t *val,    const int16_t *start,
            const int32_t *offset, const int64_t scale)
{
    /*
    // 1440 without tm, 1600 with tm
    for (size_t ch=0; ch<fNumChannels; ch++)
    {
        const int16_t spos = start[ch];
        if (spos<0)
            continue;

        const size_t pos = ch*fNumSamples;
        const size_t drs = ch>1439 ? ((ch-1440)*9+8)*1024 : ch*1024;

        for (size_t i=0; i<fNumSamples; i++)
        {
            // Value is relative to trigger
            // Offset is relative to DRS pipeline
            // Abs is corresponding index relative to DRS pipeline
            const size_t rel = pos +  i;
            const size_t abs = drs + (spos+i)%1024;

            const int64_t v = int64_t(val[rel])*scale-offset[abs];

            fSum[rel]  += v;
            fSum2[rel] += v*v;
        }
        }*/

    // This version is 1.5 times faster because the compilers optimization
    // is not biased by the evaluation of %1024
    for (size_t ch=0; ch<fNumChannels; ch++)
    {
        const int16_t &spos = start[ch];
        if (spos<0)
            continue;

        const size_t pos = ch*fNumSamples;

        const int32_t *beg_offset = offset       + ch*1024;
        const int16_t *beg_val    = val          + pos;
        int64_t *beg_sum          = fSum.data()  + pos;
        int64_t *beg_sum2         = fSum2.data() + pos;


        const int16_t *pval    = beg_val;             // val[rel]
        const int32_t *poffset = beg_offset + spos;   // offset[abs]
        int64_t *psum          = beg_sum;             // fSum[rel]
        int64_t *psum2         = beg_sum2;            // fSum2[rel]

        if (spos+fNumSamples>1024)
        {
            while (poffset < beg_offset + 1024)
            {
                const int64_t v = int64_t(*pval++)*scale - *poffset++;
                *psum++  += v;
                *psum2++ += v*v;
            }
            poffset = beg_offset;
        }

        while (psum<beg_sum+fNumSamples)
        {
            const int64_t v = int64_t(*pval++)*scale - *poffset++;

            *psum++  += v;
            *psum2++ += v*v;
        }
    }
    fNumEntries++;
}



static void DrsCalibrate::ApplyCh(float *vec, const int16_t *val, int16_t start, uint32_t roi,
                    const int32_t *offset, const int64_t scaleabs,
                    const int64_t *gain,   const int64_t scalegain){
    if (start<0)
    {
        memset(vec, 0, roi);
        return;
    }

    // This version is faster because the compilers optimization
    // is not biased by the evaluation of %1024
    // (Here we are dominated by numerics... improvement ~10%)
    const int32_t *poffset = offset + start; // offset[abs]
    const int64_t *pgain   = gain   + start; // gain[abs]
    const int16_t *pval    = val;            // val[rel]
    float         *pvec    = vec;            // vec[rel]

    if (start+roi>1024){
        while (poffset < offset + 1024){
            const int64_t v = + int64_t(*pval++)*scaleabs - *poffset++;
            *pvec++ = *pgain==0 ? 0 : double(v)*scalegain / *pgain;
            pgain++;
        }
        poffset = offset;
        pgain   = gain;
    }

    while (pvec < vec + roi){
        const int64_t v = + int64_t(*pval++)*scaleabs - *poffset++;
        *pvec++ = *pgain==0 ? 0 : double(v)*scalegain / *pgain;
        pgain++;
    }
}


static void DrsCalibrate::ApplyCh(float *vec, const int16_t *val, int16_t start, uint32_t roi,
                    const int32_t *offset, const int64_t scaleabs,
                    const int64_t *gain,   const int64_t scalegain,
                    const int64_t *trgoff, const int64_t scalerel)
{
    if (start<0){
        memset(vec, 0, roi);
        return;
    }

    // (Here we are dominated by numerics... improvement ~10%)
    const int32_t *poffset = offset + start; // offset[abs]
    const int64_t *pgain   = gain   + start; // gain[abs]
    const int16_t *pval    = val;            // val[rel]
    const int64_t *ptrgoff = trgoff;         // trgoff[rel]
    float         *pvec    = vec;            // vec[rel]

    if (start+roi>1024)
    {
        while (poffset<offset+1024){
            const int64_t v =
                + (int64_t(*pval++)*scaleabs - *poffset++)*scalerel
                - *ptrgoff++;
            const int64_t div = *pgain * scalerel;
            *pvec++ = div==0 ? 0 : double(v)*scalegain / div;
            pgain++;
        }
        poffset = offset;
        pgain   = gain;
    }

    while (pvec < vec + roi){
        const int64_t v =
            + (int64_t(*pval++)*scaleabs - *poffset++)*scalerel
            - *ptrgoff++;
        const int64_t div = *pgain * scalerel;
        *pvec++ = div==0 ? 0 : double(v)*scalegain / div;
        pgain++;
    }
}



static double DrsCalibrate::FindStep(const size_t ch0, const float *vec, int16_t roi, const int16_t pos, const uint16_t *map=nullptr){
    // We have about 1% of all cases which are not handled here,
    // because the baseline jumps up just before the readout window
    // and down just after it. In this cases we could determine the jump
    // from the board time difference or throw the event away.
    if (pos == 0 || pos >= roi)
        return 0;

    double step = 0; // before
    double rms  = 0; // before
    int    cnt  = 0;

    // Exclude TM channel
    for (int p=0; p<8; ++p){
        const size_t hw = ch0 + p;
        const size_t sw = (map ? map[hw] : hw ) * roi + pos;
        const double diff = vec[sw]-vec[sw-1];

        step += diff;
        rms  += (vec[sw]-vec[sw-1])*(vec[sw]-vec[sw-1]);
        ++cnt;
    }
    return cnt == 0 ? 0 : step/cnt;
}




static void DrsCalibrate::SubtractStep(const size_t ch0,
		const double avg, float *vec,
		int16_t roi, int32_t pos,
		const uint16_t *map=nullptr){
    if (pos == 0 || pos >= roi)
        return;

    const int begin = avg > 0 ? pos :   0;
    const int end   = avg > 0 ? roi : pos;

    const double sub = fabs(avg);

    for (int p = 0; p < 9; ++p){
        for (int j = begin; j < end; ++j){
            const size_t hw = ch0 + p;
            const size_t sw = (map ? map[hw] : hw) * roi + j;
            vec[sw] -= sub;
        }
    }
}



static DrsCalibrate::Step DrsCalibrate::AverageSteps(const StepsIterator beg, const StepsIterator end)
{
	DrsCalibrate::Step rc;
    for (auto it=beg; it!=end; it++){
        rc.pos += it->pos;
        rc.avg += it->avg;
        rc.rms += it->avg*it->avg;
    }
    rc.cnt = end - beg;
    rc.pos /= rc.cnt;
    rc.avg /= rc.cnt;
    rc.rms /= rc.cnt;
    rc.rms -= rc.avg*rc.avg;
    rc.rms  = rc.rms<0 ? 0 : sqrt(rc.rms);
    return rc;
}



static DrsCalibrate::Step DrsCalibrate::CorrectStep(float *vec, uint16_t nch, uint16_t roi,
                        const int16_t *prev, const int16_t *start,
                        const int16_t offset, const uint16_t *map=nullptr)
{

    Steps list;
    list.reserve(nch);

    // Fill steps into array
    // Exclude broken pixels?
    // Remove maximum and minimum patches (4max and 4min)?
    for (size_t ch=0; ch<nch; ch += 9)
    {
        if (prev[ch]<0 || start[ch]<0)
            continue;

        const int16_t dist = (prev[ch]-start[ch]+1024+offset)%1024;
        const double  step = FindStep(ch, vec, roi, dist, map);
        if (step==0)
            continue;

        DrsCalibrate::Step rc;
        rc.pos = dist;
        rc.avg = step;
        list.push_back(rc);
    }

    if (list.empty())
        return DrsCalibrate::Step();

    Step rc = AverageSteps(list.begin(), list.begin()+list.size());;

    if (rc.avg==0)
        return DrsCalibrate::Step();

    // std::cout << "   A0=" << rc.avg << "    rms=" << rc.rms << std::endl;
    if (rc.rms>5)
    {
        sort(list.begin(), list.end(), Step::sort);

        //for (auto it=list.begin(); it!=list.end(); it++)
        //    std::cout << "     " << it->avg << std::endl;

        const size_t skip = list.size()/10;
        rc = AverageSteps(list.begin()+skip, list.begin()+list.size()-skip);

        // std::cout << "   A1=" << rc.avg << "    rms=" << rc.rms << std::endl;
    }

    for (size_t ch=0; ch<nch; ch += 9){
        const int16_t dist = (prev[ch]-start[ch]+1024+offset)%1024;
        SubtractStep(ch, rc.avg, vec, roi, dist, map);
    }
    return rc;
}



static void DrsCalibrate::RemoveSpikes(float *p, uint32_t roi){
    if (roi<4)
        return;
    for (size_t i=1; i<roi-2; i++){
        if (p[i]-p[i-1]>25 && p[i]-p[i+1]>25)
            p[i] = (p[i-1]+p[i+1])/2;
        if (p[i] - p[i-1] > 22 && fabs(p[i] - p[i+1]) < 4 && p[i+1]- p[i+2] > 22){
            p[i] = (p[i-1] + p[i+2]) / 2;
            p[i+1] = p[i];
        }
    }
}




static void DrsCalibrate::RemoveSpikes2(float *p, uint32_t roi){//from Werner
    if (roi<4)
        return;
    std::vector<float> Ameas(p, p+roi);
    std::vector<float> diff(roi);
    for (size_t i=1; i<roi-1; i++)
        diff[i] = (p[i-1] + p[i+1])/2 - p[i];
    const float fract = 0.8;
    for (size_t i=0; i<roi-3; i++){
        if (diff[i]<5)
            continue;
        if (Ameas[i+2] - (Ameas[i] + Ameas[i+3])/2 > 10){
            p[i+1]=   (Ameas[i+3] - Ameas[i])/3 + Ameas[i];
            p[i+2]= 2*(Ameas[i+3] - Ameas[i])/3 + Ameas[i];
            i += 3;
            continue;
        }

        if ( (diff[i+1]<-diff[i]*fract*2) && (diff[i+2]>10) ){
            p[i+1]    = (Ameas[i]+Ameas[i+2])/2;
            diff[i+2] = (p[i+1] + Ameas[i+3])/2 - Ameas[i+2];

            i += 2;
        }
    }
}



static void DrsCalibrate::RemoveSpikes3(float *vec, uint32_t roi)//from Werner
{
    if (roi<4)
        return;

    const float SingleCandidateTHR = -10.;
    const float DoubleCandidateTHR =  -5.;

    const std::vector<float> src(vec, vec+roi);

    std::vector<float> diff(roi);
    for (size_t i=1; i<roi-1; i++)
        diff[i] = src[i] - (src[i-1] + src[i+1])/2;

    // find the spike and replace it by mean value of neighbors
    for (unsigned int i=1; i<roi-3; i++){
        // Speed up (no leading edge)
        if (diff[i]>=DoubleCandidateTHR)
            continue;

        // a single spike candidate
        if (diff[i]<SingleCandidateTHR){
            // check consistency with a single channel spike
            if (diff[i+1] > -1.6*diff[i]){
                vec[i+1] = (src[i] + src[i+2]) / 2;
                i += 2;
                /*** NEW ***/
                continue;
                /*** NEW ***/
            }
            /*
            else
            {
                // do nothing - not really a single spike,
                // but check if it is a double
                checkDouble = true;
            }*/
        }

        // a double spike candidate
        //if (diff[i]>DoubleCandidateTHR || checkDouble == 1)
        {
            // check the consistency with a double spike
            if ((diff[i+1] > -DoubleCandidateTHR) &&
                (diff[i+2] > -DoubleCandidateTHR))
            {
                vec[i+1] =   (src[i+3] - src[i])/3 + src[i];
                vec[i+2] = 2*(src[i+3] - src[i])/3 + src[i];

                //do not care about the next sample it was the spike
                i += 3;
            }
        }
    }
}



static void DrsCalibrate::RemoveSpikes4(float *ptr, uint32_t roi){
    if (roi<7)
        return;

    for (uint32_t i=0; i<roi-6; i++)
    {
        double d10, d21, d32, d43, d54;

        // ============================================
        d43 = ptr[i+4]-ptr[i+3];
        d54 = ptr[i+5]-ptr[i+4];

        if ((d43>35 && -d54>35) || (d43<-35 && -d54<-35))
        {
            ptr[i+4] = (ptr[i+3]+ptr[i+5])/2;
        }

        // ============================================
        d32 = ptr[i+3]-ptr[i+2];
        d54 = ptr[i+5]-ptr[i+4];

        if ((d32>9   && -d54>13  && d32-d54>31)/* || (d32<-13 && -d54<-13 && d32+d54<-63)*/)
        {
            double avg0 = (ptr[i+2]+ptr[i+5])/2;
            double avg1 = (ptr[i+3]+ptr[i+4])/2;

            ptr[i+3] = ptr[i+3] - avg1+avg0;
            ptr[i+4] = ptr[i+4] - avg1+avg0;
        }

        // ============================================
        d21 = ptr[i+2]-ptr[i+1];
        d54 = ptr[i+5]-ptr[i+4];

        if (d21>15 && -d54>17)
        {
            double avg0 = (ptr[i+1]+ptr[i+5])/2;
            double avg1 = (ptr[i+2]+ptr[i+3]+ptr[i+4])/3;

            ptr[i+2] = ptr[i+2] - avg1+avg0;
            ptr[i+3] = ptr[i+3] - avg1+avg0;
            ptr[i+4] = ptr[i+4] - avg1+avg0;
        }

        // ============================================
        d10 = ptr[i+1]-ptr[i];
        d54 = ptr[i+5]-ptr[i+4];

        if (d10>18 && -d54>20)
        {
            double avg0 = (ptr[i]+ptr[i+5])/2;
            double avg1 = (ptr[i+1]+ptr[i+2]+ptr[i+3]+ptr[i+4])/4;

            ptr[i+1] = ptr[i+1] - avg1+avg0;
            ptr[i+2] = ptr[i+2] - avg1+avg0;
            ptr[i+3] = ptr[i+3] - avg1+avg0;
            ptr[i+4] = ptr[i+4] - avg1+avg0;
        }
    }
}




static void DrsCalibrate::SlidingAverage(float *const vec, const uint32_t roi, const uint16_t w){
    if (w==0 || w>roi)
        return;
    for (float *pix=vec; pix<vec+1440*roi; pix += roi)
        for (float *ptr=pix; ptr<pix+roi-w; ptr++){
            for (float *p=ptr+1; p<ptr+w; p++)
                *ptr += *p;
            *ptr /= w;
        }
}



std::pair<std::vector<double>,std::vector<double> > DrsCalibrate::GetSampleStats() const{
    if (fNumEntries==0)
        return make_pair(std::vector<double>(),std::vector<double>());

    std::vector<double> mean(fSum.size());
    std::vector<double> error(fSum.size());

    std::vector<int64_t>::const_iterator it = fSum.begin();
    std::vector<int64_t>::const_iterator i2 = fSum2.begin();
    std::vector<double>::iterator        im = mean.begin();
    std::vector<double>::iterator        ie = error.begin();

    while (it!=fSum.end())
    {
        *im = /*cnt<fResult.size() ? fResult[cnt] :*/ double(*it)/fNumEntries;
        *ie = sqrt(double(*i2*int64_t(fNumEntries) - *it * *it))/fNumEntries;

        im++;
        ie++;
        it++;
        i2++;
    }
    return make_pair(mean, error);
}


void DrsCalibrate::GetSampleStats(float *ptr, float scale) const{
    const size_t sz = fNumSamples*fNumChannels;

    if (fNumEntries==0)
    {
        memset(ptr, 0, sizeof(float)*sz*2);
        return;
    }

    std::vector<int64_t>::const_iterator it = fSum.begin();
    std::vector<int64_t>::const_iterator i2 = fSum2.begin();

    while (it != fSum.end()){
        *ptr      = scale*double(*it)/fNumEntries;
        *(ptr+sz) = scale*sqrt(double(*i2*fNumEntries - *it * *it))/fNumEntries;

        ptr++;
        it++;
        i2++;
    }
}


static double DrsCalibrate::GetPixelStats(float *ptr, const float *data, uint16_t roi, uint16_t begskip=0, uint16_t endskip=0){
    if (roi==0)
        return -1; /// WTF?

    // Skip first 10 samples
    const uint beg = roi>begskip ? begskip : 0;
    const uint end = roi-beg>endskip ? roi-endskip : roi;
    const uint len = end-beg;

    double max = 0;
    double patch = 0;
    for (uint i=0; i<1440; i++)
    {
        const float *vec = data+i*roi;

        uint   pos  = beg;
        double sum  = vec[beg];
        double sum2 = vec[beg]*vec[beg];

        for (uint j=beg+1; j<end; j++)
        {
            sum  += vec[j];
            sum2 += vec[j]*vec[j];

            if (vec[j]>vec[pos])
                pos = j;
        }
        sum  /= len;
        sum2 /= len;
        sum2 -= sum*sum;

        if (i % 9 != 8)
            patch += vec[pos];
        else
        {
            if (patch > max)
                max = patch;
            patch = 0;
        }

        *(ptr + 0 * 1440+i) = sum;
        *(ptr + 1 * 1440+i) = sum2 < 0 ? 0 : sqrt(sum2);
        *(ptr + 2 * 1440+i) = vec[pos];
        *(ptr + 3 * 1440+i) = pos;
    }

    return max/8;
}


static void DrsCalibrate::GetPixelMax(float *max, const float *data, uint16_t roi, int32_t first, int32_t last){
    if (roi==0 || first<0 || last<0 || first>=roi || last>=roi || last<first)
        return;

    for (int i=0; i<1440; i++){
        const float *beg = data+i*roi+first;
        const float *end = data+i*roi+last;

        const float *pmax = beg;

        for (const float *ptr=beg+1; ptr<=end; ptr++)
            if (*ptr>*pmax)
                pmax = ptr;

        max[i] = *pmax;
    }
}
