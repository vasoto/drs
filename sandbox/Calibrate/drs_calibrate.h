#pragma once


class DrsCalibrate {

public:

    struct Step{
        Step() : avg(0), rms(0), pos(0), cnt(0) { }
        double   avg;
        double   rms;
        double   pos;
        uint16_t cnt;
        static bool sort(const Step &s, const Step &r) { return s.avg < r.avg; }
    };

    typedef std::vector<DrsCalibrate::Step> Steps;
    typedef Steps::iterator StepsIterator;


    DrsCalibrate();

    void Reset();

    void InitSize(uint16_t channels, uint16_t samples);

    void AddRel(const int16_t *val, const int16_t *start);

    void AddRel(const int16_t *val,    const int16_t *start,
                const int32_t *offset, const int64_t scale);

    void AddAbs(const int16_t *val,    const int16_t *start,
                const int32_t *offset, const int64_t scale);

    static void ApplyCh(float *vec, const int16_t *val, int16_t start, uint32_t roi,
                        const int32_t *offset, const int64_t scaleabs,
                        const int64_t *gain,   const int64_t scalegain);

    static void ApplyCh(float *vec, const int16_t *val, int16_t start, uint32_t roi,
                        const int32_t *offset, const int64_t scaleabs,
                        const int64_t *gain,   const int64_t scalegain,
                        const int64_t *trgoff, const int64_t scalerel);

    static double FindStep(const size_t ch0, const float *vec, int16_t roi, const int16_t pos, const uint16_t *map=nullptr);

    static void SubtractStep(const size_t ch0,
    		const double avg, float *vec,
    		int16_t roi, int32_t pos,
    		const uint16_t *map=nullptr);

    static Step AverageSteps(const std::vector<Step>::iterator beg, const std::vector<Step>::iterator end);

    static Step CorrectStep(float *vec, uint16_t nch, uint16_t roi,
                            const int16_t *prev, const int16_t *start,
                            const int16_t offset, const uint16_t *map=nullptr);

    static void RemoveSpikes(float *p, uint32_t roi);

    static void RemoveSpikes2(float *p, uint32_t roi);//from Werner

    static void RemoveSpikes3(float *vec, uint32_t roi);//from Werner

    static void RemoveSpikes4(float *ptr, uint32_t roi);

    static void SlidingAverage(float *const vec, const uint32_t roi, const uint16_t w);

    std::pair<std::vector<double>,std::vector<double> > GetSampleStats() const;

    void GetSampleStats(float *ptr, float scale) const;

    static double GetPixelStats(float *ptr, const float *data, uint16_t roi, uint16_t begskip=0, uint16_t endskip=0);

    static void GetPixelMax(float *max, const float *data, uint16_t roi, int32_t first, int32_t last);

    const std::vector<int64_t> &GetSum() const { return (fSum); }

    int64_t GetNumEntries() const { return (fNumEntries); }

protected:
    int64_t fNumEntries;
    size_t fNumSamples;
    size_t fNumChannels;
    std::vector<int64_t> fSum;
    std::vector<int64_t> fSum2;


};
