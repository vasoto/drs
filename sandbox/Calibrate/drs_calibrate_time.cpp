#include "drs_calibrate_time.h"


DrsCalibrateTime::DrsCalibrateTime() : fNumEntries(0), fNumSamples(0), fNumChannels(0){
	InitSize(160, 1024);
}



DrsCalibrateTime::DrsCalibrateTime(const DrsCalibrateTime &p) : fNumEntries(p.fNumEntries),
	fNumSamples(p.fNumSamples),
	fNumChannels(p.fNumChannels),
	fStat(p.fStat){}



virtual void DrsCalibrateTime::InitSize(uint16_t channels, uint16_t samples){
	fNumChannels = channels;
	fNumSamples  = samples;
	fNumEntries  = 0;
	fStat.clear();
	fStat.resize(samples*channels);
}



DrsCalibrateTime DrsCalibrateTime::GetResult() const{
	DrsCalibrateTime rc(*this);
	rc.CalcResult();
	return rc;
}

void DrsCalibrateTime::Reset(){
	for(auto& it : fStat){
//    for (auto it = fStat.begin(); it != fStat.end(); ++it){
        it.first = 0;
        it.second = 0;
    }
}


void DrsCalibrateTime::AddT(const float *val, const int16_t *start, signed char edge){
    if (fNumSamples != 1024 || fNumChannels != 160) //????
        return;

    // Rising or falling edge detection has the advantage that
    // we are much less sensitive to baseline shifts
    for (size_t ch = 0; ch < fNumChannels; ++ch){
        const size_t tm = ch*9+8;
        const int16_t spos = start[tm];
        if (spos < 0) continue;

        const size_t pos = ch * 1024;

        double  p_prev =  0;
        int32_t i_prev = -1;

        for (size_t i = 0; i < 1024-1; ++i){
            const size_t rel = tm * 1024 + i;

            const float &v0 = val[rel];  //-avg;
            const float &v1 = val[rel + 1];//-avg;

            // If edge is positive ignore all falling edges
            if (edge>0 && v0>0)
                continue;

            // If edge is negative ignore all falling edges
            if (edge<0 && v0<0)
                continue;

            // Check if there is a zero crossing
            if ((v0<0 && v1<0) || (v0>0 && v1>0))
                continue;

            // Calculate the position p of the zero-crossing
            // within the interval [rel, rel+1] relative to rel
            // by linear interpolation.
            const double p = v0==v1 ? 0.5 : v0/(v0-v1);

            // If this was at least the second zero-crossing detected
            if (i_prev>=0)
            {
                // Calculate the distance l between the
                // current and the last zero-crossing
                const double l = i+p - (i_prev+p_prev);

                // By summation, the average length of each
                // cell is calculated. For the first and last
                // fraction of a cell, the fraction is applied
                // as a weight.
                const double w0 = 1-p_prev;
                fStat[pos+(spos+i_prev)%1024].first  += w0*l;
                fStat[pos+(spos+i_prev)%1024].second += w0;

                for (size_t k=i_prev+1; k<i; k++)
                {
                    fStat[pos+(spos+k)%1024].first  += l;
                    fStat[pos+(spos+k)%1024].second += 1;
                }

                const double w1 = p;
                fStat[pos+(spos+i)%1024].first  += w1*l;
                fStat[pos+(spos+i)%1024].second += w1;
            }

            // Remember this zero-crossing position
            p_prev = p;
            i_prev = i;
        }
    }
    fNumEntries++;
}



void DrsCalibrateTime::FillEmptyBins(){
    for (int ch=0; ch<160; ch++)
    {
        const auto beg = fStat.begin() + ch*1024;
        const auto end = beg + 1024;

        double   avg = 0;
        uint32_t num = 0;
        for (auto it=beg; it!=end; it++)
        {
            if (it->second<fNumEntries-0.5)
                continue;

            avg += it->first / it->second;
            num++;
        }
        avg /= num;

        for (auto it=beg; it!=end; it++)
        {
            if (it->second>=fNumEntries-0.5)
                continue;

            // {
            //     result[i+1].first  = *is2;
            //     result[i+1].second = *iw2;
            // }
            // else
            // {
            it->first  = avg*fNumEntries;
            it->second = fNumEntries;
            // }
        }
    }
}


DrsCalibrateTime DrsCalibrateTime::GetComplete() const
{
    DrsCalibrateTime rc(*this);
    rc.FillEmptyBins();
    return rc;
}


void DrsCalibrateTime::CalcResult(){
    for (int ch=0; ch<160; ch++){
        const auto beg = fStat.begin() + ch*1024;
        const auto end = beg + 1024;

        // First calculate the average length s of a single
        // zero-crossing interval in the whole range [0;1023]
        // (which is identical to the/ wavelength of the
        // calibration signal)
        double s = 0;
        double w = 0;
        for (auto it = beg; it != end; ++it){
            s += it->first;
            w += it->second;
        }
        s /= w;

        // Dividing the average length s of the zero-crossing
        // interval in the range [0;1023] by the average length
        // in the interval [0;n] yields the relative size of
        // the interval in the range [0;n].
        //
        // Example:
        // Average [0;1023]: 10.00  (global interval size in samples)
        // Average [0;512]:   8.00  (local interval size in samples)
        //
        // Globally, on average one interval is sampled by 10 samples.
        // In the sub-range [0;512] one interval is sampled on average
        // by 8 samples.
        // That means that the interval contains 64 periods, while
        // in the ideal case (each sample has the same length), it
        // should contain 51.2 periods.
        // So, the sampling position 512 corresponds to a time 640,
        // while in the ideal case with equally spaces samples,
        // it would correspond to a time 512.
        //
        // The offset (defined as 'ideal - real') is then calculated
        // as 512*(1-10/8) = -128, so that the time is calculated as
        // 'sampling position minus offset'
        //
        double sumw = 0;
        double sumv = 0;
        int n = 0;

        // Sums about many values are numerically less stable than
        // just sums over less. So we do the exercise from both sides.
        // First from the left
        for (auto it=beg; it!=end-512; it++, n++)
        {
            const double valv = it->first;
            const double valw = it->second;

            it->first  = sumv>0 ? n*(1-s*sumw/sumv) : 0;

            sumv += valv;
            sumw += valw;
        }

        sumw = 0;
        sumv = 0;
        n = 1;

        // Second from the right
        for (auto it=end-1; it!=beg-1+512; it--, n++)
        {
            const double valv = it->first;
            const double valw = it->second;

            sumv += valv;
            sumw += valw;

            it->first  = sumv>0 ? n*(s*sumw/sumv-1) : 0;
        }

        // A crosscheck has shown, that the values from the left
        // and right perfectly agree over the whole range. This means
        // the a calculation from just one side would be enough, but
        // doing it from both sides might still make the numerics
        // a bit more stable.
    }
}


double DrsCalibrateTime::Offset(uint32_t ch, double pos) const {
	const auto p = fStat.begin() + ch * 1024;
	const uint32_t f = floor(pos);
	const double v0 = p[f].first;
	const double v1 = p[ (f + 1) % 1024].first;
	return v0 + fmod(pos, 1) * (v1 - v0);
}

double DrsCalibrateTime::Calib(uint32_t ch, double pos) const{
	return pos - Offset(ch, pos);
}



    /*
    // See MDrsCalibrationTime
    std::string ReadFitsImp(const std::string &str)
    {
        fits file(str);
        if (!file)
        {
            std::ostringstream msg;
            msg << "Could not open file '" << str << "': " << strerror(errno);
            return msg.str();
        }

        if (file.GetStr("TELESCOP")!="FACT")
        {
            std::ostringstream msg;
            msg << "Reading '" << str << "' failed: Not a valid FACT file (TELESCOP not FACT in header)";
            return msg.str();
        }

        if (file.GetNumRows()!=1)
        {
            std::ostringstream msg;
            msg << "Reading '" << str << "' failed: Number of rows in table is not 1.";
            return msg.str();
        }

        fNumSamples  = file.GetUInt("NROI");
        fNumChannels = file.GetUInt("NCH");
        fNumEntries  = file.GetUInt("NBTIME");

        fStat.resize(fNumSamples*fNumChannels);

        double *f = reinterpret_cast<double*>(file.SetPtrAddress("CellWidthMean"));
        double *s = reinterpret_cast<double*>(file.SetPtrAddress("CellWidthRms"));

        if (!file.GetNextRow())
        {
            std::ostringstream msg;
            msg << "Reading data from " << str << " failed.";
            return msg.str();
        }

        for (uint32_t i=0; i<fNumSamples*fNumChannels; i++)
        {
            fStat[i].first  = f[i];
            fStat[i].second = s[i];
        }

        return std::string();
    }

    std::string WriteFitsImp(const std::string &filename, uint32_t night=0) const
    {
        ofits file(filename.c_str());
        if (!file)
        {
            std::ostringstream msg;
            msg << "Could not open file '" << filename << "': " << strerror(errno);
            return msg.str();
        }

        file.SetDefaultKeys();
        file.AddColumnDouble(fStat.size(), "CellWidthMean", "ratio", "Relative cell width mean");
        file.AddColumnDouble(fStat.size(), "CellWidthRms",  "ratio", "Relative cell width rms");

        file.SetInt("ADCRANGE", 2000,    "Dynamic range of the ADC in mV");
        file.SetInt("DACRANGE", 2500,    "Dynamic range of the DAC in mV");
        file.SetInt("ADC",      12,      "Resolution of ADC in bits");
        file.SetInt("DAC",      16,      "Resolution of DAC in bits");
        file.SetInt("NPIX",     1440,    "Number of channels in the camera");
        file.SetInt("NTM",      0,       "Number of time marker channels");
        file.SetInt("NROI",     fNumSamples,  "Region of interest");
        file.SetInt("NCH",      fNumChannels, "Number of chips");
        file.SetInt("NBTIME",   fNumEntries,  "Num of entries for time calibration");

        file.WriteTableHeader("DrsCellTimes");
        if (night>0)
            file.SetInt("NIGHT", night, "Night as int");

        std::vector<double> data(fNumSamples*fNumChannels*2);

        for (uint32_t i=0; i<fNumSamples*fNumChannels; i++)
        {
            data[i] = fStat[i].first;
            data[fNumSamples*fNumChannels+i] = fStat[i].second;
        }

        if (!file.WriteRow(data.data(), data.size()*sizeof(double)))
        {
            std::ostringstream msg;
            msg << "Writing data to " << filename << " failed.";
            return msg.str();
        }

        return std::string();
    }*/
