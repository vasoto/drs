#pragma once


struct DrsCalibrateTime {

    DrsCalibrateTime();

    DrsCalibrateTime(const DrsCalibrateTime &p);
    // Missing assignment operator!!!

    virtual ~DrsCalibrateTime() = default;

    virtual void InitSize(uint16_t channels, uint16_t samples);

    void Reset();

    void AddT(const float *val, const int16_t *start, signed char edge=0);

    void FillEmptyBins();

    DrsCalibrateTime GetComplete() const;

    void CalcResult();

    DrsCalibrateTime GetResult() const;

    double Offset(uint32_t ch, double pos) const;

    double Calib(uint32_t ch, double pos) const;

    double Sum(uint32_t i) const { return fStat[i].first; }

    double W(uint32_t i) const { return fStat[i].second; }


    int64_t fNumEntries;

    size_t fNumSamples;
    size_t fNumChannels;

    std::vector<std::pair<double, double>> fStat;


};

