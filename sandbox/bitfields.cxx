/*
 * bitfields.cxx
 *
 *  Created on: Sep 5, 2016
 *      Author: vasoto
 */

#include <time.h>
#include <iostream>
#include <type_traits>


struct __attribute__((__packed__)) A
{
    void a(unsigned n) { a_ = static_cast<unsigned>(n & 1); }
    void b(unsigned n) { b_ = static_cast<unsigned>(n & 0x1F); }
    void c(unsigned n) { c_ = static_cast<unsigned>(n & 0x3); }
    void d(unsigned n) { d_ = static_cast<unsigned>(n & 0xFF); }
    unsigned a() { return (a_); }
    unsigned b() { return (b_); }
    unsigned c() { return (c_); }
    unsigned d() { return (d_); }
    //volatile
	unsigned a_:1,
                      b_:5,
                      c_:2,
                      d_:8;
};

struct __attribute__((__packed__)) B{
    void a(unsigned n) { a_ = n; }
    void b(unsigned n) { b_ = n; }
    void c(unsigned n) { c_ = n; }
    void d(unsigned n) { d_ = n; }
    unsigned a() { return (a_); }
    unsigned b() { return (b_); }
    unsigned c() { return (c_); }
    unsigned d() { return (d_); }
    volatile unsigned a_, b_, c_, d_;
};

struct C
{
    void a(unsigned n) { x_ &= ~0x01u; x_ |= n; }
    void b(unsigned n) { x_ &= ~0x3Eu; x_ |= n << 1; }
    void c(unsigned n) { x_ &= ~0xC0u; x_ |= n << 6; }
    void d(unsigned n) { x_ &= ~0xFF00u; x_ |= n << 8; }
    unsigned a() const { return (x_ & 0x01); }
    unsigned b() const { return ((x_ & 0x3E) >> 1); }
    unsigned c() const { return ((x_ & 0xC0) >> 6); }
    unsigned d() const { return ((x_ & 0xFF00) >> 8); }
    volatile unsigned x_;
};

struct Timer
{
    Timer():start_tp{} { get(&start_tp); }
    double elapsed() const {
        struct timespec end_tp;
        get(&end_tp);
        auto res = (end_tp.tv_sec - start_tp.tv_sec) * 1000000000;
        res += end_tp.tv_nsec - start_tp.tv_nsec;
        return (static_cast<double>(res)/1000000000.0);
    }
  private:
    static void get(struct timespec* p_tp) {
        if (clock_gettime(CLOCK_REALTIME, p_tp) != 0)
        {
            std::cerr << "clock_gettime() error\n";
            exit(EXIT_FAILURE);
        }
    }
    struct timespec start_tp;
};

template <typename T>
unsigned f(){
    unsigned n = 0;
    Timer timer;
    T t;
    for (int i = 0; i < 10000000; ++i)
    {
        t.a(i & 0x01);
        t.b(i & 0x1F);
        t.c(i & 0x03);
        t.d(i & 0xFF);
        n += t.a() + t.b() + t.c() + t.d();
    }
    std::cout << timer.elapsed() << '\n';
    return (n);
}

int main(){
    std::cout << "bitfields: " << f<A>() << '\n';
    std::cout << "separate ints: " << f<B>() << '\n';
    std::cout << "explicit and/or/shift: " << f<C>() << '\n';
}


