/*
 * bytes.cxx
 *
 *  Created on: Aug 29, 2016
 *      Author: Vassil Z. Verguilov
 */
//#include <cstring>
////#include <type_traits>
//// the safe way to convert byte data to a type.
//template <class Dest>
//inline Dest byte_cast(const char* source) {
//
//  /*
//     static_assert(std::is_trivially_copyable<Dest>::value,
//     "Destination type must be trivially copyable");
//     */
//  Dest dest;
//  std::memcpy(&dest, source, sizeof(dest));
//  return (dest);
//}
//
#include <cassert>
#include <iostream>
#include <bitset>

#include "drs/core/bits.h"
using drs4::core::read_region;
using drs4::core::write_region;

inline void w(unsigned& value, const unsigned bits, unsigned length, unsigned offset){
//	assert(offset > length);
	auto mask = ((1 << length) - 1);
	value = (value & ~ (mask << offset)) | ((bits & mask) << offset);
}

int main(){
	unsigned a(0), b(0), c(0);
	a = write_region(a, 1, 1, 0 );
	w(b,1, 1, 0);

	std::cout << "a = " << std::bitset<32>(a) << '\n';
	std::cout << "b = " << std::bitset<32>(b) << '\n';
	w(b, 0xAA, 4, 5);
	std::cout << "b = " << std::bitset<32>(b) << '\n';
	w(c, 0x1F, 5, 0x1B);
	std::cout << "c = " << std::bitset<32>(c) << '\n';
	return (0);
}
