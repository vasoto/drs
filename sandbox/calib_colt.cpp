/*
 * calib_colt.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: vasoto
 */
#include <iostream>
#include <iomanip>
#include <bitset>

#include "drs/daq/core/usb/session.h"
#include "drs/daq/core/usb/usb_device.h"
#include "drs/daq/module/dpnc342/module.h"
#include "drs/daq/module/dpnc342/registers.h"


using drs4::daq::module::dpnc342::DPNC342Module;
using drs4::daq::module::dpnc342::ControlRegister;
using drs4::daq::module::dpnc342::StatusRegister;
using drs4::daq::core::usb::USBDevice;

template<class MSignature, class DevicePolicy>
struct Module: public MSignature, public DevicePolicy{
	typedef typename DevicePolicy::Pointer DevPtr;
	Module(DevPtr dev ):
		MSignature(this),
		DevicePolicy(*dev)

	{}
};

typedef Module<DPNC342Module, USBDevice> DPNC342;


struct ModuleCalibration{
	ModuleCalibration(DPNC342 * t_module):module(t_module){}
void initialize(){

}

unsigned buf[ 1024 * 8 ]; // 32 kB

DPNC342 * module;

private:

};
