/*
 * check_voltage.cxx
 *
 *  Created on: Nov 1, 2016
 *      Author: vasoto
 */
#include <iostream>
#include <string>
#include <memory>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <iostream>
#include <locale>
#include <map>
#include <vector>
#include <thread>

#include "drs/daq/core/usb/session.h"
#include "drs/daq/core/usb/usb_device.h"
#include "drs/daq/module/dpnc342/module.h"
#include "drs/daq/module/dpnc342/registers.h"


using drs4::daq::module::dpnc342::DPNC342Module;
using drs4::daq::module::dpnc342::ControlRegister;
using drs4::daq::module::dpnc342::StatusRegister;
using drs4::daq::core::usb::USBDevice;
using drs4::daq::core::usb::find_devices;

template<class MSignature, class DevicePolicy>
struct Module: public MSignature, public DevicePolicy{
	typedef typename DevicePolicy::Pointer DevPtr;
	Module(DevPtr dev ):
		MSignature(this),
		DevicePolicy(*dev)
	{}
};

typedef Module<DPNC342Module, USBDevice> DPNC342;

int main(){
	std::cout << "Begin.\n";
	auto devices = find_devices(DPNC342Module::VENDOR_ID,
								DPNC342Module::PRODUCT_ID);
	if(devices.size()){
		std::cout << "Found " << devices.size() << " devices.\n";
		USBDevice::Pointer device = devices[0];
		DPNC342 module(device);
		module.open();
		module.status_reg.load();
		module.control_reg.load();
		// Set voltage here
		module.set_voltage(2.0);
		module.control_reg.commit();
		module.control_reg.load();
		std::cout << "Voltage is: " << module.get_voltage() << std::endl;
	}
	return 0;
}

