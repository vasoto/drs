/*
 * commands.cxx
 *
 *  Created on: Oct 7, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <sstream>
#include <string>
#include <iostream>
#include "drs/core/string_formatter.h"

using Fmt = StringFormatter;

template <typename T>
std::string conc_space(T value){
	std::cout << __PRETTY_FUNCTION__ << "\n";
	return (Fmt() << value >> Fmt::to_str);
}

template <typename T, typename ... Args>
std::string conc_space(T first, Args... args){
	std::cout << __PRETTY_FUNCTION__ << "\n";
	return (Fmt() << first << " " << conc_space(args ... ) >> Fmt::to_str);
}

template<typename... Args>
std::string runCommand(
  const char* const prefix,
  const char* const suffix,
  const Args&... args
) {
	std::cout << __PRETTY_FUNCTION__ << "\n";
	std::string cmd = Fmt() << prefix << conc_space( args ... ) << suffix >> Fmt::to_str;// << Prefix >> Fmt::to_str;
	std::cout << "Command: " << cmd << '\n';
}


template<typename ResultType, char * Prefix, char * Suffix, typename ... Args>
ResultType getter(const Args& ... args ){
	std::string cmd = conc( Prefix, args ..., Suffix);// << Prefix >> Fmt::to_str;
	std::cout << "Command: " << cmd << '\n';
}

template<typename ResultType, char * Prefix, char * Suffix>
struct Command{
	ResultType get();
	void set();
};

//using get_voltage = getter<double, 'V', '?'>;
//using get_actual_voltage = getter<double, 'V', "O?">;
int main(){
//	get_voltage(0);
//	get_actual_voltage(1);
	runCommand("V", "?", 1 );
	runCommand("V", "O?", 1 );
	runCommand("V", "", 1, 1);
	runCommand("V", "", 1, 0);

}

