/*
 * dpnc342_2.cxx
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */
#include <iostream>
#include <iomanip>
#include <bitset>

#include "drs/daq/core/usb/session.h"
#include "drs/daq/core/usb/usb_device.h"
#include "drs/daq/module/dpnc342/module.h"
#include "drs/daq/module/dpnc342/registers.h"


using drs4::daq::module::dpnc342::DPNC342Module;
using drs4::daq::module::dpnc342::ControlRegister;
using drs4::daq::module::dpnc342::StatusRegister;
using drs4::daq::core::usb::USBDevice;

//inline std::ostream& operator<<(std::ostream& os, const ControlRegister& reg){
//	os << "####       |       |       |       |\n";
//	for(size_t i=0; i < reg.data.size(); ++i)
//		os << std::setw(2)
//		   << std::setfill(' ')
//		   << i << ": " << std::bitset<32>(reg.data.at(i)) << '\n';
//	return (os);
//}
//
//inline std::ostream& operator<<(std::ostream& os, const StatusRegister& reg){
//	os << "####       |       |       |       |\n";
//	for(size_t i=0; i < reg.data.size(); ++i)
//		os << std::setw(2)
//		   << std::setfill(' ')
//		   << i << ": " << std::bitset<32>(reg.data.at(i)) << '\n';
//	return (os);
//}

template<class MSignature, class DevicePolicy>
struct Module: public MSignature, public DevicePolicy{
	typedef typename DevicePolicy::Pointer DevPtr;
	Module(DevPtr dev ):
		MSignature(this),
		DevicePolicy(*dev)

	{}
};

typedef Module<DPNC342Module, USBDevice> DPNC342;

int main(){
	std::cout << "Begin.\n";
	auto devices = drs4::daq::core::usb::find_devices(DPNC342Module::VENDOR_ID,
							  DPNC342Module::PRODUCT_ID);
	if(devices.size()){
		std::cout << "Found " << devices.size() << " devices.\n";
		USBDevice::Pointer device = devices[0];
		DPNC342 module(device);
		module.open(); // select endpoints are included in open
		//module.io->read();
      	module.status_reg.load();
      	module.control_reg.load();

		std::cout << module.status_reg << '\n';
		std::cout << "DRS type: " << module.status_reg.drs_type.get() << '\n';
		std::cout << "Board Magic: " << std::hex << module.status_reg.board_magic.get() << std::dec << '\n';
		std::cout << "Board Magic: " << module.status_reg.temperature_0.get() << '\n';
		std::cout << module.control_reg << '\n';
		//module.control_reg.soft_trig = 1;
		// Begin configuration
		module.control_reg.dactive = 1;
		module.control_reg.readout_mode = 0;
		module.control_reg.trigger_config = 0x10;
		module.control_reg.tca_ctrl = 0;
		module.control_reg.enable_trigger = 1;
		module.control_reg.acalib = 1;
		module.control_reg.DAC0 = 0x0000;
		module.control_reg.DAC1 = 0x0000;
		module.control_reg.DAC2 = 0x0000;
		module.control_reg.DAC3 = 0x0000;
		module.control_reg.DAC4 = 0x8000;
		module.control_reg.drs_ctl_first_chn = 0;
		module.control_reg.drs_ctl_last_chn = 7;
		module.control_reg.channel_config = 0xFF;
		module.control_reg.dmode = 1;
		module.control_reg.pllen = 1;
		module.control_reg.wrsloop = 0;
		module.control_reg.drs_ctl_config_7_3 = 0x1F; // five MSB reserved, must be all ones
		// End of configuration
		std::cout << module.control_reg << '\n';
		module.control_reg.commit();
		module.control_reg.load();
		std::cout << module.control_reg << '\n';
	} else {
		std::cerr << "No devices found.\n";
	}
	std::cout << "End.\n";
	return (0);
}


