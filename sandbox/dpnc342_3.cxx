//#include <libusb.h>

#include <iostream>
#include <iomanip>
#include <bitset>
#include <chrono>
#include <ctime>
#include <thread>
#include <iterator>
#include <memory>

// ROOT
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TApplication.h"


// DRS4
#include "drs/daq/core/usb/session.h"
#include "drs/daq/core/usb/usb_device.h"
#include "drs/daq/module/dpnc342/module.h"
#include "drs/daq/module/dpnc342/registers.h"


using drs4::daq::module::dpnc342::DPNC342Module;
using drs4::daq::module::dpnc342::ControlRegister;
using drs4::daq::module::dpnc342::StatusRegister;
using drs4::daq::core::usb::USBDevice;
using drs4::daq::core::usb::find_devices;

template<class MSignature, class DevicePolicy>
struct Module: public MSignature, public DevicePolicy{
	typedef typename DevicePolicy::Pointer DevPtr;
	Module(DevPtr dev ):
		MSignature(this),
		DevicePolicy(*dev)

	{}
};

typedef Module<DPNC342Module, USBDevice> DPNC342;


class VoltageCalibration{
	std::shared_ptr<TCanvas> canvas;
	std::shared_ptr<TH1I> h1_00;
	std::shared_ptr<TH1I> h1_01;
	std::shared_ptr<TH2I> h2_00;
	std::shared_ptr<TH2I> h2_01;

public:
	VoltageCalibration():
		canvas(new TCanvas("canvas","Voltage Calibration", 200, 10, 700, 500)),
		h1_00(new TH1I("DRS_0_0_30", "DRS 0, channel 0, cell 30", 4096, 0, 4096)),
		h1_01(new TH1I("DRS_0_0_31", "DRS 0, channel 0, cell 31", 4096, 0, 4096)),
		h2_00(new TH2I("DRS 0, channel 1", "ADC/cell", 1024, 0, 1024, 4096, 0, 4096)),
		h2_01(new TH2I("DRS 0, channel 0", "ADC/cell", 1024, 0, 1024, 4096, 0, 4096)){}


	void initialize(){
		canvas->SetFillColor(18);
		canvas->Divide( 2, 2, 0.01F, 0.01F, 0);
	}

	void fill(DPNC342& module){
//		module.memory.load(); // Load data
//		unsigned short * data = reinterpret_cast<unsigned short *>(&orig_data.front());
		module.memory.read_all();
		h1_00->Fill(module.memory.read_cell(0, 30), 1);//((data)[30+0*1024+0*8192])&((1<<12)-1),1);
// 	    h1_01->Fill(module.memory.read_cell(0, 31), 1);//((data)[31+0*1024+0*8192])&((1<<12)-1),1);
		for(unsigned j=0; j < 1024; ++j) { // all cells for one DRS channel

		  h2_00->Fill(j, module.memory.read_cell(0, j), 1);
		  auto cell = module.memory.read_cell(1, j);
		  std::cout << "Cell[" << j << "] = " << cell << '\n';
		  h2_01->Fill(j, cell, 1);
		  h1_01->Fill(cell);//((data)[31+0*1024+0*8192])&((1<<12)-1),1);
		}
	}

	void draw(){
		canvas->cd(1);
		h2_00->Draw();
		canvas->cd(2);
		h1_00->Draw();
		canvas->cd(4);
		h1_01->Draw();
		canvas->cd(3);
		h2_01->Draw();

		canvas->Update();
		canvas->Draw();
	}
};

void set_config(DPNC342& module){
	module.control_reg.load();
//	std::cout << module.control_reg << std::endl;
	module.control_reg.start_trig = 0;
	module.control_reg.reinit_trig = 1;
	module.control_reg.soft_trig = 0;
	module.control_reg.commit();

	module.control_reg.clear();
	// Begin configuration
//	module.control_reg.reinit_trig = 0;
	module.control_reg.dactive = 1;
//	module.control_reg.readout_mode = 0;
	module.control_reg.trigger_config = 0x10;
//	module.control_reg.tca_ctrl = 0;
	module.control_reg.enable_trigger = 1;
	module.control_reg.acalib = 0;
//	module.control_reg.DAC0 = 0x0000;
//	module.control_reg.DAC1 = 0x0000;
//	module.control_reg.DAC2 = 0x0000;
//	module.control_reg.DAC3 = 0x0000;
//	module.control_reg.DAC4 = //0x8000;
	module.control_reg.DAC0 = 0x0000;
	module.set_voltage(1.0);
//	module.control_reg.drs_ctl_first_chn = 0;
	module.control_reg.drs_ctl_last_chn = 7;
	module.control_reg.channel_config = 0xFF;
	module.control_reg.dmode = 1;
	module.control_reg.pllen = 1;
	module.control_reg.wrsloop = 0;
	module.control_reg.drs_ctl_config_7_3 = 0x1F; // five MSB reserved, must be all ones
	module.control_reg.commit();
	std::cout << module.control_reg << std::endl;
	// End of configuration
}

bool wait_for_data(DPNC342& module){
	const unsigned max_laps = 100; // 10 seconds timeout
	const unsigned wait_time_ms = 100;
	unsigned laps(0);
	module.status_reg.load();
//	std::cout << "Module is " << ((module.status_reg.stat_idle.get())?"idle":"not idle") << '\n';
	while( !module.status_reg.stat_idle && (laps < max_laps) ){
//		std::cout << "Module is " << ((module.status_reg.stat_idle.get())?"idle":"not idle");
//		std::cout << " laps: " << laps << " Max laps: " << max_laps << '\n';
		std::this_thread::sleep_for(std::chrono::milliseconds(wait_time_ms));
		module.status_reg.load(); // Load status register from the device;
		++laps;
	}
	std::cout << "Waited for " << laps * wait_time_ms << " miliseconds.\n";
	return (laps < max_laps);
}

int readout(DPNC342& module, VoltageCalibration& calib, const unsigned max_events = 10){
	if( wait_for_data(module) ){
		for(unsigned evt_num = 0; evt_num < max_events; ++ evt_num){
//			// Enable trigger
			module.control_reg.start_trig = 1;
			module.control_reg.commit();
			module.status_reg.load();
			std::cout << module.control_reg << '\n';
			std::cout << "Trigger is "<< ((module.status_reg.arm_trig.get())? "enabled" : "disabled")
					  <<  ". " << ((module.status_reg.arm_trig.get())? "Waiting" : "Not Waiting")
					  << " for events...\n";
			if(wait_for_data(module)){
//				// Read data into vector of unsigned ints
				std::vector<unsigned> buffer(module.memory.Waveform_DRS1.size, 0);
				module.memory.Waveform_DRS1.read(&buffer.front(),
												 module.memory.Waveform_DRS1.size, 0);

				calib.fill(module);

//				std::cout << std::string(35, '+' )
//					  << " Begin Event " << evt_num + 1
//					  << std::string(35, '+' ) << '\n';
//				std::copy(buffer.begin(),
//					  buffer.end(),
//					  std::ostream_iterator<unsigned>(std::cout, "\n" ) );
				std::cout << std::string(35, '-' )
					  << " End Event " << evt_num + 1
					  << std::string(35, '-' ) << '\n' << std::endl;
			} else {
				std::cerr << "Timeout while waiting for data.\n";
			}
		}
	} else {
		std::cerr << "Timeout while waiting for data.\n";
	}

}

int main(int argc, char **argv){
	std::cout << "Begin.\n";
	auto devices = find_devices(DPNC342Module::VENDOR_ID,
								DPNC342Module::PRODUCT_ID);
	if(devices.size()){

		std::cout << "Found " << devices.size() << " devices.\n";
		USBDevice::Pointer device = devices[0];
		DPNC342 module(device);
		module.open(); // select endpoints are included in open
		//module.io->read();
		module.status_reg.load();
		module.control_reg.load();
		set_config(module);
		TApplication theApp("App", &argc, argv);
		VoltageCalibration calib;
		calib.initialize();
		readout(module, calib, 1);
		calib.draw();
//		std::cin.ignore();
		theApp.Run();
	}

	return (0);
}
