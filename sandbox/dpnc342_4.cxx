//#include <libusb.h>

#include <iostream>
#include <iomanip>
#include <bitset>
#include <chrono>
#include <ctime>
#include <thread>
#include <iterator>


#include "drs/daq/core/usb/session.h"
#include "drs/daq/core/usb/usb_device.h"
#include "drs/daq/module/dpnc342/module.h"
#include "drs/daq/module/dpnc342/registers.h"


using drs4::daq::module::dpnc342::DPNC342Module;
using drs4::daq::module::dpnc342::ControlRegister;
using drs4::daq::module::dpnc342::StatusRegister;
using drs4::daq::core::usb::USBDevice;
using drs4::daq::core::usb::find_devices;

template<class MSignature, class DevicePolicy>
struct Module: public MSignature, public DevicePolicy{
	typedef typename DevicePolicy::Pointer DevPtr;
	Module(DevPtr dev ):
		MSignature(this),
		DevicePolicy(*dev)

	{}
};

typedef Module<DPNC342Module, USBDevice> DPNC342;

void set_config(DPNC342& module){
	module.control_reg.load();
	module.control_reg.start_trig = 0;
	module.control_reg.reinit_trig = 1;
	module.control_reg.soft_trig = 0;
	module.control_reg.commit();

	// Begin configuration
	module.control_reg.reinit_trig = 0;
	module.control_reg.dactive = 1;
	module.control_reg.readout_mode = 0;
	module.control_reg.trigger_config = 0x10;
	module.control_reg.tca_ctrl = 0;
	module.control_reg.enable_trigger = 1;
	module.control_reg.acalib = 1;
	module.control_reg.DAC0 = 0x0000;
	module.control_reg.DAC1 = 0x0000;
	module.control_reg.DAC2 = 0x0000;
	module.control_reg.DAC3 = 0x0000;
	module.control_reg.DAC4 = 0x8000;
	module.control_reg.drs_ctl_first_chn = 0;
	module.control_reg.drs_ctl_last_chn = 7;
	module.control_reg.channel_config = 0xFF;
	module.control_reg.dmode = 1;
	module.control_reg.pllen = 1;
	module.control_reg.wrsloop = 0;
	module.control_reg.drs_ctl_config_7_3 = 0x1F; // five MSB reserved, must be all ones
	module.control_reg.commit();
	// End of configuration
}

bool wait_for_data(DPNC342& module){
	const unsigned max_laps = 50; // 5 seconds timeout
	unsigned laps(0);
	module.status_reg.load();
	while( module.status_reg.stat_idle || (laps < max_laps)){
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		module.status_reg.load(); // Load status register from the device;
		++laps;
	}
	std::cout << "Waited for " << laps * 100 << " miliseconds.\n";
	return (laps < max_laps);
}

int readout(DPNC342& module, const unsigned max_events = 10){
	if( wait_for_data(module) ){
		for(unsigned evt_num = 0; evt_num < max_events; ++ evt_num){
			// Enable trigger
			module.control_reg.start_trig = 1;
			module.control_reg.commit();
			if(wait_for_data(module)){
				// Read data into vector of unsigned ints

				std::vector<unsigned> buffer(module.memory.Waveform_DRS1.size, 0);
				module.memory.Waveform_DRS1.read(&buffer.front(), module.memory.Waveform_DRS1.size, 0);
				std::cout << std::string(35, '+' ) 
					  << " Begin Event " << evt_num + 1 
					  << std::string(35, '+' ) << '\n';
				std::copy(buffer.begin(),
					  buffer.end(),
					  std::ostream_iterator<unsigned>(std::cout, "\n" ) );
				std::cout << std::string(35, '-' ) 
					  << " End Event " << evt_num + 1 
					  << std::string(35, '-' ) << '\n' << std::endl;
			} else {
				std::cerr << "Timeout while waiting for data.\n";
			}
		}
	} else {
		std::cerr << "Timeout while waiting for data.\n";
	}

}

int main(){
	std::cout << "Begin.\n";
	auto devices = find_devices(DPNC342Module::VENDOR_ID,
								DPNC342Module::PRODUCT_ID);
	if(devices.size()){
		std::cout << "Found " << devices.size() << " devices.\n";
		USBDevice::Pointer device = devices[0];
		DPNC342 module(device);
		module.open(); // select endpoints are included in open
		//module.io->read();
		module.status_reg.load();
		module.control_reg.load();
		set_config(module);
		readout(module);

	}

	return (0);
}
