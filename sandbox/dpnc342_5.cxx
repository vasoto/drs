//#include <libusb.h>

#include <iostream>
#include <iomanip>
#include <bitset>
#include <chrono>
#include <ctime>
#include <thread>
#include <iterator>
#include <sstream>
#include <chrono>


// ROOT
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TApplication.h"

#include "drs/daq/core/usb/session.h"
#include "drs/daq/core/usb/usb_device.h"
#include "drs/daq/module/dpnc342/module.h"
#include "drs/daq/module/dpnc342/registers.h"


using drs4::daq::module::dpnc342::DPNC342Module;
using drs4::daq::module::dpnc342::ControlRegister;
using drs4::daq::module::dpnc342::StatusRegister;
using drs4::daq::core::usb::USBDevice;
using drs4::daq::core::usb::find_devices;

template<class MSignature, class DevicePolicy>
struct Module: public MSignature, public DevicePolicy{
	typedef typename DevicePolicy::Pointer DevPtr;
	Module(DevPtr dev ):
		MSignature(this),
		DevicePolicy(*dev)

	{}
};

typedef Module<DPNC342Module, USBDevice> DPNC342;

struct VoltageCalibration{
	VoltageCalibration(DPNC342& t_module):module(t_module),
			data(32 * 1024),
			canvas(32,nullptr),
			plots(32, nullptr),
			rms( 32 * 1024),
			rms_plots(32, nullptr)
//			cnvs(new TCanvas("cnvs", "General", 200, 10, 1920, 1200)),
//			rms_plot()
			{};

	void initialize(){
		//Disconnect analog inputs
		module.control_reg.load();
		module.control_reg.start_trig = 0;
		module.control_reg.reinit_trig = 1;
		module.control_reg.soft_trig = 0;
		module.control_reg.commit();

		module.control_reg.clear();
		// Begin configuration
		module.control_reg.dactive = 1;
		module.control_reg.trigger_config = 0x10;
	//	module.control_reg.tca_ctrl = 0;
		module.control_reg.enable_trigger = 1;
		module.control_reg.acalib = 1;
		module.control_reg.DAC4 = 0x0000;
	//	module.control_reg.drs_ctl_first_chn = 0;
		module.control_reg.drs_ctl_last_chn = 7;
		module.control_reg.channel_config = 0xFF;
		module.control_reg.dmode = 1;
		module.control_reg.pllen = 1;
	//	module.control_reg.wrsloop = 0;
		module.control_reg.drs_ctl_config_7_3 = 0x1F; // five MSB reserved, must be all ones
		module.control_reg.commit();
		std::cout << module.control_reg << std::endl;
		// End of configuration
	}

	void start_domino(){
//		// Start domino sampling
		module.control_reg.start_trig = 1;
		module.control_reg.commit();
		module.control_reg.start_trig = 0;
	}

	void soft_trigger(){
//		// Send a software trigger
		module.control_reg.soft_trig = 1;
		module.control_reg.commit();
		module.control_reg.soft_trig = 0;
	}

	bool wait_for_data(){
		const unsigned max_laps = 200000; // 10 seconds timeout
		const unsigned wait_time_ns = 50;
		unsigned laps(0);
		module.status_reg.load();
	//	std::cout << "Module is " << ((module.status_reg.stat_idle.get())?"idle":"not idle") << '\n';
		while( !module.status_reg.stat_idle && (laps < max_laps) ){
	//		std::cout << "Module is " << ((module.status_reg.stat_idle.get())?"idle":"not idle");
	//		std::cout << " laps: " << laps << " Max laps: " << max_laps << '\n';
			std::this_thread::sleep_for(std::chrono::nanoseconds(wait_time_ns));
			module.status_reg.load(); // Load status register from the device;
			++laps;
		}
		std::cout << "Waited for " << laps * wait_time_ns << " ns.\n";
		return (laps < max_laps);
	}

	void transfer_data(){
		module.memory.read_all();
		for(unsigned channel = 0; channel < module.MaxChannels; ++channel){
			for(unsigned cell=0; cell < module.CellsPerChannel; ++cell){
//				plots[channel]->Fill(cell, module.memory.read_cell(channel, cell, true));
				auto chunk = module.memory.read_cell(channel, cell, true);
				rms[channel * 1024 + cell].push_back(chunk);
				plots[channel]->Fill(cell, chunk);
			}
//				data[channel * 1024 + cell] = module.memory.read_cell(channel, cell);
		}
	}

	void read_event(){
		start_domino();
		soft_trigger();
		wait_for_data();
		transfer_data();
	}

	void init_plots(){
		std::stringstream ss;
		for( int channel = 0; channel < module.MaxChannels; ++channel){
			ss.str(std::string()); // More efficient than calling ss.str(""); --> this invokes std::string constructor for char *
			ss << "canvas_" << channel + 1;
			std::string name = ss.str();
			ss.str(std::string());
			ss << "Voltage Calibration for Channel " << channel + 1;
			canvas[channel].reset(new TCanvas( name.c_str(), ss.str().c_str(), 200, 10, 1920, 1200));
			canvas[channel]->Divide(1,2);
			canvas[channel]->SetFillColor(10);
			canvas[channel]->cd(1);

			ss.str(std::string());
			ss << "DataChannel" << channel + 1 << "Plot";
			name = ss.str();
			ss.str(std::string());
			ss << "Data in Channel " << channel + 1<< ";Cell;Voltage";
			plots[channel].reset(new TH2I(name.c_str(), ss.str().c_str(), 1024, 0, 1024, 4096, 0, 4096));
			plots[channel]->SetStats(0);

			canvas[channel]->cd(2);
			ss.str(std::string());
			ss << "RMSChannel" << channel + 1 << "Plot";
			name = ss.str();
			ss.str(std::string());
			ss << "RMS for Channel " << channel + 1 << ";Cell;Voltage";
			rms_plots[channel].reset(new TH2I(name.c_str(), ss.str().c_str(), 1024, 0, 1024, 200, 0, 20));
			rms_plots[channel]->SetStats(0);
			plots[channel]->Draw();
			canvas[channel]->Update();
			canvas[channel]->Draw();
//			std::this_thread::sleep_for(std::chrono::seconds(1));
			std::cout << "Draw from init...\n";
		}
	}

	void plot(){
		for(unsigned ch=0; ch < module.MaxChannels; ++ch){
			canvas[ch]->cd(2);
			for(unsigned cell = 0; cell < 1024; ++cell){
				auto points = rms[ch * cell];
				//calculate mean
				double sum(0.0);
				for(auto p : points){
					sum += p;
				}
				// calculate rms
				double mean = sum/points.size();
				sum = 0.0;
				for(auto p : points){
					auto a = (p - mean);
					sum += a*a;
				}

				std::cout << "RMS(" << ch << ", " << cell << ") = " << sqrt(sum/points.size()) << "Num of points: " << points.size() << '\n';
				rms_plots[ch]->Fill(cell, sqrt(sum/points.size()));

			}
			rms_plots[ch]->Draw();
			canvas[ch]->cd(1);
			plots[ch]->Draw();
			std::cout << "Plotting histogram for channel " << ch << '\n';
			canvas[ch]->Update();
			canvas[ch]->Draw();
			std::cout << "Plotting canvas for channel " << ch << '\n';

		}
	}

	void run(const unsigned max_events=1){

		double step = 0.1;
		const double max_voltage = 0.0;
		double voltage = 0.0;
		while( voltage <= max_voltage){
			module.set_voltage(voltage);
			module.control_reg.commit();
			init_plots();
			for( unsigned evt_num = 0; evt_num < max_events; ++evt_num){
				read_event();
			}
			plot();
			voltage += step;
		}
	}

	DPNC342& module;
	std::vector<unsigned short> data;
	std::vector<std::shared_ptr<TCanvas>> canvas;
	std::vector<std::shared_ptr<TH2I>> plots;
	std::vector<std::shared_ptr<TH2I>> rms_plots;
//	std::shared_ptr<TH2I> rms_plot;
//	std::shared_ptr<TCanvas> cnvs;
	std::vector<std::vector<unsigned short>> rms;
};

#include<math.h>

int main(int argc, char **argv){
	std::cout << "Begin.\n";
	auto devices = find_devices(DPNC342Module::VENDOR_ID,
								DPNC342Module::PRODUCT_ID);
	if(devices.size()){
		std::cout << "Found " << devices.size() << " devices.\n";
		//gStyle->SetCanvasColor(0);
	   //gStyle->SetStatBorderSize(1)
		USBDevice::Pointer device = devices[0];
		DPNC342 module(device);
		module.open(); // select endpoints are included in open
		//module.io->read();
		module.status_reg.load();
		module.control_reg.load();
		TApplication theApp("App", &argc, argv);
		VoltageCalibration vcalib(module);
		vcalib.initialize();
		//vcalib.canvas->Draw();
		vcalib.run(1000);
//		std::vector<double> RMS(32*1024);
//		std::shared_ptr<TH1D> RMS(new TH1D("RMS", "RMS", 32 * 1024, 0, 32 * 1024));
//		std::shared_ptr<TCanvas> cnvs(new TCanvas());
//		unsigned cell = 0;
//		for(auto cells : vcalib.rms){
//			double sum = 0.0;
//			for(auto c : cells){
//				sum += c*c;
//			}
//			RMS->Fill(cell, sqrt(sum/cells.size()));
//			++cell;
//		}
//		cnvs->cd();
//		cnvs->Draw();
//		RMS->Draw();
//		cnvs->Update();
//		cnvs->Draw();
		theApp.Run();
		std::cout << "Success!\n";
	}

	return (0);
}
