/*
 * power.cxx
 *
 *  Created on: Oct 7, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <iostream>

#include "drs/daq/core/serial/SerialCom.h"
#include "drs/core/string_formatter.h"

using drs::daq::core::serial::SerialCom;
using drs::daq::core::serial::ComPortConfig;


struct TTiDevice{

	TTiDevice() = delete;
	explicit TTiDevice(ComPortConfig config):serial_port(config){};

	void connect(){ serial_port.connect(); }



	double get_voltage(unsigned channel){
		auto cmd (StringFormatter() << 'V' << channel << '?' >> StringFormatter::to_str );
		std::cout << "Command: " << cmd << '\n';
		auto result = serial_port.query(cmd);
		std::cout << "Result: " << result << '\n';
//		double result = ;
		return 0.0;//std::stod(result);
	}

  std::string get_id(){
    auto result = serial_port.query("*IDN?");
    std::cout << "Result: " << result << '\n';
    return result;
  }

  double get_actual_voltage(unsigned channel){
    auto cmd (StringFormatter() << 'V' << channel << "O?" >> StringFormatter::to_str );
    std::cout << "Command: " << cmd << '\n';
    auto result = serial_port.query(cmd);
    std::cout << "Result: " << result << '\n';
    return 0.0;//std::stod(result);        
  }

  double get_current(unsigned channel){
    auto cmd (StringFormatter() << 'I' << channel << '?' >> StringFormatter::to_str );
    std::cout << "Command: " << cmd << '\n';
    auto result = serial_port.query(cmd);
    std::cout << "Result: " << result << '\n';
    return 0.0;//std::stod(result);  
  }

  double get_actual_current(unsigned channel){
    auto cmd (StringFormatter() << 'I' << channel << "O?" >> StringFormatter::to_str );
    std::cout << "Command: " << cmd << '\n';
    auto result = serial_port.query(cmd);
    std::cout << "Result: " << result << '\n';
    return 0.0;//std::stod(result);  
  }

  bool get_status(int channel){
    std::string cmd (StringFormatter() << "OP" << channel << "?" >> StringFormatter::to_str );
    std::cout << "Command: " << cmd << '\n';
    auto result = serial_port.query(cmd);
    std::cout << "Result: " << result << '\n';
    return 0;
  }

  void set_status(int channel, bool value){
    std::string cmd (StringFormatter() << "OP" << channel << " " << value >> StringFormatter::to_str );
    std::cout << "Command: " << cmd << '\n';
    auto result = serial_port.query(cmd);
    std::cout << "Result: " << result << '\n';
  }


  SerialCom serial_port;
};


int main(){
	ComPortConfig conf;
	conf.serial_port = "/dev/ttyACM0";
//	SerialCom com_port(conf);
//	com_port.connect();
	TTiDevice supply(conf);
	supply.connect();
	supply.get_id();
	supply.get_voltage(1);
	supply.get_actual_voltage(1);
	supply.get_voltage(2);
	supply.get_actual_voltage(2);

	supply.get_current(1);
        supply.get_actual_current(1);
        supply.get_current(2);
        supply.get_actual_current(2);

	supply.get_status(1);
	supply.get_status(2);
	//supply.set_status(1, false);
	//	supply.get_status(2);
	//supply.set_status(1, true);
	//supply.get_status(2);

	supply.get_current(2);
        supply.get_actual_current(2);

	return 0;
}


