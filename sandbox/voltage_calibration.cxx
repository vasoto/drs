/*
 * voltage_calibration.cxx
 *
 *  Created on: Oct 24, 2016
 *      Author: V. Verguilov
 */

#include <iostream>
#include <string>
#include <memory>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <iostream>
#include <locale>
#include <map>
#include <vector>
#include <thread>

#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

#include <TApplication.h>

#include "drs/daq/core/usb/session.h"
#include "drs/daq/core/usb/usb_device.h"
#include "drs/daq/module/dpnc342/module.h"
#include "drs/daq/module/dpnc342/registers.h"


using drs4::daq::module::dpnc342::DPNC342Module;
using drs4::daq::module::dpnc342::ControlRegister;
using drs4::daq::module::dpnc342::StatusRegister;
using drs4::daq::core::usb::USBDevice;
using drs4::daq::core::usb::find_devices;

template<class MSignature, class DevicePolicy>
struct Module: public MSignature, public DevicePolicy{
	typedef typename DevicePolicy::Pointer DevPtr;
	Module(DevPtr dev ):
		MSignature(this),
		DevicePolicy(*dev)
	{}
};

typedef Module<DPNC342Module, USBDevice> DPNC342;


struct TFileDeleter{
	void operator()(TFile * file) {
		std::string name("");
		std::cout << "Closing file " << name << '\n';
		file->Close();
		delete file;
		std::cout << "TFile object " << name << " deleted...\n";
	};
};

struct TTreeDeleter{
	void operator()(TTree * tree){
		std::string name("");
		std::cout << "Writing tree \""<< name <<"\".\n";
//		tree->Write();
		tree->AutoSave();
		delete tree;
		std::cout << name << "tree deleted.\n";
	}
};


struct VoltageCalibration{
	VoltageCalibration() = delete;
	explicit VoltageCalibration(size_t t_iterations, double t_min_voltage = 0.0,
			double t_max_voltage = 1.0, double t_step = 0.1):iterations(t_iterations),
					min_voltage(t_min_voltage), max_voltage(t_max_voltage), step(t_step),
					start_times(iterations), end_times(iterations)
	{
		for(unsigned i = 0; i < 32; ++i)
			cells[i] = new unsigned short[32 * 1024];
	}

	char * get_timestamp() const{
		std::locale::global(std::locale("C"));
		std::time_t now = std::time(NULL);
//		auto now = std::chrono::system_clock::now();
//		auto now_c = std::chrono::system_clock::to_time_t(now);
		char mbstr[100];
		std::strftime( mbstr, sizeof(mbstr), "%Y%m%d%H%M", std::localtime(&now));
		return std::move(mbstr);
	}

  std::string get_file_name() {
		std::stringstream ss;
		ss << "VoltageCalibration" << '-' // Prefix: TODO: get from command line
		   << std::setw(4) << std::setfill('0') << 0 << '-' // TODO: should include module's ID
		   << iterations << "-"
                  //<< get_timestamp()
                   << ".root";
                std::cout << ss.str() << '\n';
		return ss.str();//.c_str();
	}


	void init_conditions(){
		conditions.reset(new TTree("Conditions", "Voltage Calibration Conditions"), TTreeDeleter());
		conditions->Branch("MaxVoltage", &max_voltage, "D");
		conditions->Branch("MinVoltage", &min_voltage, "D");
		conditions->Branch("Step", &step, "D");
		conditions->Branch("Iterations", &iterations, "i");
		conditions->Fill();
	}

	void finalize(){}

	void initialize(){
		auto f_name = get_file_name();
		std::cout << "File name is '" << f_name << "';\n";
		data_file.reset(new TFile(f_name.c_str(), "RECREATE"), TFileDeleter());
		init_conditions();
	}

	char * get_dataset_name(const double step_voltage) const{
		std::stringstream ss;
		ss << "CalibrationStep_" << step_voltage << "V";
		return (std::move(const_cast<char *>(ss.str().c_str())));
	}

	double get_read_mean_time(){
//		auto time_diff_iter = std::adjacent_difference(times.begin(), times.end() - 1, times.begin() + 1);
		volatile double sum = 0.0;
		for(unsigned i=1; i < start_times.size(); ++i){
			std::chrono::duration<double> diff = end_times[i] - start_times[i];
			std::cout << "Diff: " << diff.count() << '\n';
			sum = sum + diff.count();
		}
		std::cout << "Sum: " << sum << std::endl;
		return (sum/(start_times.size()));
	}

	void exec_step(DPNC342& module, const double step_voltage){
		// Create new tree
		init_dataset(module, step_voltage);
		std::cout << "Taking data with " << step_voltage << "V (" <<  module.control_reg.DAC4.get() <<")\n";
		size_t iteration {0};
		while(iteration < iterations){
			exec_iteration(module, iteration++);
		}
		// Calculate transfer rate
		auto time = get_read_mean_time();
		std::cout << "Mean transfer rate was "
				  << (16 * time) << " sec. per MB ("
				  << 1.0/(16 * time) << " MB/sec\n";
	}

	void init_dataset(DPNC342& module, const double step_voltage){
//		module.control_reg.load();
		module.set_voltage(step_voltage);
//		module.control_reg.commit();
//		module.control_reg.load();
		std::cout << "Voltage is set to: " << module.get_voltage() << '\n';
		// Close the old three and create new one.
		dataset.reset(new TTree(get_dataset_name(step_voltage),
								"Calibration data"),
					  TTreeDeleter());
		// (Re)create the branches
		std::stringstream ss;
		std::string name;
		std::string leaf_type;
		for(unsigned i = 0; i < module.MaxChannels; ++i){
			//TODO: Ugly! do it better!!!
			get_branch_name(name, leaf_type, "Channel", i, "[1024]/s");
			dataset->Branch(name.c_str(),
							cells[i],
							leaf_type.c_str());
		}

		get_branch_name(name, leaf_type, "Temperature", -1, "[4]/I");
		std::cout << name << " " << leaf_type << '\n';
		dataset->Branch(name.c_str(), &temperature, leaf_type.c_str());
		get_branch_name(name, leaf_type, "StopCell", -1, "[4]/I");
		dataset->Branch(name.c_str(), &stop_cell, leaf_type.c_str());
		get_branch_name(name, leaf_type, "StopWRS", -1, "[4]/I");
		dataset->Branch(name.c_str(), &stop_wrs, leaf_type.c_str());
		get_branch_name(name, leaf_type, "PLLLock", -1, "[4]/I");
		dataset->Branch(name.c_str(), &pll_lock, leaf_type.c_str());
		dataset->Branch("ADCPhase", &adc_phase, "ADCPhase/I");
	}

	void get_branch_name(std::string& name,
			std::string& leaf_type,
			const std::string prefix,
			const int index,
			const std::string type_name){
		std::stringstream ss;
		ss << prefix;
		if(index >= 0)
			ss << index;
		name = ss.str();
		ss << type_name;
		leaf_type = ss.str();
	}

	bool wait_for_data(DPNC342& module){
			const unsigned max_laps = 200000; // 10 seconds timeout
			const unsigned wait_time_ns = 50;
			unsigned laps(0);
			module.status_reg.load();
		//	std::cout << "Module is " << ((module.status_reg.stat_idle.get())?"idle":"not idle") << '\n';
			while( !module.status_reg.stat_idle && (laps < max_laps) ){
		//		std::cout << "Module is " << ((module.status_reg.stat_idle.get())?"idle":"not idle");
		//		std::cout << " laps: " << laps << " Max laps: " << max_laps << '\n';
				std::this_thread::sleep_for(std::chrono::nanoseconds(wait_time_ns));
				module.status_reg.load(); // Load status register from the device;
				++laps;
			}
			//std::cout << "Waited for " << laps * wait_time_ns << " ns.\n";
			return (laps < max_laps);
		}

	void transfer_data(DPNC342& module, const size_t iteration_num){
		start_times[iteration_num] = std::chrono::steady_clock::now();
		module.memory.read_all();
		end_times[iteration_num] = std::chrono::steady_clock::now();
//			cells.reset(&module.memory.buffer.front());
//			if(!cells)
//				cells = new unsigned short [32 * 1024];
			for(unsigned i = 0; i < 32; ++i)
				std::copy(module.memory.buffer.begin() + i * 1024,
						  module.memory.buffer.end(),
						  cells[i]);
//			for(unsigned channel = 0; channel < module.MaxChannels; ++channel){
//				for(unsigned cell=0; cell < module.CellsPerChannel; ++cell){
//	//				plots[channel]->Fill(cell, module.memory.read_cell(channel, cell, true));
//					auto chunk = module.memory.read_cell(channel, cell, true);
//				}
//	//				data[channel * 1024 + cell] = module.memory.read_cell(channel, cell);
//			}
			module.status_reg.load();
			module.control_reg.load();
			temperature[0] = module.status_reg.temperature_0.get();
			temperature[1] = module.status_reg.temperature_1.get();
			temperature[2] = module.status_reg.temperature_2.get();
			temperature[3] = module.status_reg.temperature_3.get();

			stop_cell[0] = module.status_reg.stop_cell_0.get();
			stop_cell[1] = module.status_reg.stop_cell_1.get();
			stop_cell[2] = module.status_reg.stop_cell_2.get();
			stop_cell[3] = module.status_reg.stop_cell_3.get();

			stop_wrs[0] = module.status_reg.stop_wrs_0.get();
			stop_wrs[1] = module.status_reg.stop_wrs_1.get();
			stop_wrs[2] = module.status_reg.stop_wrs_2.get();
			stop_wrs[3] = module.status_reg.stop_wrs_3.get();

			pll_lock[0] = module.status_reg.plllck0.get();
			pll_lock[1] = module.status_reg.plllck1.get();
			pll_lock[2] = module.status_reg.plllck2.get();
			pll_lock[3] = module.status_reg.plllck3.get();

			adc_phase = module.control_reg.adcclk_phase.get();
		}




	void read_data(DPNC342& module, const size_t iteration_num){
		module.control_reg.start_trig = 1;
		module.control_reg.commit();
		module.control_reg.start_trig = 0;
		// Send a software trigger
		module.control_reg.soft_trig = 1;
		module.control_reg.commit();
		module.control_reg.soft_trig = 0;
		wait_for_data(module);
		transfer_data(module, iteration_num);
	}

	void exec_iteration(DPNC342& module, const size_t iteration_num){
//		if( (iterations % iteration_num) == 0)
//			std::cout << '.';
		read_data(module, iteration_num);
		dataset->Fill();
	}

	void run(DPNC342& module){
		double voltage {min_voltage};
		while(voltage < max_voltage){
			exec_step(module, voltage);
			voltage += step;
		}
	}

	size_t iterations;
	double min_voltage, max_voltage, step;
	std::shared_ptr<TFile> data_file {nullptr}; // This should be declared before all trees as the objects are deleted in reverse order of declaration
	std::shared_ptr<TTree> dataset {nullptr};
	std::shared_ptr<TTree> conditions {nullptr};
	//std::shared_ptr<
	unsigned short * cells[32] {nullptr};//[32 * 1024];
	int temperature[4];
	int stop_cell[4];
	int stop_wrs[4];
	int pll_lock[4];
	int adc_phase;
	std::vector<std::chrono::steady_clock::time_point> start_times, end_times;
};

void init_module(DPNC342& module){
	//Disconnect analog inputs
	module.control_reg.load();
	module.control_reg.start_trig = 0;
	module.control_reg.reinit_trig = 1;
	module.control_reg.soft_trig = 0;
	module.control_reg.commit();

	module.control_reg.clear();
	// Begin configuration
	module.control_reg.dactive = 1;
	module.control_reg.trigger_config = 0x1F; // 0x1F --> all trigger sources 0x10 --> front panel trigger only
//	module.control_reg.tca_ctrl = 0;
	module.control_reg.enable_trigger = 1;
	module.control_reg.acalib = 1;
	module.control_reg.DAC4 = 0x0000;
//	module.control_reg.drs_ctl_first_chn = 0;
	module.control_reg.drs_ctl_last_chn = 7;
	module.control_reg.channel_config = 0xFF;
	module.control_reg.dmode = 1;
	module.control_reg.pllen = 1;
//	module.control_reg.wrsloop = 0;
	module.control_reg.drs_ctl_config_7_3 = 0x1F; // five MSB reserved, must be all ones
	module.control_reg.commit();
	std::cout << module.control_reg << std::endl;
	// End of configuration
}

int main(int argc, char **argv){
	std::cout << "Begin.\n";
		auto devices = find_devices(DPNC342Module::VENDOR_ID,
									DPNC342Module::PRODUCT_ID);
		if(devices.size()){
			std::cout << "Found " << devices.size() << " devices.\n";

			USBDevice::Pointer device = devices[0];
			DPNC342 module(device);

			module.open();
			init_module(module);
			module.control_reg.commit(); // clean control register
			module.status_reg.load();
			module.control_reg.load();
//			TApplication theApp("DPNC342 DRS4 module voltage calibration", &argc, argv);
			VoltageCalibration calibration(1000, 0.0, 2.0, 0.2);
			calibration.initialize();
			calibration.run(module);

//			the_
		}
	return 0;
}
