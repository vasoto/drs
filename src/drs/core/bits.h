/*
 * bits.h
 *
 *  Created on: Sep 25, 2016
 *      Author: Vassil Z. Verguilov
 */
#pragma once

namespace drs4{
namespace core{

/*
#define GETMASK(index, size) (((1 << (size)) - 1) << (index))
#define READFROM(data, index, size) (((data) & GETMASK((index), (size))) >> (index))
#define WRITETO(data, index, size, value) ((data) = ((data) & (~GETMASK((index), (size)))) | ((value) << (index)))
#define FIELD(data, name, index, size) \
  inline decltype(data) name() { return READFROM(data, index, size); } \
  inline void set_##name(decltype(data) value) { WRITETO(data, index, size, value); }
 */


inline int get_mask(int length, int offset){
	return (((1 << length) - 1) << offset);
}

inline int read_region(int value, int length, int offset=0){
//	return ((value  >> offset) & get_mask(length, offset));
	return ((value >> offset) & ((1 << length) - 1));
}


inline int write_region(int value, int data, int length, int offset=0){
	// ((data & (1 << length) - 1) << offset) ==
	// truncate data to the right size and move it to the appropriate position
//	return ((value & ~get_mask(length, offset)) | (((data & (1 << length)) - 1) << offset));
	auto mask = ((1 << length) - 1);
	return (value & ~ (mask << offset)) | ((data & mask) << offset);
}

//template<typename T>
//T create_mask(const short length, const short start){
//	return (T)((1 << length) - 1) << start;
//}


}
}

