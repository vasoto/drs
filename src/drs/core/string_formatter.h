/*
 * string_formatter.h
 *
 *  Created on: Oct 7, 2016
 *      Author: Vassil Z. Verguilov
 */
#pragma once

#include <sstream>


class StringFormatter{
public:
	StringFormatter() = default;
    StringFormatter(const StringFormatter&) = delete;
    StringFormatter & operator= (StringFormatter &) = delete;

    ~StringFormatter() = default;

    template <typename Type>
    StringFormatter & operator << (const Type & value){
        _stream << value;
        return (*this);
    }

    std::string str() const         { return (_stream.str()); }
    operator std::string () const   { return (_stream.str()); }

    enum ConvertToString{ to_str };

    std::string operator >> (ConvertToString) { return (_stream.str()); }
private:
    std::stringstream _stream {};
};

