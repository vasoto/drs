/*
 * type_tools.h
 *
 *  Created on: Aug 25, 2016
 *      Author: Vassil Z. Verguilov
 */
#pragma once


// Convert enum value to int
template<typename EnumType>
constexpr int enum_to_int(EnumType value){
	return (static_cast<int>(value));
}

template<typename EnumType>
constexpr EnumType int_to_enum(int value){
	return (static_cast<EnumType>(value));
}
