/*
 * memory_map.h
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */
#pragma once

#include <iostream>
#include <memory>
#include <bitset>
#include <cassert>

#include "drs/core/bits.h"
//#include "drs/io/base.h"
#include "drs/daq/core/module_signature.h"

using drs4::core::read_region;
using drs4::core::write_region;
using drs4::io::IOBase;
using drs4::daq::core::base::ModuleSignature;
//using drs4::daq::core::detail::ModuleConstituent;

namespace drs4{
namespace daq{
namespace core{



//struct MemoryMap{
//	// @class MemoryMap
//	// Same as register, the only difference is that memory chunks are not cached but read/set on every access
//	MemoryMap() = delete; // Explicitly remove the default constructor
//	MemoryMap(ModuleSignature::Pointer t_owner):owner(t_owner){};
//
//	ModuleSignature::Pointer owner;
//};


struct MemoryFragment : public detail::ModuleConstituent{
	MemoryFragment(size_t t_address, size_t t_size, ModuleSignature::Pointer t_module):
		ModuleConstituent(t_module),
			address(t_address),
			size(t_size){
//		std::cout << "Base Address: " << TOHEX(8) << address
//				  << " Size: " << TOHEX(8) << size
//				  << " Highest address: " << TOHEX(8) << address + size - 1 << TODEC() << std::endl;
	}

	MemoryFragment(size_t t_address, size_t t_size, MemoryFragment& parent):
		ModuleConstituent(parent.module),
		address(parent.address + t_address),
		size(t_size){
//					std::cout << "Base Address: " << TOHEX(8) << address
//						  << " Size: " << TOHEX(8) << size
//						  << " Highest address: " << TOHEX(8) << address + size - 1 << TODEC() << std::endl;
					assert( (address + size <= parent.size) );
				}

	void read(unsigned * buffer, size_t length = 0, size_t offset = 0){
		if (length == 0)
			length = size;
		assert(address + offset + length <= address + size); // Check boundaries
//		std::cout << "Request to read " << length << " words (" << length * 4 << " bytes)\n";
		module->io->read_block(address + offset, buffer, length);
	}

	void write(unsigned * buffer, size_t length = 0, size_t offset = 0){
		if (length == 0)
				length = size;
		assert(address + offset + length <= address + size); // Check boundaries
		module->io->write_block(address + offset, buffer, length);
	}

	const size_t address;
	const size_t size;
};

}
}
}
