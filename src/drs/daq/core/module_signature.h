/*
 * module_signature.h
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */

#pragma once
#include "drs/io/io.h"

//using drs4::io::detail::IOEnabled;

namespace drs4 {
namespace daq {
namespace core {
namespace base {


struct ModuleSignature : public drs4::io::detail::IOEnabled{
	// Holds definition of variables and methods specific for a given module
	typedef ModuleSignature* Pointer;

	using IOEnabled::IOEnabled;

	virtual ~ModuleSignature() = default;

	virtual void load() = 0;
};



}  // namespace base

namespace detail {

struct ModuleConstituent{
	// Part of the module
	explicit ModuleConstituent(base::ModuleSignature::Pointer t_owner):module(t_owner){};
	virtual ~ModuleConstituent() = default;
	ModuleConstituent& operator=(const ModuleConstituent& other) = delete;
	ModuleConstituent(const ModuleConstituent& other)=delete;
	base::ModuleSignature::Pointer module;
};


}  // namespace detail
}  // namespace core

}  // namespace daq

}  // namespace drs4
