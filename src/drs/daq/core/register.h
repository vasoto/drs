/*
 * register.h
 *
 *  Created on: Sep 25, 2016
 *      Author: Vassil Z. Verguilov
 */
#pragma once
#include <iostream>
#include <memory>
#include <bitset>
#include <vector>
#include <cstring>
#include <type_traits>


#include "drs/core/bits.h"
#include "drs/daq/core/module_signature.h"

using drs4::core::read_region;
using drs4::core::write_region;
using drs4::io::IOBase;

namespace drs4{
namespace daq{
namespace core{
namespace detail{

template<size_t Count>
struct RegisterData{
	unsigned data[Count];
};

}// namespace detail


//struct Register{
//	/* @class Register - base class for interpreting hardware register data
//	 *
//	 */
//	Register() = delete; // Explicitly remove the default constructor
//	Register(size_t register_size, size_t address): data(register_size, 0),
//			size(register_size),
//			base_address(address){};
//
//	virtual void read(IOBase::Pointer reader){ reader->read(base_address, data.data(), size * sizeof(unsigned)); };
//	virtual void write(IOBase::Pointer writter){ writter->write(base_address, data.data(), size * sizeof(unsigned));};
//
//	virtual ~Register(){};
//
//
//	void set_data(unsigned * other){
//		// just for debugging and testing should be removed in the final version
////		data.reset(reinterpret_cast<unsigned[]>(std::move(other)));
//		for(size_t i = 0; i < size; ++i)
//			data[i] = other[i];
//	}
//
//	//std::unique_ptr<unsigned[]> data;
//	std::vector<unsigned> data;
//	size_t size;
//	const unsigned base_address;// {Address};
//
//};

struct Register: public detail::ModuleConstituent{
	typedef Register * Pointer;
	explicit Register( size_t t_size,
			  size_t t_base_address,
			  base::ModuleSignature::Pointer owner):
		ModuleConstituent(owner),
		data(t_size, 0),
		base_address(t_base_address){}
	Register() = delete;
	//using ModuleConstituent::ModuleConstituent; // Propagate ModuleConstituent constructors

	void load(){
		clear();
//		std::cout << "Reading " << data.size()
//				  << " words from base_address " << base_address << std::endl;
		module->io->read_block(base_address,
							   &data.front(),
							   data.size());

	} // Example use of the IO read
	void commit(){
		module->io->write_block( base_address,
			&data.front(),
			data.size());
		load();
//		clear();
	} // Example use the IO write

	void clear(){
		std::fill(data.begin(), data.end(), 0);
	}
	std::vector<unsigned> data;
	size_t base_address;
};

namespace detail {

template<size_t Index, size_t Length, size_t Offset>
struct RegisterAttribute{
	RegisterAttribute() = delete;
	RegisterAttribute(Register * parent):_parent(parent){}

	// setter
#pragma GCC diagnostic ignored "-Weffc++"
	void operator = (const int &i) {
		_parent->data[Index] = static_cast<std::make_unsigned<int>::type>(write_region(
				static_cast<std::make_signed<unsigned>::type>(_parent->data[Index]),
				i,
				Length,
				Offset));
		//	return (i); 
	} // implement bit(s) set
#pragma GCC diagnostic pop

	// getters
	inline int get(){
		return (read_region(
					static_cast<std::make_signed<unsigned>::type>(_parent->data[Index]),
					static_cast<std::make_signed<unsigned>::type>(Length),
					static_cast<std::make_signed<unsigned>::type>(Offset)));
	}

	inline operator int () { return (get()); }

	static constexpr size_t index = Index;
	static constexpr size_t length = Length;
	static constexpr size_t offset = Offset;

	// pointer is not owned by us,
	// so no destructor is needed
	Register * _parent; //Shared_ptr?
};

template<size_t Index, size_t Length, size_t Offset>
std::ostream& operator << (std::ostream& out, RegisterAttribute<Index, Length, Offset>& rattr){
	out << std::bitset<rattr.length>(rattr) << " (" << reinterpret_cast<int>(rattr)
		<< ") Index: " << rattr.index
		<< " Length: " << rattr.length
		<< " Offset: " << rattr.offset;
//	out << std::endl << std::bitset<32>(rattr);
 	return (out);

}

#define REG_ATTR(name, index, length, offset) drs4::daq::core::detail::RegisterAttribute<index, length, offset> name {this};

} // namespace detail


} // namespace core
} // namespace daq
} // namespace drs4

