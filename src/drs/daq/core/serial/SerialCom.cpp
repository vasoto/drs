/*
 * SerialCom.cpp
 *
 *  Created on: Oct 7, 2016
 *      Author: Vassil Z. Verguilov
 */
#include <iostream>
#include <sstream>
#include <iosfwd>

// Linux
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <unistd.h>

#include "SerialCom.h"

namespace drs {
namespace daq {
namespace core {
namespace serial {

//SerialCom::SerialCom():config(),status(0) {}

SerialCom::SerialCom(ComPortConfig t_config) : status(0),config(t_config),handle(-1){}

SerialCom::~SerialCom(){
	disconnect();
}

int SerialCom::connect() {
	 // Open the port
	handle = open(config.serial_port, (O_RDWR | O_NOCTTY) & ~O_NONBLOCK);
	status = 0;
	if(handle == -1){
		std::cerr << "Error: Cannot open COM port " << config.serial_port << '\n';
		status = -1;
		return -1;
	}
	if(!isatty(handle)){
		std::cerr << "Error: Handle doesn't point to a TTY device.\n";
		status = -2;
		return -2;
	}
	termios options;
	//get the current settings of the serial port
	tcgetattr(handle, &options);

	{
	//set read and write speed
	unsigned speed = static_cast<unsigned int>(config.baud_rate);
	if( cfsetispeed(&options, speed) || cfsetospeed(&options, speed)){
		std::cerr << "Error: Cannot set port speed.\n";
		status = -3;
		return -3;
	}
	}
	options.c_cflag = static_cast<unsigned int>(config.byte_size) | CREAD | CLOCAL; //8n1
	std::cout << "cflags: " << options.c_cflag << '\n';
	options.c_cflag = ~PARENB & ~CSTOPB & ~CSIZE;
	std::cout << "cflags: "<< options.c_cflag << '\n';
	options.c_cc[VMIN] = 2; //minimum amount of characters to read
	options.c_cc[VTIME] = 10;
	if(tcsetattr(handle, TCSANOW, &options) != 0){
		std::cerr << "Error: Cannot set options.\n";
		status = -4;
		return -4;
	}
	status = 1;
	return 1;
}

int SerialCom::disconnect() {
	if( status > 0){
		close(handle);
	}
}

int SerialCom::send(const std::string& command) {
	 if(status<1){
		 std::cerr << "Error: Serial port is not connected.\n";
		 return -1;
	 }
	 // mutex lock here
	 std::string cmd = command;
	 cmd += 13; cmd += 10; // CR+LF
	 if( write(handle, cmd.c_str(), cmd.size()) < 1 ){
		 std::cerr << "Error: Cannot write " << cmd << '\n';
		 // mutex unlock
		 return -1;
	 }
	 // mutex unlock
	 return 0;
}

std::string SerialCom::query(const std::string& command) {
	std::string result;
	if( send(command) < 0 ){
		return result; // TODO throw
	}
	// mutex lock here
	const size_t buffer_size = 100;
	char buffer[buffer_size + 1] {0};
	int bytes = 0;
	bytes = read(handle, buffer, buffer_size);
	if(bytes < 1){
		std::cerr << "Error: Cannot read serial port.\n";
		// unlock mutex
		return result; // TODO throw
	}
	// mutex unlock
	result = buffer;
	return result;

}



} /* namespace serial */
} /* namespace core */
} /* namespace daq */
} /* namespace drs */

