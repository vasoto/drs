/*
 * SerialCom.h
 *
 *  Created on: Oct 7, 2016
 *      Author: Vassil Z. Verguilov
 */

#ifndef SRC_DRS_DAQ_CORE_SERIAL_SERIALCOM_H_
#define SRC_DRS_DAQ_CORE_SERIAL_SERIALCOM_H_
#include <string>

namespace drs {
namespace daq {
namespace core {
namespace serial {

//#define  B110	0000003
//#define  B134	0000004
//#define  B150	0000005
//#define  B200	0000006
//#define  B300	0000007
//#define  B600	0000010
//#define  B1200	0000011
//#define  B1800	0000012
//#define  B2400	0000013
//#define  B4800	0000014
//#define  B9600	0000015
//#define  B19200	0000016
//#define  B38400	0000017

enum struct ComPortSpeed{
	Baud0 = 0,
	Baud50 = 1,
	Baud75 = 2,
	Baud110 = 3,
	Baud134 = 4,
	Baud150 = 5,
	Baud200 = 6,
	Baud300 = 7,
	Baud600 = 10,
	Baud1200 = 11,
	Baud1800 = 12,
	Baud2400 = 13,
	Baud4800 = 14,
	Baud9600 = 15,
	Baud19200 = 16,
	Baud38400 = 17
};

enum struct ComPortByteSize{
	Byte5 = 0000000,
    Byte6 = 0000020,
	Byte7 = 0000040,
	Byte8 = 0000060
};

struct ComPortConfig{
	char * serial_port;
	ComPortSpeed baud_rate = ComPortSpeed::Baud9600;
	ComPortByteSize byte_size = ComPortByteSize::Byte8;
	int stop_bits = 0;
	int parity = 0;
};

class SerialCom {
public:
	SerialCom() = delete;
	explicit SerialCom(ComPortConfig t_config);

	virtual ~SerialCom();
	//TODO: implement constructors and operators

	int connect();
	int disconnect();
	int send(const std::string& command);
	std::string query(const std::string& command);

private:
	int status;
	ComPortConfig config;
	int handle;
};

} /* namespace serial */
} /* namespace core */
} /* namespace daq */
} /* namespace drs */

#endif /* SRC_DRS_DAQ_CORE_SERIAL_SERIALCOM_H_ */
