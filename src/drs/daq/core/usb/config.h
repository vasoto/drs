/*
 * USBConfig.h
 *
 *  Created on: Sep 20, 2016
 *      Author: vasoto
 */

#pragma once
#include <memory>
#include "utils.h"

#include <libusb-1.0/libusb.h>
//#include "./utils.h"
//#include "./types.h"

namespace drs4 {
namespace daq {
namespace core {
namespace usb {

namespace detail {

struct usb_config_deleter{
	void operator()(libusb_config_descriptor *config) { libusb_free_config_descriptor(config); };

};

typedef std::shared_ptr<libusb_device> DevicePointer;

struct USBConfig{
	typedef std::shared_ptr<libusb_config_descriptor> ConfigPointer;
	typedef std::shared_ptr<USBConfig> Pointer;

	USBConfig() = default;

	explicit USBConfig(const DevicePointer& device):_config(nullptr){
		libusb_config_descriptor *config;
		auto err = libusb_get_config_descriptor(device.get(), 0, &config);
		if(!is_error(err, "Cannot get device configuration.")){
			_config.reset(config, usb_config_deleter());
		}
	}

	ConfigPointer _config;


};

}
}
}
}
}



//EOF
