/*
 * endpoint.h
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */

#pragma once
//#include "types.h"
//#include "utils.h"
#include <memory>
#include <vector>

#include <libusb-1.0/libusb.h>

#include "drs/core/type_tools.h"


namespace drs4{
namespace daq{
namespace core{
namespace usb{
namespace detail{


typedef std::shared_ptr<libusb_endpoint_descriptor> EndpointPointer;

struct usb_endpoint_deleter{
  void operator()(volatile libusb_endpoint_descriptor* ep_handle){
    // Empty. Endpoints are managed by the libusb.
    std::cout << "Going to kill endpoint " << ep_handle << '\n';
    return;
  }
};

struct Endpoint{

	typedef std::shared_ptr<Endpoint> Pointer;

	Endpoint() = default;

	explicit Endpoint(libusb_endpoint_descriptor * endpoint):
		_endpoint(endpoint, usb_endpoint_deleter()){}

	static Pointer create(libusb_endpoint_descriptor * endpoint){
		Endpoint * ep = new Endpoint(endpoint);
		return (std::move(Pointer(ep)));
	}

	short id(){
		return (_endpoint.get()->bEndpointAddress & 0xF); //bits 0-3
	}

	libusb_endpoint_direction direction(){
		return (int_to_enum<libusb_endpoint_direction>(_endpoint.get()->bEndpointAddress & 0x80)); // bit 7
	}

	short transfer_type(){
		/* Transfer Type
		    00 (0) = Control
		    01 (1) = Isochronous
		    10 (2) = Bulk
		    11 (3) = Interrupt
		*/
		return (_endpoint->bmAttributes & 0x3);
	}

	short synchronisation_type(){
		/*Synchronisation Type (Iso Mode)
    	 00 = No Synchonisation
    	 01 = Asynchronous
    	 10 = Adaptive
    	 11 = Synchronous
		 */
		return ((_endpoint->bmAttributes >> 2) & 0x3);
	}

	short usage_type(){
		/*Usage Type (Iso Mode)

		    00 = Data Endpoint
		    01 = Feedback Endpoint
		    10 = Explicit Feedback Data Endpoint
		    11 = Reserved
			*/
		return ((_endpoint->bmAttributes >> 4) & 0x3);
	}

	uint16_t maximum_packet_size(){
		return (_endpoint->wMaxPacketSize);
	}

	uint8_t poll_interval(){
		return (_endpoint->bInterval);
	}

	EndpointPointer _endpoint;
};

typedef std::vector<Endpoint::Pointer> Endpoints;

}
}
}
}
}





//EOF
