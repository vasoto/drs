/*
 * session.h
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */

#pragma once
#include <iostream>
#include <memory>

#include <libusb-1.0/libusb.h>

#include "utils.h"

const int USB_DEBUG_LEVEL = 1;//LIBUSB_LOG_LEVEL_DEBUG;

namespace drs4 {

namespace daq {

namespace core {

namespace usb {

namespace detail{

	struct usb_session_deleter{
		void operator()(libusb_context* context) { libusb_exit(context); };
	};

}  // namespace detail

struct USBSession : std::enable_shared_from_this<USBSession> {
public:
	typedef std::shared_ptr<libusb_context> ContextPointer;
	typedef std::shared_ptr<USBSession> Pointer;

	USBSession() = default;

	static void initialize(const int debug_level){
		if(!_context){
			libusb_context* context = nullptr;
			auto result = libusb_init(&context);
			if( is_error(result, "Error while initializing context") ){
				return; // exit function and leave _context = nullptr;
			}
			libusb_set_debug(context, debug_level);
			// Store in a shared_ptr
			_context.reset(context, detail::usb_session_deleter());
		}
	}

	static std::shared_ptr<USBSession> create(const int debug_level=USB_DEBUG_LEVEL){
		USBSession * session = new USBSession();
		if(!_context){
			session->initialize(debug_level);
		}
		return(std::move(Pointer(session)));
	}

	static ContextPointer get_context(){return (_context);}
private:
	static ContextPointer _context;
};

USBSession::ContextPointer USBSession::_context;


}  // namespace usb

}  // namespace core

}  // namespace daq

}  // namespace drs4


//EOF
