/*

 * usb_device.cpp
 *
 *  Created on: Sep 28, 2016
 *      Author: vasoto
 */
#include <iostream>
#include <vector>
#include <unistd.h>
#include <bitset>
#include <string.h>

#include "usb_device.h"

namespace drs4 {

namespace daq {

namespace core {

namespace usb {

namespace detail{



USBDeviceImpl::USBDeviceImpl(libusb_device* device):
		_device(libusb_ref_device(device),
		 usb_device_deleter()),
		 _handle(nullptr),
		 _descriptor(nullptr),
		 _config(nullptr),
//		 _endpoints(),
		 _ep_in(0x81),
		 _ep_out(0x01)
{
	_config.reset( new USBConfig(_device) );
//	std::cout << "USBDeviceImpl::USBDeviceImpl Ctor\n";
}


USBDeviceImpl::USBDeviceImpl(const USBDeviceImpl& impl):
		_device(impl._device),
		_handle(impl._handle),
		_descriptor(impl._descriptor),
		_config(new USBConfig(impl._device)),
//		_endpoints(),
		_ep_in(0x81),
		_ep_out(0x01){
//  std::cout << "USBDeviceImpl::USBDeviceImpl Ctor\n";
}

DescriptorPointer& USBDeviceImpl::get_descriptor(){
	// Lazy load - loads only once, usually when searching by VID+PID
	if(!_descriptor){
		//DEBUG("Loading libusb_device_descriptor.")
		libusb_device_descriptor* tmp = new libusb_device_descriptor();
		auto dev = _device.get();
		auto result = libusb_get_device_descriptor(dev, tmp);
		if(!is_error(result, "Error: libusb_get_device_descriptor() failed" ))
			_descriptor = DescriptorPointer(std::move(tmp));
	}
	return (_descriptor);
}


void USBDeviceImpl::close(){
	// Invalidate device handle and related structures
	_descriptor.reset();
	_handle.reset();
}

void USBDeviceImpl::open(){
  if(!is_open()){
    libusb_device_handle * handle;// = new libusb_device_handle();
    auto result = libusb_open(_device.get(), &handle);

    if(is_error(result, "Error: libusb_open failed") )
      return;

    _handle = detail::HandlePointer(std::move(handle),
				    usb_handle_deleter());
    result = libusb_reset_device(_handle.get()); // Reset the device
        if(is_error(result, "Error: libusb_reset_device failed") )
              return;
    // load all lazy loads data
    get_max_packet_read();
    get_max_packet_write();
    get_max_speed();
    std::cout << "After setting handle is: " << _handle.get() << " count: " << _handle.use_count() << std::endl;
    //select_endpoints();
  } else
    std::cerr << "Warning: Device is already opened!" << std::endl;

}

  std::ostream& operator<<(std::ostream& os, const USBCmdPacket& pkt){
    os << "Command: " << pkt.command 
       << " Address: " << pkt.address
       << " Length: " << pkt.byte_size;
    return (os);
  }

  void USBDeviceImpl::read_block(size_t address, unsigned * buffer, size_t length){
	  const int bytes_in_word = 4;
	  int size = static_cast<std::make_signed<unsigned>::type>(length) * bytes_in_word;
	  USBCmdPacket packet {USBCommands::Read,
		  static_cast<unsigned>(address),
		  static_cast<unsigned>(size) };
	  int transferred;
	  int packet_size = static_cast<std::make_signed<unsigned>::type>(sizeof(packet));
	  int rv = out(reinterpret_cast<unsigned char*>(&packet),
			  	    packet_size,
					transferred);
	  assert(transferred == packet_size);
	  if(is_error(rv, "Cannot send request for read packet!")){
		  return;
	  }

//	  std::cout << "Write successful. Going to read "<< size << " " << (length*bytes_in_word) << " bytes.\n";
	  transferred = 0;
	  unsigned char * buf = reinterpret_cast<unsigned char *>(buffer);
	  int bytes_left = size;
	  int cnt = 0;
	  while( bytes_left > max_size_in ){
//		  std::cout << cnt <<  " bytes read. Left: " << bytes_left << " bytes to read\n";
		  rv = in(buf + size - bytes_left,
				  max_size_in,
				  transferred);
	  	  bytes_left -= transferred;

	  	  if(is_error(rv, "Error while reading block data.")){
	  		  return;
	  	  }
		  assert(transferred == max_size_in);
//		  cnt+= transferred;
  	  }
//	  std::cout << "size: " << size
//			    << " bytes_left: " << bytes_left
//				<< " max_size_in: " << max_size_in << '\n';
  	  rv = in(buf + (size - bytes_left),
  			  bytes_left,
  			  transferred);
  	  if(is_error(rv, "Error while reading block data.")){
		  return;
	  }
//  	  std::cout << "Transferred: " << transferred << std::endl;
	  assert(transferred == bytes_left);
  }


void USBDeviceImpl::write_block(size_t address, unsigned * buffer, size_t length){
	int data_size = static_cast<int>(sizeof(unsigned) * length);
	int size = static_cast<int>(sizeof(USBCmdPacket)) + data_size;
	unsigned char * buf = new unsigned char[size];
	int transferred;
	(reinterpret_cast<USBCmdPacket *>(buf))->command  = USBCommands::Write;
	(reinterpret_cast<USBCmdPacket *>(buf))->address  = static_cast<unsigned>(address);
	(reinterpret_cast<USBCmdPacket *>(buf))->byte_size = static_cast<unsigned>(data_size);
	memcpy((reinterpret_cast<unsigned *>(buf + sizeof(USBCmdPacket))),
			buffer,
			static_cast<size_t>(data_size));

	int rv = out(buf, size, transferred);
	if (is_error(rv, "Couldn't write data")){
		return;
	}
	assert(transferred == size);
}


int USBDeviceImpl::in( unsigned char* data,
       int buffer_size /* bytes */,
       int& transferred /* bytes */) {
// for bufsize == 0 (from request for ZLP) we adjust bufsize to 1 to avoid errors
// was necessary for windows, we keep it here because it won't hurt
//	if (!buffer_size) buffer_size++;
	assert(buffer_size);
	auto rv = libusb_bulk_transfer(
			_handle.get(),
			_ep_in,
			data,
						   buffer_size,
						   &transferred,
						   1000);
	is_error(rv,"Read data failed.");
	return (rv);
}

unsigned USBDeviceImpl::get_max_packet_read() {
	if(max_size_in == 0){
		//max_size_in = get_max_packet_size(_ep_in);
		max_size_in = 3670016; // Hardcode to 3.5Mb
	}
	return (max_size_in);


}

unsigned USBDeviceImpl::get_max_packet_write() {
	if(max_size_out == 0){
		max_size_out = get_max_packet_size(_ep_out);
	}
	return (max_size_out);

}

int USBDeviceImpl::out(unsigned char* data,
	       int buffer_size /* bytes */,
	       int& transferred /* bytes */) {
  auto rv=libusb_bulk_transfer(_handle.get(),
			       _ep_out,
			       data,
			       buffer_size,
			       &transferred,
			       1000u);
  is_error(rv, "Write data failed.");
  return (rv);
}

unsigned USBDeviceImpl::get_max_packet_size(unsigned char endpoint) {
	/*** Get transfer size EP_OUT ***/
	return (libusb_get_max_packet_size(_device.get(), endpoint));
}

unsigned USBDeviceImpl::get_max_speed() {
	if(max_speed == 0)
		max_speed = static_cast<int>(libusb_get_device_speed(this->_device.get()));
	return max_speed;
}

} // namespace detail

USBDeviceList enumerate_devices(){
	auto session = USBSession::create();
	libusb_device **device_list = nullptr;
	auto dev_count = libusb_get_device_list(USBSession::get_context().get(),
											&device_list);
	// static?
	USBDeviceList result(static_cast<size_t>(dev_count), nullptr);
	std::transform(device_list,
				   device_list + dev_count,
				   result.begin(),
				   [](libusb_device* dev){
						//std::cout << "dev: " << dev << std::endl;
						return(std::move(USBDevice::create(std::move(dev))));});
	libusb_free_device_list(device_list, 1);
	return (result);
}

USBDeviceList find_devices(int vendor_id, int product_id){
	auto devices = enumerate_devices();
	USBDeviceList result;
	//TODO(vasoto): Do it with copy_if or another function
	for(auto& dev : devices){
		auto descr = *dev->get_descriptor(); // loads
		if ((descr.idVendor == vendor_id) &&
			(descr.idProduct == product_id)){
			result.push_back(dev);
		}
	}
	return(result);
}




}  // namespace usb

}  // namespace core

}  // namespace daq

}  // namespace drs4


//EOF
