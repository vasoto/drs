/*
 * usb_device.h
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */

#pragma once
#include <memory>
#include <algorithm>
#include <cassert>
#include <vector>

#include <libusb-1.0/libusb.h>

#include "drs/io/io.h"
#include "config.h"
#include "endpoint.h"
#include "session.h"



namespace drs4 {

namespace daq {

namespace core {

namespace usb {

namespace detail{

typedef std::shared_ptr<libusb_device_handle> HandlePointer;
typedef std::shared_ptr<libusb_device> DevicePointer;
typedef std::shared_ptr<libusb_device_descriptor> DescriptorPointer;

const unsigned TIMEOUT_RW = 1000; /* ms */

enum USBCommands { Read=1, Write=2, ReadZlp=5, USBCmds };


//struct __attribute__((__packed__))
struct		USBCmdPacket {
	unsigned command; // As specified in USBCommands
	unsigned address; // Address to read from
	unsigned byte_size; // Size of data to r/w, in bytes
};


// Deleter functor
struct usb_device_deleter{
	void operator()(libusb_device* dev) { libusb_unref_device(dev); };
};

struct usb_handle_deleter{
	void operator()(libusb_device_handle* dev_handle){
		std::cout << "Handle " << dev_handle << " is going to be deleted.\n";
		libusb_close(dev_handle); }
};



class USBDeviceImpl : public drs4::io::IOBase {
public:
	explicit USBDeviceImpl(libusb_device* device);

	USBDeviceImpl(const USBDeviceImpl& impl);

	DevicePointer get_device(){return( _device);}
	HandlePointer get_handle(){return( _handle);}

	DescriptorPointer& get_descriptor();

	void close();

	bool is_open(){ return (!!_handle.get());};

//	void select_endpoints();

	void open();

	virtual void read(){ std::cout << "Read USB!" << std::endl; };
	virtual void write(){ std::cout << "Read USB!" << std::endl; };
	virtual void read_block(size_t address, unsigned * buffer, size_t length);
	virtual void write_block(size_t address, unsigned * buffer, size_t length);

	unsigned get_max_speed();
	unsigned get_max_packet_read();
	unsigned get_max_packet_write();

protected:
	DevicePointer _device;
	HandlePointer _handle;
	DescriptorPointer _descriptor;
	USBConfig::Pointer _config;
//	Endpoints _endpoints;
	unsigned char _ep_in, _ep_out;
	unsigned max_size_in {0};
	unsigned max_size_out {0};
	unsigned max_speed {0};

	unsigned get_max_packet_size(unsigned char endpoint);
//	unsigned get_max_speed();

//	enum USBRWOperation { usb_read, usb_write };

	int in(unsigned char* data,
	       int buffer_size /* bytes */,
	       int& transferred /* bytes */);
	int out(unsigned char* data,
		       int buffer_size /* bytes */,
		       int& transferred /* bytes */);
};

}

template
<class USBImplementation>
class USBGenericDevice : public USBImplementation{
public:
//	typedef IOBase::Pointer Pointer;
	typedef std::shared_ptr<USBGenericDevice> Pointer;
	typedef USBGenericDevice<USBImplementation> Instance;
	USBGenericDevice() = delete;
	USBGenericDevice(libusb_device* dev) : USBImplementation(dev){};

	~USBGenericDevice() = default;

	explicit USBGenericDevice(const USBGenericDevice& ) = default;

		static Pointer create(libusb_device* dev){
			Pointer dev_ptr = Pointer(std::move(new Instance(dev)));
			return (dev_ptr);
		}

//		drs4::io::IOBase::Pointer get_io(){
//			return (dynamic_cast<drs4::io::IOBase::Pointer>(this));
//		}
};

typedef USBGenericDevice<detail::USBDeviceImpl> USBDevice;

typedef std::vector<USBDevice::Pointer> USBDeviceList;

USBDeviceList enumerate_devices();

USBDeviceList find_devices(int vendor_id, int product_id);


}  // namespace usb

}  // namespace core

}  // namespace daq

}  // namespace drs4

//EOF
