/*
 * utils.h
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */

#pragma once

#include<iostream>

#include "libusb-1.0/libusb.h"

#include "drs/core/type_tools.h"


bool is_error(int result, const std::string error_str = "Error:"){
	if(result<0){
		std::cerr << error_str << " (" << result << "): "
			  << libusb_error_name(int_to_enum<libusb_error>(result))
		  //				  <<  libusb_strerror(int_to_enum<libusb_error>(result))
				  << std::endl;
		return (true);
	}
	return (false);
}

//EOF
