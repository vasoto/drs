/*
 * memory.h
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */

#pragma once

#include "drs/daq/core/module_signature.h"
#include "drs/daq/core/memory_map.h"

using drs4::daq::core::base::ModuleSignature;
using drs4::daq::core::MemoryFragment;

//template<typename InIter, typename OutIter, typename In, typename Out>
//void split_words(InIter start, OutIter end, OutIter out, Out (*func)())


struct DPNC342MemoryMap: public MemoryFragment{
	explicit DPNC342MemoryMap(ModuleSignature::Pointer t_module):
		MemoryFragment(0x00000000, 64 * 16 * 1024, t_module),buffer(32 * 1024){};

	MemoryFragment EEPROM {0x00020000, 4 * 16 * 1024, *this};

	MemoryFragment Waveform_DRS1 {0x00040000, 16 * 1024, *this};
	MemoryFragment Waveform_DRS2 {0x00044000, 16 * 1024, *this};
	MemoryFragment Waveform_DRS3 {0x00048000, 16 * 1024, *this};
	MemoryFragment Waveform_DRS4 {0x0004C000, 16 * 1024, *this};

	MemoryFragment Pedestal_DRS1 {0x00080000, 16 * 1024, *this};
	MemoryFragment Pedestal_DRS2 {0x00084000, 16 * 1024, *this};
	MemoryFragment Pedestal_DRS3 {0x00088000, 16 * 1024, *this};
	MemoryFragment Pedestal_DRS4 {0x0008C000, 16 * 1024, *this};
/*
	void load_waveform(){
		//TODO(vasoto): create generic load function with enum
		// parameter to read either pedestals or waveforms
		unsigned * tmp = reinterpret_cast<unsigned *>(&buffer.front());
		this->read(tmp, this->size, 0);
//		buffer.data() = std::move(&tmp.front()));
	}
*/
	short read_cell(const unsigned channel, const unsigned cell, const bool cached = true){
		assert(channel < 32);
		assert(cell < 1024);
//
//		const unsigned max_cells = 1024;
//		const unsigned max_data_per_board = 8 * max_cells;
		const unsigned resolution = 12;
//		auto board = channel / 8;
		if( not cached ){
			read_all();
		}
		return (buffer[channel * 1024 + cell] & ((1<<resolution)-1) );

//		//TODO(vasoto): make it purrdy
////		std::cout << "Data is read.\n";
//		auto ch = channel % 8;
//		//auto buf = read_board(board);
//		MemoryFragment * drs;
//		switch(board){
//			case 0 : drs = const_cast<MemoryFragment *>(&Waveform_DRS1);
//			break;
//			case 1 : drs = const_cast<MemoryFragment *>(&Waveform_DRS2);
//			break;
//			case 2 : drs = const_cast<MemoryFragment *>(&Waveform_DRS3);
//			break;
//			case 3 : drs = const_cast<MemoryFragment *>(&Waveform_DRS4);
//			break;
//			default:
//				std::cout << "Error: This module has only 4 boards. Readout from board " << board << " was requested\n";
//		};
//		std::vector<unsigned> buffer(drs->size);
//		drs->read(&buffer.front(), drs->size, 0);
//		unsigned short * buf = reinterpret_cast<unsigned short *>(&buffer.front());
//		return ((buf[cell + ch * max_cells + board * max_data_per_board])&((1<<resolution)-1));
	}

//	unsigned short * read_board(const unsigned board) const{
//		MemoryFragment * drs;
//
//		switch(board){
//			case 0 : drs = const_cast<MemoryFragment *>(&Waveform_DRS1);
//			break;
//			case 1 : drs = const_cast<MemoryFragment *>(&Waveform_DRS2);
//			break;
//			case 2 : drs = const_cast<MemoryFragment *>(&Waveform_DRS3);
//			break;
//			case 3 : drs = const_cast<MemoryFragment *>(&Waveform_DRS4);
//			break;
//			default:
//				std::cout << "Error: This module has only 4 boards. Readout from board " << board << " was requested\n";
//		};
//		std::vector<unsigned> buffer(drs->size);
//		drs->read(&buffer.front(), drs->size, 0);
//		std::vector<unsigned short> result(8*1024, 0);
//		unsigned short * p = &result[0];
////		p = reinterpret_cast<unsigned short *>(&buffer.front()));
////		return (std::move(result));
//	}

	void read_all(){
		const unsigned data_size = 16 * 1024; // * 4 bytes for unsigned
		unsigned * tmp_ptr = reinterpret_cast<unsigned*>(&buffer.front());
		this->read(tmp_ptr, data_size, Waveform_DRS1.address);
	}

	std::vector<unsigned short> buffer;
};

