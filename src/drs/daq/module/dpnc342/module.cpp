/*
 * module.cpp
 *
 *  Created on: Sep 28, 2016
 *      Author: vasoto
 */

#include "module.h"

namespace drs4 {

namespace daq {

using core::base::ModuleSignature;

namespace module {

namespace dpnc342 {

DPNC342Module::DPNC342Module(IOBase::Pointer t_io):
			ModuleSignature(t_io),
			control_reg(this),
			status_reg(this),
			memory(this){
	//std::cout << "T_IO: " << t_io << '\n';

}


void DPNC342Module::load(){
	//
	control_reg.load();
	status_reg.load();
}


void DPNC342Module::set_voltage(double voltage, const unsigned dac){
	//std::cout << DACBITS << ", "<< voltage / MAXTRIGLEVEL << ", " << (voltage / MAXTRIGLEVEL) * DACBITS << std::endl;
	unsigned short value = (unsigned short) floor(((voltage / MAXTRIGLEVEL) * DACBITS) + 0.5);
	//static_cast<unsigned short>(((voltage / MAXTRIGLEVEL) * DACBITS) + 0.5);
	//std::cout << "Value: " << value << '\n';
	if(voltage < 0.0) value = 0;
	else if(voltage >= MAXTRIGLEVEL) value = DACBITS - 1;
	//std::cout << "Setting VCAL to " << value << " (" << voltage << "V)\n";
	switch (dac){
		case 0: control_reg.DAC0 = value; break;
		case 1: control_reg.DAC1 = value; break;
		case 2: control_reg.DAC2 = value; break;
		case 3: control_reg.DAC3 = value; break;
		case 4: control_reg.DAC4 = value; break;
		default: {
			std::cerr << "DAC index value " << dac << " is not valid. Valid values are [0-4]\n";
			return;
		}
	};
	control_reg.commit();
}

double DPNC342Module::get_voltage(const unsigned dac){
	control_reg.load();
	unsigned dac_value;// = control_reg.DAC4.get();

	switch (dac){
		case 0: dac_value = control_reg.DAC0.get(); break;
		case 1: dac_value = control_reg.DAC1.get(); break;
		case 2: dac_value = control_reg.DAC2.get(); break;
		case 3: dac_value = control_reg.DAC3.get(); break;
		case 4: dac_value = control_reg.DAC4.get(); break;
		default: {
			std::cerr << "DAC index value " << dac << " is not valid. Valid values are [0-4]\n";
			return (-100.0);
		}
	};
	// Readback DAC value from control register
	//std::cout << "VCAL: " << dac4 << '\n';
	unsigned char * buffer = reinterpret_cast<unsigned char *>(&dac_value);
	//const double MAXVCAL = 2.048;
	return (MAXVCAL * (buffer[0] + (buffer[1] << 8)) / 0xFFFF);
}


}  // namespace dpnc342
}  // namespace module
}  // namespace daq
}  // namespace drs4

