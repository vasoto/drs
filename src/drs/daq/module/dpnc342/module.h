/*
 * module.h
 *
 *  Created on: Sep 25, 2016
 *      Author: Vassil Z. Verguilov
 */
#pragma once
#include "drs/daq/core/module_signature.h"
#include "drs/io/io.h"
#include "registers.h"
#include "memory.h"
#include <math.h>

//using drs4::daq::module::dpnc342::ControlRegister;
//using drs4::daq::module::dpnc342::StatusRegister;
//using drs4::daq::

namespace drs4 {

namespace daq {

namespace module {

namespace dpnc342 {



struct DPNC342Module: public daq::core::base::ModuleSignature{
	static const unsigned VENDOR_ID = 0x04b4;
	static const unsigned PRODUCT_ID = 0x00f1;
	static const unsigned ChannelsPerBoard = 8;
	static const unsigned BoardCount = 4;
	static const unsigned CellsPerChannel = 1024;
	static const unsigned MaxChannels = ChannelsPerBoard * BoardCount;
	static constexpr double MAXVCAL = 2.048;
	static constexpr double MAXTRIGLEVEL = 2.048;
	static const unsigned short DACBITS = 0xFFFF;

	DPNC342Module() = delete;
	explicit DPNC342Module(drs4::io::IOBase::Pointer t_io);

	// Registers
	ControlRegister control_reg {this};
	StatusRegister status_reg {this};

	// Memory mapping
	DPNC342MemoryMap memory {this};

	// Load all registers
	virtual void load();
	
	void set_voltage(double voltage, const unsigned dac=4);

	double get_voltage(const unsigned dac=4);


};


}  // namespace dpnc342

}  // namespace module

}  // namespace daq

}  // namespace drs4

