/*
 * registers.h
 *
 *  Created on: Sep 25, 2016
 *      Author: Vassil Z. Verguilov
 */
#pragma once

#include <iostream>
#include <iomanip>

#include "drs/core/bits.h"
#include "drs/daq/core/register.h"
#include "drs/daq/core/module_signature.h"


using drs4::core::read_region;
//using drs4::daq::core::detail::RegisterData;
using drs4::daq::core::Register;
using drs4::daq::core::base::ModuleSignature;

namespace drs4{
namespace daq{
namespace module{
namespace dpnc342{

// DPNC342 Control register
// Size: 14 words
// Base address: 0x00000000
struct ControlRegister : public Register {
	explicit ControlRegister(ModuleSignature::Pointer owner):Register(15, 0x00000000, owner){};
	// Register 0x0

	// Start trigger
	// Write 0: no effect
	// Write 1: start domino wave
	// Read: last value written
	REG_ATTR(start_trig,  0x0, 0x01, 0x00)

	// Reinitialize trigger
	// Write 0: no effect
	// Write 1: stop and reset DRS chip
	// Read: last value written
	REG_ATTR(reinit_trig, 0x0, 0x01, 0x01)

	// Soft trigger
	// Write 0: no effect
	// Write 1: stop DRS chip and start readout
	// Read: last value written
	REG_ATTR(soft_trig, 0x0, 0x01, 0x02)

	// Register 0x1


	// Autostart
	// Not implemented
	REG_ATTR(autostart, 0x1, 0x01, 0x00)

	// ADC Active
	// Write 0: stop ADC if running
	// Write 1: keep ADC clocked all the time
	REG_ATTR(adc_active, 0x1, 0x01, 0x01)

	// TCA Control 100 MHz calibration signal
	// Write 0: off
	// Write 1: on
	REG_ATTR(tca_ctrl, 0x1, 0x01, 0x03)

	// Hardware trigger
	// Write 0: disable
	// Write 1: enable
	REG_ATTR(enable_trigger, 0x1, 0x01, 0x06)

	// Readout mode
	// Write 0: start from first cell
	// Write 1: domino stop
	REG_ATTR(readout_mode, 0x1, 0x01, 0x07)

	// Negative trigger
	// Write 0: trigger on leading edge
	// Write 1: trigger on trailing edge
	REG_ATTR(neg_trigger, 0x1, 0x01, 0x08)

	// Calibration Voltage (DAC4)
	// Write 0: disable
	// Write 1: enable
	REG_ATTR(acalib, 0x1, 0x01, 0x09)

	// Reference clock source
	// Write 0: FPGA
	// Write 1: external clock
	REG_ATTR(refclk_source, 0x1, 0x01, 0x0A)

	// Domino wave active during readout
	// Write 0: Stop domino wave
	// Write 1: Keep domino wave active during readout
	REG_ATTR(dactive, 0x1, 0x01, 0x0B)

	// Standby mode
	// Write 0: off
	// Write 1: on
	REG_ATTR(standby, 0x1, 0x01, 0x0C)

	// Register 0x2
	// DRS1 Trigger level
	REG_ATTR(DAC0, 0x2, 0x10, 0x00)

	// Register 0x3
	// DRS2 Trigger level
	REG_ATTR(DAC1, 0x3, 0x10, 0x00)

	// Register 0x4
	// DRS3 Trigger level
	REG_ATTR(DAC2, 0x4, 0x10, 0x00)

	// Register 0x5

	// DRS4 Trigger level
	REG_ATTR(DAC3, 0x5, 0x10, 0x00)


	// Register 0x6
	// VCAL
	REG_ATTR(DAC4, 0x6, 0x10, 0x00)

	// Registers 0x7 - 0x9 are reserved

	// Register 0xA

	// DRS Channel Mask
	// DRS 1: bit n = 0: don't read channel n
	// DRS 1: bit n = 1: read channel n
//	REG_ATTR( drs_mask, 0xA, 0x8, 0x00)

	REG_ATTR(drs_ctl_last_chn,  0xA, 0x4, 0x0)
	REG_ATTR(drs_ctl_first_chn, 0xA, 0x4, 0x4)

	// ADC Clock Phase (not yet implemented)
	REG_ATTR(adcclk_phase, 0xA, 0x8, 0x08)

	// Channel configuration
	// 0x01: 1 x 8k
	// 0x11: 2 x 4k
	// 0x55: 4 x 2k
	// 0xFF: 8 x 1k
	REG_ATTR(channel_config, 0xA, 0x8, 0x10)

	// Domino Wave Mode
	// Write 0: single shot
	// Write 1: continuous cycling
	REG_ATTR(dmode, 0xA, 0x1, 0x18)

	// PLL in DRS
	// Write 0: disable
	// Write 1: enable
	REG_ATTR(pllen, 0xA, 0x1, 0x19)

	// WRS Loop (WRSIN to WRSOUT) in DRS
	// Write 0: disconnect
	// Write 1: connect
	REG_ATTR(wrsloop, 0xA, 0x1, 0x1A)

	REG_ATTR(drs_ctl_config_7_3, 0xA, 0x5, 0x1B)

	// Register 0xB

	// Sampling frequency (in ticks) not yet implemented
	REG_ATTR(sampling_freq, 0xB, 0x10, 0x0)

	// Trigger delay in ticks of roughly 2.3 ns - not yet implemented
	REG_ATTR(trigger_delay, 0xB, 0x8, 0x10)

	// Register 0xC

	// Trigger config - not yet implemented
	REG_ATTR(trigger_config, 0xC, 0x10, 0x10)

	// Register 0xD

	// EEPROM Page - must be 0
	REG_ATTR(eeprom_page, 0xD, 0x10, 0x0)

	// EEPROM Write Trigger
	// Write 0: no effect
	// Write 1: write RAM to EEPROM page (32kB)
	REG_ATTR(eeprom_write_trig, 0xD, 0x01, 0x1E)

	// EEPROM Write Trigger
	// Write 0: no effect
	// Write 1: read EEPROM page to RAM (32kB)
	REG_ATTR(eeprom_read_trig, 0xD, 0x01, 0x1F)

	// Register 0xE
	REG_ATTR(adcclk_phase_shift, 0xE, 0x08, 0x0)
	REG_ATTR(adcclk_phase_updn, 0xE, 0x01, 0x1E)
	REG_ATTR(adcclk_phase_trig, 0xE, 0x01, 0x1F)
};


// DPNC342 Status register
// Size: 16 words
// Base address: 0x00010000
// Read-only Access
struct StatusRegister : public Register {
	explicit StatusRegister(ModuleSignature::Pointer owner):Register(16, 0x00010000, owner){};
	// Register 0x00

	// DRS Type = 4
	REG_ATTR( drs_type,  0x0, 0x08, 0x00)

	// Board Type = 9
	REG_ATTR( board_type,  0x0, 0x08, 0x08)


	// Board Magic = 0xC0DE
	REG_ATTR( board_magic,  0x0, 0x10, 0x10)

	// Register 0x01

	// PLL Lock Signal for DRS1
	REG_ATTR( plllck0,  0x1, 0x01, 0x00)

	// PLL Lock Signal for DRS2
	REG_ATTR( plllck1,  0x1, 0x01, 0x01)

	// PLL Lock Signal for DRS3
	REG_ATTR( plllck2,  0x1, 0x01, 0x02)

	// PLL Lock Signal for DRS4
	REG_ATTR( plllck3,  0x1, 0x01, 0x03)

	// Domino Wave on/off
	REG_ATTR( denable,  0x1, 0x01, 0x04)

	// Sampling on/off
	REG_ATTR( dwrite,  0x1, 0x01, 0x05)

	// Trigger disabled/enabled
	REG_ATTR( arm_trig,  0x1, 0x01, 0x06)

	// Trigger as seen by the FPGA
	REG_ATTR( trig_ff,  0x1, 0x01, 0x07)

	// Soft trigger off/on
	REG_ATTR( soft_trig,  0x1, 0x01, 0x08)

	// Busy signal off/on
	REG_ATTR( stat_busy,  0x1, 0x01, 0x09)

	// Not waiting/waiting for trigger
	REG_ATTR( stat_running,  0x1, 0x01, 0x0A)

	// DRS not idle/idle
	REG_ATTR( stat_idle,  0x1, 0x01, 0x0B)

	// Serdes idle/busy
	REG_ATTR( serdes_busy,  0x1, 0x01, 0x1D)

	// Serial bus idle/busy
	REG_ATTR( serial_busy,  0x1, 0x01, 0x1E)

	// EEPROM not busy/busy
	REG_ATTR( eeprom_busy,  0x1, 0x01, 0x00)

	// Register 0x02

	// Stop Domino Wave Cell on DRS1
	REG_ATTR( stop_cell_0, 0x02, 0x0A, 0x00)

	// DRS1: Write shift register at stop
	REG_ATTR( stop_wrs_0, 0x02, 0x08, 0x10)

	// Register 0x03

	// Stop Domino Wave Cell on DRS2
	REG_ATTR( stop_cell_1, 0x03, 0x0A, 0x00)

	// DRS2: Write shift register at stop
	REG_ATTR( stop_wrs_1, 0x03, 0x08, 0x10)


	// Register 0x04

	// Stop Domino Wave Cell on DRS3
	REG_ATTR( stop_cell_2, 0x04, 0x0A, 0x00)

	// DRS3: Write shift register at stop
	REG_ATTR( stop_wrs_2, 0x04, 0x08, 0x10)


	// Register 0x05

	// Stop Domino Wave Cell on DRS4
	REG_ATTR( stop_cell_3, 0x05, 0x0A, 0x00)

	// DRS4: Write shift register at stop
	REG_ATTR( stop_wrs_3, 0x05, 0x08, 0x10)


	// Register 0x07

	// DRS1 temperature
	REG_ATTR( temperature_0, 0x07, 0x10, 0x00)

	// DRS2 temperature
	REG_ATTR( temperature_1, 0x07, 0x10, 0x10)

	// Register 0x08

	// DRS3 temperature
	REG_ATTR( temperature_2, 0x08, 0x10, 0x00)

	// DRS4 temperature
	REG_ATTR( temperature_3, 0x08, 0x10, 0x10)

	// Register 0x09

	// Firmware revision number = 0x2B16
	REG_ATTR( version_fw, 0x09, 0x10, 0x00)

	// Serial Number
	REG_ATTR( serial_number, 0x09, 0x10, 0x10)

	// Register 0x0A

	// Trigger count DRS1
	REG_ATTR( scaler0, 0x0A, 0x20, 0x0)

	// Register 0x0B

	// Trigger count DRS2
	REG_ATTR( scaler1, 0x0B, 0x20, 0x0)

	// Register 0x0C

	// Trigger count DRS3
	REG_ATTR( scaler2, 0x0C, 0x20, 0x0)

	// Register 0x0D

	// Trigger count DRS4
	REG_ATTR( scaler3, 0x0D, 0x20, 0x0)

	// Register 0x0E

	// Hard trigger count
	REG_ATTR( scaler4, 0x0E, 0x20, 0x0)

	// Register 0x0F

	// External Trigger Count
	REG_ATTR( scaler5, 0x0F, 0x20, 0x0)

};


} // namespace dpnc342

} // namespace module

} // namespace daq

} // namespace drs4

inline std::ostream& operator<<(std::ostream& os, const drs4::daq::module::dpnc342::ControlRegister& reg){
	os << "####       |       |       |       |\n";
	for(size_t i=0; i < reg.data.size(); ++i)
		os << std::setw(2)
		   << std::setfill(' ')
		   << i << ": " << std::bitset<32>(reg.data.at(i)) << '\n';
	return (os);
}

inline std::ostream& operator<<(std::ostream& os, const drs4::daq::module::dpnc342::StatusRegister& reg){
	os << "####       |       |       |       |\n";
	for(size_t i=0; i < reg.data.size(); ++i)
		os << std::setw(2)
		   << std::setfill(' ')
		   << i << ": " << std::bitset<32>(reg.data.at(i)) << '\n';
	return (os);
}
