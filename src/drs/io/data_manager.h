/*
 * data_manager.h
 *
 *  Created on: Nov 17, 2016
 *      Author: vasoto
 */

#pragma once
#include <iostream>
#include <fstream>


namespace drs {
namespace io {




/*
 *
 */
class DataManager {
public:
	DataManager();
	explicit DataManager(size_t t_page_size):page_size(t_page_size){}
	virtual ~DataManager();
	
	
private:
	size_t page_size;
};

} // namespace io
} // namespace drs

//EOF
