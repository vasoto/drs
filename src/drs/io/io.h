/*
 * io.h
 *
 *  Created on: Sep 26, 2016
 *      Author: vasoto
 */

#pragma once
#include <functional>

namespace drs4 {
namespace io {

template<typename IntType, typename AddressIntType = size_t>
struct IOOperations{
	static constexpr size_t value_size = sizeof(IntType);
	typedef std::function<IntType(AddressIntType)> ReadOperation;
	typedef std::function<void(AddressIntType, IntType)> WriteOperation;
	typedef std::function<size_t(AddressIntType, IntType*)> ReadBlockOperation;
	typedef std::function<size_t(AddressIntType, IntType*)> WriteBlockOperation;
};


struct IOBase{
	// IO Interface class
	typedef IOBase * Pointer;
	IOBase()=default;
//	{//std::cout << "IOBase: " << this << std::endl;
//	}
	virtual ~IOBase(){};

	virtual void read() = 0;
	virtual void write() = 0;
	virtual void read_block(size_t address, unsigned * buffer, size_t length) = 0;
	virtual void write_block(size_t address, unsigned * buffer, size_t length) = 0;
};

namespace detail {

struct IOEnabled{
	explicit IOEnabled(IOBase::Pointer t_io):io(t_io){};
	virtual ~IOEnabled() = default;

	IOEnabled() = delete;
	IOEnabled(const IOEnabled& other) = delete;
	IOEnabled operator=(const IOEnabled& other) = delete;

	IOBase::Pointer io;
};


}  // namespace detail


}  // namespace io


}  // namespace drs4
//EOF
