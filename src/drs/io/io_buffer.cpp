/*
 * io_buffer.cpp
 *
 *  Created on: Nov 17, 2016
 *      Author: vasoto
 */

#include "io_buffer.h"

namespace drs {
namespace io {

IOBuffer::IOBuffer(uint64_t capacity):payload(new unsigned char(capacity)) {}

IOBuffer::~IOBuffer() {
	// TODO Auto-generated destructor stub
}

IOBuffer::UPointer IOBuffer::create(const uint64_t capacity) {
	return (IOBuffer::UPointer(new IOBuffer(capacity)));
}

} // namespace io
} // namespace drs
