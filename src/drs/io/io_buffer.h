/*
 * io_buffer.h
 *
 *  Created on: Nov 17, 2016
 *      Author: vasoto
 */

#pragma once
#include <memory>


namespace drs {
namespace io {

/*
 *
 */
struct IOBuffer {
	// Types
	typedef std::unique_ptr<IOBuffer> UPointer;
	
	// Constructors
	IOBuffer() = delete;
	explicit IOBuffer(uint64_t capacity);
	//Destructor
	virtual ~IOBuffer();
	
	//Create buffer and allocate memory for `capacity` bytes
	static UPointer create(const uint64_t capacity);
	
	template<typename T>
	void add_data(std::unique_ptr<T> chunk);
	
	std::unique_ptr<unsigned char> payload;
};

} // namespace io
} // namespace drs

template<typename T>
inline void drs::io::IOBuffer::add_data(std::unique_ptr<T> chunk) {
}
//EOF
